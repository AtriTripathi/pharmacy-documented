package com.prezcription.pharmacyapp;

import java.util.List;

/**
 * Created by amit on 09/02/18.
 */

public class ItemAvailabilityResponse {


    /**
     * lineitems : [{"lineitem":0,"itemid":"37106","itemName":"MEFTAL P TAB  ","batchid":"HMP1729  ","mfgr":"BLUE CROSS LABORATORIES LTD  ","expiry":"2020-08-01T00:00:00.000Z","mrp":25,"itemCount":5,"NetRate":2.5,"cGST":0.75,"sGST":0.75,"itemDisc":0,"NetAmt":12.5},{"lineitem":1,"itemid":"8976","itemName":"MEDOMOL 300MG TAB  ","batchid":"7160","mfgr":"MEDOPHARM  ","expiry":"2020-08-01T00:00:00.000Z","mrp":14.25,"itemCount":10,"NetRate":1.43,"cGST":0.858,"sGST":0.858,"itemDisc":0,"NetAmt":14.299999999999999}]
     * totalAmtDue : 26.799999999999997
     * totalcGST : 1.608
     * totalsGST : 1.608
     * totalDiscount : 0
     */

    private double totalAmtDue;
    private double totalcGST;
    private double totalsGST;
    private double totalDiscount;
    private List<LineitemsBean> lineitems;

    public double getTotalAmtDue() {
        return totalAmtDue;
    }

    public void setTotalAmtDue(double totalAmtDue) {
        this.totalAmtDue = totalAmtDue;
    }

    public double getTotalcGST() {
        return totalcGST;
    }

    public void setTotalcGST(double totalcGST) {
        this.totalcGST = totalcGST;
    }

    public double getTotalsGST() {
        return totalsGST;
    }

    public void setTotalsGST(double totalsGST) {
        this.totalsGST = totalsGST;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(int totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public List<LineitemsBean> getLineitems() {
        return lineitems;
    }

    public void setLineitems(List<LineitemsBean> lineitems) {
        this.lineitems = lineitems;
    }

    public static class LineitemsBean {
        /**
         * lineitem : 0
         * itemid : 37106
         * itemName : MEFTAL P TAB
         * batchid : HMP1729
         * mfgr : BLUE CROSS LABORATORIES LTD
         * expiry : 2020-08-01T00:00:00.000Z
         * mrp : 25
         * itemCount : 5
         * NetRate : 2.5
         * cGST : 0.75
         * sGST : 0.75
         * itemDisc : 0
         * NetAmt : 12.5
         */

        private int lineitem;
        private String itemid;
        private String itemName;
        private String batchid;
        private String mfgr;
        private String expiry;
        private double mrp;
        private int itemCount;
        private double NetRate;
        private double cGST;
        private double sGST;
        private double itemDisc;
        private double NetAmt;

        public int getLineitem() {
            return lineitem;
        }

        public void setLineitem(int lineitem) {
            this.lineitem = lineitem;
        }

        public String getItemid() {
            return itemid;
        }

        public void setItemid(String itemid) {
            this.itemid = itemid;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getBatchid() {
            return batchid;
        }

        public void setBatchid(String batchid) {
            this.batchid = batchid;
        }

        public String getMfgr() {
            return mfgr;
        }

        public void setMfgr(String mfgr) {
            this.mfgr = mfgr;
        }

        public String getExpiry() {
            return expiry;
        }

        public void setExpiry(String expiry) {
            this.expiry = expiry;
        }

        public double getMrp() {
            return mrp;
        }

        public void setMrp(double mrp) {
            this.mrp = mrp;
        }

        public int getItemCount() {
            return itemCount;
        }

        public void setItemCount(int itemCount) {
            this.itemCount = itemCount;
        }

        public double getNetRate() {
            return NetRate;
        }

        public void setNetRate(double NetRate) {
            this.NetRate = NetRate;
        }

        public double getCGST() {
            return cGST;
        }

        public void setCGST(double cGST) {
            this.cGST = cGST;
        }

        public double getSGST() {
            return sGST;
        }

        public void setSGST(double sGST) {
            this.sGST = sGST;
        }

        public double getItemDisc() {
            return itemDisc;
        }

        public void setItemDisc(double itemDisc) {
            this.itemDisc = itemDisc;
        }

        public double getNetAmt() {
            return NetAmt;
        }

        public void setNetAmt(double NetAmt) {
            this.NetAmt = NetAmt;
        }
    }
}
