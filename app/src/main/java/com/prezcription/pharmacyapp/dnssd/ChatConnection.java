package com.prezcription.pharmacyapp.dnssd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.NoRouteToHostException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.data.PharmacyContentProvider;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.PharmacyDbHelper;
import com.prezcription.pharmacyapp.data.dto.InvoiceDataFormatted;
import com.prezcription.pharmacyapp.data.dto.Medicine;
import com.prezcription.pharmacyapp.data.dto.Patient;
import com.prezcription.pharmacyapp.data.dto.ReceivedPatientData;
import com.prezcription.pharmacyapp.data.dto.RegisteredPatientData;
import com.prezcription.pharmacyapp.data.dto.Vaccination;
import com.prezcription.pharmacyapp.data.dto.Vitals;


/**
 * Created by anubhav on 24/09/16.
 */
public class ChatConnection {

    private Handler mUpdateHandler;
    private ChatServer mChatServer;
    private ChatClient mChatClient;
    public static final String LOG_TAG = ChatConnection.class.getSimpleName();
    private Socket mSocket;
    private int mPort = -1;
    private OnChatConnectionListener mListener;
    private Context mContext;

    private boolean mOnlineMode = false;
    private Thread mRecThread;

    public ChatConnection(Handler handler, OnChatConnectionListener listener, Context context) {
        mListener = listener;
        mUpdateHandler = handler;
        mChatServer = new ChatServer(handler);
        mContext = context;
    }

    public ChatConnection(Handler handler) {
        mUpdateHandler = handler;
        mChatServer = new ChatServer(handler);
    }

    public interface OnChatConnectionListener {
        void onSocketSet();
    }

    public void tearDown() {
        mChatServer.tearDown();
        if (mChatClient != null) {
            mChatClient.tearDown();
        }
    }

    public void connectToServer(InetAddress address, int port) {
        mChatClient = new ChatClient(address, port);
    }

    public void connectToServer(InetAddress address, int port, ArrayList<RegisteredPatientData> patientDetails){
        mChatClient = new ChatClient(address, port, patientDetails);
    }




    public void sendMessage(String msg) {
        if (mChatClient != null) {
            mChatClient.sendMessage(msg);
        }
    }

    public int getLocalPort() {
        return mPort;
    }

    public void setLocalPort(int port) {
        mPort = port;
    }

    public synchronized void updateMessages(String msg, boolean local) {
        Log.e(LOG_TAG, "Updating message: " + msg);
//        if (local) {
//            msg = "me: " + msg;
//        } else {
//            msg = "them: " + msg;
////            mListener.onPatientAdded();
//        }
        Bundle messageBundle = new Bundle();
        messageBundle.putString("msg", msg);
        Message message = new Message();
        message.setData(messageBundle);
        mUpdateHandler.sendMessage(message);
    }

    private synchronized void setSocket(Socket socket) {
        Log.d(LOG_TAG, "setSocket being called.");

        if (mSocket != null) {
            Log.d(LOG_TAG, "previous socket not null");
            if (mSocket.isConnected()) {
                Log.d(LOG_TAG, "previous socket is connected");
                try {
                    mSocket.close();
                } catch (IOException e) {
                 Log.d(LOG_TAG, "couldnt close the socket"+ e.toString());
                    e.printStackTrace();
                }
            }

        }

        if (socket == null) {
            Log.d(LOG_TAG, "Setting a null socket.");
        }
//        else {
//            try {
//                mSocket = new Socket(socket.getInetAddress(), socket.getPort());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        mSocket = socket;

    }

    private Socket getSocket() {
        return mSocket;
    }

    private class ChatServer {
        ServerSocket mServerSocket = null;
        Thread mThread = null;


        public ChatServer(Handler handler) {
            mThread = new Thread(new ServerThread());
            mThread.start();
        }

        public void tearDown() {
            mThread.interrupt();
            try {
                mServerSocket.close();
            } catch (IOException ioe) {
                Log.e(LOG_TAG, "Error when closing server socket.");
            }
        }

        class ServerThread implements Runnable {
            @Override
            public void run() {
                try {

                    mServerSocket = new ServerSocket(1300);

                    setLocalPort(mServerSocket.getLocalPort());
                    while (!Thread.currentThread().isInterrupted()) {
                        Log.d(LOG_TAG, "ServerSocket Created, awaiting connection");
//                        Log.d(LOG_TAG, "socket is "+ mServerSocket.accept());

                        setSocket(mServerSocket.accept());

                        Log.d(LOG_TAG, "Connected.");

                        if(mChatClient!=null){
                            mChatClient.receiveMessage();
                        }else {
                            mChatClient = new ChatClient(getSocket().getInetAddress(), getSocket().getPort());
//                            mChatClient.receiveMessage();
                        }

                    }



                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error creating ServerSocket: ", e);
                    e.printStackTrace();
                }
            }
        }


    }

    private class ChatClient {
        private InetAddress mAddress;
        private int PORT;
        private final String CLIENT_TAG = "ChatClient";
        private Thread mSendThread;
        private Thread mRecThread;
        private ArrayList<RegisteredPatientData> mRegisteredPatientDatas;


        public ChatClient(InetAddress address, int port) {
            Log.d(CLIENT_TAG, "Creating chatClient");
            this.mAddress = address;
            this.PORT = port;
            mSendThread = new Thread(new SendingThread());
            mSendThread.start();
        }


        public ChatClient(InetAddress address, int port, ArrayList<RegisteredPatientData> registeredPatientDatas) {
            Log.d(CLIENT_TAG, "Creating chatClient");
            this.mAddress = address;
            this.PORT = port;
            mSendThread = new Thread(new SendingThread());
            mSendThread.start();
            mRegisteredPatientDatas = registeredPatientDatas;
        }

        private String FETCH_BILL_COMMAND = "bill_pharmacy";


        class SendingThread implements Runnable {
            BlockingQueue<String> mMessageQueue;
            private int QUEUE_CAPACITY = 5;

            public SendingThread() {
                mMessageQueue = new ArrayBlockingQueue<String>(QUEUE_CAPACITY);
            }

            @Override
            public void run() {
                try {

                    setSocket(new Socket(mAddress,PORT));

//                    if (getSocket() == null) {
//                        setSocket(new Socket(mAddress, PORT));
//                        Log.d(CLIENT_TAG, "Client-side socket initialized.");
//                    } else {
//
//                        Log.d(CLIENT_TAG, "Socket already initialized. skipping!");
//                    }
                    //Todo: refactor to include in server thread


                    if (mRegisteredPatientDatas != null && mRegisteredPatientDatas.size() > 0) {
                        Log.d(LOG_TAG, "registered patients size: " + mRegisteredPatientDatas.size());
                        sendMessage(new Gson().toJson(mRegisteredPatientDatas));
                        mRegisteredPatientDatas = null;
                    }else {
                        Log.d(LOG_TAG, "sending command to fetch bills");
                       sendMessage(FETCH_BILL_COMMAND);
                    }

                    receiveMessage();

//                    mListener.onSocketSet();
                } catch (UnknownHostException e) {
                    Log.d(CLIENT_TAG, "Initializing socket failed, UHE", e);
                }catch (NoRouteToHostException e) {
//                    DNSSDHelper.getInstance().onReset();
                    Log.d(CLIENT_TAG, "Initializing socket failed", e);
                } catch (IOException e) {
                    Log.d(CLIENT_TAG, "Initializing socket failed, IOE.", e);
                }
                while (!mMessageQueue.isEmpty()) {
                    try {
                        String msg = mMessageQueue.take();
                        Log.d(CLIENT_TAG, "Sending thread, message sending" + msg);


                    } catch (InterruptedException ie) {
                        Log.d(CLIENT_TAG, "Message sending loop interrupted, exiting");
                        mSendThread.interrupt();
//                        mChatClient.tearDown();
//                        mChatClient = null;
                    }
//                }
                }
            }
        }

        class ReceivingThread implements Runnable {
            @Override
            public void run() {
                BufferedReader input;
                try {
                    input = new BufferedReader(new InputStreamReader(
                            mSocket.getInputStream()));
                    while (!Thread.currentThread().isInterrupted()) {
                        String messageStr = null;
                        messageStr = input.readLine();
                        if (messageStr != null) {
                            Log.d(LOG_TAG, "Read from the stream: " + messageStr);

                            boolean billType = false;
                            try {
                                Gson gson = new Gson();
                                Type type = new TypeToken<ArrayList<ReceivedPatientData>>() {}.getType();
                                ArrayList<ReceivedPatientData> receivedPatientDatas = gson.fromJson(messageStr, type);
                                for (ReceivedPatientData data:receivedPatientDatas) {
                                    if (data.isSync()) {
                                        Log.d(LOG_TAG, "Data synced");
//                                        PharmacyContentProvider.updateSyncUpdateReRegisterFlag(mContext.getContentResolver(), data.getRegId(),
//                                                data.isSync(),false,false);
//                                        tearDown();
                                    }
                                }
                            }
                            catch (JsonParseException e) {
                                billType = true;
                                Log.d(LOG_TAG,"Different data type maybe bill type");
                            }

                            if(billType){

                                try {
                                    Gson gson = new Gson();

                                    Log.d(LOG_TAG," Invoice data is there "+ messageStr);

                                    InvoiceDataFormatted dataFormatted = gson.fromJson(messageStr,InvoiceDataFormatted.class);


                                    ReceivedPatientData receivedPatientData;
                                    if(dataFormatted!=null){

                                        int billId = getBillIdAndUpdatePrescriptionFlag(dataFormatted);

                                        if(billId>0){

                                            Log.d(LOG_TAG,"bill id created "+ billId);
                                            receivedPatientData = new ReceivedPatientData(dataFormatted.getPatient().getProfileId(), dataFormatted.getRegId(), 2);
                                            updateMessages(gson.toJson(dataFormatted.getPatient()),true);

                                        }else {
                                            Log.d(LOG_TAG,"bill id is less than 0 "+ billId);

                                            receivedPatientData = new ReceivedPatientData(dataFormatted.getPatient().getProfileId(), dataFormatted.getRegId(), -1);
                                        }


                                    }else {
                                        Log.d(LOG_TAG,"dataformatted is null");

                                        receivedPatientData = new ReceivedPatientData(dataFormatted.getPatient().getProfileId(), dataFormatted.getRegId(), -1);

                                    }
                                    sendMessage(gson.toJson(receivedPatientData));

                                }
                                catch (JsonParseException e) {
                                    Log.d(LOG_TAG,"Different data type not bill type also");
                                }

                            }
                            updateMessages(messageStr, false);
                        } else {
                            Log.d(LOG_TAG, "The nulls! The nulls!");
                            break;
                        }
                    }
                    input.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Server loop error: ", e);
                }
            }
        }

        private int getBillIdAndUpdatePrescriptionFlag(InvoiceDataFormatted dataFormatted){

            final SQLiteDatabase db = new PharmacyDbHelper(mContext).getWritableDatabase();

            int regId = -1;
            int billId = -1;

            db.beginTransaction();

            try {

                String patientSelection = PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME+" = ?";
                String[] patientSelectionArgs = {dataFormatted.getPatient().getUsername()};

                Cursor patCursor = db.query(PharmacyContract.PatientEntry.TABLE_NAME,null,patientSelection,patientSelectionArgs,null,null,null);

                int patId = -1;

                if (patCursor.moveToNext()){
                    patId = patCursor.getInt(patCursor.getColumnIndex(PharmacyContract.PatientEntry._ID));
                }

                patCursor.close();

                Log.d(LOG_TAG,"patient id is "+patId);

                if(patId>0){

                    Log.d(LOG_TAG,"doctor id is "+dataFormatted.getDoctorId());

                    String selectionReg = PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID
                            + " = ? and "
                            + PharmacyContract.PatientRegistrationEntry.COLUMN_DOCTOR_ID
                            +" = ? ";

                    String[] selectionRegArgs = {String.valueOf(patId), String.valueOf(dataFormatted.getDoctorId())};

                    String sortOrder = PharmacyContract.PatientRegistrationEntry._ID + " DESC";

                    Cursor regCursor = db.query(PharmacyContract.PatientRegistrationEntry.TABLE_NAME,
                            null,selectionReg,selectionRegArgs,null,null,sortOrder);

                    while (regCursor.moveToNext()){
                        regId = regCursor.getInt(regCursor.getColumnIndex(PharmacyContract.PatientRegistrationEntry._ID));
                        break;
                    }

                    Log.d(LOG_TAG,"reg id is "+regId);

                    regCursor.close();

                    if(regId>0){

                        ContentValues updateValues = new ContentValues();

                        updateValues.put(PharmacyContract.PatientRegistrationEntry.COLUMN_PRESCRIPTION_FLAG,1);
//                        updateValues.put(PharmacyContract.PatientRegistrationEntry.COLUMN_DATE, Utility.getCurrentDate());

                        String selectionRegUpdate = PharmacyContract.PatientRegistrationEntry._ID
                                + " = ?";

                        String[] selectionRegUpdateArgs = {String.valueOf(regId)};

                        long updateCount = db.update(PharmacyContract.PatientRegistrationEntry.TABLE_NAME,updateValues,selectionRegUpdate,selectionRegUpdateArgs);

                        if(updateCount>0){
                            billId = (int)updateBillingData(dataFormatted,regId,db);
                        }

                        Log.d(LOG_TAG,"updated count "+updateCount+ " and "+ billId);


                    }else {

                        Patient patient  = dataFormatted.getPatient();

                        ContentValues insertValue = new ContentValues();
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID, patId);
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_DATE, Utility.getCurrentDate());
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_SYNC_FLAG,"0");
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_UPDATE_FLAG,0);
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_DOCTOR_ID,dataFormatted.getDoctorId());
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_RE_REGISTER_FLAG,0);

                        //inserting prescription flag as one, since data is coming from doc app
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_PRESCRIPTION_FLAG,1);
                        insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_VISIT_TYPE, "SICK");

                        long insertedRegRow = db.insert(PharmacyContract.PatientRegistrationEntry.TABLE_NAME, null,insertValue);

//                        if (patient.getVitals() != null){
//                            if (insertVitals(patient.getVitals(), (int)insertedRegRow, db)){
//                                Log.d(LOG_TAG, "patient vitals inserted if available");
//                            }else {
//                                Log.d(LOG_TAG, "failed to inser vitals");
//                            }
//                        }

                        if (insertedRegRow > 0) {
                            billId = (int) updateBillingData(dataFormatted, (int)insertedRegRow, db);
                        }
                        else{
                            Log.d(LOG_TAG, "failed to insert into registration table");
                        }
                        Log.d(LOG_TAG,"bill id: " + billId);

                    }


                }else {

                    Patient patient  = dataFormatted.getPatient();

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME,patient.getName());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_FATHERS_NAME, patient.getFatherName());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_MOTHERS_NAME, patient.getMotherName());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE, patient.getAge());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_DOB, patient.getDob());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME, patient.getUsername());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE_TYPE, patient.getAgeType());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE, patient.getPhoneNumber());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_AREA, patient.getArea());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_PIN_CODE, patient.getPinCode());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_RELATIONSHIP, patient.getRelationship());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_EMAIL, patient.getPatientEmail());
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_SEX, patient.getGender());

                    if (mOnlineMode) {
                        contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_WEB_ID, patient.getWebId());
                        contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_IMAGE, patient.getPhotoUrl());
                    } else {
                        contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_WEB_ID, -1);
                    }
                    contentValues.put(PharmacyContract.PatientEntry.COLUMN_PATIENT_RECENT, Utility.getCurrentDate());

                     long insertedPatientRow = db.insertWithOnConflict(PharmacyContract.PatientEntry.TABLE_NAME,null,contentValues, SQLiteDatabase.CONFLICT_REPLACE);

                     Log.d(LOG_TAG, "inserted patient id is: "+(int)insertedPatientRow);

                   if (insertedPatientRow > 0){

                       ContentValues insertValue = new ContentValues();
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID, (int)insertedPatientRow);
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_DATE, Utility.getCurrentDate());
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_SYNC_FLAG,"0");
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_UPDATE_FLAG,0);
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_DOCTOR_ID,dataFormatted.getDoctorId());
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_RE_REGISTER_FLAG,0);

                       //inserting prescription flag as one, since data is coming from doc app
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_PRESCRIPTION_FLAG,1);
                       insertValue.put(PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_VISIT_TYPE, "SICK");

                       long insertedRegRow = db.insert(PharmacyContract.PatientRegistrationEntry.TABLE_NAME, null,insertValue);

//                       if (patient.getVitals() != null){
//                           if (insertVitals(patient.getVitals(), (int)insertedRegRow, db)){
//                               Log.d(LOG_TAG, "patient vitals inserted if available");
//                           }else {
//                               Log.d(LOG_TAG, "failed to inser vitals");
//                           }
//                       }

                       if (insertedRegRow > 0) {
                           billId = (int) updateBillingData(dataFormatted, (int)insertedRegRow, db);
                       }
                       else{
                           Log.d(LOG_TAG, "failed to insert into registration table");
                       }
                       Log.d(LOG_TAG,"bill id: " + billId);


                   }else {
                       Log.d(LOG_TAG, "failed to insert new patient");
                   }

                }

                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

            return billId;

        }

//        private boolean insertVitals(Vitals vitals, int patientId, final SQLiteDatabase db) {
//
//
//            if (patientId != -1) {
//
//                Log.d(LOG_TAG, "spo2: inside insertVitals " + vitals.getSpo2());
//
//                ContentValues insertValue = new ContentValues();
//                //insert registration id of patient
//                insertValue.put(PharmacyContract.VitalsEntry.COLUMN_REGISTRATION_KEY, patientId);
//
//                if (vitals.getWeight() != Float.NaN) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_WEIGHT, vitals.getWeight());
//                }
//
//                if (vitals.getHeight() != Float.NaN) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_HEIGHT, vitals.getHeight());
//                }
//
//                if (vitals.getTemperature() != Float.NaN) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_TEMPERATURE, vitals.getTemperature());
//                }
//                if (vitals.getRespiratoryRate() != 0) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_RESPERATORY_RATE, vitals.getRespiratoryRate());
//                }
//                if (vitals.getHeartRate() != 0) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_HEART_BEAT, vitals.getHeartRate());
//                }
//                if (vitals.getDiastolicPressure() != 0) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_DIASTOLIC_PRESSURE, vitals.getDiastolicPressure());
//                }
//                if (vitals.getSystolicPressure() != 0) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_SYSTOLIC_PRESSURE, vitals.getSystolicPressure());
//                }
//                if (vitals.getHeadCircumference() != Float.NaN) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_HEAD_CIRCUMFERENCE, vitals.getHeadCircumference());
//                }
//                if (vitals.getSpo2() != 0) {
//                    insertValue.put(PharmacyContract.VitalsEntry.COLUMN_SPO2, vitals.getSpo2());
//                }
//
//                long value = db.insert(PharmacyContract.VitalsEntry.TABLE_NAME,null,insertValue);
//
//                return value > 0;
//            }
//
//            return false;
//
//        }

        private long updateBillingData(InvoiceDataFormatted dataFormatted, int regId, final SQLiteDatabase db){


            String userName = "";
            ContentValues contentValues= new ContentValues();


            contentValues.put(PharmacyContract.BillsEntry.COLUMN_DOC_ID,dataFormatted.getDoctorId());
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_CONSULTATION_FEE,dataFormatted.getConsultation_fee());
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_PATIENT_REG_ID,regId);

            //TODO: update proper username
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_PATIENT_USER_NAME, userName);
            //TODO: remove bill date from here
//            contentValues.put(PharmacyContract.BillsEntry.COLUMN_BILL_DATE, Utility.getCurrentDate());
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_UPDATED_DATE, Utility.getCurrentDate());
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_PAID_AMOUNT,0);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_TOTAL_DISCOUNT_GIVEN,0);

            long bill_id =db.insertWithOnConflict(PharmacyContract.BillsEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);

            Log.d(LOG_TAG," bill inserted id "+bill_id);

            if(bill_id>0) {

                if (dataFormatted.getListOfMedicine() != null && dataFormatted.getListOfMedicine().size() > 0) {

                    String currentName = "";
                    String lastName = "";
                    float lastQuantity = 0;
                    long lastAddedId = -1;

                    for (Medicine medicine : dataFormatted.getListOfMedicine()) {

                        currentName = medicine.getName();

                        if(!currentName.equals(lastName)) {

                            ContentValues medValues = new ContentValues();

                            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_NAME, medicine.getName());
                            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_PRICE, medicine.getPrice());
                            if ((!Utility.checkDoseExist(medicine.getRoute()))|| Utility.checkIfDefaultQuantity(medicine.getRoute())) {
                                medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, 1);
                                lastQuantity = 1;
                            } else {
                                medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, medicine.getMedicineQuantity().totalQuantity());
                                lastQuantity = medicine.getMedicineQuantity().totalQuantity();
                            }
                            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID, bill_id);
                            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_DISCOUNT, 0);
                            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID, medicine.getGoFrugalItemId());
                            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_ITEM_BILLED, 1);

                            long med_id = db.insertWithOnConflict(PharmacyContract.MedicineInBillsEntry.TABLE_NAME, null, medValues, SQLiteDatabase.CONFLICT_REPLACE);
                            Log.d(LOG_TAG,"med bill id "+med_id);
                            lastAddedId = med_id;
                            lastName = currentName;

                        }else {

//                            Log.d(LOG_TAG,"last and current name is same so updating quantity "+currentName+ " and id is "+ lastAddedId);
//                            Log.d(LOG_TAG,"last quantity "+lastQuantity+ " and current quant "+ medicine.getMedicineQuantity().totalQuantity());

                            if(lastAddedId!=-1) {
                                float total;
                                ContentValues medValues = new ContentValues();
                                if (!Utility.checkDoseExist(medicine.getRoute()) || Utility.checkIfDefaultQuantity(medicine.getRoute())) {
                                    total =lastQuantity + 1;
                                    medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, total);
                                } else {
                                    total =lastQuantity + medicine.getMedicineQuantity().totalQuantity();
                                    medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY,total );
                                }
                                String where = PharmacyContract.MedicineInBillsEntry._ID + " =? ";
                                String[] whereArgs = {String.valueOf(lastAddedId)};
                                db.update(PharmacyContract.MedicineInBillsEntry.TABLE_NAME, medValues, where, whereArgs);
                            }

                        }


                    }

                }

                if (dataFormatted.getVaccinationList() != null&& dataFormatted.getVaccinationList().size()>0) {


                    for (Vaccination vaccination:dataFormatted.getVaccinationList() ){

                        ContentValues vaccValues = new ContentValues();

                        vaccValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_NAME, vaccination.getName());
                        vaccValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_PRICE, vaccination.getPrice());
                        vaccValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_QUANTITY, 1);
                        vaccValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_BILL_ID, bill_id);
                        vaccValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_DISCOUNT, 0);
                        vaccValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ID, -1);
                        vaccValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_ITEM_BILLED, 1);

                        long vacc_id =db.insertWithOnConflict(PharmacyContract.VaccineInBillsEntry.TABLE_NAME, null, vaccValues, SQLiteDatabase.CONFLICT_REPLACE);

                        Log.d(LOG_TAG,"vacc bill id "+vacc_id);

                    }

                }
            }

            return bill_id;

        }


        public void tearDown() {
            try {
                if(getSocket()!=null) {
                    getSocket().close();
                }
            } catch (IOException ioe) {
                Log.e(CLIENT_TAG, "Error when closing server socket.");
            }
        }

        public void receiveMessage() {
            Log.d(LOG_TAG,"Receive thread starting");
            mRecThread = new Thread(new ReceivingThread());
            mRecThread.start();
        }

            public void sendMessage(String msg) {

                Log.d(LOG_TAG, "patient details inside send message: " + msg);

                try {
                    Socket socket = getSocket();
                    if (socket == null) {
                        Log.d(CLIENT_TAG, "Socket is null, wtf?");
                    } else if (socket.getOutputStream() == null) {
                        Log.d(CLIENT_TAG, "Socket output stream is null, wtf?");
                    }
                    PrintWriter out = new PrintWriter(
                            new BufferedWriter(
                                    new OutputStreamWriter(getSocket().getOutputStream())), true);
                    out.println(msg);
                    out.flush();
                    updateMessages(msg, true);
                } catch (UnknownHostException e) {
                    Log.d(CLIENT_TAG, "Unknown Host", e);
                } catch (IOException e) {
                    Log.d(CLIENT_TAG, "I/O Exception", e);
                } catch (Exception e) {
                    Log.d(CLIENT_TAG, "Error3", e);
                }
                Log.d(CLIENT_TAG, "Client sent message: " + msg);
            }


        }
    }


