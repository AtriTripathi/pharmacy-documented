package com.prezcription.pharmacyapp.dnssd;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.druk.rxdnssd.BonjourService;
import com.github.druk.rxdnssd.RxDnssd;
import com.github.druk.rxdnssd.RxDnssdBindable;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;


import java.lang.reflect.Type;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.activity.BillingActivity;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.dto.DocData;
import com.prezcription.pharmacyapp.data.dto.Patient;
import com.prezcription.pharmacyapp.data.dto.RegisteredPatientData;
import com.prezcription.pharmacyapp.fragment.AddBilledPatientFragment;
import com.prezcription.pharmacyapp.service.CloseService;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by amit on 13/12/17.
 */

public class DNSSDHelper implements ChatConnection.OnChatConnectionListener {

    //initializing when class is loaded, hence thread safe
    private static DNSSDHelper mInstance;

    public static final String SERVICES_DOMAIN = "_rxdnssd._tcp";


    private DNSSDHelper() {
    }

    public static DNSSDHelper getInstance() {
        if (mInstance == null) {
            mInstance = new DNSSDHelper();
            connected = false;
            initialised = false;
        }
        return mInstance;
    }

    private RegisteredPatientData mPatientData;

    private ArrayList<RegisteredPatientData> mRegisteredPatientList;

    private boolean isServiceFound = false;

    private static List<BonjourService> services = new ArrayList<>();
    BonjourService bonjourService;
    private boolean isSingleMode = false;

    private static InetAddress IP_SERVER = null;

    private static BonjourService mServiceInfo = null;

    private static boolean connected;

    private static boolean initialised;

    private RxDnssd rxDnssd;

    private Subscription browseSubscription;
    private Subscription registerSubscription;
    private ChatConnection mChatConnection;
    private static final String LOG_TAG =  DNSSDHelper.class.getSimpleName();

    private Handler mUpdateHandler;
    private String mDoctorServiceName = null;



    private ArrayList<String> mDoctorServiceNameList;

    private ServiceStateChangeListener mListener;


    private String mServiceName;

    private boolean isSync = false;



    private final String BILLED_PATIENT_FRAGMENT_TAG = "billed_patient_frag";

    private Context callingContext;


    public void initialize(final Context context, String doctorPhoneNumber) {

//        this.isSync = isSync;
        this.mDoctorServiceName = doctorPhoneNumber;

        rxDnssd = new RxDnssdBindable(context);

//        mDoctorServiceNameList =new ArrayList<>();
//        mDoctorServiceNameList.addAll(doctorList);
        SharedPreferences sharedPreferences = context.getSharedPreferences(Utility.USER_LOG_PREF, MODE_PRIVATE);
        String phoneNumber = sharedPreferences.getString(Utility.PHONE_NUMBER_KEY, null);
        if (phoneNumber != null) {
            mServiceName = context.getString(R.string.nsd_pharmacy_app_service_name, phoneNumber);
        } else {
            mServiceName = context.getString(R.string.nsd_pharmacy_app_service_name, "");
        }

        mUpdateHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                String chatLine = msg.getData().getString("msg");
                Log.d(LOG_TAG, "msg display kodi: " + chatLine + "-");


//                Toast.makeText(context, "msg from server:" + chatLine, Toast.LENGTH_SHORT).show();
                try {
                    Log.d(LOG_TAG, "before parsing patient ");
                    Patient patient = new Gson().fromJson(chatLine, Patient.class);


                    if (context instanceof BillingActivity) {
                        Fragment fragment = ((Activity) context).getFragmentManager().
                                findFragmentByTag(BILLED_PATIENT_FRAGMENT_TAG);


                        if (fragment instanceof AddBilledPatientFragment) {
                            ((AddBilledPatientFragment) fragment).addRecentData();
                            Toast.makeText(context, "Bill Queue Updated", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d(LOG_TAG, "bill frag is null");
                        }

                    }
                } catch (JsonParseException e) {
//                    e.printStackTrace();
                    Log.d(LOG_TAG, "not able to parse patient ");
                }

            }
        };

        mChatConnection = new ChatConnection(mUpdateHandler, this, context);

        Log.d(LOG_TAG, "Starting close service");
        Intent intent = new Intent(context, CloseService.class);
        context.startService(intent);

        initialised = true;
    }

    public boolean isConnected() {
        return connected;
    }

    public void register(final Context context) {

        Log.d(LOG_TAG, "Inside register");

        bonjourService = new BonjourService.Builder(0, 0, mServiceName, SERVICES_DOMAIN,
                "local.").port(1300).build();

        registerSubscription = rxDnssd.register(bonjourService)
                .doOnNext(new Action1<BonjourService>() {
                    @Override
                    public void call(BonjourService service) {
                        Log.d(LOG_TAG, "index:- " + service.getIfIndex() + ", service name:- " + service.getServiceName()
                                + ", regtype:- " + service.getRegType() + ", domain:- " + service.getDomain());
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BonjourService>() {
                    @Override
                    public void call(BonjourService bonjourService) {
                        if (bonjourService.isLost()) {
                            Log.d(LOG_TAG, "Service is lost to return " + bonjourService.toString());
                            return;
                        }
                        Log.d(LOG_TAG, "Register successfully " + bonjourService.toString());
                        connected = true;
                        if ( context instanceof BillingActivity && Utility.isNetworkAvailable(context) ) {
                            Log.d(LOG_TAG, "inside switch true");
                            BillingActivity activity = (BillingActivity) context;
                            activity.setServiceFlagVisibility(true);

                        }
                        Toast.makeText(context, "Rgstrd " + mServiceName, Toast.LENGTH_SHORT).show();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Toast.makeText(context, "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
//
                    }
                });

    }

    public boolean isServiceFound() {
        for (int i = 0; i < services.size(); i++) {
            if (services.get(i).getServiceName().equals(mServiceName)) {

                return true;
            }
        }
        return false;
    }

    public interface ServiceStateChangeListener {
        void onStateChange(boolean state);
    }


    @Override
    public void onSocketSet() {

    }

    public boolean isInitialised() {
        return initialised;
    }

    public void setInitialised(boolean initialised) {
        DNSSDHelper.initialised = initialised;
    }

    public void unregister() {

        if (registerSubscription != null) {
            if (!registerSubscription.isUnsubscribed()) {
                registerSubscription.unsubscribe();
            }
            registerSubscription = null;
        }
        connected = false;
    }

    public void startBrowse(final Context context) {
        Log.d(LOG_TAG, "start browse");
        //save service and check if already exits
        //what if service is not reachable??
        //check if data is sent, interrupt thread if data is not sent
        //

//        services.clear();

//        if (services != null) {
//            for (int i = 0; i < services.size(); i++) {
//                Log.d(LOG_TAG, "service name: " + services.get(i));
//            }
//
//        }

        if (rxDnssd != null) {

            if (browseSubscription != null && !browseSubscription.isUnsubscribed()) {
                browseSubscription.unsubscribe();
            }

            browseSubscription = rxDnssd.browse("_rxdnssd._tcp", "local.")
                    .compose(rxDnssd.resolve())
                    .compose(rxDnssd.queryRecords())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<BonjourService>() {
                        @Override
                        public void call(BonjourService bonjourService) {
                            Log.d(LOG_TAG, bonjourService.toString());
                            if (bonjourService.isLost()) {

                                remove(bonjourService);
                                Log.d(LOG_TAG, "bonjour service lost: " + bonjourService.getServiceName());

                                if (bonjourService.getServiceName().equals(mServiceName)) {

                                    connected = false;

                                    if (context instanceof BillingActivity) {
                                        Log.d(LOG_TAG, "inside switch false");
                                        BillingActivity activity = (BillingActivity) context;
                                        activity.setServiceFlagVisibility(false);
                                    }

                                }

                                Log.d(LOG_TAG, "bonjour service lost: " + bonjourService.getServiceName());
                                Log.d(LOG_TAG, "service size:lost " + services.size());
                            } else {
//                            mServiceAdapter.add(bonjourService);
                                Log.d(LOG_TAG, "bonjour service found: " + bonjourService.getServiceName());

//                                if (!bonjourService.getServiceName().equals(mServiceName))
                                    add(bonjourService);
                                Log.d(LOG_TAG, "service size:add " + services.size());

//                            if (bonjourService.getServiceName().equals(mDoctorServiceName)) {
//                            Log.d(LOG_TAG, "isRegistered: "+isRegistered());
                                Log.d(LOG_TAG, "service name:" + mServiceName);
                                if (bonjourService.getServiceName().equals(mServiceName)) {
                                    if (context instanceof BillingActivity && Utility.isNetworkAvailable(context)) {
                                        Log.d(LOG_TAG, "inside switch true");
                                        BillingActivity activity = (BillingActivity) context;
                                        activity.setServiceFlagVisibility(true);
                                        connected = true;

                                    }

                                }


                            }
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.d(LOG_TAG, "error in browse");
                            Log.e(LOG_TAG, "error", throwable);
                        }
                    });
        }else {
            Toast.makeText(context,"Please wait..initialising",Toast.LENGTH_LONG).show();
        }
    }

    public RxDnssd getRxDnssd() {
        return rxDnssd;
    }

    public void stopBrowse() {
        Log.d("TAG", "Stop browsing");
        browseSubscription.unsubscribe();
        browseSubscription = null;
    }


    public void onReset() {
        if (browseSubscription != null) {
            Log.d(LOG_TAG, "inside browse not null");
            if (!browseSubscription.isUnsubscribed()) {
                browseSubscription.unsubscribe();
            }
            browseSubscription = null;
        }
        if (registerSubscription != null) {
            Log.d(LOG_TAG, "inside registration not null");
            if (!registerSubscription.isUnsubscribed()) {
                registerSubscription.unsubscribe();
            }
            registerSubscription = null;
        }

        connected = false;

    }

//    public void onPatientSync(BonjourService service) {
//        Log.d(LOG_TAG, "inside patient sync service: " + service);
//
//
//        if (mChatConnection != null && mRegisteredPatientList != null) {
////                if (services.get(i).getServiceName().equals(mDoctorServiceName)) {
////                    mServiceInfo = services.get(i);
//            if (service.getInet4Address() != null) {
////                    IP_SERVER = services.get(i).getInet4Address();
//                Log.d(LOG_TAG, "ip4:" + service.getInet4Address());
//                mChatConnection.connectToServer(service.getInet4Address(), service.getPort(), mRegisteredPatientList);
//            } else {
//                mChatConnection.connectToServer(service.getInet6Address(), service.getPort(), mRegisteredPatientList);
//                Log.d(LOG_TAG, "ip6:" + service.getInet6Address());
//
////                    IP_SERVER = services.get(i).getInet6Address();
//            }
////                }else if (services.get(i).getServiceName().equals(mServiceName)){
////                    Log.d(LOG_TAG, "same machine:");
////                else {
////                    Log.d(LOG_TAG, "service not found");
//        } else {
//            Log.d(LOG_TAG, "chat conn or registered list of patients is null");
//        }
//
//
//    }

    public boolean fetchBillForService(Context context) {

        BonjourService mService = null;

        if (services.size() > 0) {

            for (int i = 0; i < services.size(); i++) {
                if (services.get(i).getServiceName().equals(mDoctorServiceName)) {
                    mService = services.get(i);
                    Log.d(LOG_TAG, "service to connect to: " + mService);
                } else {
                    startBrowse(context);
                }
            }
        } else {
            Toast.makeText(context, "Finding Device..Please wait", Toast.LENGTH_SHORT).show();
            startBrowse(context);
        }


        if (mService != null) {
            if (mChatConnection != null) {
                if (mService.getInet4Address() != null) {
                    Log.d(LOG_TAG, "ip4:" + mService.getInet4Address());
                    mChatConnection.connectToServer(mService.getInet4Address(), mService.getPort());
                    return true;
                } else {
                    Toast.makeText(context, "Finding device..Please wait and try again", Toast.LENGTH_SHORT).show();
                    remove(mService);
                    startBrowse(context);
                }
            }
        }
        return false;
    }

    private synchronized void remove(BonjourService service) {
            services.remove(service);
        }


    public RegisteredPatientData getPatientData() {
        return mPatientData;
    }

    public void setPatientData(RegisteredPatientData patientData) {
        this.mPatientData = patientData;
    }

    public boolean isSingleMode() {
        return isSingleMode;
    }

    public void setSingleMode(boolean singleMode) {
        isSingleMode = singleMode;
    }

    public String getDoctorServiceName() {
        return mDoctorServiceName;
    }

    public void setDoctorServiceName(String doctorServiceName) {
        this.mDoctorServiceName = doctorServiceName;
        Log.d(LOG_TAG, "doctor service name: " + mDoctorServiceName);
    }

    public void setRegisteredPatientList(Context context) {

        BonjourService mService = null;

        for (int i = 0; i < services.size(); i++) {
            if (services.get(i).getServiceName().equals(mDoctorServiceName)) {
                mService = services.get(i);
                Log.d(LOG_TAG, "service to connect to: " + mService);
            }
        }

        if (mService != null) {
//            getPatientsAndConnect(context, mService);
        }

    }

    private synchronized void add(BonjourService service) {
        if (!services.contains(service)) {
            services.add(service);
        }
    }



    private boolean isRegistered() {
        for (int i = 0; i < services.size(); i++) {
            if (services.get(i).getServiceName().equals(mServiceName)) {
                return true;
            }
        }
        return false;
    }

    public Subscription getBrowseSubscription() {
        return browseSubscription;
    }

    public Subscription getRegisterSubscription() {
        return registerSubscription;
    }


}
