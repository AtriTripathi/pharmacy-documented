package com.prezcription.pharmacyapp.async;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.print.PrintManager;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.qrcode.WriterException;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.adapter.DetailBillRecyclerAdapter;
import com.prezcription.pharmacyapp.adapter.PrintAdapter;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.dto.BillData;
import com.prezcription.pharmacyapp.data.dto.Patient;

/**
 * Created by anubhav on 17/12/17.
 */

public class ConvertToPdf {

    ProgressDialog progressBar;
    int mPatientId;

    private static final String LOG_TAG =  ConvertToPdf.class.getSimpleName();

    private float mFees;

    private float headerSpacing;
    private float footerSpacing;
    private SharedPreferences mSettingsPreferences;
    private SharedPreferences mArrangementPreferences;
    private Context mContext;
    protected Font font;
    private ArrayList<Integer> mHeadingList;
    private ArrayList<String> mPathOfImages;
    String mPatientReference;
    String mFollowUpDate;
    String date;
    String mPath;
    Rectangle pageSize;
    Font defaultRegularFont;
    Font defaultItalicFont;
    Font defaultSemiBoldFont;
    Font defaultBoldFont;
    Font defaultTableHeadingFont;
    Font defaultBoldItalicFont;
    Font defaultHeadingFont;
    int fontSize;
    private final String proximaNovaRegular = "Proxima-nova-regular";
    private final String proximaNovaItalic = "Proxima-nova-italic";
    private final String proximaNovaBold = "Proxima-nova-bold";
    private final String proximaNovaSemiBold = "Proxima-nova-semibold";
    private final String proximaNovaBoldItalic = "Proxima-nova-bold-italic";
    private SharedPreferences mUserLogPreferences;

    String mBillNumber, mBillDate,mDoctorName;

    private final int PRESCRIPTION_TYPE = 0;
    private final int INVOICE_TYPE = 1;



    ArrayList<BillData> mInvoiceDataFormatted;
    HashMap<String, Double> mAmountMap;

    public ConvertToPdf(Context context, ArrayList<BillData> invoiceDataFormatted, String date){
        mContext = context;
        mSettingsPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.date = date;
        mInvoiceDataFormatted = invoiceDataFormatted;
    }

    public ConvertToPdf(Context context, HashMap<String,Double> amountMap, String date, int type){
        mContext = context;
        mSettingsPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.date = date;
        mAmountMap = amountMap;
    }

    public void viewPdf(File file) {

        Uri fileUri = FileProvider.getUriForFile(mContext,
                mContext.getString(R.string.file_provider_authority),
                file);


        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(fileUri, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);



        Log.d(LOG_TAG, "file path: "+file.getAbsolutePath());

        if (intent.resolveActivity(mContext.getPackageManager()) != null) {
            mContext.startActivity(intent);
        } else {
//            Toast.makeText(mContext, "No application available for pdf", Toast.LENGTH_SHORT).show();
        }

    }

    public void emailNote(File file, String subject, String body) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, body);
        Uri uri = Uri.fromFile(file);
        email.putExtra(Intent.EXTRA_STREAM, uri);
        email.setType("message/rfc822");

        if (email.resolveActivity(mContext.getPackageManager()) != null) {
            mContext.startActivity(email);
        } else {
//            Toast.makeText(mContext, "Device unable to send email", Toast.LENGTH_SHORT).show();
        }
    }

    public void promptForNextAction(final File file, final String subject, final String body) {
        final String[] options = {mContext.getString(R.string.label_print), mContext.getString(R.string.label_email), mContext.getString(R.string.label_preview),
                mContext.getString(R.string.label_cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Note Saved, What Next?");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (file != null) {
                    if (options[which].equals(mContext.getString(R.string.label_email))) {
                        emailNote(file, subject, body);
                    } else if (options[which].equals(mContext.getString(R.string.label_preview))) {
                        viewPdf(file);
                    } else if (options[which].equals(mContext.getString(R.string.label_print))) {
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                            PrintManager printManager = (PrintManager) mContext.getSystemService(Context.PRINT_SERVICE);
                            String jobName = mContext.getString(R.string.app_name) + " Document";
                            printManager.print(jobName, new PrintAdapter(file), null);
                        } else {
//                            Toast.makeText(mContext, "Please update your OS to print", Toast.LENGTH_SHORT).show();
                        }
                    } else if (options[which].equals(mContext.getString(R.string.label_cancel))) {
                        dialog.dismiss();
                    }
                } else {
//                    Toast.makeText(mContext, "Not able to create pdf, Please bo back and try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }






    public File createInvoicePdf() throws IOException, DocumentException {

        String mPath;

        font = new Font(Font.FontFamily.HELVETICA, 10);


        if (Utility.checkWritePermission(mContext)) {
            String root = Environment.getExternalStorageDirectory().toString();
            String dirPath = root + "/prezcription/saved_invoice_pdfs";
            File myDir = new File(dirPath);
            boolean isDirectoryCreated = myDir.mkdir();
            if (!isDirectoryCreated) {
                isDirectoryCreated = myDir.mkdirs();
                Log.i(LOG_TAG, "Pdf Directory created");
            }

            //Create time stamp
            Date date = new Date();
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
            String timeStamp = "patient_prescription";

            Log.d(LOG_TAG, " Absolute path " + myDir.getAbsolutePath());

            mPath = dirPath + "/" + timeStamp + ".pdf";
            File myFile = new File(myDir, timeStamp + ".pdf");
//            Log.d(LOG_TAG,"ffff "+ myFile.getAbsolutePath());
            boolean created = myFile.createNewFile();
//            Log.d(LOG_TAG,"Created or not "+ created);
            if (myFile.isFile()) {

//                Log.d(LOG_TAG,"File exist");

                if (myFile.exists()) {
                    Log.d(LOG_TAG, "File exists");
                }

                Log.d(LOG_TAG, myFile.getAbsolutePath());

                FileOutputStream output = new FileOutputStream(myFile);

                float headerSpacing = (float) 3 * mSettingsPreferences.getInt(Utility.SETTINGS_PDF_HEADER_SPACING, 12);
                float footerSpacing = (float) 2 * mSettingsPreferences.getInt(Utility.SETTINGS_PDF_FOOTER_SPACING, 18);
                float leftSpacing = (float) 2 * mSettingsPreferences.getInt(Utility.SETTINGS_PDF_LEFT_SPACING, 18);
                float rightSpacing = (float) 2 * mSettingsPreferences.getInt(Utility.SETTINGS_PDF_RIGHT_SPACING, 18);

                int pageTypeValue = Integer.parseInt(mSettingsPreferences.getString(Utility.SETTINGS_PDF_SIZE_TYPE, "4"));

                pageSize = getSelectedPageSize(pageTypeValue);

                Log.d(LOG_TAG, "page size is " + mSettingsPreferences.getString(Utility.SETTINGS_PDF_SIZE_TYPE, "4"));

                //Step 1
                Document document;


                if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, rightSpacing, headerSpacing, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, 36, headerSpacing, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false)) {
                    document = new Document(pageSize, 36, rightSpacing, headerSpacing, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, rightSpacing, headerSpacing, 36);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, rightSpacing, 36, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false)) {
                    document = new Document(pageSize, 36, 36, headerSpacing, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, 36, headerSpacing, 36);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false)) {
                    document = new Document(pageSize, 36, rightSpacing, headerSpacing, 36);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, 36, 36, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false)) {
                    document = new Document(pageSize, 36, rightSpacing, 36, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false) && mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, rightSpacing, 36, 36);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_HEADER_SPACING, false)) {
                    document = new Document(pageSize, 36, 36, headerSpacing, 36);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FOOTER_SPACING, false)) {
                    document = new Document(pageSize, 36, 36, 36, footerSpacing);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_LEFT_SPACING, false)) {
                    document = new Document(pageSize, leftSpacing, 36, 36, 36);
                } else if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_RIGHT_SPACING, false)) {
                    document = new Document(pageSize, 36, rightSpacing, 36, 36);
                } else {
                    document = new Document(pageSize, 36, 36, 36, 36);
                }

                //Register required fonts
//                FontFactory.register("assets/fonts/proxima-nova-regular.ttf", proximaNovaRegular);
//                FontFactory.register("assets/fonts/proxima-nova-italic.ttf", proximaNovaItalic);
//                FontFactory.register("assets/fonts/proxima-nova-bold.ttf", proximaNovaBold);
//                FontFactory.register("assets/fonts/proxima-nova-semibold.ttf", proximaNovaSemiBold);
//                FontFactory.register("assets/fonts/proxima-nova-bold-italic.ttf", proximaNovaBoldItalic);

                //Step 2
                PdfWriter writer = PdfWriter.getInstance(document, output);

                MyFooter event = new MyFooter();
                writer.setPageEvent(event);

                //Step 3
                document.open();


//                addInvoicePdfItem(document);
                addInvoiceItem(document);

                //Step 5: Close the document
                document.close();

                Log.d(LOG_TAG, myFile.getAbsolutePath());
//                progressBar.cancel();
//                progressBar.dismiss();
                return myFile;
            } else {
                return null;
            }
        } else {
            return null;
        }

    }


    private Rectangle getSelectedPageSize(int entryValue) {

        switch (entryValue) {
            case 0:
                return PageSize.A0;
            case 1:
                return PageSize.A1;
            case 2:
                return PageSize.A2;
            case 3:
                return PageSize.A3;
            case 4:
                return PageSize.A4;
            case 5:
                return PageSize.A5;
            case 6:
                return PageSize.A6;
            case 7:
                return PageSize.A7;
            case 8:
                return PageSize.A8;
            case 9:
                return PageSize.LETTER;
            case 10:
                return PageSize.POSTCARD;
        }
        return null;

    }

    private void addQRImageInPdf(Document document, Bitmap bitmap, int scale, int align){
        try {

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
            Image image = null;
            image = Image.getInstance(stream.toByteArray());
            image.scalePercent(scale);
            image.setAlignment(align);
            document.add(image);
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    private void addImageInPdf(Document document, String path, int scale, int align) {


        try {
            // get input stream
            InputStream ims = new FileInputStream(path);
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 50, stream);
            Image image = null;
            image = Image.getInstance(stream.toByteArray());
            image.scalePercent(scale);
            image.setAlignment(align);
            document.add(image);
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    private void addLogoInPdf(Document document, String bitmapString, int scale, int align) {


        try {
            // get input stream
            Bitmap bitmap = Utility.decodeBase64(bitmapString);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            Image image = null;
            image = Image.getInstance(stream.toByteArray());
            image.scalePercent(scale);
            image.setAlignment(align);
            document.add(image);
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


//    private void addImageInPdfFromAsset(Document document, String path) {
//
//        try {
//            // get input stream
//            Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_rx);
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            Image image = Image.getInstance(stream.toByteArray());
//            if (pageSize == PageSize.A5) {
//                image.scalePercent(20);
//            } else {
//                image.scalePercent(25);
//            }
//            image.setAlignment(Element.ALIGN_LEFT);
//            document.add(image);
//        } catch (BadElementException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
//
//
//    }

    private static void addMetaData(Document document) {
        document.addTitle("Prescription");
        document.addSubject("Prescription");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Prezcription");
        document.addCreator("Prezcription");
    }

    private void setProximaNovaAsDefaultFonts(int fontSize) {

        defaultRegularFont = FontFactory.getFont(proximaNovaRegular, "Cp1253", true, fontSize);
        defaultBoldItalicFont = FontFactory.getFont(proximaNovaBoldItalic, "Cp1253", true, fontSize);
        defaultBoldFont = FontFactory.getFont(proximaNovaBold, "Cp1253", true, fontSize);
        defaultItalicFont = FontFactory.getFont(proximaNovaItalic, "Cp1253", true, fontSize);
        defaultSemiBoldFont = FontFactory.getFont(proximaNovaSemiBold, "Cp1253", true, fontSize);
        defaultTableHeadingFont = FontFactory.getFont(proximaNovaRegular, "Cp1253", true, fontSize - 4);

    }

    private void setAsDefaultFonts(int fontSize) {

        int fontTypeValue = Integer.parseInt(mSettingsPreferences.getString(Utility.SETTINGS_PDF_FONT_TYPE, "0"));

        if (fontTypeValue == 0) {
            defaultRegularFont = FontFactory.getFont(FontFactory.HELVETICA, fontSize, Font.NORMAL);
            defaultBoldItalicFont = FontFactory.getFont(FontFactory.HELVETICA_BOLDOBLIQUE, fontSize, Font.NORMAL);
            defaultBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, fontSize);
            defaultItalicFont = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, fontSize);
            defaultSemiBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, fontSize);
            defaultTableHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, fontSize - 4);
        } else if (fontTypeValue == 1) {

            defaultRegularFont = FontFactory.getFont(proximaNovaRegular, "Cp1253", true, fontSize);
            defaultBoldItalicFont = FontFactory.getFont(proximaNovaBoldItalic, "Cp1253", true, fontSize);
            defaultBoldFont = FontFactory.getFont(proximaNovaBold, "Cp1253", true, fontSize);
            defaultItalicFont = FontFactory.getFont(proximaNovaItalic, "Cp1253", true, fontSize);
            defaultSemiBoldFont = FontFactory.getFont(proximaNovaSemiBold, "Cp1253", true, fontSize);
            defaultTableHeadingFont = FontFactory.getFont(proximaNovaRegular, "Cp1253", true, fontSize - 4);

        } else {
            defaultRegularFont = FontFactory.getFont(FontFactory.HELVETICA, fontSize, Font.NORMAL);
            defaultBoldItalicFont = FontFactory.getFont(FontFactory.HELVETICA_BOLDOBLIQUE, fontSize, Font.NORMAL);
            defaultBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, fontSize);
            defaultItalicFont = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, fontSize);
            defaultSemiBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, fontSize);
            defaultTableHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, fontSize - 4);
        }

    }

    private float getConsultationFees(){

        for(BillData data:mInvoiceDataFormatted){

            if(data.getViewType()== DetailBillRecyclerAdapter.CONSULTATION_HEADER){
                Log.d(LOG_TAG,"inside fees is "+ data.getPrice());
                return data.getPrice();
            }

        }
        return 0;

    }



    private void addInvoiceItem(Document document){

        double amount = 0;
        double amountToBePaid = 0;

        float allMedicineAmount = 0f;
        float allVaccineAmount = 0f;

        mFees = getConsultationFees();

        Log.d(LOG_TAG, "fee:convertToPDf "+mFees);


        int fontSize = 0;

        Font chapterFont;
        if (pageSize == PageSize.A5) {

            setAsDefaultFonts(10);
            fontSize = 10;

            chapterFont = defaultBoldFont;
            chapterFont.setSize(12);
        } else {
            setAsDefaultFonts(9);
            fontSize = 9;
            chapterFont = defaultBoldFont;
            chapterFont.setSize(10);
        }

        if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_FONT_SIZING, false)) {
            fontSize = mSettingsPreferences.getInt(Utility.SETTINGS_PDF_FONT_SIZING, 9);

            setAsDefaultFonts(fontSize);
            chapterFont = defaultBoldFont;
            chapterFont.setSize(fontSize + 2);
        }

        if (mSettingsPreferences.getBoolean(Utility.SETTINGS_ALLOW_LOGO, false)) {

            String bitmapString = mContext.getSharedPreferences(Utility.USER_LOG_PREF, Context.MODE_PRIVATE).
                    getString(Utility.PHOTO_KEY, null);

            addLogoInPdf(document,bitmapString,25,Element.ALIGN_LEFT);
        }



        try {
            Paragraph paragraphHeadingName = new Paragraph();
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100);
            table.addCell(getCell("--------------------------------------------",Element.ALIGN_LEFT));
            table.addCell(createCellWithFont("Tax Invoice",0,1,Element.ALIGN_CENTER,0,chapterFont));
            table.addCell(getCell("--------------------------------------------",Element.ALIGN_RIGHT));

            paragraphHeadingName.add(table);
            document.add(paragraphHeadingName);

        } catch (DocumentException e) {
            e.printStackTrace();
        }

        //Patient Details




        //Bill ID
        try {

            Log.d(LOG_TAG, "inside bill id");
            Paragraph pp2 = new Paragraph();
            pp2.setFont(defaultRegularFont);
            pp2.setSpacingAfter(10f);

            PdfPTable patientTable2 = new PdfPTable(2);
            patientTable2.setWidthPercentage(100);

            PdfPTable patientTable3 = new PdfPTable(10);
            patientTable3.setWidthPercentage(100);
            patientTable3.setWidths(new int[]{1, 4, 2, 2,1,2,2,2,2,2});


            PdfPTable patientTable4 = new PdfPTable(2);
            patientTable4.setWidthPercentage(100);

            float paddingBottom;

            if (pageSize == PageSize.A5) {
                paddingBottom = 4.0f;
            } else {
                paddingBottom = 6.0f;
            }

            Font font = new Font();
            font.setSize(7);

            double totalWithoutTaxes = 0;



            for (BillData data : mInvoiceDataFormatted) {

                if (data.getItemType() == DetailBillRecyclerAdapter.DETAILS) {


                    mBillNumber = data.getGlobalBillId();
//                   mDoctorName = doctorName;
                    mBillDate = data.getBillDate();

//                    patientTable2.addCell(getCell("Bill No: " +data.getGlobalBillId(), PdfPCell.ALIGN_LEFT));
//                    patientTable2.addCell(getCell("Doctor : "+doctorName,PdfPCell.ALIGN_RIGHT));
//                    patientTable2.addCell(getCell("Bill Date: " + Utility.getTimeInCurrentDateFormat(data.getBillDate()), PdfPCell.ALIGN_LEFT));
//                    patientTable2.addCell(getCell("", PdfPCell.ALIGN_RIGHT));



                    Log.d(LOG_TAG, "inside bill id");
                    Paragraph pp3 = new Paragraph();
                    pp3.setFont(font);
                    pp3.setSpacingAfter(10f);

//                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_id), 2, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_serial_number), 1, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_name), 1, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_batchId), 1, 1, Element.ALIGN_CENTER,font));
//                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_mfgr), 2, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_exp), 1, 1, Element.ALIGN_CENTER,font));
//                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_mrp), 2, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_quantity), 1, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_mrp), 1, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_rate), 1, 1, Element.ALIGN_CENTER,font));
//                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_discount), 2, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_sGst), 1, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_cGst), 1, 1, Element.ALIGN_CENTER,font));
                    patientTable3.addCell(createCellWithSemiBoldFont(mContext.getResources().getString(R.string.bill_item_amount), 1, 1, Element.ALIGN_CENTER,font));

//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));



                    for (int i=0; i<data.getLineitemsBean().size(); i++) {

                        if(i==(data.getLineitemsBean().size()-1)&&data.getConsultationFees()==0){

                            patientTable3.addCell(createCellWithFontLastItem(String.valueOf(i+1), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastItem(data.getLineitemsBean().get(i).getItemName(), 1, 1, Element.ALIGN_LEFT, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastItem(data.getLineitemsBean().get(i).getBatchid(), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
//                        patientTable3.addCell(createCellWithFont(data.getLineitemsBean().get(i).getMfgr(), 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastItem(Utility.getTimeInSimpleDateFormat(data.getLineitemsBean().get(i).getExpiry()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
//                        patientTable3.addCell(createCellWithFont((Utility.getTimeInSimpleDateFormat(data.getLineitemsBean().get(i).getExpiry())), 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
//                        patientTable3.addCell(createCellWithFont(String.valueOf(data.getLineitemsBean().get(i).getMrp()), 0, 1, Element.ALIGN_CENTER, paddingBottom,font));

                            patientTable3.addCell(createCellWithFontLastItem(String.valueOf(data.getLineitemsBean().get(i).getItemCount()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastItem(String.valueOf(data.getLineitemsBean().get(i).getNetRate()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getItemGross())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));

//                        patientTable3.addCell(createCellWithFont(String.valueOf(data.getLineitemsBean().get(i).getItemDisc()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));

                            patientTable3.addCell(createCellWithFontLastItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getSGST())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getCGST())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastRightItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getItemGross()*data.getLineitemsBean().get(i).getItemCount())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            totalWithoutTaxes += data.getLineitemsBean().get(i).getItemGross()*data.getLineitemsBean().get(i).getItemCount();
                        }else {

//                        patientTable3.addCell(createCellWithFont(data.getLineitemsBean().get(i).getItemid(), 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFont(String.valueOf(i+1), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFont(data.getLineitemsBean().get(i).getItemName(), 1, 1, Element.ALIGN_LEFT, paddingBottom,font));
                            patientTable3.addCell(createCellWithFont(data.getLineitemsBean().get(i).getBatchid(), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
//                        patientTable3.addCell(createCellWithFont(data.getLineitemsBean().get(i).getMfgr(), 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFont(Utility.getTimeInSimpleDateFormat(data.getLineitemsBean().get(i).getExpiry()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
//                        patientTable3.addCell(createCellWithFont((Utility.getTimeInSimpleDateFormat(data.getLineitemsBean().get(i).getExpiry())), 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
//                        patientTable3.addCell(createCellWithFont(String.valueOf(data.getLineitemsBean().get(i).getMrp()), 0, 1, Element.ALIGN_CENTER, paddingBottom,font));

                            patientTable3.addCell(createCellWithFont(String.valueOf(data.getLineitemsBean().get(i).getItemCount()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFont(String.valueOf(data.getLineitemsBean().get(i).getNetRate()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontLastItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getItemGross())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
//                        patientTable3.addCell(createCellWithFont(String.valueOf(data.getLineitemsBean().get(i).getItemDisc()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));

                            patientTable3.addCell(createCellWithFont(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getSGST())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFont(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getCGST())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            patientTable3.addCell(createCellWithFontRightItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getLineitemsBean().get(i).getItemGross()*data.getLineitemsBean().get(i).getItemCount())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                            totalWithoutTaxes += data.getLineitemsBean().get(i).getItemGross()*data.getLineitemsBean().get(i).getItemCount();
                        }

                    }

                    if(data.getConsultationFees()!=0&&data.getLineitemsBean().size()>0){

                        patientTable3.addCell(createCellWithFont(String.valueOf(data.getLineitemsBean().size()+1), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("CONSULTATION FEES", 1, 1, Element.ALIGN_LEFT, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("-", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("-", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("1", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont(String.valueOf(data.getConsultationFees()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("-", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("0", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("0", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontRightItem(String.valueOf(data.getConsultationFees()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));


                    }else if(data.getConsultationFees()!=0&&data.getLineitemsBean().size()==0){

                        patientTable3.addCell(createCellWithFontLastItem(String.valueOf(data.getLineitemsBean().size()+1), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastItem("CONSULTATION FEES", 1, 1, Element.ALIGN_LEFT, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastItem("-", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastItem("-", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastItem("1", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastItem(String.valueOf(data.getConsultationFees()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFont("-", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastItem("0", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastItem("0", 1, 1, Element.ALIGN_CENTER, paddingBottom,font));
                        patientTable3.addCell(createCellWithFontLastRightItem(String.valueOf(data.getConsultationFees()), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));

                    }

                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCellWithFont("Sub Total(Without taxes): ", 1, 3, Element.ALIGN_RIGHT, paddingBottom,font));



                    if(data.getConsultationFees()!=0){
                        totalWithoutTaxes = totalWithoutTaxes+data.getConsultationFees();
                    }

                    patientTable3.addCell(createCellWithFontRightItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(totalWithoutTaxes)), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));


                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCellWithFont("State GST: ", 1, 3, Element.ALIGN_RIGHT, paddingBottom,font));
                    patientTable3.addCell(createCellWithFontRightItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getTotalsGST())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));

                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCellWithFont("Center GST: ", 1, 3, Element.ALIGN_RIGHT, paddingBottom,font));
                    patientTable3.addCell(createCellWithFontRightItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getTotalcGST())), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));


                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCellWithFont("Total Amount : ", 1, 3, Element.ALIGN_RIGHT, paddingBottom,font));

                    double totalAfterTaxes = Utility.getValueUptoTwoPlacesOfDecimal(Double.valueOf(data.getTotalAmount()) + Double.valueOf(data.getTotalDiscount()));

                    if(data.getConsultationFees()!=0){
                        totalAfterTaxes = totalAfterTaxes+data.getConsultationFees();
                    }

                    patientTable3.addCell(createCellWithFontRightItem(String.valueOf(totalAfterTaxes), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));

                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    patientTable3.addCell(createCellWithFont("Discount on medicine (-): ", 1, 3, Element.ALIGN_RIGHT, paddingBottom,font));
                    patientTable3.addCell(createCellWithFontRightItem(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(Double.valueOf(data.getTotalDiscount()))), 1, 1, Element.ALIGN_CENTER, paddingBottom,font));


                    patientTable3.addCell(createCellWithFont(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                    patientTable3.addCell(createCellWithFont(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                    patientTable3.addCell(createCellWithFont(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                    patientTable3.addCell(createCellWithFont(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                    patientTable3.addCell(createCellWithFont(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                    patientTable3.addCell(createCellWithFont(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom,font));
                    String amountPayableString = "Amount Payable: "+"\n"+"Round off: ";
                    patientTable3.addCell(createCellWithFontLastItem(amountPayableString, 1, 3, Element.ALIGN_RIGHT, paddingBottom,font));

                    double finalPayableValue = Utility.getValueUptoTwoPlacesOfDecimal(Double.valueOf(data.getTotalAmount()));

                    if(data.getConsultationFees()!=0){
                        finalPayableValue = finalPayableValue+data.getConsultationFees();
                    }

                    double finalPayableValueAfterRoundOff = Math.round(finalPayableValue);
                    double roundOffValue = Utility.getValueUptoTwoPlacesOfDecimal(finalPayableValue-finalPayableValueAfterRoundOff);

                    String finalStringForPayableAmount = mContext.getString(R.string.rupee)+" "+String.valueOf(finalPayableValueAfterRoundOff)+"\n"+"("+String.valueOf(roundOffValue)+")";

                    patientTable3.addCell(createCellWithFontLastRightItem(finalStringForPayableAmount, 1, 1, Element.ALIGN_CENTER, paddingBottom,font));


//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
//                    patientTable3.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));

//                    Log.d(LOG_TAG, "inside bill id");
//                    Paragraph pp4 = new Paragraph();
//                    pp4.setFont(font);
//                    pp4.setSpacingAfter(10f);
//
//                    try {
//                        patientTable4.addCell(getCell("", PdfPCell.ALIGN_LEFT));
//                        patientTable4.addCell(getCell("Amount Due: "+String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(Double.valueOf(data.getTotalAmount()))),PdfPCell.ALIGN_RIGHT ));
//                    }catch (NumberFormatException e){
//                        e.printStackTrace();
//                    }
//
//                    patientTable4.addCell(getCell("",PdfPCell.ALIGN_LEFT ));
//
//                    patientTable4.addCell(getCell("State GST: "+String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getTotalsGST())),PdfPCell.ALIGN_RIGHT ));
//                    patientTable4.addCell(getCell("",PdfPCell.ALIGN_LEFT ));
//                    patientTable4.addCell(getCell("Center GST: "+String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(data.getTotalcGST())),PdfPCell.ALIGN_RIGHT ));
//
//
//                    try {
//                        patientTable4.addCell(getCell("", PdfPCell.ALIGN_LEFT));
//                        patientTable4.addCell(getCell("Discount: "+String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(Double.valueOf(data.getTotalDiscount()))),PdfPCell.ALIGN_RIGHT ));
//
//                    }catch (NumberFormatException e){
//                        e.printStackTrace();
//                    }
//



                    pp2.add(patientTable2);
                    pp2.setSpacingBefore(2f);
                    pp3.add(patientTable3);
//                    pp4.add(patientTable4);

                    document.add(pp2);
                    document.add(pp3);
//                    document.add(pp4);

                }else if (data.getItemType() == DetailBillRecyclerAdapter.PATIENT){

                    Uri uri = PharmacyContract.DoctorEntry.CONTENT_URI;

                    ContentResolver contentResolver = mContext.getContentResolver();

                    String selection = PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID+ " = ? ";
                    String[] selectionArgs = {String.valueOf(data.getDocId())};

                    Log.d(LOG_TAG, "customer id: "+data.getDocId());
//                    String[] selectionArgs = {String.valueOf(1)};

                    String[] projection = {PharmacyContract.DoctorEntry.COLUMN_DOCTOR_NAME,
                            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_QUALIFICATION,
                            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_REGISTRATION_NUMBER,
                            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID};

                    Cursor cursor = contentResolver.query(uri,projection,selection,selectionArgs,null);



                    String doctorName = null;
                    String doctorQualification = null;
                    String doctorRegNumber = null;
                    String doctorWebId = null;

                    while (cursor.moveToNext()){

                        doctorName = cursor.getString(0);
                        doctorQualification = cursor.getString(1);
                        doctorRegNumber = cursor.getString(2);
                        doctorWebId = cursor.getString(3);
                    }


                    if(doctorName==null){
                        doctorName = "NA";
                    }else {
                        doctorName =  "Dr. "+doctorName;
                    }

                    if(cursor!=null){
                        cursor.close();
                    }


                    Patient patient = data.getPatient();

                    try {
                        Paragraph pp1 = new Paragraph();
                        pp1.setFont(defaultRegularFont);
                        pp1.setSpacingBefore(8);

                        PdfPTable organisationTable = new PdfPTable(2);
                        organisationTable.setWidthPercentage(100);

                        if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_ORGANISATION_DETAILS, false)) {


                            String organisationName = mSettingsPreferences.getString(Utility.SETTINGS_PDF_ORGANISATION_NAME, null);
                            String websiteName = mSettingsPreferences.getString(Utility.SETTINGS_PDF_WEBSITE_NAME, null);
                            String address = mSettingsPreferences.getString(Utility.SETTINGS_PDF_ADDRESS, null);
                            String gstNUmber = mSettingsPreferences.getString(Utility.SETTINGS_GST_NUMBER, null);
                            Log.d(LOG_TAG,"---- gst number availabl"+ gstNUmber);
                            String[] gstNumbers = null;
                            if(gstNUmber!=null){
                                gstNumbers = gstNUmber.split(";");
                            }

                            if (organisationName != null && !organisationName.equals(mContext.getString(R.string.pref_clinic_or_hospital_name_default_value))) {
                                organisationTable.addCell(getCell(organisationName, PdfPCell.ALIGN_LEFT,defaultBoldFont));

                            }else {
                                organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));
                            }

                            Phrase phrase = new Phrase();
                            phrase.add(new Chunk("Doctor: ", defaultRegularFont));
                            phrase.add(new Chunk(doctorName, defaultBoldFont));

                            organisationTable.addCell(getCell(phrase , PdfPCell.ALIGN_RIGHT,defaultBoldFont));

                            if (websiteName != null && !websiteName.equals(mContext.getString(R.string.pref_website_name_default_value))) {
                                organisationTable.addCell(getCell(websiteName, PdfPCell.ALIGN_LEFT));

                            }else {
                                organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));
                            }

                            organisationTable.addCell(getCell("Qual: " +doctorQualification, PdfPCell.ALIGN_RIGHT));


                            if (address != null && !address.equals(mContext.getString(R.string.pref_address_default_value))) {
                                organisationTable.addCell(getCell(address, PdfPCell.ALIGN_LEFT));

                            }else {
                                organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));

                            }
                            organisationTable.addCell(getCell("Reg No: "+ doctorRegNumber, PdfPCell.ALIGN_RIGHT));

                            String doctorGstNumber = null;

                            if(gstNumbers!=null){

                                for(String val:gstNumbers){

                                    String[] vals = val.split("/");

                                    if(vals!=null&&vals.length>1){

                                        if(vals[0].equals(doctorWebId)){
                                            doctorGstNumber = vals[1];
                                        }

                                    }

                                }
                                if(doctorGstNumber!=null) {
                                    organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));
                                    Phrase phraseGst = new Phrase();
                                    phraseGst.add(new Chunk("GSTIN: ", defaultRegularFont));
                                    phraseGst.add(new Chunk(doctorGstNumber.toUpperCase(), defaultBoldFont));
                                    organisationTable.addCell(getCell(phraseGst, PdfPCell.ALIGN_RIGHT,defaultBoldFont));
                                }
                            }



                        }else {
                            organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));
                            organisationTable.addCell(getCell("Doctor: " +doctorName , PdfPCell.ALIGN_RIGHT));
                            organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));
                            organisationTable.addCell(getCell("Qual: " +doctorQualification, PdfPCell.ALIGN_RIGHT));
                            organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));
                            organisationTable.addCell(getCell("Reg No: "+ doctorRegNumber.toUpperCase(), PdfPCell.ALIGN_RIGHT));

                            String gstNUmber = mSettingsPreferences.getString(Utility.SETTINGS_GST_NUMBER, null);
                            String[] gstNumbers = null;
                            if(gstNUmber!=null){
                                gstNumbers = gstNUmber.split(";");
                            }else {
                                Log.d(LOG_TAG,"no gst numbwe availabl");
                            }

                            String doctorGstNumber = null;

                            if(gstNumbers!=null){

                                for(String val:gstNumbers){

                                    String[] vals = val.split("/");

                                    if(vals!=null&&vals.length>1){

                                        if(vals[1].equals(doctorWebId)){
                                            doctorGstNumber = vals[0];
                                        }

                                    }

                                }
                                if(doctorGstNumber!=null) {
                                    organisationTable.addCell(getCell("", PdfPCell.ALIGN_LEFT));
                                    Phrase phraseGst = new Phrase();
                                    phraseGst.add(new Chunk("GST No.: ", defaultRegularFont));
                                    phraseGst.add(new Chunk(doctorGstNumber.toUpperCase(), defaultBoldFont));
                                    organisationTable.addCell(getCell(phraseGst, PdfPCell.ALIGN_RIGHT,defaultBoldFont));
                                }
                            }

                        }



                        PdfPTable patientTable1 = new PdfPTable(2);
                        patientTable1.setWidthPercentage(100);
                        patientTable1.setSpacingBefore(8);

                        patientTable1.addCell(getCell("Patient Name: " + patient.getName(), PdfPCell.ALIGN_LEFT));

                        patientTable1.addCell(getCell("Bill No: " + data.getGlobalBillId(), PdfPCell.ALIGN_RIGHT));

                        float age = patient.getAge();
                        String ageType = patient.getAgeType();
                        String ageText = null;

//                        if (age == 1) {
//                            if (patient.getAgeType().toLowerCase().equals("years")) {
//                                ageText = 1 + " year";
//                            } else if (patient.getAgeType().toLowerCase().equals("months")) {
//                                ageText = 1 + " month";
//                            } else if (patient.getAgeType().toLowerCase().equals("days")) {
//                                ageText = 1 + " day";
//                            }
//                        } else {
//                            ageText = String.valueOf(patient.getAge() + " " + patient.getAgeType().toLowerCase());
//                        }

                        String[] dobArray = patient.getDob().split("-");
                        //dobArray to hold age in day-month-year
                        Log.d(LOG_TAG, "day: "+dobArray[0]+"month: "+dobArray[1]+"year: "+dobArray[2]);

                        ageText = Utility.getAge(Integer.valueOf(dobArray[2]),Integer.valueOf(dobArray[1]),Integer.valueOf(dobArray[0]));
                        Log.d(LOG_TAG, "age: "+age);

                        patientTable1.addCell(getCell("Gender/Age: " + (patient.getGender().equals("M") ? "M" : "F") + "/" + ageText, PdfPCell.ALIGN_LEFT));
                        patientTable1.addCell(getCell("Bill Date: " +Utility.getTimeInCurrentDateFormat(data.getBillDate()), PdfPCell.ALIGN_RIGHT));

//                        patientTable1.addCell(getCell("Date: " + date, PdfPCell.ALIGN_RIGHT));

//                        pp1.add(patientTable1);
//                        pp1.setSpacingBefore(2f);
//

//                        Paragraph pp5 = new Paragraph();
//                        pp5.setFont(defaultRegularFont);
//                        pp5.setSpacingAfter(10f);
//                        PdfPTable patientTable5 = new PdfPTable(2);
//                        patientTable5.setWidthPercentage(100);




//
                        patientTable1.addCell(getCell("Patient Id: " + String.valueOf(patient.getUsername()), PdfPCell.ALIGN_LEFT));
                        patientTable1.addCell(getCell("Payment method: cash/card", PdfPCell.ALIGN_RIGHT));







//                        if (mSettingsPreferences.getBoolean(Utility.SETTINGS_PDF_ALLOW_PATIENT_PHONE_NUMBER, true)) {
//                            patientTable2.addCell(getCell("ID: " + String.valueOf(patient.getPhoneNumber()), PdfPCell.ALIGN_RIGHT));
//                        } else {
//                            patientTable2.addCell(getCell("", PdfPCell.ALIGN_RIGHT));
//                        }

                        pp1.add(organisationTable);
                        pp1.add(patientTable1);
                        document.add(pp1);
//                        document.add(pp5);


                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }


                }
            }
        }catch (DocumentException e){
            e.printStackTrace();
        }








        try {

            Log.d(LOG_TAG, "inside medicine invoice inside");


            PdfPTable table= new PdfPTable(4);
            float paddingBottom;

            if (pageSize == PageSize.A5) {
                paddingBottom = 4.0f;
            } else {
                paddingBottom = 6.0f;
            }
            int serialNumber = 0;
            int count = 0;
            float quantity = 0;
            boolean isFirst = true;

            for (BillData data: mInvoiceDataFormatted){

                if(data.getItemType() == DetailBillRecyclerAdapter.MEDICINE && data.isItemBilled()){

                    if(isFirst){

                        Paragraph paragraphHeadingName = new Paragraph();
                        paragraphHeadingName.setFont(chapterFont);
                        paragraphHeadingName.add("MEDICINES");
                        paragraphHeadingName.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragraphHeadingName);

                        table = new PdfPTable(4);
                        table.setSpacingBefore(10.0f);
                        table.setWidthPercentage(100);
                        table.setWidths(new int[]{6, 5, 6, 3});

                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.medicine_heading), 2, 1, Element.ALIGN_CENTER));
                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.quantity_heading), 2, 1, Element.ALIGN_CENTER));
                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.medicine_price_heading), 2, 1, Element.ALIGN_CENTER));
                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.amount_heading), 2, 1, Element.ALIGN_CENTER));

                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));

                        isFirst = false;
                    }

                    serialNumber++;

                    quantity = data.getQuantity();
                    amount = data.getPrice()*data.getQuantity();

                    allMedicineAmount += amount;

                    table.addCell(createCell(mContext.getString(R.string.medicine_name, serialNumber,
                            data.getName()), 0, 1,
                            Element.ALIGN_LEFT, paddingBottom));
                    table.addCell(createCell(String.valueOf(quantity), 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(data.getPrice()), 0,
                            1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(amount), 0, 1, Element.ALIGN_CENTER, paddingBottom));

                    amountToBePaid = amountToBePaid + amount;

                    count++;

                }

            }

            document.add(table);

        } catch(DocumentException e){
            e.printStackTrace();
        }


        try {

            PdfPTable table= new PdfPTable(4);

            table.setSpacingBefore(10.0f);
            table.setWidthPercentage(100);
            table.setWidths(new int[]{6, 5, 6, 3});

            float paddingBottom;

            if (pageSize == PageSize.A5) {
                paddingBottom = 4.0f;
            } else {
                paddingBottom = 6.0f;
            }

            for (BillData data : mInvoiceDataFormatted) {

                if (data.getItemType() == DetailBillRecyclerAdapter.DISCOUNT && data.isItemBilled()
                        && data.getDiscountType() == DetailBillRecyclerAdapter.DISCOUNT_TYPE_MED) {
//                    Paragraph paragraphHeadingName = new Paragraph();
//                    paragraphHeadingName.setFont(chapterFont);
//                    paragraphHeadingName.add("MEDICINE DISCOUNT");
//                    paragraphHeadingName.setAlignment(Element.ALIGN_CENTER);
//                    document.add(paragraphHeadingName);

                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));

                    float medicineDiscount = data.getDiscountMedicine() * allMedicineAmount * .01f;

                    table.addCell(createCell("MEDICINE DISCOUNT(%)", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(data.getDiscountMedicine()), 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(data.getDiscountMedicine() * allMedicineAmount * .01f), 0, 1, Element.ALIGN_CENTER, paddingBottom));

                    amountToBePaid = amountToBePaid - medicineDiscount;
                }
            }

            document.add(table);

        } catch(DocumentException e){
            e.printStackTrace();
        }



        try {

            Log.d(LOG_TAG, "inside vaccine invoice inside");

            PdfPTable table= new PdfPTable(4);
            float paddingBottom;

            if (pageSize == PageSize.A5) {
                paddingBottom = 4.0f;
            } else {
                paddingBottom = 6.0f;
            }

            int serialNumber = 0;
            int count = 0;
            float quantity = 0;

            boolean isFirst = true;


            for (BillData data: mInvoiceDataFormatted){

                if(data.getItemType()==DetailBillRecyclerAdapter.VACCINATION && data.isItemBilled()){

                    if(isFirst){
                        Paragraph paragraphHeadingName = new Paragraph();
                        paragraphHeadingName.setFont(chapterFont);
                        paragraphHeadingName.add("VACCINATION");
                        paragraphHeadingName.setAlignment(Element.ALIGN_CENTER);
                        document.add(paragraphHeadingName);

                        table = new PdfPTable(4);
                        table.setSpacingBefore(10.0f);
                        table.setWidthPercentage(100);
                        table.setWidths(new int[]{6, 5, 6, 3});
                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.medicine_heading), 2, 1, Element.ALIGN_CENTER));
                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.quantity_heading), 2, 1, Element.ALIGN_CENTER));
                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.medicine_price_heading), 2, 1, Element.ALIGN_CENTER));
                        table.addCell(createCellWithSemiBoldFontAndColor(mContext.getResources().getString(R.string.amount_heading), 2, 1, Element.ALIGN_CENTER));

                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                        table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));

                        isFirst = false;
                    }

                    serialNumber++;

                    quantity = data.getQuantity();
                    amount = data.getPrice()*data.getQuantity();
                    allVaccineAmount += amount;

                    table.addCell(createCell(mContext.getString(R.string.medicine_name, serialNumber,
                            data.getName()), 0, 1,
                            Element.ALIGN_LEFT, paddingBottom));
                    table.addCell(createCell(String.valueOf(quantity), 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(data.getPrice()), 0,
                            1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(amount), 0, 1, Element.ALIGN_CENTER, paddingBottom));

                    amountToBePaid = amountToBePaid+ amount;

                    count++;

                }

            }

            document.add(table);

        } catch(DocumentException e){
            e.printStackTrace();
        }

        try {

            PdfPTable table= new PdfPTable(4);
            table.setSpacingBefore(10.0f);
            table.setWidthPercentage(100);
            table.setWidths(new int[]{6, 5, 6, 3});

            float paddingBottom;

            if (pageSize == PageSize.A5) {
                paddingBottom = 4.0f;
            } else {
                paddingBottom = 6.0f;
            }

            for (BillData data : mInvoiceDataFormatted) {

                if (data.getItemType() == DetailBillRecyclerAdapter.DISCOUNT && data.isItemBilled()
                        && data.getDiscountType() == DetailBillRecyclerAdapter.DISCOUNT_TYPE_VACC) {
//                    Paragraph paragraphHeadingName = new Paragraph();
//                    paragraphHeadingName.setFont(chapterFont);
//                    paragraphHeadingName.add("VACCINE DISCOUNT");
//                    paragraphHeadingName.setAlignment(Element.ALIGN_CENTER);
//                    document.add(paragraphHeadingName);

                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));

                    float vaccineDiscount = data.getDiscountVaccine() * allVaccineAmount * .01f;

                    table.addCell(createCell("VACCINE DISCOUNT(%)", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(data.getDiscountVaccine()), 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(" ", 0, 1, Element.ALIGN_CENTER, paddingBottom));
                    table.addCell(createCell(String.valueOf(data.getDiscountVaccine() * allVaccineAmount * .01f), 0, 1, Element.ALIGN_CENTER, paddingBottom));

                    amountToBePaid = amountToBePaid - vaccineDiscount;

                }
            }

            document.add(table);

        } catch(DocumentException e){
            e.printStackTrace();
        }






//        try {
//
//
//            float paddingBottom;
//
//            if(pageSize == PageSize.A5){
//                paddingBottom = 4.0f;
//            }else {
//                paddingBottom = 6.0f;
//            }
//
//            Log.d(LOG_TAG, "consultation invoice inside");
////            Paragraph paragraphHeadingName = new Paragraph();
////            paragraphHeadingName.setFont(chapterFont);
////            paragraphHeadingName.add("CONSULTATION");
////            paragraphHeadingName.setAlignment(Element.ALIGN_CENTER);
////            document.add(paragraphHeadingName);
//
//            Log.d(LOG_TAG, "fee:convertToPdfconsul "+mFees);
//
//            PdfPTable table = new PdfPTable(2);
//            table.setSpacingBefore(10.0f);
//            table.setWidthPercentage(100);
//            table.setWidths(new int[]{6, 5});
//            table.addCell(createCell("Consultation Fee", 1, 1, Element.ALIGN_LEFT, paddingBottom));
//            table.addCell(createCell(mFees!=-1?String.valueOf(mFees):"NA", 1, 1, Element.ALIGN_RIGHT, paddingBottom));
//
//            document.add(table);
//
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
//
//        try{
//
//            float totalSalesAmount=0;
//
//            for(BillData data:mInvoiceDataFormatted){
//                if(data.getItemType()==DetailBillRecyclerAdapter.DETAILS){
//                    totalSalesAmount = Float.valueOf(data.getTotalAmount());
//                    break;
//                }
//            }
//
//            Log.d(LOG_TAG,"Total sales amount = "+totalSalesAmount);
//
//            double total = mFees +totalSalesAmount ;
//
//            Paragraph paragraphHeadingName = new Paragraph();
//            paragraphHeadingName.setFont(chapterFont);
//            paragraphHeadingName.setSpacingBefore(10f);
//            PdfPTable patientTable = new PdfPTable(2);
//            patientTable.setWidthPercentage(100);
//            patientTable.addCell(getCell("TOTAL", PdfPCell.ALIGN_LEFT, defaultBoldFont));
//            patientTable.addCell(getCell(total!= -1?"Rs: " + String.format(Locale.getDefault(), "%.2f", total): "Rs 0", PdfPCell.ALIGN_RIGHT, defaultRegularFont));
//
//
//            paragraphHeadingName.add(patientTable);
////                paragraphHeadingName.setSpacingBefore(2f);
//            document.add(paragraphHeadingName);
//
////                paragraphHeadingName.add("TOTAL");
////                paragraphHeadingName.setSpacingAfter(10f);
//
//
////                paragraphHeadingName.setAlignment(Element.ALIGN_LEFT);
//
//
////                Paragraph pp = new Paragraph();
////                pp.setFont(defaultRegularFont);
////                pp.setSpacingAfter(10f);
////                pp.setSpacingBefore(10f);
//
////                patientTable.setWidthPercentage(100);
////                patientTable.addCell(getCell(total!= -1?"Rs: " + String.valueOf(total): "Rs 0", PdfPCell.ALIGN_RIGHT));
////
////                pp.add(patientTable);
////                document.add(pp);
//        }catch (DocumentException e){
//            e.printStackTrace();
//        }

    }


//    private void generateAndAddQRCode(PrescriptionDataFormatted dataFormatted, Document document){
//
//        if (dataFormatted != null) {
//            String patientDetailsString = null;
//
//            Gson gson = new Gson();
//            Patient patient = dataFormatted.getPatient();
//            if (patient != null) {
//                patientDetailsString = gson.toJson(patient);
//            }
//
//
//            try {
//                Bitmap bitmap = encodeAsBitmap(patientDetailsString);
//                addQRImageInPdf(document, bitmap, 15, Element.ALIGN_CENTER);
//            } catch (WriterException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        com.google.zxing.common.BitMatrix result;
        int WIDTH = 512;
        int HEIGHT = 512;
        try {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, 512, 512, null);
        } catch (com.google.zxing.WriterException e) {
            // Unsupported format
            e.printStackTrace();
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? Color.BLACK : Color.WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 512, 0, 0, w, h);
        return bitmap;

    }



//    public void manipulatePdf(String src, String dest) throws IOException, DocumentException {
//        Log.d(LOG_TAG," path of pdf is "+ mPath);
//        PdfReader reader = new PdfReader(src);
//        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
//        Image image = Image.getInstance(mContext.getSharedPreferences(Utility.USER_LOG_PREF, Context.MODE_PRIVATE).getString(Utility.SIGNATURE_LOCAL_KEY,null));
//        AffineTransform at = AffineTransform.getTranslateInstance(36, 300);
//        at.concatenate(AffineTransform.getScaleInstance(image.getScaledWidth(), image.getScaledHeight()));
//        PdfContentByte canvas = stamper.getOverContent(1);
//        canvas.addImage(image, at);
//        stamper.close();
//        reader.close();
//    }



    public PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, defaultRegularFont));
        cell.setPadding(3.0f);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public PdfPCell getCell(Phrase text, int alignment, Font font) {
        PdfPCell cell = new PdfPCell(text);
        cell.setPadding(3.0f);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public PdfPCell getCell(String text, int alignment, Font font){
        PdfPCell cell = new PdfPCell(new Phrase(text, font));
        cell.setPadding(3.0f);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public PdfPCell getCell(String text) {
        PdfPCell cell = new PdfPCell(new Phrase(text));
        cell.setPadding(0);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public PdfPCell createCell(String content, float borderWidth, int colspan, int alignment, float paddingBottom) {
        PdfPCell cell = new PdfPCell(new Phrase(content, defaultRegularFont));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidth(borderWidth);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);
        return cell;
    }

    public PdfPCell createCellWithFont(String content, float borderWidth, int colspan, int alignment, float paddingBottom, Font font) {
        font.setStyle(Font.NORMAL);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidth(borderWidth);
//        cell.setBorderWidthLeft(borderWidth);
//        cell.setPaddingLeft(0);
//        cell.setBorderWidthBottom(0);
//        cell.setBorderWidthTop(0);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);
        cell.setBorderColor(new BaseColor(208, 208, 225));
        return cell;
    }

    public PdfPCell createCellWithFontRightBorder(String content, float borderWidth, int colspan, int alignment, float paddingBottom, Font font) {
        font.setStyle(Font.NORMAL);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthLeft(borderWidth);
        cell.setBorderWidthBottom(0);
        cell.setBorderWidthTop(0);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);
        cell.setBorderColor(new BaseColor(208, 208, 225));
        return cell;
    }

    public PdfPCell createCellWithFontLastItem(String content, float borderWidth, int colspan, int alignment, float paddingBottom, Font font) {
        font.setStyle(Font.NORMAL);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidth(borderWidth);
//        cell.setBorderWidthLeft(borderWidth);
//        cell.setBorderWidthBottom(1);
//        cell.setBorderWidthTop(0);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);
        cell.setBorderColor(new BaseColor(208, 208, 225));
        return cell;
    }

    public PdfPCell createCellWithFontRightItem(String content, float borderWidth, int colspan, int alignment, float paddingBottom, Font font) {
        font.setStyle(Font.NORMAL);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidth(borderWidth);
//        cell.setPaddingRight(2);

//        cell.setBorderWidthLeft(borderWidth);
//        cell.setBorderWidthBottom(0);
//        cell.setBorderWidthTop(0);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);
        cell.setBorderColor(new BaseColor(208, 208, 225));
        return cell;
    }

    public PdfPCell createCellWithFontLastRightItem(String content, float borderWidth, int colspan, int alignment, float paddingBottom, Font font) {
        font.setStyle(Font.NORMAL);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidth(borderWidth);
//        cell.setBorderWidthLeft(borderWidth);

//        cell.setBorderWidthBottom(1);
//        cell.setBorderWidthTop(0);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);
        cell.setBorderColor(new BaseColor(208, 208, 225));
        cell.setBorderColorBottom(new BaseColor(255,255,255));
        cell.setBorderColorTop(new BaseColor(255,255,255));
        return cell;
    }


    public PdfPCell createCellWithoutBorder(String content, int colspan, int alignment, float paddingBottom) {
        PdfPCell cell = new PdfPCell(new Phrase(content, defaultRegularFont));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidth(0f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);
        return cell;
    }

    public PdfPCell createCellWithSemiBoldFont(String content, float borderWidth, int colspan, int alignment, Font font) {
        font.setStyle(Font.BOLD);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setPaddingBottom(5.0f);
        cell.setBorderWidth(borderWidth);
        cell.setColspan(colspan);

//        Log.d(LOG_TAG,"lite ink mode: "+mSettingsPreferences.getBoolean(Utility.SETTINGS_ALLOW_INK_LITE_MODE,false));

        cell.setHorizontalAlignment(alignment);
        if (!mSettingsPreferences.getBoolean(Utility.SETTINGS_ALLOW_INK_LITE_MODE,false)) {
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        }
        return cell;
    }

    public PdfPCell createCellWithSemiBoldFontAndColor(String content, float borderWidth, int colspan, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(content, defaultTableHeadingFont));
        cell.setPaddingBottom(5.0f);
        cell.setBorderWidth(borderWidth);
        cell.setColspan(colspan);

//        Log.d(LOG_TAG,"lite ink mode: "+mSettingsPreferences.getBoolean(Utility.SETTINGS_ALLOW_INK_LITE_MODE,false));

        cell.setHorizontalAlignment(alignment);
        if (!mSettingsPreferences.getBoolean(Utility.SETTINGS_ALLOW_INK_LITE_MODE,false)) {
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        }
        return cell;
    }

    public PdfPCell createCellWithSize(String content, float borderWidth, int colspan, int alignment, int size, float paddingBottom) {

        Font font = FontFactory.getFont(FontFactory.HELVETICA, size, Font.NORMAL);
        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setPaddingBottom(paddingBottom);
        cell.setBorderWidth(borderWidth);
        cell.setColspan(colspan);
        cell.setHorizontalAlignment(alignment);

        return cell;
    }

    class MyFooter extends PdfPageEventHelper {
        Font ffont = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 12);

        public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte cb = writer.getDirectContent();
//            Phrase header = new Phrase("this is a header", ffont);
            Phrase footer = new Phrase("This bill is generated by Tealthcare", ffont);
//            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
//                    header,
//                    (document.right() - document.left()) / 2 + document.leftMargin(),
//                    document.top() + 10, 0);
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                    footer,
                    (document.right() - document.left()) / 2 + document.leftMargin(),
                    document.bottom() - 10, 0);
        }
    }




//    public PdfPCell createCell(String content, int colspan, int rowspan, int border) {
//        PdfPCell cell = new PdfPCell(new Phrase(content, font));
//        cell.setColspan(colspan);
//        cell.setRowspan(rowspan);
//        cell.setBorder(border);
//        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        return cell;
//    }

}
