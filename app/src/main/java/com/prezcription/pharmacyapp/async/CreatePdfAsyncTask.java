package com.prezcription.pharmacyapp.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;

/**
 * Created by anubhav on 17/12/17.
 */

public class CreatePdfAsyncTask extends AsyncTask<ConvertToPdf, Integer, File> {

    private Context mContext;
    private AsyncResult callback;
    ProgressDialog progressBar;
    public static final int NOT_STARTED = -1;
    public static final int STARTED = 0;
    public static final int FINISHED = 1;
    private final int PRESCRIPTION_TYPE = 0;
    private final int INVOICE_TYPE = 1;
    private int pdfType = -1;


    public CreatePdfAsyncTask(Context context, AsyncResult callback) {
        mContext = context;
        this.callback = callback;
    }

    public int getPdfType() {
        return pdfType;
    }

    public void setPdfType(int pdfType) {
        this.pdfType = pdfType;
    }

    @Override

    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if(callback!=null)
            callback.onProgressUpdate(values[0]);
    }

    @Override
    protected File doInBackground(ConvertToPdf... params) {
        File file = null;

        Log.d("log_tag", "inside prescriptionType invoice");
        publishProgress(STARTED);
        try {
            if (isCancelled())
                return null;
            file = params[0].createInvoicePdf();
            publishProgress(FINISHED);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        return file;
    }

    @Override
    protected void onPostExecute(File file) {

        if(mContext!=null)
//            Toast.makeText(mContext,"Pdf generated",Toast.LENGTH_SHORT).show();
        callback.onResult(file, pdfType);
    }

    public interface AsyncResult{
        void onResult(File file, int pdfType);
        void onProgressUpdate(int progress);
    }

}
