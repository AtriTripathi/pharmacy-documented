package com.prezcription.pharmacyapp.adapter;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.List;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.data.dto.PatientData;
import com.prezcription.pharmacyapp.data.dto.RecyclerSearchableData;
import com.prezcription.pharmacyapp.data.dto.RegisteredPatientData;
import com.prezcription.pharmacyapp.interfaces.SearchListener;

/**
 * Created by amit on 31/10/17.
 */

public class SearchDialogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static final int PATIENT = 15;
    public static final int PATIENT_QUEUED = 16;
    public static final int PATIENT_BILLED = 17;
//    public static final int SCOPE_ALL = 0;
//    public static final int SCOPE_QUEUED = 1;
    public static final int SEARCHABLE_PATIENT_ITEM = 2;

    public static final int PATIENT_VIEW_TYPE = 100;
    public static final int REGISTERED_PATIENT_VIEW_TYPE = 200;


    public static final String LOG_TAG = SearchDialogAdapter.class.getSimpleName();
    private Context mContext;


    List<? extends RecyclerSearchableData> dataArrayList = new ArrayList<>();
    private OnItemClickListener mListener;
    int type;

    public SearchDialogAdapter(List<? extends RecyclerSearchableData> dataArrayList, int type, OnItemClickListener listener) {
        this.dataArrayList = dataArrayList;
        this.type = type;
        mListener = listener;
    }

    public interface OnItemClickListener extends SearchListener {
        void onItemEdit(int position);
        void onItemForwarded(int position);
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(LOG_TAG, "type: "+dataArrayList.get(position).getViewType());

        switch (dataArrayList.get(position).getViewType()){

            case RecyclerSearchAdapter.SEARCHABLE_ITEM:
                SearchableItemViewHolder searchableItemViewHolder = (SearchableItemViewHolder) holder;
                searchableItemViewHolder.tvMainText.setText(dataArrayList.get(position).getName());
                break;
            case RecyclerSearchAdapter.STATIC_BAR_TOP:

                StaticBarTopViewHolder staticBarTopViewHolder = (StaticBarTopViewHolder) holder;

                if (dataArrayList.get(position) != null){
                    staticBarTopViewHolder.searchIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.male));
                    staticBarTopViewHolder.searchIcon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primaryBlue));
                    staticBarTopViewHolder.edtSearch.setHint("Search Patient");
                }
                break;
            case PATIENT_VIEW_TYPE:
                SearchablePatientItemViewHolder patientItemViewHolder = (SearchablePatientItemViewHolder) holder;
                if (dataArrayList.get(position) instanceof PatientData) {
                    Log.d(LOG_TAG, "patient phone: "+((PatientData) dataArrayList.get(position)).getPhoneNumber());
                    patientItemViewHolder.tvMainText.setText(dataArrayList.get(position).getName());
                    patientItemViewHolder.tvSubText.setText(((PatientData) dataArrayList.get(position)).getPhoneNumber());
                    patientItemViewHolder.tvUserName.setText(((PatientData) dataArrayList.get(position)).getPatient().getUsername());
                }
                break;
            case REGISTERED_PATIENT_VIEW_TYPE:
                SearchablePatientItemViewHolder registeredItemViewHolder = (SearchablePatientItemViewHolder) holder;
                if (dataArrayList.get(position) instanceof RegisteredPatientData) {
                    registeredItemViewHolder.tvMainText.setText(dataArrayList.get(position).getName());
                    registeredItemViewHolder.tvSubText.setText(((RegisteredPatientData) dataArrayList.get(position)).getPatient().getPhoneNumber());
                    registeredItemViewHolder.tvUserName.setText(((RegisteredPatientData) dataArrayList.get(position)).getPatient().getUsername());
                }
                break;


        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.d(LOG_TAG, "viewType: "+viewType);
        mContext = parent.getContext();

        View view;

        switch (viewType) {
            case RecyclerSearchAdapter.SEARCHABLE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_top, parent, false);
                return new SearchableItemViewHolder(view);
            case RecyclerSearchAdapter.STATIC_BAR_TOP:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.uniform_search_bar, parent, false);
                return new StaticBarTopViewHolder(view);
            case PATIENT_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.patient_item_view_holder,parent,false);
                return new SearchablePatientItemViewHolder(view);
            case REGISTERED_PATIENT_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.patient_item_view_holder,parent,false);
                return new SearchablePatientItemViewHolder(view);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        Log.d(LOG_TAG, "get view Type");
        return dataArrayList.get(position).getViewType();

    }

    @Override
    public int getItemCount() {
        Log.d(LOG_TAG, "list count: "+dataArrayList.size());
        return dataArrayList.size();
    }

    public class SearchableItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView tvHeading, tvMainText;
        private ImageButton unchecked, editButton;

        public SearchableItemViewHolder(View itemView) {
            super(itemView);
            tvHeading = (TextView) itemView.findViewById(R.id.tvHeading);
            tvMainText = (TextView) itemView.findViewById(R.id.tvMainText);
            tvHeading.setVisibility(View.GONE);
            unchecked = (ImageButton) itemView.findViewById(R.id.btn_unchecked);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_search_item);
            unchecked.setOnClickListener(this);
            editButton.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemForwarded(getAdapterPosition());
                }
            });

        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            switch (v.getId()) {
                case R.id.edit_search_item:
                    if (mListener != null && position != -1) {
                        mListener.onItemEdit(position);
                    }
                    break;
                case R.id.btn_unchecked:
                    if (mListener != null && position != -1) {
                        mListener.onItemForwarded(position);
                    }
                    break;
            }
        }
    }

    public class SearchablePatientItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView tvSubText, tvMainText ,tvUserName;
        private ImageButton unchecked, editButton;

        public SearchablePatientItemViewHolder(View itemView) {
            super(itemView);
            tvSubText = (TextView) itemView.findViewById(R.id.tvSubText);
            tvMainText = (TextView) itemView.findViewById(R.id.tvMainText);
            unchecked = (ImageButton) itemView.findViewById(R.id.btn_unchecked);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_search_item);
            tvUserName = (TextView) itemView.findViewById(R.id.tv_patient_userName);
//            unchecked.setOnClickListener(this);
//            editButton.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemForwarded(getAdapterPosition());
                }
            });

        }
        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            switch (v.getId()) {
                case R.id.edit_search_item:
                    if (mListener != null && position != -1) {
                        mListener.onItemEdit(position);
                    }
                    break;
//                case R.id.btn_unchecked:
//                    if (mListener != null && position != -1) {
//                        mListener.onItemForwarded(position);
//                    }
//                    break;
            }
        }

    }




    public class StaticBarTopViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageButton remove;
        private ImageView searchIcon;
        private EditText edtSearch;
        private TextInputLayout til;


        public StaticBarTopViewHolder(View itemView) {
            super(itemView);
            edtSearch = (EditText) itemView.findViewById(R.id.edtSearch);
            remove = (ImageButton) itemView.findViewById(R.id.btn_remove);
            searchIcon = (ImageView) itemView.findViewById(R.id.search_icon);
            til = (TextInputLayout) itemView.findViewById(R.id.text_input_layout);

            edtSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    mListener.doSearch(s.toString());
                }
            });

            remove.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.d(LOG_TAG, "view id " + view.getId());
            switch (view.getId()) {

                case R.id.btn_remove:
                    mListener.clearSearch();
                    break;

            }
        }

    }
}
