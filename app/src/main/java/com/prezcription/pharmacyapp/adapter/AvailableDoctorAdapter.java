package com.prezcription.pharmacyapp.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.data.dto.DocData;

/**
 * Created by amit on 28/03/18.
 */

public class AvailableDoctorAdapter extends RecyclerView.Adapter {

    private final OnDoctorSelectedListener listener;
    private ArrayList<DocData> doctorDatas;
    private final ColorMatrixColorFilter filter;
    private Context mContext;

    private static final int SEARCHABLE_ITEM = 1;

    private static final String LOG_TAG =  AvailableDoctorAdapter.class.getSimpleName();

    public AvailableDoctorAdapter(Context context, OnDoctorSelectedListener listener, ArrayList<DocData> dataList) {
        mContext = context;
        this.listener = listener;
        doctorDatas = dataList;

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        filter = new ColorMatrixColorFilter(matrix);
    }

    public interface OnDoctorSelectedListener{
        void onDoctorSelected(String phoneNumber, int docId);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_doctor, parent, false);
            return new DoctorViewHolder(view);

            }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


                DoctorViewHolder doctorViewHolder = (DoctorViewHolder) holder;
                String name  = mContext.getResources().getString(R.string.dr,doctorDatas.get(position).getName());
                doctorViewHolder.tvName.setText(name);
                doctorViewHolder.tvPhoneNumber.setText(doctorDatas.get(position).getPhone());

                doctorViewHolder.imgMain.setColorFilter(filter);
                doctorViewHolder.tvSpecialization.setText(doctorDatas.get(position).getSpecialization());

        }


    public class DoctorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView tvName, tvPhoneNumber, tvQualification,tvSpecialization;
        protected CircleImageView imgMain;

        public DoctorViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvDoctorName);
            tvName.setTypeface(Utility.getProximaNovaTypeface(mContext,Utility.REGULAR));
            tvPhoneNumber = (TextView) itemView.findViewById(R.id.tvDoctorPhoneNumber);
            tvPhoneNumber.setTypeface(Utility.getProximaNovaTypeface(mContext,Utility.REGULAR));
            tvQualification = (TextView) itemView.findViewById(R.id.tvDoctorQualification);
            tvQualification.setTypeface(Utility.getProximaNovaTypeface(mContext,Utility.REGULAR));
            tvSpecialization = (TextView) itemView.findViewById(R.id.tvDoctorSpecialization);
            tvSpecialization.setTypeface(Utility.getProximaNovaTypeface(mContext,Utility.REGULAR));
            imgMain = (CircleImageView) itemView.findViewById(R.id.imgMain);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            if (listener != null && position != -1) {
                listener.onDoctorSelected(doctorDatas.get(position).getPhone(),doctorDatas.get(position).getWebId());
            }
        }
    }

    @Override
    public int getItemCount() {
        return doctorDatas.size();
    }
}
