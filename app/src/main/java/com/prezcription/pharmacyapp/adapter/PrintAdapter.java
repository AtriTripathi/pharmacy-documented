package com.prezcription.pharmacyapp.adapter;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by anubhav on 17/12/17.
 */

@TargetApi(Build.VERSION_CODES.KITKAT)
public class PrintAdapter extends PrintDocumentAdapter {

    File mFile;

    public PrintAdapter(File file) {
        mFile = file;
    }

    @Override
    public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback){
        InputStream input = null;
        OutputStream output = null;

        try {

            input = new FileInputStream(mFile);
            output = new FileOutputStream(destination.getFileDescriptor());

            byte[] buf = new byte[1024];
            int bytesRead;

            while ((bytesRead = input.read(buf)) > 0) {
                output.write(buf, 0, bytesRead);
            }

            callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

        } catch (FileNotFoundException ee){
            //Catch exception
        } catch (Exception e) {
            //Catch exception
        } finally {
            try {
                input.close();
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras){

        if (cancellationSignal.isCanceled()) {
            callback.onLayoutCancelled();
            return;
        }

//        int pages = computePageCount(newAttributes);

        PrintDocumentInfo pdi = new PrintDocumentInfo.Builder("Name of file").setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).build();

        callback.onLayoutFinished(pdi, true);
    }

//    private int computePageCount(PrintAttributes printAttributes) {
//        int itemsPerPage = 4; // default item count for portrait mode
//
//        PrintAttributes.MediaSize pageSize = printAttributes.getMediaSize();
//        if (!pageSize.isPortrait()) {
//            // Six items per page in landscape orientation
//            itemsPerPage = 6;
//        }
//
//        // Determine number of print items
//        int printItemCount = getPrintItemCount();
//
//        return (int) Math.ceil(printItemCount / itemsPerPage);
//    }

//    Context mContext;
//    PrintedPdfDocument mPdfDocument;
//
//    public PrintAdapter(Context context) {
//
//        mContext = context;
//
//    }
//
//    @Override
//    public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras) {
//        // Create a new PdfDocument with the requested page attributes
//        mPdfDocument = new PrintedPdfDocument(mContext, newAttributes);
//
//        // Respond to cancellation request
//        if (cancellationSignal.isCanceled() ) {
//            callback.onLayoutCancelled();
//            return;
//        }
//
//        // Compute the expected number of printed pages
//        int pages = computePageCount(newAttributes);
//
//        if (pages > 0) {
//            // Return print information to print framework
//            PrintDocumentInfo info = new PrintDocumentInfo
//                    .Builder("print_output.pdf")
//                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
//                    .setPageCount(pages);
//            .build();
//            // Content layout reflow is complete
//            callback.onLayoutFinished(info, true);
//        } else {
//            // Otherwise report an error to the print framework
//            callback.onLayoutFailed("Page count calculation failed.");
//        }
//    }
//
//    @Override
//    public void onWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback) {
//        // Iterate over each page of the document,
//        // check if it's in the output range.
//        for (int i = 0; i < totalPages; i++) {
//            // Check to see if this page is in the output range.
//            if (containsPage(pageRanges, i)) {
//                // If so, add it to writtenPagesArray. writtenPagesArray.size()
//                // is used to compute the next output page index.
//                writtenPagesArray.append(writtenPagesArray.size(), i);
//                PdfDocument.Page page = mPdfDocument.startPage(i);
//
//                // check for cancellation
//                if (cancellationSignal.isCanceled()) {
//                    callback.onWriteCancelled();
//                    mPdfDocument.close();
//                    mPdfDocument = null;
//                    return;
//                }
//
//                // Draw page content for printing
//                drawPage(page);
//
//                // Rendering is complete, so page can be finalized.
//                mPdfDocument.finishPage(page);
//            }
//        }
//
//        // Write PDF document to file
//        try {
//            mPdfDocument.writeTo(new FileOutputStream(
//                    destination.getFileDescriptor()));
//        } catch (IOException e) {
//            callback.onWriteFailed(e.toString());
//            return;
//        } finally {
//            mPdfDocument.close();
//            mPdfDocument = null;
//        }
//        PageRange[] writtenPages = computeWrittenPages();
//        // Signal the print framework the document is complete
//        callback.onWriteFinished(writtenPages);
//
//    }
//

//
//    private void drawPage(PdfDocument.Page page) {
//        Canvas canvas = page.getCanvas();
//
//        // units are in points (1/72 of an inch)
//        int titleBaseLine = 72;
//        int leftMargin = 54;
//
//        Paint paint = new Paint();
//        paint.setColor(Color.BLACK);
//        paint.setTextSize(36);
//        canvas.drawText("Test Title", leftMargin, titleBaseLine, paint);
//
//        paint.setTextSize(11);
//        canvas.drawText("Test paragraph", leftMargin, titleBaseLine + 25, paint);
//
//        paint.setColor(Color.BLUE);
//        canvas.drawRect(100, 100, 172, 172, paint);
//    }
}
