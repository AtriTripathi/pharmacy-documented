package com.prezcription.pharmacyapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.data.dto.BillData;

/**
 * Created by amit on 25/05/17.
 */

public class DetailBillRecyclerAdapter extends RecyclerView.Adapter {

    public DetailBillRecyclerAdapter() {}


    public static final int DOCTOR = 0;
    public static final int MEDICINE = 1;
    public static final int MEDICINE_SELECTED = 4;
    public static final int HEADER = 5;
    public static final int BILL_HEADER = 6;
    public static final int VACCINATION = 7;
    public static final int DIVIDER = 2;
    public static final int CONSULTATION_HEADER = 3;
    public static final int DISPLAY_ONLY = 8;
    public static final int DISCOUNT = 9;
    public static final int DISCOUNT_DISPLAY = 10;
    public static final int DETAILS = 11;
    public static final int PATIENT = 12;
    public static final int BILL_HEADER_AFTER_BILL = 13;
    public static final int BILL_TOTAL_AMOUNT = 14;

    public static final int DISCOUNT_TYPE_MED = 100;
    public static final int DISCOUNT_TYPE_VACC = 200;


    //
//
//    public static final String MEDICINE_KEY = "medicine_key";
//    public static final String VACCINATION_KEY = "vaccination_key";
//    public boolean isMedChecked;
    OnFragmentInteractionListener mListener;
    //    private int lastItem, currentItem;
//    private boolean isLoaded =false;
//
    private Context mContext;
    ArrayList<BillData> mDataList;
    //
    public static final String LOG_TAG =  DetailBillRecyclerAdapter.class.getSimpleName();

    //
    public interface OnFragmentInteractionListener {
        //        void onItemCheck(boolean checked, int position, int quantity);
//        void onQuantityChanged(int position, int quantity, double price);
//        void onVaccineCheck(boolean checked, double price);
        void onConsultationEdit();

        void onMedicineItemClicked();

        void onItemClickForScan(int localId, int itemType, int pos );

        void onBarCodeSet(String itemId,String batchId,String barcode, int localId, int itemType);

//        void onItemAdd(String itemId,String batchId,String barcode, int localId, int itemType);

//        void onTotalAmountChanged();
    }

    //
    public DetailBillRecyclerAdapter(Context context, ArrayList<BillData> mInvoiceDataFormatted,
                                     OnFragmentInteractionListener listener) {
        mContext = context;
        mDataList = mInvoiceDataFormatted;
        mListener = listener;
    }

    //
//
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        Log.d(LOG_TAG, "inside onBindHolder: ");

        int viewType = mDataList.get(position).getViewType();

        switch (viewType) {

            case HEADER:
                ((HeaderViewHolder) holder).mHeader.setText(mDataList.get(position).getName());
                ((HeaderViewHolder) holder).mHeader.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.SEMI_BOLD));
                break;

            case MEDICINE: {
                Log.d(LOG_TAG, "inside onBindHolder: Medicine");

                final MedicineViewHolder medicineViewHolder = ((MedicineViewHolder) holder);

                ((MedicineViewHolder) holder).tvMedicineName.setText(mDataList.get(position).getName());

                float totalAmount = mDataList.get(position).getPrice() * mDataList.get(position).getQuantity();

//                medicineViewHolder.etMedicinePrice.setText(String.valueOf(mDataList.get(position).getPrice()));
                if (mDataList.get(position).getAvailableQty() != 0) {
                    medicineViewHolder.etMedicineQuantity.setText(((int) mDataList.get(position).getQuantity() == 0) ? "1" :
                            String.valueOf((int) mDataList.get(position).getQuantity()));
                }else {
                    medicineViewHolder.etMedicineQuantity.setText("0");
                }
//                medicineViewHolder.etMedicineItemAmount.setText(String.valueOf(totalAmount));

                if (mDataList.get(position).isItemBilled()) {
                    medicineViewHolder.checkBoxBilling.setChecked(true);
                } else {
                    medicineViewHolder.checkBoxBilling.setChecked(false);
                }

                Log.d(LOG_TAG, "medicine item id adapter"+mDataList.get(position).getItemId());
//
//                if (!mDataList.get(position).getItemId().equals("-1")){
//                    medicineViewHolder.itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.blue_btn_bg_color));
//                }else {
//                    medicineViewHolder.itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.light_grey));
//                }

                Log.d(LOG_TAG, "item bar code: "+mDataList.get(position).getBarCode());
                if (mDataList.get(position).getBarCode() != null && !mDataList.get(position).getBarCode().equals("")) {
                    medicineViewHolder.etMedicineBarCode.setText(String.valueOf(mDataList.get(position).getBarCode()));
                }else {
                    medicineViewHolder.etMedicineBarCode.setText("NA");
                }

                if (!mDataList.get(position).isAvailableOnline()){
                    medicineViewHolder.medItemCardView.setCardBackgroundColor(ContextCompat.getColor(mContext,R.color.light_red));
                }else {
                    medicineViewHolder.medItemCardView.setCardBackgroundColor(ContextCompat.getColor(mContext,R.color.transparent));
                }


                break;
            }
            case VACCINATION:

                final VaccineViewHolder vaccineViewHolder = ((VaccineViewHolder) holder);

                ((VaccineViewHolder) holder).tvVaccineName.setText(mDataList.get(position).getName());

                float totalAmount = mDataList.get(position).getPrice() * mDataList.get(position).getQuantity();

//                vaccineViewHolder.etVaccinePrice.setText(String.valueOf(mDataList.get(position).getPrice()));
                vaccineViewHolder.etVaccineQuantity.setText(String.valueOf((int)mDataList.get(position).getQuantity()));
//                vaccineViewHolder.etVaccineItemAmount.setText(String.valueOf(totalAmount));

                if (mDataList.get(position).isItemBilled()) {
                    vaccineViewHolder.checkBoxBilling.setChecked(true);
                } else {
                    vaccineViewHolder.checkBoxBilling.setChecked(false);
                }

                if (mDataList.get(position).getBarCode() != null) {
                    vaccineViewHolder.etVaccineItemBarCode.setText(String.valueOf(mDataList.get(position).getBarCode()));
                }else {
                    vaccineViewHolder.etVaccineItemBarCode.setText("NA");
                }

                if (!mDataList.get(position).isAvailableOnline()){
                    vaccineViewHolder.vacItemCardView.setCardBackgroundColor(ContextCompat.getColor(mContext,R.color.gray));
                }else {
                    vaccineViewHolder.vacItemCardView.setCardBackgroundColor(ContextCompat.getColor(mContext,R.color.transparent));
                }


                break;


            case BILL_HEADER:

                break;
            case BILL_HEADER_AFTER_BILL:

                break;

            case DETAILS:

                break;

            case DISPLAY_ONLY:

                DisplayItemViewHolder displayItemViewHolder = (DisplayItemViewHolder) holder;

                displayItemViewHolder.tvName.setText(mDataList.get(position).getName());
//                displayItemViewHolder.tvPrice.setText(String.valueOf(mDataList.get(position).getPrice()));
                displayItemViewHolder.tvQuantity.setText(String.valueOf(mDataList.get(position).getQuantity()));

                if (mDataList.get(position).getBarCode() != null) {
                    displayItemViewHolder.tvBarcode.setText(String.valueOf(mDataList.get(position).getBarCode()));
                }else {
                    displayItemViewHolder.tvBarcode.setText("NA");
                }

                if (mDataList.get(position).getNetRate() != null) {
                    displayItemViewHolder.tvNetRate.setText(String.valueOf(mDataList.get(position).getNetRate()));
                }else {
                    displayItemViewHolder.tvNetRate.setText("NA");
                }

                if (mDataList.get(position).getMrp() != null) {
                    displayItemViewHolder.tvMrp.setText(String.valueOf(mDataList.get(position).getMrp()));
                }else {
                    displayItemViewHolder.tvMrp.setText("NA");
                }

                if (mDataList.get(position).getDiscountMedicine() != 0) {
                    displayItemViewHolder.tvItemDiscount.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(mDataList.get(position).getDiscountMedicine())));
                }else {
                    displayItemViewHolder.tvItemDiscount.setText("0");
                }

                if (mDataList.get(position).getItemNetAmt() != null) {
                    displayItemViewHolder.tvItemAmount.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(Float.valueOf(mDataList.get(position).getItemNetAmt()))));
                }else {
                    displayItemViewHolder.tvItemAmount.setText("NA");
                }

//                displayItemViewHolder.tvItemAmount.setText(String.valueOf(mDataList.get(position).getTotalPrice()));

                if (mDataList.get(position).getGlobalBillId() != null) {
                    displayItemViewHolder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.transparent));
                } else {
                    displayItemViewHolder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.gray));
                    displayItemViewHolder.tvBarcode.setText("NA");
                }

                break;

            case CONSULTATION_HEADER:

                float fees = mDataList.get(position).getPrice();

                Log.d(LOG_TAG, "consultation fee: " + fees);


                if (fees != -1) {
                    ((ConsultationHeaderViewHolder) holder).mFees.setText(String.valueOf(fees));
                } else {
                    ((ConsultationHeaderViewHolder) holder).mFees.setText("");
                }

                if (mDataList.get(position).isBillCreated()) {
                    ((ConsultationHeaderViewHolder) holder).mFees.setEnabled(false);
                } else {
                    ((ConsultationHeaderViewHolder) holder).mFees.setEnabled(true);
                }

                ((ConsultationHeaderViewHolder) holder).mHeader.setText("Consultation Fee");
                break;

            case BILL_TOTAL_AMOUNT:
                BillItemDetailTotalViewHolder totalViewHolder = (BillItemDetailTotalViewHolder)holder;

                float finalAmount = getConsultationFees()+Float.valueOf(mDataList.get(position).getTotalAmount());

                float finalPayableAmount = Math.round(finalAmount);
                float roundOffAmount = Utility.getValueUptoTwoPlacesOfDecimal(finalAmount-finalPayableAmount);
                String displayString = mContext.getString(R.string.rupee_with_amount,String.valueOf(finalPayableAmount))+"\n"+"("+String.valueOf(roundOffAmount)+")";
                totalViewHolder.itemTotalAmount.setText(displayString);

//            case DISCOUNT:
//                ((DiscountViewHolder)holder).tvDiscount.setText(
//                        (mDataList.get(position).getDiscountType() == DISCOUNT_TYPE_MED) ? "DISCOUNT MEDICINE(%)": "DISCOUNT VACCINE(%)");
//                ((DiscountViewHolder)holder).etDiscount.setText(String.valueOf ((mDataList.get(position).getDiscountType() == DISCOUNT_TYPE_MED) ?
//                        mDataList.get(position).getDiscountMedicine(): mDataList.get(position).getDiscountVaccine()));
//                break;
//            case DISCOUNT_DISPLAY:
//
//                Log.d(LOG_TAG, "med discount: "+mDataList.get(position).getDiscountMedicine());
//                Log.d(LOG_TAG, "vac discount: "+mDataList.get(position).getDiscountVaccine());
//
//                ((DiscountDisplayViewHolder) holder).tvNameDiscountDisplay.setText(
//                        (mDataList.get(position).getDiscountType() == DISCOUNT_TYPE_MED) ? "DISCOUNT MEDICINE(%)": "DISCOUNT VACCINE(%)");
//
//                ((DiscountDisplayViewHolder)holder).tvDiscountDisplay.setText(String.valueOf((mDataList.get(position).getDiscountType() == DISCOUNT_TYPE_MED) ?
//                        mDataList.get(position).getDiscountMedicine(): mDataList.get(position).getDiscountVaccine()));
//                break;

        }


    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.get(position).getViewType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();

        View v;
        switch (viewType) {

            case HEADER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item, parent, false);
                return new HeaderViewHolder(v);

            case CONSULTATION_HEADER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.consultation_header_item, parent, false);
                return new ConsultationHeaderViewHolder(v);

            case BILL_HEADER:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.bill_item_heading, parent, false);
                return new BillHeaderItemViewHolder(v);

            case BILL_HEADER_AFTER_BILL:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.bill_item_heading_after_bill, parent, false);
                return new BillHeaderAfterBillItemViewHolder(v);

            case VACCINATION:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.bill_item_medicine, parent, false);
                return new VaccineViewHolder(v);

            case MEDICINE:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.bill_item_medicine, parent, false);
                return new MedicineViewHolder(v);

            case DISPLAY_ONLY:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_bill_display, parent, false);
                return new DisplayItemViewHolder(v);

            case DETAILS:
                return null;

            case BILL_TOTAL_AMOUNT:
                View totalView = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_bill_item_total_layout,parent,false);
                return new BillItemDetailTotalViewHolder(totalView);

//            case DISCOUNT:
//                v = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.discount_item,parent,false);
//                return new DiscountViewHolder(v);
//
//            case DISCOUNT_DISPLAY:
//                v = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.discount_item_display,parent,false);
//                return new DiscountDisplayViewHolder(v);

//            case DIVIDER:
//
////                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_divider, parent, false);
////                return new DividerViewHolder(v);
        }


        return null;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView mHeader;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mHeader = (TextView) itemView.findViewById(R.id.header);
        }

    }

    private class BillItemDetailTotalViewHolder extends RecyclerView.ViewHolder {

        TextView itemTotalAmount;
        private BillItemDetailTotalViewHolder(View itemView){
            super(itemView);
            itemTotalAmount = (TextView) itemView.findViewById(R.id.tv_total_amount);
        }


    }

    public class ConsultationHeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView mHeader;
        private EditText mFees;
        private ImageButton mEdit;

        public ConsultationHeaderViewHolder(View itemView) {
            super(itemView);
            mHeader = (TextView) itemView.findViewById(R.id.header);
            mFees = (EditText) itemView.findViewById(R.id.et_consultation_total);

            mFees.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    int position = getAdapterPosition();
                    if (!editable.toString().equals("")) {
                        mDataList.get(position).setPrice(Float.parseFloat(editable.toString()));
                        Log.d(LOG_TAG,"consultation fees is "+ editable.toString());
//                        mListener.onTotalAmountChanged();
                    }
                }
            });


//            mEdit = (ImageButton) itemView.findViewById(R.id.ib_edit_consultation);

//            mEdit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mListener.onConsultationEdit();
//                }
//            });

        }

    }

    private float getConsultationFees(){

        for (int i=0;i< mDataList.size();i++){
            if(mDataList.get(i).getViewType()==CONSULTATION_HEADER){
                return mDataList.get(i).getPrice();
            }
        }

        return 0;

    }

    private class MedicineViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMedicineName;
        private EditText etMedicineQuantity;
        private EditText etMedicineBarCode;
        private ImageButton ibAddMedicineItem;
        //        private EditText etMedicinePrice;
//        private EditText etMedicineItemAmount;
        private CheckBox checkBoxBilling;
        private ImageButton ibBarCodeScan;
        private CardView medItemCardView;

        private MedicineViewHolder(View itemView) {
            super(itemView);


            tvMedicineName = (TextView) itemView.findViewById(R.id.tv_medicine_name);
            etMedicineQuantity = (EditText) itemView.findViewById(R.id.et_medicine_quantity);
            etMedicineBarCode = (EditText) itemView.findViewById(R.id.et_medicine_barcode_id);
            ibBarCodeScan = (ImageButton) itemView.findViewById(R.id.ib_barcode_scan);
            medItemCardView = (CardView) itemView.findViewById(R.id.card_view_med_item);
//            ibAddMedicineItem = (ImageButton) itemView.findViewById(R.id.ib_add_billing_item);
//            etMedicinePrice = (EditText) itemView.findViewById(R.id.et_medicine_price);
//            etMedicineItemAmount = (EditText) itemView.findViewById(R.id.et_medicine_item_amount);
            checkBoxBilling = (CheckBox) itemView.findViewById(R.id.checkbox_billing);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (getAdapterPosition() != -1)
//                        if (mDataList.get(getAdapterPosition()).isAvailableOnline())
                                new MaterialDialog.Builder(mContext).
                                        title("Add Item").
                                        content("Add item to bill for different bar code").
                                        onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                dialog.dismiss();
                                            }}).

                                        onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                               try {
                                                   mDataList.add(getAdapterPosition() + 1, mDataList.get(getAdapterPosition()).clone());
                                                   notifyItemChanged(getAdapterPosition() + 1);
                                               }catch (CloneNotSupportedException e){
                                                   e.printStackTrace();
                                               }
                                            }}).
                                        positiveText("Add item").
                                        negativeText("Cancel").show();
                    return true;
                }
            });

//            ibAddMedicineItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    try {
//                        if (getAdapterPosition() != -1)
//                            mDataList.add(getAdapterPosition() +1, mDataList.get(getAdapterPosition()).clone());
//                            notifyItemChanged(getAdapterPosition()+1);
//                    }catch (CloneNotSupportedException e){
//                        e.printStackTrace();
//                    }
//                }
//            });

            ibBarCodeScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != -1) {
                        mListener.onItemClickForScan(mDataList.get(getAdapterPosition()).getLocalId(), DetailBillRecyclerAdapter.MEDICINE, getAdapterPosition());
                    }
//                    ScanFragment.newInstance();
                }
            });

            checkBoxBilling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != -1)
                        mDataList.get(position).setItemBilled(checkBoxBilling.isChecked());
//                    mListener.onTotalAmountChanged();
                }
            });

            etMedicineBarCode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (getAdapterPosition() != -1) {
                        mDataList.get(getAdapterPosition()).setBarCode("");
                    }

                    if (!s.toString().equals("") && s.toString().length() == 9) {

                        String itemId = s.toString().substring(4, 9);
                        String batchId = s.toString().substring(0, 4);

                        String barcode = s.toString();

                        Log.d(LOG_TAG, "bar code: " + barcode);

                        try {
                            int temp = Integer.parseInt(itemId);
                            Log.d(LOG_TAG, "num is " + temp);
                            itemId = String.valueOf(temp);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        Log.d(LOG_TAG, "item id to match: " + itemId);
                        Log.d(LOG_TAG, "item id inside adapter: "+mDataList.get(getAdapterPosition()).getItemId());

                        if ((mDataList.get(getAdapterPosition()).getItemId() != null) &&
                                mDataList.get(getAdapterPosition()).getItemId().equals(itemId)) {

                            mDataList.get(getAdapterPosition()).setBarCode(s.toString());

                            int pos = getAdapterPosition();
                            if (pos != -1) {
                                mDataList.get(getAdapterPosition()).setItemId(itemId);
                                mDataList.get(getAdapterPosition()).setBatchId(batchId);
                                mListener.onBarCodeSet(itemId, batchId, barcode, mDataList.get(getAdapterPosition()).getLocalId(),
                                        DetailBillRecyclerAdapter.MEDICINE);
                            }
                        }else {
                            Toast.makeText(mContext,"Item id mismatch, please scan correct item for billing",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                }

            });


//            etMedicineItemAmount.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable editable) {
//                    int position = getAdapterPosition();
//                    if (!editable.toString().equals("")) {
//                        mDataList.get(position).setTotalPrice(Float.parseFloat(editable.toString()));
//                        if (mDataList.get(position).getQuantity() != 0) {
////                            Log.d(LOG_TAG, "price should be " + (mDataList.get(position).getTotalPrice() / mDataList.get(position).getQuantity()));
//                            DecimalFormat df = new DecimalFormat("0.0");
//                            etMedicinePrice.setText(String.valueOf(df.format(mDataList.get(position).getTotalPrice() / mDataList.get(position).getQuantity())));
//                        }
////                        Log.d(LOG_TAG,"inside item amount text changed listener");
//                        mListener.onTotalAmountChanged();
//                    }
//                }
//            });
//
//            etMedicinePrice.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                }
//
//                @Override
//                public void afterTextChanged(Editable editable) {
//                    int position = getAdapterPosition();
//                    if (!editable.toString().equals("")) {
//                        mDataList.get(position).setPrice(Float.parseFloat(editable.toString()));
//                        float totalAmount = mDataList.get(position).getPrice() * mDataList.get(position).getQuantity();
//                        mDataList.get(position).setTotalPrice(totalAmount);
////                        Log.d(LOG_TAG,"total amount "+ totalAmount+ " --- "+Float.valueOf(etMedicineItemAmount.getText().toString()));
//                        if (totalAmount != Float.valueOf(etMedicineItemAmount.getText().toString())) {
//                            DecimalFormat df = new DecimalFormat("0.0");
//                            etMedicineItemAmount.setText(String.valueOf(df.format(totalAmount)));
//                        }
//                        mListener.onTotalAmountChanged();
//                    }
//                }
//            });

            etMedicineQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    int position = getAdapterPosition();
                    if (!editable.toString().equals("")) {
                        if (Float.parseFloat(editable.toString()) <= mDataList.get(position).getAvailableQty())
                                mDataList.get(position).setQuantity(Float.parseFloat(editable.toString()));
                        else {
                            Log.d(LOG_TAG, "avail qty: "+mDataList.get(position).getAvailableQty());
                            Toast.makeText(mContext, "selected quantity is not available", Toast.LENGTH_SHORT).show();
                            mDataList.get(position).setQuantity(mDataList.get(position).getAvailableQty());
                            notifyItemChanged(position);
                        }

                    }
                }
            });


        }
    }


    private class BillHeaderItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvQuantity;
        private TextView tvPrice;
        private TextView tvItemAmount;

        private BillHeaderItemViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_item_name);
//            tvPrice = (TextView) itemView.findViewById(R.id.tv_item_price);
            tvQuantity = (TextView) itemView.findViewById(R.id.tv_item_quantity);
//            tvItemAmount = (TextView) itemView.findViewById(R.id.tv_total_price);
        }
    }

    private class BillHeaderAfterBillItemViewHolder extends RecyclerView.ViewHolder {


        private BillHeaderAfterBillItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class DisplayItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvQuantity;
        private TextView tvItemAmount;
        private TextView tvNetRate;
        private TextView tvMrp;
        private TextView tvItemDiscount;
        private CardView cardView;
        private TextView tvBarcode;

        private DisplayItemViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_item_name);
            tvQuantity = (TextView) itemView.findViewById(R.id.tv_item_quantity);
            tvItemAmount = (TextView) itemView.findViewById(R.id.tv_total_price);
            tvNetRate = (TextView) itemView.findViewById(R.id.tv_item_net_rate);
            tvMrp = (TextView) itemView.findViewById(R.id.tv_item_price);
            tvItemDiscount = (TextView) itemView.findViewById(R.id.tv_item_discount);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            tvBarcode = (TextView) itemView.findViewById(R.id.tv_item_id);
        }
    }

    private class VaccineViewHolder extends RecyclerView.ViewHolder {

        private TextView tvVaccineName;
        private EditText etVaccineQuantity;
        //        private EditText etVaccinePrice;
//        private EditText etVaccineItemAmount;
        private EditText etVaccineItemBarCode;
        private CheckBox checkBoxBilling;
        private  ImageButton ibBarCodeScan;
        private CardView vacItemCardView;


        private VaccineViewHolder(View itemView) {
            super(itemView);
            tvVaccineName = (TextView) itemView.findViewById(R.id.tv_medicine_name);
            etVaccineQuantity = (EditText) itemView.findViewById(R.id.et_medicine_quantity);
//            etVaccinePrice = (EditText) itemView.findViewById(R.id.et_medicine_price);
//            etVaccineItemAmount = (EditText) itemView.findViewById(R.id.et_medicine_item_amount);
            checkBoxBilling = (CheckBox) itemView.findViewById(R.id.checkbox_billing);
            etVaccineItemBarCode = (EditText) itemView.findViewById(R.id.et_medicine_barcode_id);
            ibBarCodeScan = (ImageButton) itemView.findViewById(R.id.ib_barcode_scan);
            vacItemCardView = (CardView) itemView.findViewById(R.id.card_view_med_item);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (getAdapterPosition() != -1)
                        if (mDataList.get(getAdapterPosition()).isAvailableOnline())
                            new MaterialDialog.Builder(mContext).
                                    title("Add Item").
                                    content("Add item to bill for different bar code").
                                    onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();
                                        }}).

                                    onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            try {
                                                mDataList.add(getAdapterPosition() + 1, mDataList.get(getAdapterPosition()).clone());
                                                notifyItemChanged(getAdapterPosition() + 1);
                                            }catch (CloneNotSupportedException e){
                                                e.printStackTrace();
                                            }
                                        }}).
                                    positiveText("Add item").
                                    negativeText("Cancel").
                                    show();
                    return true;
                }
            });

            ibBarCodeScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != -1) {
                        mListener.onItemClickForScan(mDataList.get(getAdapterPosition()).getLocalId(),DetailBillRecyclerAdapter.VACCINATION, getAdapterPosition());
                    }
//                    ScanFragment.newInstance();
                }
            });

            etVaccineItemBarCode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().equals("") && s.toString().length() == 9) {

                        String itemId = s.toString().substring(4, 9);
                        String batchId = s.toString().substring(0, 4);

                        String barcode = s.toString();

                        Log.d(LOG_TAG, "bar code: " + barcode);

                        try {
                            int temp = Integer.parseInt(itemId);
                            Log.d(LOG_TAG, "num is " + temp);
                            itemId = String.valueOf(temp);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        Log.d(LOG_TAG, "item id to match:vaccine " + itemId);
                        Log.d(LOG_TAG, "item id inside adapter: "+mDataList.get(getAdapterPosition()).getItemId());

                        if ((mDataList.get(getAdapterPosition()).getItemId() != null) &&
                                mDataList.get(getAdapterPosition()).getItemId().equals(itemId)) {

                            mDataList.get(getAdapterPosition()).setBarCode(s.toString());

                            int pos = getAdapterPosition();
                            if (pos != -1) {
                                mDataList.get(getAdapterPosition()).setItemId(itemId);
                                mDataList.get(getAdapterPosition()).setBatchId(batchId);
                                mListener.onBarCodeSet(itemId, batchId, barcode, mDataList.get(getAdapterPosition()).getLocalId(),
                                        DetailBillRecyclerAdapter.VACCINATION);
                            }
                        }else {
                            Toast.makeText(mContext,"Item id mismatch, please scan correct item for billing",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            checkBoxBilling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    mDataList.get(position).setItemBilled(checkBoxBilling.isChecked());
//                    mListener.onTotalAmountChanged();
                }
            });

//            etVaccineItemAmount.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable editable) {
//                    int position = getAdapterPosition();
//                    if (!editable.toString().equals("")) {
//                        mDataList.get(position).setTotalPrice(Float.parseFloat(editable.toString()));
//                        if (mDataList.get(position).getQuantity() != 0) {
//                        etVaccinePrice.setText(String.valueOf(mDataList.get(position).getTotalPrice() / mDataList.get(position).getQuantity()));
//                        }
//                        Log.d(LOG_TAG,"inside item amount text changed listener");
//                        mListener.onTotalAmountChanged();
//                    }
//
//                }
//            });
//
//            etVaccinePrice.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                }
//
//                @Override
//                public void afterTextChanged(Editable editable) {
//                    int position = getAdapterPosition();
//                    if (!editable.toString().equals("")) {
//                        mDataList.get(position).setPrice(Float.parseFloat(editable.toString()));
//                        float totalAmount = mDataList.get(position).getPrice() * mDataList.get(position).getQuantity();
//                        mDataList.get(position).setTotalPrice(totalAmount);
//
//                        if (totalAmount != Float.valueOf(etVaccineItemAmount.getText().toString())) {
//                            DecimalFormat df = new DecimalFormat("0.0");
//                            etVaccineItemAmount.setText(df.format(totalAmount));
//                        }
//
//                        mListener.onTotalAmountChanged();
//                    }
//                }
//            });

            etVaccineQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    int position = getAdapterPosition();
                    if (!editable.toString().equals("")) {
                        mDataList.get(position).setQuantity(Float.parseFloat(editable.toString()));
//                        float totalAmount = mDataList.get(position).getPrice() * mDataList.get(position).getQuantity();
//                        mDataList.get(position).setTotalPrice(totalAmount);
//                        DecimalFormat df = new DecimalFormat("0.0");
//                        etVaccineItemAmount.setText(df.format(totalAmount));
//                        mListener.onTotalAmountChanged();
                    }
                }
            });

        }
    }

    private class DividerViewHolder extends RecyclerView.ViewHolder {

        public DividerViewHolder(View itemView) {
            super(itemView);
        }
    }

//    private class DiscountViewHolder extends RecyclerView.ViewHolder {
//
//        EditText etDiscount;
//        CheckBox checkBox;
//        TextView tvDiscount;
//        private DiscountViewHolder(View itemView) {
//            super(itemView);
//            etDiscount = (EditText) itemView.findViewById(R.id.et_discount);
//
//            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox_discount);
//
//            tvDiscount = (TextView) itemView.findViewById(R.id.tv_discount);
//
//            checkBox.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    int position = getAdapterPosition();
//                    mDataList.get(position).setItemBilled(checkBox.isChecked());
//                    mListener.onTotalAmountChanged();
//                }
//            });
//
//            etDiscount.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                    int position = getAdapterPosition();
//                    if (!s.toString().equals("")) {
//                        if (mDataList.get(position).getDiscountType() == DetailBillRecyclerAdapter.DISCOUNT_TYPE_MED) {
//                            Log.d(LOG_TAG, "medicine discount"+ Float.valueOf(s.toString()));
//                            mDataList.get(position).setDiscountMedicine(Float.valueOf(s.toString()));
//                        }else {
//                            Log.d(LOG_TAG, "vaccine discount"+Float.valueOf(s.toString()));
//                            mDataList.get(position).setDiscountVaccine(Float.valueOf(s.toString()));
//                        }
//                        mListener.onTotalAmountChanged();
//
//                    }
//
//                }
//            });
//
//
//
//
//        }
//
//    }

//    private class DiscountDisplayViewHolder extends RecyclerView.ViewHolder{
//        TextView tvDiscountDisplay;
//        TextView tvNameDiscountDisplay;
//        private DiscountDisplayViewHolder(View itemView){
//            super(itemView);
//            tvDiscountDisplay = (TextView) itemView.findViewById(R.id.tv_discount_display);
//            tvNameDiscountDisplay = (TextView) itemView.findViewById(R.id.tv_name_discount_display);
//        }
//    }
}