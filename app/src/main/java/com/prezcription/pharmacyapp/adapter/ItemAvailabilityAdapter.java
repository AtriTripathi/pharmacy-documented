package com.prezcription.pharmacyapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.ArrayList;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.data.dto.BillItem;

/**
 * Created by amit on 12/02/18.
 */

public class ItemAvailabilityAdapter extends RecyclerView.Adapter {

    public static final int HEADER = 1;
    public static final int DETAIL = 2;

    public static final String LOG_TAG = ItemAvailabilityAdapter.class.getSimpleName();

    private ArrayList<BillItem> billItemArrayList = new ArrayList<>();

    public ItemAvailabilityAdapter(ArrayList<BillItem> billItems){
        this.billItemArrayList = billItems;
    }

    @Override
    public int getItemCount() {
        Log.d(LOG_TAG, "item count:avail "+ billItemArrayList.size());
        return billItemArrayList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(LOG_TAG, "viewType:onCreate "+viewType);
        switch (viewType){
            case HEADER:
                View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_availablity_headline,parent,false);
                return new ItemAvailabilityHeaderViewHolder(headerView);
            case DETAIL:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_availability_layout,parent,false);
                return new ItemAvailabilityViewHolder(view);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(LOG_TAG, "viewType:onBind "+billItemArrayList.get(position).getViewType() + "pos: "+position);

        switch (billItemArrayList.get(position).getViewType()){
            case HEADER:
                break;
            case DETAIL:
                ItemAvailabilityViewHolder itemHolder = (ItemAvailabilityViewHolder) holder;
                Log.d(LOG_TAG, "inside detail viewType");
                Log.d(LOG_TAG, "inside detail viewType name"+billItemArrayList.get(position).getName()+
                        "avail Qty:"+billItemArrayList.get(position).getAvailableCount()+"req qty"+billItemArrayList.get(position).getItemCount());

                itemHolder.itemAvailQty.setText(String.valueOf(billItemArrayList.get(position).getAvailableCount()));
                itemHolder.itemName.setText(billItemArrayList.get(position).getName());
                itemHolder.itemReqQty.setText(String.valueOf(billItemArrayList.get(position).getItemCount()));

                break;
        }
    }

    private class ItemAvailabilityViewHolder extends RecyclerView.ViewHolder {

        TextView itemName,itemAvailQty,itemReqQty;

        private ItemAvailabilityViewHolder(View itemView){
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            itemAvailQty = (TextView) itemView.findViewById(R.id.tv_available_qty);
            itemReqQty = (TextView) itemView.findViewById(R.id.tv_item_required_qty);
        }

    }

    private class ItemAvailabilityHeaderViewHolder extends RecyclerView.ViewHolder {

        private ItemAvailabilityHeaderViewHolder(View itemView){
            super(itemView);
        }


    }

    @Override
    public int getItemViewType(int position) {
        return billItemArrayList.get(position).getViewType();
    }
}
