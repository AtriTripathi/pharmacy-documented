package com.prezcription.pharmacyapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;



import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.data.dto.RegisteredPatientData;

/**
 * Created by amit on 02/12/17.
 */

public class BilledPatientAdapter extends RecyclerView.Adapter {

    ArrayList<RegisteredPatientData> registeredPatientDataArrayList;
    private Context mContext;
    OnItemClickListener mListener;

    public BilledPatientAdapter(ArrayList<RegisteredPatientData> registeredPatientDataArrayList, OnItemClickListener listener){
        this.registeredPatientDataArrayList = registeredPatientDataArrayList;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view;
        if (viewType == RecyclerSearchAdapter.SEARCHABLE_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_bill, parent, false);
            return new PatientViewHolder(view);
        }

        return null;

    }

    public interface OnItemClickListener{
        void onItemCLick(int position);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (registeredPatientDataArrayList.get(position).getViewType()) {
            case RecyclerSearchAdapter.SEARCHABLE_ITEM:
                PatientViewHolder patientViewHolder = (PatientViewHolder) holder;
                patientViewHolder.tvName.setText(registeredPatientDataArrayList.get(position).getPatient().getName());
//                patientViewHolder.tvName.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.SEMI_BOLD));
                patientViewHolder.tvPhoneNumber.setText(registeredPatientDataArrayList.get(position).getPatient().getPhoneNumber());
//                patientViewHolder.tvPhoneNumber.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.SEMI_BOLD));
//                patientViewHolder.tvDoctorName.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.SEMI_BOLD));
                patientViewHolder.tvDoctorName.setText(registeredPatientDataArrayList.get(position).getDoctorName());
                patientViewHolder.ibSync.setVisibility(View.GONE);
                patientViewHolder.tvWaitingTime.setText(Utility.formatDateToSimpleFormat(registeredPatientDataArrayList.get(position).getBillUpdateDate()));
//                patientViewHolder.tvWaitingTime.setTextColor(ContextCompat.getColor(mContext,R.color.));
                patientViewHolder.tvWaitingTime.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.ITALIC));
                patientViewHolder.mCardView.setCardBackgroundColor(
                        registeredPatientDataArrayList.get(position).isSelected()? ContextCompat.getColor(mContext,R.color.white):
                        ContextCompat.getColor(mContext,R.color.primaryBlue));

//                patientViewHolder.ivSyncData.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        return registeredPatientDataArrayList.size();
    }

    public class PatientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView tvName, tvPhoneNumber, tvDoctorName, tvWaitingTime;
//        protected ImageView ivSyncData;
        protected CircleImageView imgMain;
        protected ImageButton ibSync;
        protected CardView mCardView;

        public PatientViewHolder(View itemView) {
            super(itemView);
            tvWaitingTime = (TextView) itemView.findViewById(R.id.waiting_time);
            tvName = (TextView) itemView.findViewById(R.id.tvPatientName);
            tvName.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.REGULAR));
            tvPhoneNumber = (TextView) itemView.findViewById(R.id.tvPatientPhoneNumber);
            tvPhoneNumber.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.REGULAR));
            tvDoctorName = (TextView) itemView.findViewById(R.id.tvDoctorName);
            tvDoctorName.setTypeface(Utility.getProximaNovaTypeface(mContext, Utility.REGULAR));
            imgMain = (CircleImageView) itemView.findViewById(R.id.imgMain);
            ibSync = (ImageButton) itemView.findViewById(R.id.sync_data);
            mCardView = (CardView) itemView.findViewById(R.id.cardview_bill);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            if (mListener != null && position != -1) {
                mListener.onItemCLick(getAdapterPosition());
            }
        }
    }
}
