package com.prezcription.pharmacyapp.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;


import java.util.ArrayList;
import java.util.Collections;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.data.dto.RecyclerSearchableData;

/**
 * Created by anubhav on 19/09/16.
 */

public class RecyclerSearchAdapter extends RecyclerView.Adapter {

    public static final int HEADER_ITEM = -1;
    public static final int SEARCHABLE_ITEM = 0;
    public static final int SELECTED_ITEM = 1;
    public static final int STATIC_BAR_TOP = 111;

    private final OnItemClickListener listener;
    private final ColorMatrixColorFilter filter;

    public ArrayList<? extends RecyclerSearchableData> mDataList;

    Context context;



    public RecyclerSearchAdapter(OnItemClickListener listener, ArrayList<? extends RecyclerSearchableData> dataList){
        this.listener = listener;
        mDataList = dataList;

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        filter = new ColorMatrixColorFilter(matrix);

    }

//    @Override
//    public boolean onItemMove(int fromPosition, int toPosition) {
//        Collections.swap(mDataList, fromPosition, toPosition);
//        notifyItemMoved(fromPosition, toPosition);
//        return true;
//    }
//
//    @Override
//    public void onItemDismiss(final int position) {
//
//        if(mDataList.get(position).getViewType()==SEARCHABLE_ITEM) {
//            if(context!=null) {
//                if (this instanceof SearchPatientAdapter) {
//
//                    RegisteredPatientData registeredPatientData = (RegisteredPatientData) mDataList.get(position);
//
//                    if (registeredPatientData.isSyncFlag()){ Toast.makeText(context,
//                            "Synced Item cannot be deleted", Toast.LENGTH_SHORT).show();
//                        notifyItemChanged(position);}
//                    else {
//
//
//                        new MaterialDialog.Builder(context)
//                                .title("Are you sure?")
//                                .content("Won't be able to recover this!")
//                                .positiveText("Yes,delete it!")
//                                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                        if (deleteItem(position)) {
//                                            mDataList.remove(position);
//                                            notifyItemRemoved(position);
//                                        }
//                                        dialog.dismiss();
//                                    }
//                                })
//                                .negativeText("No")
//                                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                    @Override
//                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                        dialog.dismiss();
//                                        notifyItemChanged(position);
//                                    }
//                                })
//                                .show();
//
//                    }
//                }
//                else {
//
//                    new MaterialDialog.Builder(context)
//                            .title("Are you sure?")
//                            .content("Won't be able to recover this!")
//                            .positiveText("Yes,delete it!")
//                            .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                    if (deleteItem(position)) {
//                                        mDataList.remove(position);
//                                        notifyItemRemoved(position);
//                                    }
//                                    dialog.dismiss();
//                                }
//                            })
//                            .negativeText("No")
//                            .onNegative(new MaterialDialog.SingleButtonCallback() {
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                    dialog.dismiss();
//                                    notifyItemChanged(position);
//                                }
//                            })
//                            .show();
//                }
//            }
//                else {
//                notifyItemChanged(position);
//            }
//        }else {
//            if(context!=null){
//                Toast.makeText(context,"Selected and header items cannot be deleted", Toast.LENGTH_SHORT).show();
//                notifyItemChanged(position);
//            }
//        }
//
//    }
//
//
//    private boolean deleteItem(int position){
//
//        if(this instanceof SearchAllergiesAdapter){
//            return MyContentProvider.deleteAllergySearch(context.getContentResolver(),mDataList.get(position).getName());
//        }else if(this instanceof SearchSelfHistoryAdapter){
//            return MyContentProvider.deleteSelfHistorySearch(context.getContentResolver(),mDataList.get(position).getName());
//        }else if(this instanceof SearchPatientAdapter){
//
//            RegisteredPatientData registeredPatientData = (RegisteredPatientData) mDataList.get(position);
//           return MyContentProvider.deleteRegisteredPatient(context.getContentResolver(), (int)registeredPatientData.getRegId(),
//                    registeredPatientData.getPatient().getProfileId());
//        }
////        else if(this instanceof SearchPatientAdapter){
////            if(mDataList.get(position) instanceof PatientData) {
////                return MyContentProvider.deletePatient(context.getContentResolver(), ((PatientData) mDataList.get(position)).getProfileId());
////            }else {
////                return false;
////            }
////        }
//        else {
//            return false;
//        }
//
//    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);

        void onItemRemove(int position);

        void save();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view;
        switch (viewType) {

            case HEADER_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
                return new HeaderViewHolder(view);

            case SELECTED_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_singleline_selected, parent, false);
                return new SingleLineViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (mDataList.get(position).getViewType()) {

            case HEADER_ITEM:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.tvHeader.setText(mDataList.get(position).getName());
                break;


            case SELECTED_ITEM:
                SingleLineViewHolder singleLineViewHolder = (SingleLineViewHolder) holder;
                singleLineViewHolder.tvMainText.setText(mDataList.get(position).getName());
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {

        return mDataList.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public class SingleLineViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView tvMainText;
        protected ImageButton imageButton;

        public SingleLineViewHolder(View itemView) {
            super(itemView);
            tvMainText = (TextView) itemView.findViewById(R.id.tvMainText);
            imageButton = (ImageButton) itemView.findViewById(R.id.btRemove);
            imageButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            if (listener != null && position != -1) {
                listener.onItemRemove(position);
            }

        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        protected TextView tvHeader;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            tvHeader = (TextView) itemView.findViewById(R.id.tvHeading);
        }
    }


}
