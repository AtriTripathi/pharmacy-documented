package com.prezcription.pharmacyapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.data.dto.BillItem;

public class BillItemDetailAdapter extends RecyclerView.Adapter {
    public static final int HEADER = 1;
    public static final int DETAIL = 2;
    public static final int TOTAL = 3;
    public static final int CONSULTATION = 4;
    public static final int DIFFERENT_BAR_CODE = 5;
    private onBillDetailItemClickListener mListener;

    public static final String LOG_TAG = BillItemDetailAdapter.class.getSimpleName();
    private Context mContext;

    ArrayList<BillItem> billDetailsItems;

    public interface onBillDetailItemClickListener{
        void onDifferentBatchItemAdd(BillItem item);
    }

    public BillItemDetailAdapter(ArrayList<BillItem> billDetailsItems, onBillDetailItemClickListener listener) {
        this.billDetailsItems = billDetailsItems;
        this.mListener = listener;
        Log.d(LOG_TAG, "size of list is " + billDetailsItems.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        Log.d(LOG_TAG, "viewType:onCreate " + viewType);
        switch (viewType) {
            case HEADER:
                View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_item_detail_header, parent, false);
                return new BillItemDetailHeaderViewHolder(headerView);
            case DETAIL: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_item_detail_layout, parent, false);
                return new BillItemDetailViewHolder(view);
            }
            case CONSULTATION:
                View consultationView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_item_consultation, parent, false);
                return new ConsultationItemViewHolder(consultationView);
            case TOTAL:
                View totalView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_item_total, parent, false);
                return new BillItemDetailTotalViewHolder(totalView);
            case DIFFERENT_BAR_CODE:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_item_different_bar_code, parent, false);
                return new DifferentBatchItemViewHolder(view);


        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (billDetailsItems.get(position).getViewType()) {
            case HEADER:
                break;
            case DETAIL: {
                BillItemDetailViewHolder itemHolder = (BillItemDetailViewHolder) holder;
                Log.d(LOG_TAG, "inside detail viewType");

                itemHolder.itemName.setText(billDetailsItems.get(position).getName());
                itemHolder.itemBatch.setText(String.valueOf(billDetailsItems.get(position).getBatchId()));
                itemHolder.itemNetRate.setText(String.valueOf(billDetailsItems.get(position).getNetRate()));
                itemHolder.itemQty.setText(String.valueOf(billDetailsItems.get(position).getAvailableCount()));
                itemHolder.itemDiscount.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(billDetailsItems.get(position).getDiscount())));
                itemHolder.itemAmount.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(billDetailsItems.get(position).getAmount())));
//                itemHolder.itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.gray));


                break;
            }

            case CONSULTATION:
                ConsultationItemViewHolder consultationItemViewHolder = (ConsultationItemViewHolder) holder;
                consultationItemViewHolder.consultationAmt.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(billDetailsItems.get(position).getAmount())));
                break;
            case TOTAL:
                BillItemDetailTotalViewHolder totalViewHolder = (BillItemDetailTotalViewHolder) holder;
                totalViewHolder.itemTotalAmount.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(billDetailsItems.get(position).getAmount())));
                break;
            case DIFFERENT_BAR_CODE:
                DifferentBatchItemViewHolder itemHolder = (DifferentBatchItemViewHolder) holder;
                Log.d(LOG_TAG, "inside detail viewType");

//                itemHolder.itemHeadingName.setText("DIFFERENT BAR CODE ITEM AVAILABLE");
                itemHolder.itemName.setText(billDetailsItems.get(position).getName());
                itemHolder.itemBacth.setText(String.valueOf(billDetailsItems.get(position).getBatchId()));
                itemHolder.itemNetRate.setText(String.valueOf(billDetailsItems.get(position).getNetRate()));
                itemHolder.itemQty.setText(String.valueOf(billDetailsItems.get(position).getAvailableCount()));
                itemHolder.itemDiscount.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(billDetailsItems.get(position).getDiscount())));
                itemHolder.itemAmount.setText(String.valueOf(Utility.getValueUptoTwoPlacesOfDecimal(billDetailsItems.get(position).getAmount())));
//                itemHolder.itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.gray));


                break;
        }
    }

    private class ConsultationItemViewHolder extends RecyclerView.ViewHolder {
        TextView consultationAmt;

        private ConsultationItemViewHolder(View itemView) {
            super(itemView);
            consultationAmt = (TextView) itemView.findViewById(R.id.tv_consul_amt);
        }
    }

    private class DifferentBatchItemViewHolder extends RecyclerView.ViewHolder{
        TextView itemHeadingName,itemName, itemAdd, itemQty, itemBacth, itemNetRate, itemAmount, itemDiscount;;

        private DifferentBatchItemViewHolder(View itemView){
            super(itemView);
//            itemHeadingName = (TextView) itemView.findViewById(R.id.tv_different_item);
            itemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            itemAdd = (TextView) itemView.findViewById(R.id.tv_add);
            itemBacth = (TextView) itemView.findViewById(R.id.tv_item_batch);
            itemNetRate = (TextView) itemView.findViewById(R.id.tv_item_net_rate);
            itemQty = (TextView) itemView.findViewById(R.id.tv_item_qty);
            itemAmount = (TextView) itemView.findViewById(R.id.tv_amount);
            itemDiscount = (TextView) itemView.findViewById(R.id.tv_item_discount);

            itemAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != -1) {
                        BillItem item = billDetailsItems.get(getAdapterPosition());
                        mListener.onDifferentBatchItemAdd(item);
                    }
                }
            });

        }

    }

        private class BillItemDetailViewHolder extends RecyclerView.ViewHolder {

            TextView itemName, itemQty, itemBatch, itemNetRate, itemAmount, itemDiscount;

            private BillItemDetailViewHolder(View itemView) {
                super(itemView);
                itemName = (TextView) itemView.findViewById(R.id.tv_item_name);
                itemBatch = (TextView) itemView.findViewById(R.id.tv_item_mrp);
                itemNetRate = (TextView) itemView.findViewById(R.id.tv_item_net_rate);
                itemQty = (TextView) itemView.findViewById(R.id.tv_item_qty);
                itemAmount = (TextView) itemView.findViewById(R.id.tv_amount);
                itemDiscount = (TextView) itemView.findViewById(R.id.tv_item_discount);
            }

        }

        private class BillItemDetailHeaderViewHolder extends RecyclerView.ViewHolder {

            private BillItemDetailHeaderViewHolder(View itemView) {
                super(itemView);
            }


        }

        private class BillItemDetailTotalViewHolder extends RecyclerView.ViewHolder {

            TextView itemTotalAmount;

            private BillItemDetailTotalViewHolder(View itemView) {
                super(itemView);
                itemTotalAmount = (TextView) itemView.findViewById(R.id.tv_total_amount);
            }


        }

        @Override
        public int getItemViewType(int position) {
            return billDetailsItems.get(position).getViewType();
        }

        @Override
        public int getItemCount() {
            return billDetailsItems.size();
        }
    }
