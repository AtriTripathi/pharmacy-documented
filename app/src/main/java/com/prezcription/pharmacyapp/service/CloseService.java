package com.prezcription.pharmacyapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;



import com.prezcription.pharmacyapp.dnssd.DNSSDHelper;

public class CloseService extends Service {

    public static final String LOG_TAG = CloseService.class.getSimpleName();

    public CloseService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
//        throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(LOG_TAG,"oncreate close service");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(LOG_TAG,"close service starting");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
//        super.onTaskRemoved(rootIntent);
        DNSSDHelper.getInstance().onReset();
        stopSelf();
        Log.d(LOG_TAG,"on task removed called");
    }
}
