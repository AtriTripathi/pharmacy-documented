package com.prezcription.pharmacyapp;

import static com.prezcription.pharmacyapp.data.PharmacyContract.PATH_DOCTOR;

public class ApiConfig {

    public static final String API_BILL_URL = "http://bill.prezcription.com";
    public static final String API_BASE_URL ="http://api.prezcription.com";
    public static final String PATH_HOSPITAL = "hospital";
    public static final String PATH_REGISTER = "register";
    public static final String PATH_GENERATE_BILLS = "patient_bills/generateBill";
    public static final String PATH_CHECK_BILLS = "patient_bills/checkItems";
    public static final String BACK_SLASH = "/";
    public static final String PATH_LOGIN = "login";
    public static final String PATH_RESET_PASSWORD = "reset_password";
    public static final String PATH_GET_INVENTORY_DATA = "inventories";
    public static final String PATH_LOGOUT = "logout";
    public static final String BASE_DOCTOR_INVENTORY_URL = API_BILL_URL+BACK_SLASH+PATH_GET_INVENTORY_DATA;
    public static final String PATH_QUERY_INVENTORY = "query";

    public static class DoctorApi {
        public static final String DOCTOR_INVENTORY_CHECK = BASE_DOCTOR_INVENTORY_URL + BACK_SLASH + PATH_QUERY_INVENTORY;

    }


    public static final String GET_BILL_DETAIL = API_BILL_URL+BACK_SLASH+PATH_GENERATE_BILLS;
    public static final String CHECK_ITEM_AVAILABILITY = API_BILL_URL + BACK_SLASH +PATH_CHECK_BILLS;

    public static final String BASE_HOSPITAL_URL = API_BASE_URL+BACK_SLASH+PATH_HOSPITAL+BACK_SLASH;

    public static final String REGISTER = BASE_HOSPITAL_URL +PATH_REGISTER;

    public static final String LOGIN = BASE_HOSPITAL_URL +PATH_LOGIN;

    public static final String RESET_PASSWORD = BASE_HOSPITAL_URL +PATH_RESET_PASSWORD;

    public static final String LOGOUT = BASE_HOSPITAL_URL +PATH_LOGOUT;
}
