package com.prezcription.pharmacyapp.activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.prezcription.pharmacyapp.ApiConfig;
import com.prezcription.pharmacyapp.BillCreationResponse;
import com.prezcription.pharmacyapp.ItemAvailabilityResponse;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.adapter.BillItemDetailAdapter;
import com.prezcription.pharmacyapp.adapter.DetailBillRecyclerAdapter;
import com.prezcription.pharmacyapp.adapter.ItemAvailabilityAdapter;
import com.prezcription.pharmacyapp.async.ConvertToPdf;
import com.prezcription.pharmacyapp.async.CreatePdfAsyncTask;
import com.prezcription.pharmacyapp.custom.VolleySingleton;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.dto.BillData;
import com.prezcription.pharmacyapp.data.dto.BillItem;
import com.prezcription.pharmacyapp.data.dto.DocData;
import com.prezcription.pharmacyapp.data.dto.DoctorData;
import com.prezcription.pharmacyapp.data.dto.Patient;
import com.prezcription.pharmacyapp.data.gson.DoctorInfo;
import com.prezcription.pharmacyapp.data.gson.PatientInfo;
import com.prezcription.pharmacyapp.dnssd.DNSSDHelper;
import com.prezcription.pharmacyapp.fragment.AddBilledPatientFragment;
import com.prezcription.pharmacyapp.fragment.AddSearchDialogFragment;
import com.prezcription.pharmacyapp.fragment.BarCodeScannerFragment;
import com.prezcription.pharmacyapp.fragment.BillDetailFragment;

import io.fabric.sdk.android.Fabric;

/**
 * Created by amit on 02/12/17.
 */

public class BillingActivity extends AppCompatActivity implements AddBilledPatientFragment.OnBillItemFragmentInteractionListener
        , CreatePdfAsyncTask.AsyncResult, AddSearchDialogFragment.OnSearchDialogFragmentListener, BillDetailFragment.OnBillItemClickListener,
        BillItemDetailAdapter.onBillDetailItemClickListener {

    RelativeLayout rlPatientBilled, rlBilling, rlScan;
    FloatingActionButton fab;

    CreatePdfAsyncTask createPdfAsyncTask;
    ConvertToPdf invoicePdf;
    private File mInvoiceFile;

    private int VACCINATION = 222;
    private int MEDICINE = 111;

    private boolean isItemMedicine;

    //map to store item type
    HashMap<String, String> itemMap = new HashMap<>();

    private final int CALLER_FRAG_BILL = 1;

    private int mLocalId = -1;
    private String mBatchNumber;
    private int mPosition = -1;
    private int mDocId = -1;
    private int mBillId = -1;
    private int mPatientID = -1;
    private String mPatientGlobalID = "";
    private ArrayList<BillItem> billItemArrayList = new ArrayList<>();

    int pdfAsyncProgress = CreatePdfAsyncTask.NOT_STARTED;

    private final int PRINT = 0;
    private final int MAIL = 1;

    private ArrayList<BillItem> notAvailableItemList = new ArrayList<>();

    public static final String BILLED_PATIENT_FRAGMENT_TAG = "billed_patient_frag";
    public static final String BILL_DETAIL_FRAGMENT_TAG = "bill_detail_frag_tag";
    public static final String BILL_ITEM_FRAGMENT_SCAN = "bill_item_frag_scan";

    private static final int BILLED_PATIENT_FRAGMENT = 111;
    private static final int BILL_DETAIL_FRAGMENT = 222;
    private static final int BILL_ITEM_FRAGMENT_SCAN_INT = 333;

    ArrayList<BillItem> billDetailsItemArrayList;


    private static final String LOG_TAG = BillingActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);


//        rlPatientBilled = (RelativeLayout) findViewById(R.id.rl_patient_search);

        rlBilling = (RelativeLayout) findViewById(R.id.rl_bill_frag_layout);
        rlScan = (RelativeLayout) findViewById(R.id.rl_bill_item_scan_layout);
        fab = (FloatingActionButton) findViewById(R.id.fab_create_bill);

//        Button crashButton = new Button(this);
//        crashButton.setText("Crash!");
//        crashButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Crashlytics.getInstance().crash(); // Force a crash
//            }
//        });
//        addContentView(crashButton,
//                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                        ViewGroup.LayoutParams.WRAP_CONTENT));

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)           // Enables Crashlytics debugger
                .build();
        Fabric.with(fabric);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getFragmentManager();
                final BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
                if (billDetailFragment != null) {

                    int billId = billDetailFragment.getBillId();

                    if (billDetailFragment.isBilled()) {
                        createPdfAfterBilled(billDetailFragment.getBillDataAfterBilled());
                    } else if (billId > 0) {

                        mDocId = billDetailFragment.getDoctorId();
                        mPatientID = billDetailFragment.getPatientId();
                        mPatientGlobalID = billDetailFragment.getPatient().getUsername();
                        Log.d(LOG_TAG, "patient id: " + mPatientGlobalID);

                        float consultationFee = billDetailFragment.getConsultationFees();

//
                        if (billDetailFragment.isValid()) {

                            ArrayList<BillItem> billItemList = billDetailFragment.getBillItems();
//
//                    if (billDataArrayList.size()>0){
//                        Toast.makeText(BillingActivity.this, "fetching medicine details from server",Toast.LENGTH_SHORT).show();
//                        getItemDetailFromServer(billId);
//                    }


                            if (billItemList.size() > 0) {
                                Log.d(LOG_TAG, "bill item list size: " + billItemList.size());
                                checkItemsAvailability(billItemList, billId, consultationFee);
//                                getItemDetailFromServer(billItemList, billId);
                            } else {

                                Log.d(LOG_TAG, "in only consultation: ");

                                if (consultationFee > 0) {
                                    if (billDetailsItemArrayList != null) {
                                        billDetailsItemArrayList.clear();
                                    }
                                    ArrayList<BillItem> list = new ArrayList<>();
                                    showConfirmationDialog(list, billId, consultationFee);
                                } else {
                                    Toast.makeText(BillingActivity.this, "consultation fee cannot be 0", Toast.LENGTH_LONG).show();
                                }


//                                Toast.makeText(BillingActivity.this, "No items queued for billing", Toast.LENGTH_SHORT).show();
                            }

//                    showDialog(billDetailFragment, billDataArrayList, billId);

                        }
                    } else {
                        Toast.makeText(BillingActivity.this, "No Items for queued for billing", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });


        String usernameToBill = null;

        if (getIntent().getStringExtra("userName") != null) {
            usernameToBill = getIntent().getStringExtra("userName");
            Log.d(LOG_TAG, "patint name:Billing" + usernameToBill);

        }

        FragmentTransaction tr = getFragmentManager().beginTransaction();

        tr.replace(R.id.rl_search_frag_layout,
                AddBilledPatientFragment.newInstance(usernameToBill), BILLED_PATIENT_FRAGMENT_TAG);


        tr.replace(R.id.rl_bill_frag_layout,
                BillDetailFragment.newInstance(), BILL_DETAIL_FRAGMENT_TAG);


        tr.commit();


    }


    //    public void formatInvoiceData(){
//        ArrayList<InvoiceDataFormatted> mInvoiceDataArrayList =  new ArrayList<>();
//        mInvoiceDataArrayList.add(new InvoiceDataFormatted(mFees,mListOfMedicine,mListOfVaccination,mListOfTreatment,mListOfProcedure));
//        generateInvoice(mInvoiceDataArrayList);
//    }
//
    public void generateInvoice(ArrayList<BillData> billDataArrayList) {

        invoicePdf = new ConvertToPdf(this, billDataArrayList, new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new Date()));
        createPdfAsyncTask = new CreatePdfAsyncTask(this, this);
        createPdfAsyncTask.execute(invoicePdf);
//        Toast.makeText(getBaseContext(), "Please wait,invoice is being generated.", Toast.LENGTH_SHORT).show();

    }


    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public void onItemClick(int patientId, int regId, int doctorId, int billId) {

        FragmentManager fragmentManager = getFragmentManager();

        BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
        if (billDetailFragment != null) {
            Log.d(LOG_TAG, "Bill detail activity is not null, setting ids");
            billDetailFragment.setIds(patientId, regId, doctorId, billId);
        } else {
            Log.d(LOG_TAG, "Bill detail activity is null");
        }

    }


    @Override
    public void onResult(File file, int pdfType) {
        mInvoiceFile = file;
        invoicePdf.promptForNextAction(mInvoiceFile, "Digital prescription generated on " + Utility.getCurrentDate(), "PFA");

//        if(buttonType == MAIL){
//        }else if(buttonType==PRINT){
//            mInvoiceFile = file;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
//                String jobName = getString(R.string.app_name) + " Document";
//                printManager.print(jobName, new PrintAdapter(mInvoiceFile), null);
//            }else {
//                Toast.makeText(getBaseContext(), "Please update your OS to print", Toast.LENGTH_SHORT).show();
//            }
//        }

    }

    public final static int REQUEST_CODE = 10101;

    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (Settings.canDrawOverlays(this)) {
                FragmentManager fragmentManager = getFragmentManager();

                BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
                if (billDetailFragment != null) {
                    ArrayList<BillData> billDataArrayList = billDetailFragment.getBillDataArrayList();
                    if (billDataArrayList != null && billDataArrayList.size() > 0) {
                        generateInvoice(billDataArrayList);
                    }
                }
            }
        } else {

            //move this code to setScanREsult
            Log.d(LOG_TAG, "inside from scan frag");
            rlBilling.setVisibility(View.VISIBLE);
            rlScan.setVisibility(View.GONE);

            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                //if barcode has nothing in it
                if (result.getContents() == null) {
                    Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
                } else {
                    //if barcode contains data
                    Log.d(LOG_TAG, " result String: " + result.getContents());
                    int batchId = -1;
                    int itemId = -1;

                    FragmentManager fragmentManager = getFragmentManager();
                    BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
                    if (billDetailFragment != null) {
                        Log.d(LOG_TAG, "local id of item" + mLocalId);
                        if (mLocalId != -1) {
//                            billDetailFragment.updateBillItem(itemId, batchId, mLocalId);
                        }

                    }


                }

            }

        }
    }


    @Override
    public void onProgressUpdate(int progress) {
        pdfAsyncProgress = progress;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Utility.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE) {
            Log.d(LOG_TAG, "inside permission request coedE:");
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(LOG_TAG, "permission granted:" + permissions[0] + "isgranted: " + grantResults[0]);

            } else {
                Log.d(LOG_TAG, "permission not granted:" + permissions[0] + "isgranted: " + grantResults[0]);

            }

        }

    }

    @Override
    public void onSearchItemSelected(int id, String name) {
        Fragment fragment = getFragmentManager().findFragmentByTag(BILLED_PATIENT_FRAGMENT_TAG);
        if (fragment instanceof AddBilledPatientFragment) {
            ((AddBilledPatientFragment) fragment).onSearchItemSelected(id, name);
        }
    }


    @Override
    public void callerFragment(int calledId) {

    }

    @Override
    public void onItemClickScan(int localId, int itemType, int pos) {

        rlBilling.setVisibility(View.GONE);
        rlScan.setVisibility(View.VISIBLE);

        fab.setVisibility(View.GONE);
        Log.d(LOG_TAG, "inside onItemSclickScan" + localId);

        if (itemType == DetailBillRecyclerAdapter.VACCINATION) {
            isItemMedicine = false;
        } else {
            isItemMedicine = true;
        }

        mLocalId = localId;
        mPosition = pos;

        getFragmentManager().beginTransaction().replace(R.id.rl_bill_item_scan_layout,
                BarCodeScannerFragment.newInstance(), BILL_ITEM_FRAGMENT_SCAN).commit();

    }

    private String mItemId;

//    private void onItemScanFromItemDetailDialog(String itemId, String batchId, int pos){
//
//        rlBilling.setVisibility(View.GONE);
//        rlScan.setVisibility(View.VISIBLE);
//
//        fab.setVisibility(View.GONE);
//        Log.d(LOG_TAG, "inside onItemSclickScan" + itemId);
//
////        if (itemType == DetailBillRecyclerAdapter.VACCINATION) {
////            isItemMedicine = false;
////        } else {
////            isItemMedicine = true;
////        }
//
//        mItemId = itemId;
//        mBatchNumber = batchId;
//        mPosition = pos;
//
//        getFragmentManager().beginTransaction().replace(R.id.rl_bill_item_scan_layout,
//                BarCodeScannerFragment.newInstance(), BILL_ITEM_FRAGMENT_SCAN).commit();
//
//    }

    private void getItemDetailFromServer(ArrayList<BillItem> list, final int billId, float consultationFee) {
        Log.d(LOG_TAG, "billId" + billId);

        Log.d(LOG_TAG, "inside getItemDEtail method");

        RequestQueue requestQueue;
        StringRequest stringRequest;
        final String VOLLEY_REQUEST_ITEM_DETAIL = "item detail";


//        requestQueue = Volley.newRequestQueue(getBaseContext());

        String baseUrl = ApiConfig.GET_BILL_DETAIL;
//        String baseUrl = "http://192.168.0.167:3000/patient_bills/generateBill";


        Fragment frag = getFragmentManager().findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);

//        float consultationFees = ((BillDetailFragment) frag).getConsultationFees();
        Patient patient = ((BillDetailFragment) frag).getPatient();

        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setName(patient.getName());
        patientInfo.setUsername(patient.getUsername());
        patientInfo.setGender(patient.getGender());
        patientInfo.setPhone(patient.getPhoneNumber());
        String ageText;
        if (patient.getDob() != null) {
            String[] dobArray = patient.getDob().split("-");
            ageText = Utility.getAge(Integer.valueOf(dobArray[2]), Integer.valueOf(dobArray[1]), Integer.valueOf(dobArray[0]));
        } else {
            ageText = String.valueOf(patient.getAge()) + " " + patient.getAgeType();
        }
        patientInfo.setAge(ageText);
        patientInfo.setEmail(patient.getPatientEmail());

        Uri uri = PharmacyContract.DoctorEntry.CONTENT_URI;

        ContentResolver contentResolver = getContentResolver();

        String selection = PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID + " = ? ";
        String[] selectionArgs = {String.valueOf(mDocId)};

        String[] projection = {PharmacyContract.DoctorEntry.COLUMN_DOCTOR_NAME,
                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_QUALIFICATION,
                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_REGISTRATION_NUMBER};

        Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null);


        String doctorName = null;
        String doctorQualification = null;
        String doctorRegNumber = null;

        while (cursor.moveToNext()) {

            doctorName = cursor.getString(0);
            doctorQualification = cursor.getString(1);
            doctorRegNumber = cursor.getString(2);
        }

        DoctorInfo doctorInfo = new DoctorInfo();

        doctorInfo.setName(doctorName);
        doctorInfo.setQualification(doctorQualification);
        doctorInfo.setDoctorId(String.valueOf(mDocId));
        doctorInfo.setKmcNumber(doctorRegNumber);

        SharedPreferences settingsPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String discount = settingsPreferences.getString(Utility.SETTINGS_BILL_DISCOUNT, "0");

        String organisationName = settingsPreferences.getString(Utility.SETTINGS_PDF_ORGANISATION_NAME, null);
        String website = settingsPreferences.getString(Utility.SETTINGS_PDF_WEBSITE_NAME, null);
        String address = settingsPreferences.getString(Utility.SETTINGS_PDF_ADDRESS, null);

        String addressValue = "";

        if (organisationName != null) {
            doctorInfo.setClinicName(organisationName);
        }
        if (address != null) {
            addressValue = addressValue + address;
        }
        if (website != null) {
            addressValue = addressValue + "\n" + website;
        }

        if (!addressValue.equals("")) {
            doctorInfo.setAddress(addressValue);
        }

        Uri.Builder builder = Uri.parse(baseUrl).buildUpon().appendQueryParameter("doctorId", String.valueOf(mDocId))
                .appendQueryParameter("patientId", String.valueOf(mPatientGlobalID));

        for (int i = 0; i < list.size(); i++) {
            builder
                    .appendQueryParameter("items[][itemid]", list.get(i).getItemId())
                    .appendQueryParameter("items[][itemCount]", String.valueOf(list.get(i).getItemCount()))
                    .appendQueryParameter("items[][batchid]", list.get(i).getBatchId());
        }
        builder.appendQueryParameter("consultationFee", String.valueOf(consultationFee));
        builder.appendQueryParameter("discount", discount);
        builder.appendQueryParameter("patientInfo", new Gson().toJson(patientInfo));
        builder.appendQueryParameter("doctorInfo", new Gson().toJson(doctorInfo));

        final String urlString = builder.build().toString();
        Log.d(LOG_TAG, "urlString: " + urlString);


        Log.d(LOG_TAG, "base url is " + baseUrl);


        stringRequest = new StringRequest(Request.Method.GET, urlString, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d(LOG_TAG, "response from server: " + response);
                try {
                    BillCreationResponse billResponse = new Gson().fromJson(response, BillCreationResponse.class);
                    if (billResponse != null) {
//                        Toast.makeText(BillingActivity.this,"bill created..initialising pdf",Toast.LENGTH_SHORT).show();
                        updateItemIntoDb(billResponse, billId);
                        createPdfFromResponse(billResponse);
                    }

                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG, "Some error in item detail post");
                Utility.getVolleyErrorResponse(error, BillingActivity.this, null);
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        stringRequest.setTag(VOLLEY_REQUEST_ITEM_DETAIL);

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);


    }

    ProgressDialog progressBar;

    private void checkItemsAvailability(final ArrayList<BillItem> list, final int billId, final float consultationFee) {
        notAvailableItemList.clear();

        Log.d(LOG_TAG, "inside check item method");
        final int initialCount = list.size();

        RequestQueue requestQueue;
        StringRequest stringRequest;
        final String VOLLEY_REQUEST_ITEM_AVAIL = "item availability";

        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Loading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();


//        requestQueue = Volley.newRequestQueue(getBaseContext());

        String checkItemUrl = ApiConfig.CHECK_ITEM_AVAILABILITY;
//        String checkItemUrl = "http://192.168.0.167:3000/patient_bills/checkItems";

        SharedPreferences settingsPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String discount = settingsPreferences.getString(Utility.SETTINGS_BILL_DISCOUNT, "0");


        Uri.Builder itemCheckBuilder = Uri.parse(checkItemUrl).buildUpon().appendQueryParameter("doctorId", String.valueOf(mDocId))
                .appendQueryParameter("patientId", String.valueOf(mPatientGlobalID)).appendQueryParameter("doctorDiscount", discount);


        for (int j = 0; j < list.size(); j++) {
            itemCheckBuilder.
                    appendQueryParameter("items[][itemid]", list.get(j).getItemId())
                    .appendQueryParameter("items[][itemCount]", String.valueOf(list.get(j).getItemCount()))
                    .appendQueryParameter("items[][batchid]", list.get(j).getBatchId());
        }


        final String itemCheckUrl = itemCheckBuilder.build().toString();
        Log.d(LOG_TAG, "urlString: " + itemCheckUrl);

        stringRequest = new StringRequest(Request.Method.GET, itemCheckUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressBar.cancel();
                progressBar.dismiss();
                boolean differentBarCodeItemAvailable = false;
                int countAvailable = 0;

                Log.d(LOG_TAG, "response from server: item check" + response);
                try {
                    ItemAvailabilityResponse itemAvailabilityResponse = new Gson().fromJson(response, ItemAvailabilityResponse.class);

                    if (itemAvailabilityResponse != null) {

                        billDetailsItemArrayList = new ArrayList<>();

                        List<ItemAvailabilityResponse.LineitemsBean> beanArrayList = itemAvailabilityResponse.getLineitems();

                        for (int i = 0; i < beanArrayList.size(); i++) {
                            if (beanArrayList.get(i).getItemName().equals("Item not available")) {

                                Log.d(LOG_TAG, "list size before remov: " + list.size());
                                removeItemFromList(list, beanArrayList.get(i).getItemid());
                                Log.d(LOG_TAG, "list size after remov: " + list.size());
//                                notAvailableItemList.add(beanArrayList.get(i).getItemid());
                            } else {
                                countAvailable += beanArrayList.get(i).getItemCount();

                                BillItem billDetailItem = new BillItem();
                                billDetailItem.setName(beanArrayList.get(i).getItemName());
                                billDetailItem.setDiscount(beanArrayList.get(i).getItemDisc());
                                billDetailItem.setItemId(beanArrayList.get(i).getItemid());
                                billDetailItem.setBatchId(beanArrayList.get(i).getBatchid());
                                billDetailItem.setExpiry(beanArrayList.get(i).getExpiry());
                                billDetailItem.setMrp(beanArrayList.get(i).getMrp());
                                billDetailItem.setcGst(beanArrayList.get(i).getCGST());
                                billDetailItem.setsGst(beanArrayList.get(i).getSGST());
                                billDetailItem.setNetRate(beanArrayList.get(i).getNetRate());
                                billDetailItem.setAmount(beanArrayList.get(i).getNetAmt());
                                billDetailItem.setAvailableCount(beanArrayList.get(i).getItemCount());

                                if (isItemFromDifferentBatch(list, beanArrayList.get(i).getBatchid(), beanArrayList.get(i).getItemid())) {
                                    billDetailItem.setDifferentBarCodeItem(true);
                                    differentBarCodeItemAvailable = true;
                                    billDetailItem.setViewType(BillItemDetailAdapter.DIFFERENT_BAR_CODE);
                                } else {
                                    billDetailItem.setDifferentBarCodeItem(false);
                                    billDetailItem.setViewType(BillItemDetailAdapter.DETAIL);
                                }

                                billDetailsItemArrayList.add(billDetailItem);
                            }
                        }

                        BillItem consultationItem = new BillItem();
                        consultationItem.setViewType(BillItemDetailAdapter.CONSULTATION);
                        consultationItem.setAmount(consultationFee);
                        billDetailsItemArrayList.add(consultationItem);

                        if (billDetailsItemArrayList.size() > 0) {
                            BillItem billDetailItem = new BillItem();
                            billDetailItem.setAmount(itemAvailabilityResponse.getTotalAmtDue() + consultationFee);
                            billDetailItem.setViewType(BillItemDetailAdapter.TOTAL);
                            billDetailsItemArrayList.add(billDetailItem);
                        }

                        int updatedCount = list.size();

                        for (int j = 0; j < list.size(); j++) {
                            BillItem item = new BillItem();
                            int availableCount = getAvailableItemCount(list.get(j).getItemId(), beanArrayList);
                            Log.d(LOG_TAG, "avail count: "+availableCount + "item id: "+list.get(j).getItemId()
                                    + "item count:"+list.get(j).getItemCount());

                            if (list.get(j).getItemCount() > availableCount) {
                                item.setAvailableCount(availableCount);
                                item.setName(list.get(j).getName());
                                item.setItemCount(list.get(j).getItemCount());
                                item.setViewType(ItemAvailabilityAdapter.DETAIL);
                                notAvailableItemList.add(item);
                            }
                        }

                        boolean insufficientCount = (updatedCount != initialCount) || (notAvailableItemList.size() > 0);

                        Log.d(LOG_TAG, "updated count: "+updatedCount + "initial count "+initialCount);

                        if (list.size() > 0) {
                            if (differentBarCodeItemAvailable){
                                if (!insufficientCount)
                                    showDifferentBarCodeDialog();
                                else
                                    Toast.makeText(BillingActivity.this,"Total count available for different bar code: "+countAvailable, Toast.LENGTH_LONG).show();
                            }else if (insufficientCount){
                                showDialogToConfirm(list, billId, consultationFee);
                            }
                            else {
                                showConfirmationDialog(list, billId, consultationFee);
//                                getItemDetailFromServer(list, billId);
                            }
//                           getItemDetailFromServer(list,billId);
                        } else {
                            Toast.makeText(BillingActivity.this, "None of the item available in inventory", Toast.LENGTH_SHORT).show();
                        }
                    }


                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG, "Some error in item detail post");
                Utility.getVolleyErrorResponse(error, BillingActivity.this, progressBar);
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        stringRequest.setTag(VOLLEY_REQUEST_ITEM_AVAIL);

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private int getAvailableItemCount(String itemId, List<ItemAvailabilityResponse.LineitemsBean> beanArrayList) {

        int count = 0;
        for (int i = 0; i < beanArrayList.size(); i++) {
            if (beanArrayList.get(i).getItemid().equals(itemId)) {
                count += beanArrayList.get(i).getItemCount();
            }
        }

        return count;

    }

    private void showDialogToConfirm(final ArrayList<BillItem> list, final int billId, final float consultationFee) {

        BillItem item = new BillItem();
        item.setViewType(ItemAvailabilityAdapter.HEADER);
        notAvailableItemList.add(0, item);

        Log.d(LOG_TAG, "non avail item size: " + notAvailableItemList.size());


        ItemAvailabilityAdapter mAdapter = new ItemAvailabilityAdapter(notAvailableItemList);


        new MaterialDialog.Builder(this)
                .title(R.string.item_non_avail)
                .content("Following list of items can not be billed")
                .adapter(mAdapter, null)
                .positiveText("Continue billing with available quantity")
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showConfirmationDialog(list, billId, consultationFee);
//                        getItemDetailFromServer(list, billId);
                    }
                })
                .show();
    }

    private void removeItemFromList(ArrayList<BillItem> list, String itemId) {
        Iterator<BillItem> itr = list.iterator();
        while (itr.hasNext()) {
            BillItem billItem = itr.next();
            if (billItem.getItemId().equals(itemId)) {
                billItem.setAvailableCount(0);
                billItem.setViewType(ItemAvailabilityAdapter.DETAIL);
                notAvailableItemList.add(billItem);
                itr.remove();

            }
        }
    }

    private boolean isItemFromDifferentBatch(ArrayList<BillItem> list, String batchNumber, String itemId) {
        boolean isItemInList = false;

        for (BillItem item : list) {
            if (item.getItemId().equals(itemId) && item.getBatchId().equals(batchNumber)) {
                isItemInList = true;
                break;
            }
        }
        return !isItemInList;
    }

    private void createPdfFromResponse(final BillCreationResponse response) {
        Log.d(LOG_TAG, "inside bill creation method");


        new MaterialDialog.Builder(BillingActivity.this)
                .title(R.string.bill_created_title)
                .content(R.string.bill_created_content)
                .positiveText(R.string.bill_created_positive_text)
                .negativeText(R.string.bill_created_negative_text)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (checkDrawOverlayPermission()) {
//                                    int billId = billDetailFragment.getBillId();

                            ArrayList<BillData> billDataArrayList = generateBillDataFromResponse(response);



                            Fragment frag = getFragmentManager().findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);

                            if (frag instanceof BillDetailFragment) {

                                //Add patient and bill data from response

                                BillData basicBillData = new BillData();
                                basicBillData.setDocId(String.valueOf(response.getCustomerId()));
                                basicBillData.setPatientId(response.getPatientId());
                                basicBillData.setBillDate(response.getBillDate());
                                basicBillData.setGlobalBillId(response.getBillNo());
                                basicBillData.setTotalDiscount(String.valueOf(response.getTotalDiscount()));
                                basicBillData.setTotalsGST(response.getTotalsGST());
                                basicBillData.setTotalcGST(response.getTotalcGST());
                                basicBillData.setTotalAmount(String.valueOf(response.getTotalAmtDue()));
                                basicBillData.setConsultationFees(response.getConsultationFee());
                                basicBillData.setViewType(DetailBillRecyclerAdapter.BILL_TOTAL_AMOUNT);

                                Patient patient = ((BillDetailFragment) frag).getPatient();
                                basicBillData.setItemType(DetailBillRecyclerAdapter.PATIENT);

                                basicBillData.setPatient(patient);

                                billDataArrayList.add(0, basicBillData);
                            }


                            if (billDataArrayList.size() > 0) {
                                Toast.makeText(BillingActivity.this, "Please wait....generating Bill pdf", Toast.LENGTH_SHORT).show();
                                generateInvoice(billDataArrayList);
                            }
                        }
                    }
                })
                .show();

    }

    private Dialog differentBarCodeDialog;

    private void showDifferentBarCodeDialog() {

         BillItem item = new BillItem();
         item.setViewType(BillItemDetailAdapter.HEADER);
         billDetailsItemArrayList.add(0, item);

         Log.d(LOG_TAG, "avail item size: " + billDetailsItemArrayList.size());


        BillItemDetailAdapter mAdapter = new BillItemDetailAdapter(billDetailsItemArrayList, this);

        differentBarCodeDialog = new MaterialDialog.Builder(BillingActivity.this)
                .title(R.string.different_barcode_title)
                .content(R.string.different_barcode_content)
                .adapter(mAdapter, null)
                .positiveText(R.string.different_barcode_positive_text)
                .negativeText(R.string.different_barcode_negative_text)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                       dialog.dismiss();
                    }
                })
                .show();
    }

    private void showConfirmationDialog(final ArrayList<BillItem> list, final int billId, final float consultationFee) {

        if (billDetailsItemArrayList == null || billDetailsItemArrayList.size() == 0) {
            billDetailsItemArrayList = new ArrayList<>();

            BillItem consultationItem = new BillItem();
            consultationItem.setViewType(BillItemDetailAdapter.CONSULTATION);
            consultationItem.setAmount(consultationFee);
            billDetailsItemArrayList.add(consultationItem);

            BillItem billDetailItem = new BillItem();
            billDetailItem.setAmount(consultationFee);
            billDetailItem.setViewType(BillItemDetailAdapter.TOTAL);
            billDetailsItemArrayList.add(billDetailItem);


        } else {

            BillItem item = new BillItem();
            item.setViewType(BillItemDetailAdapter.HEADER);
            billDetailsItemArrayList.add(0, item);


        }

        Log.d(LOG_TAG, "avail item size: " + billDetailsItemArrayList.size());


        BillItemDetailAdapter mAdapter = new BillItemDetailAdapter(billDetailsItemArrayList, this);

        new MaterialDialog.Builder(BillingActivity.this)
                .title(R.string.bill_warning_title)
                .content(R.string.bill_warning_content)
                .adapter(mAdapter, null)
                .positiveText(R.string.bill_warning_positive_text)
                .negativeText(R.string.bill_warning_negative_text)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (checkDrawOverlayPermission()) {
                            getItemDetailFromServer(list, billId, consultationFee);
                        }
                    }
                })
                .show();
    }

    //This method add basic patient and bill data for the selected patient,
    //however this could give wrong info if loader has not finished loading after data change
    //Use the following method to add details only for the billed patients
    private BillData addPatientAndBillData() {
        BillData patientData = new BillData();

        Fragment frag = getFragmentManager().findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
        if (frag instanceof BillDetailFragment) {
            patientData = ((BillDetailFragment) frag).getBillDetail();
            Patient patient = ((BillDetailFragment) frag).getPatient();
            patientData.setItemType(DetailBillRecyclerAdapter.PATIENT);
            patientData.setPatient(patient);
        }

        return patientData;
    }

    private void updateItemIntoDb(BillCreationResponse billResponse, int billId) {
        Log.d(LOG_TAG, "updating bill into db");

        if (billResponse != null) {
            String billNumber = billResponse.getBillNo();
            String dateTime = billResponse.getBillDate();
            String totalAmount = String.valueOf(billResponse.getTotalAmtDue());
            String totalDiscount = String.valueOf(billResponse.getTotalDiscount());
            String totatcGst = String.valueOf(billResponse.getTotalcGST());
            String totalsGst = String.valueOf(billResponse.getTotalsGST());
            String consultationFees = String.valueOf(billResponse.getConsultationFee());


            Log.d(LOG_TAG, "bill num " + billNumber + "dateTime: " + dateTime + "totalAmt: " +
                    totalAmount + "totalDisc " + totalDiscount + "totolcGst " + totatcGst + "totalsGst: " + totalsGst);


            List<BillCreationResponse.LineitemsBean> itemList = billResponse.getLineitems();
            Log.d(LOG_TAG, "itemList size:");

            if (itemList.size() > 0) {
                Log.d(LOG_TAG, "itelist size:before remove " + itemList.size());
                removeLineItemsIfItemNotAvailable(itemList);
                Log.d(LOG_TAG, "itelist size:after remove " + itemList.size());
            }


            Uri uri = PharmacyContract.BillsEntry.CONTENT_URI;
            String where = PharmacyContract.BillsEntry._ID + " =? ";
            String[] whereArgs = new String[]{String.valueOf(billId)};


            ContentValues contentValues = new ContentValues();
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_BILL_DATE, dateTime);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_GLOBAL_BILL_ID, billNumber);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_TOTAL_DISCOUNT_GIVEN, totalDiscount);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_BILL_CREATED, 1);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_CENTER_GST, totatcGst);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_STATE_GST, totalsGst);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_PAID_AMOUNT, totalAmount);
            contentValues.put(PharmacyContract.BillsEntry.COLUMN_CONSULTATION_FEE, consultationFees);


            int rowsUpdated = getContentResolver().update(uri, contentValues, where, whereArgs);
//            getContentResolver().notifyChange(uri,);

            HashSet<String> itemIdSet = new HashSet<>();
            HashSet<String> batchSet = new HashSet<>();

            if (rowsUpdated > 0 && itemList.size() > 0) {
                Log.d(LOG_TAG, "inside bill entry updated rows " + rowsUpdated);
                for (int i = 0; i < itemList.size(); i++) {
                    //ToDO: identify item type as vaccine and medicine and update accordingly2
                    Log.d(LOG_TAG, "itemId: " + itemList.get(i).getItemid());
                    Log.d(LOG_TAG, "batchNumber: " + itemList.get(i).getBatchid());
                    Log.d(LOG_TAG, "map string" + itemMap.toString());
                    Log.d(LOG_TAG, "map string item" + itemMap.get(itemList.get(i).getItemid()));

                    Log.d(LOG_TAG, "item map: " + new Gson().toJson(itemMap));

                    if (itemMap.get(itemList.get(i).getItemid()) != null && (itemMap.get(itemList.get(i).getItemid()).
                            equals(String.valueOf(MEDICINE)))) {
                        Log.d(LOG_TAG, "inserting medicine item ");

                        Log.d(LOG_TAG, "med item id: " + itemList.get(i).getItemid());
                        Log.d(LOG_TAG, "med batch id: " + itemList.get(i).getBatchid());
                        Log.d(LOG_TAG, "med mnf: " + itemList.get(i).getMfgr());
                        Log.d(LOG_TAG, "med sGst: " + itemList.get(i).getSGST());
                        Log.d(LOG_TAG, "med cGst: " + itemList.get(i).getCGST());
                        Log.d(LOG_TAG, "med dis: " + itemList.get(i).getItemDisc());
                        Log.d(LOG_TAG, "med net rate: " + itemList.get(i).getNetRate());
                        Log.d(LOG_TAG, "med net amount: " + itemList.get(i).getNetAmt());
                        Log.d(LOG_TAG, "med expiry: " + itemList.get(i).getExpiry());
                        Log.d(LOG_TAG, "med global bill id: " + billNumber);




                        if (itemIdSet.add(itemList.get(i).getItemid()) || batchSet.add(itemList.get(i).getBatchid()) ) {
                            batchSet.add(itemList.get(i).getBatchid());
                            Uri medicineUri = PharmacyContract.MedicineInBillsEntry.CONTENT_URI;
                            String selection = PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID + " =? AND " +
                                    PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID + " = ? AND " +
                                    PharmacyContract.MedicineInBillsEntry.COLUMN_BATCH_ID + " = ?";
                            String[] selectionArgs = new String[]{String.valueOf(billId), itemList.get(i).getItemid(), itemList.get(i).getBatchid()};

                            ContentValues medicineContentValues = new ContentValues();
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID, itemList.get(i).getItemid());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_NAME, itemList.get(i).getItemName());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BATCH_ID, itemList.get(i).getBatchid());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_ITEM_BILLED, 1);
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, itemList.get(i).getItemCount());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MNF, itemList.get(i).getMfgr());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MRP, itemList.get(i).getMrp());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AMOUNT, itemList.get(i).getNetAmt());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_PRICE, itemList.get(i).getNetRate());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ITEM_GROSS, itemList.get(i).getItemGross());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_CGST, itemList.get(i).getCGST());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_SGST, itemList.get(i).getSGST());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_EXPIRY, itemList.get(i).getExpiry());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_DISCOUNT, itemList.get(i).getItemDisc());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_GLOBAL_BILL_ID, billNumber);


                            int medicineRowsUpdated = getContentResolver().update(medicineUri, medicineContentValues, selection, selectionArgs);
                            Log.d(LOG_TAG, "medicineRowsUpdated: " + medicineRowsUpdated);
                        }else {
                            Uri medicineUri = PharmacyContract.MedicineInBillsEntry.CONTENT_URI;
                            Log.d(LOG_TAG, "inserting other item");

                            String barCode = itemList.get(i).getBatchid() + itemList.get(i).getItemid();

                            ContentValues medicineContentValues = new ContentValues();
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID, itemList.get(i).getItemid());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_NAME, itemList.get(i).getItemName());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BATCH_ID, itemList.get(i).getBatchid());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_ITEM_BILLED, 1);
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BAR_CODE, barCode);
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, itemList.get(i).getItemCount());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MNF, itemList.get(i).getMfgr());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MRP, itemList.get(i).getMrp());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AMOUNT, itemList.get(i).getNetAmt());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_PRICE, itemList.get(i).getNetRate());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ITEM_GROSS, itemList.get(i).getItemGross());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_CGST, itemList.get(i).getCGST());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_SGST, itemList.get(i).getSGST());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_EXPIRY, itemList.get(i).getExpiry());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_DISCOUNT, itemList.get(i).getItemDisc());
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_GLOBAL_BILL_ID, billNumber);
                            medicineContentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID, billId);


                            Uri insertedMedUri = getContentResolver().insert(medicineUri, medicineContentValues);

                            Log.d(LOG_TAG, "inserted med uri: "+ ContentUris.parseId(insertedMedUri));
                        }

                    } else if (itemMap.get(itemList.get(i).getItemid()) != null &&
                            itemMap.get(itemList.get(i).getItemid()).equals(String.valueOf(VACCINATION))) {

                        Log.d(LOG_TAG, "inserting vaccine item");
                        Uri vaccineUri = PharmacyContract.VaccineInBillsEntry.CONTENT_URI;
                        String selection = PharmacyContract.VaccineInBillsEntry.COLUMN_BILL_ID + " =? AND " +
                                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ID + " = ? AND "+
                                PharmacyContract.VaccineInBillsEntry.COLUMN_BATCH_ID + " = ?";
                        String[] selectionArgs = new String[]{String.valueOf(billId), itemList.get(i).getItemid(), itemList.get(i).getBatchid()};

                        ContentValues vaccineContentValues = new ContentValues();
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ID, itemList.get(i).getItemid());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_NAME, itemList.get(i).getItemName());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_BATCH_ID, itemList.get(i).getBatchid());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_ITEM_BILLED, 1);
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_QUANTITY, itemList.get(i).getItemCount());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_MNF, itemList.get(i).getMfgr());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_MRP, itemList.get(i).getMrp());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_AMOUNT, itemList.get(i).getNetAmt());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_PRICE, itemList.get(i).getNetRate());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ITEM_GROSS, itemList.get(i).getItemGross());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_CGST, itemList.get(i).getCGST());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_SGST, itemList.get(i).getSGST());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_EXPIRY, itemList.get(i).getExpiry());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_DISCOUNT, itemList.get(i).getItemDisc());
                        vaccineContentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_GLOBAL_BILL_ID, billNumber);

                        int vaccineRowsUpdated = getContentResolver().update(vaccineUri, vaccineContentValues, selection, selectionArgs);
                        Log.d(LOG_TAG, "vaccineRowsupdated: " + vaccineRowsUpdated);

                    }
                }
            }
        }

    }

    private void removeLineItemsIfItemNotAvailable(List<BillCreationResponse.LineitemsBean> itemList) {
        Iterator<BillCreationResponse.LineitemsBean> itr = itemList.iterator();
        while (itr.hasNext()) {
            if (itr.next().getItemName().equals("Item not available")) {
                itr.remove();
            }
        }

    }

    public void setScanResult(String text) {
        Log.d(LOG_TAG, "scan result" + text);
//        Toast.makeText(this, "item id is" + text, Toast.LENGTH_SHORT).show();

        Log.d(LOG_TAG, "inside from scan frag");
        rlBilling.setVisibility(View.VISIBLE);
        rlScan.setVisibility(View.GONE);


        fab.setVisibility(View.VISIBLE);


        if (text != null && text.length() >= 9) {
            //if barcode has nothing in it

            //if barcode contains data
            Log.d(LOG_TAG, " result String: " + text);
            String batchId = text.substring(0, 4);
            String itemId = text.substring(4, 9);
            int itemType;

            try {
                int temp = Integer.parseInt(itemId);
                Log.d(LOG_TAG, "num is " + temp);
                itemId = String.valueOf(temp);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


            if (checkIfCorrectItemIsScanned(mLocalId, itemId)) {

                Log.d(LOG_TAG, "batchId " + batchId);
                Log.d(LOG_TAG, "itemId " + itemId);

                if (isItemMedicine) {
                    Log.d(LOG_TAG, "medicine item");
                    itemMap.put(itemId, String.valueOf(MEDICINE));
                    itemType = DetailBillRecyclerAdapter.MEDICINE;
                } else {
                    Log.d(LOG_TAG, "vaccine item");
                    itemMap.put(itemId, String.valueOf(VACCINATION));
                    itemType = DetailBillRecyclerAdapter.VACCINATION;
                }


                FragmentManager fragmentManager = getFragmentManager();
                BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
                if (billDetailFragment != null) {
                    Log.d(LOG_TAG, "local id of item " + mLocalId);

                    if (mLocalId != -1) {
                        billDetailFragment.updateBillItem(itemId, batchId, text, mLocalId, itemType);
                        Log.d(LOG_TAG, "position: " + mPosition);
                        billDetailFragment.updateBarCode(mPosition, text);
                    }

                }

            } else {
                Toast.makeText(this, "Item id mismatch, please scan correct item for billing", Toast.LENGTH_LONG).show();
            }
        }


    }

    private boolean checkIfCorrectItemIsScanned(int localId, String itemId) {
        Log.d(LOG_TAG, "item test:localId " + localId + "itemId: " + itemId);
        FragmentManager fragmentManager = getFragmentManager();
        final BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
        return billDetailFragment != null && billDetailFragment.checkIfCorrectItemIsScanned(localId, itemId);

    }

//    private boolean checkIfCorrectItemIsScannedFromDialog( String itemId, String batchNumber) {
//        Log.d(LOG_TAG, "item test:localId " + localId + "itemId: " + itemId);
//
//
//        FragmentManager fragmentManager = getFragmentManager();
//        final BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
//        return billDetailFragment != null && billDetailFragment.checkIfCorrectItemIsScanned(localId, itemId);
//
//    }


    private ArrayList<BillData> generateBillDataFromResponse(BillCreationResponse response) {
        ArrayList<BillData> billDataArrayList = new ArrayList<>();

        if (response != null) {
            BillData billData = new BillData();
            billData.setBillDate(response.getBillDate());
            billData.setGlobalBillId(response.getBillNo());
            billData.setTotalcGST(response.getTotalcGST());
            billData.setTotalsGST((response.getTotalsGST()));
            billData.setDocId(String.valueOf(response.getCustomerId()));
//            billData.setCustomerId(response.getCustomerId());
            billData.setTotalDiscount(String.valueOf(response.getTotalDiscount()));
            billData.setTotalAmount(String.valueOf(response.getTotalAmtDue()));
            billData.setItemType(DetailBillRecyclerAdapter.DETAILS);
            billData.setPatientId(response.getPatientId());
            billData.setLineitemsBean(response.getLineitems());
            billData.setConsultationFees(response.getConsultationFee());
            billDataArrayList.add(billData);


        }

        return billDataArrayList;

    }


    @Override
    public void onBackPressed() {
        if (rlScan.getVisibility() == View.VISIBLE) {
            rlBilling.setVisibility(View.VISIBLE);
            rlScan.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);

            FragmentManager fragmentManager = getFragmentManager();
            BarCodeScannerFragment barCodeScannerFragment = (BarCodeScannerFragment) fragmentManager.findFragmentByTag(BILL_ITEM_FRAGMENT_SCAN);

            if (barCodeScannerFragment != null){
                barCodeScannerFragment.onPause();
            }

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onBarCodeSet(String itemId, String batchId, String barcode, int localId, int itemType) {


        if (itemType == DetailBillRecyclerAdapter.MEDICINE) {
            itemMap.put(itemId, String.valueOf(MEDICINE));
        } else if (itemType == DetailBillRecyclerAdapter.VACCINATION) {
            itemMap.put(itemId, String.valueOf(VACCINATION));
        }

        Fragment frag = getFragmentManager().findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
        if (frag instanceof BillDetailFragment) {
            ((BillDetailFragment) frag).updateBillItem(itemId, batchId, barcode, localId, itemType);
        }
    }


    private void createPdfAfterBilled(final ArrayList<BillData> billDataArrayList) {
        Log.d(LOG_TAG, "inside bill creation method");

        if (checkDrawOverlayPermission()) {
            Log.d(LOG_TAG, "bill list size: " + billDataArrayList.size());
            if (billDataArrayList.size() > 0) {
                billDataArrayList.add(0, addPatientAndBillData());
                Toast.makeText(BillingActivity.this, "Please wait....generating Bill pdf", Toast.LENGTH_SHORT).show();
                generateInvoice(billDataArrayList);
            }
        }

    }

    private void toggle(int type) {

//        PatientSearchFragment searchFragment = (PatientSearchFragment) getFragmentManager().findFragmentByTag(PATIENT_SEARCH_FRAGMENT_TAG);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

//                setCustomAnimations(R.animator.slide_up,
//                R.animator.slide_down,
//                R.animator.slide_up,
//                R.animator.slide_down);

        Fragment fragment = null;

        switch (type) {

            case BILL_ITEM_FRAGMENT_SCAN_INT:

                fragment = fragmentManager.findFragmentByTag(BILL_ITEM_FRAGMENT_SCAN);
                if (fragment != null) {
//                    if(searchFragment!=null){Utility.animateInFromBottom(mCardMenu, PatientInteractionActivity.this);}
                    fragmentManager.popBackStack();
                } else {
                    Log.d(LOG_TAG, "here inside add patient fragment null");
//                    if(searchFragment!=null){
//                        Utility.animateOutFromBottom(mCardMenu, PatientInteractionActivity.this);
//                    }
                    fragmentTransaction.replace(R.id.rl_bill_item_scan_layout, BarCodeScannerFragment.newInstance(), BILL_ITEM_FRAGMENT_SCAN);
                    fragmentTransaction.addToBackStack(null).commit();
                }

                break;

            case BILL_DETAIL_FRAGMENT:

                fragment = fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
                if (fragment != null) {
//                    if(searchFragment!=null){Utility.animateInFromBottom(mCardMenu, PatientInteractionActivity.this);}
                    fragmentManager.popBackStack();
                } else {
                    Log.d(LOG_TAG, "here inside add patient fragment null");
//                    if(searchFragment!=null){
//                        Utility.animateOutFromBottom(mCardMenu, PatientInteractionActivity.this);
//                    }
                    fragmentTransaction.replace(R.id.rl_bill_frag_layout, BillDetailFragment.newInstance(), BILL_DETAIL_FRAGMENT_TAG);
                    fragmentTransaction.addToBackStack(null).commit();
                }


        }
    }

    @Override
    public void onNewBillsInserted() {
        Toast.makeText(this, "Bill Queue updated", Toast.LENGTH_SHORT).show();
//       Fragment fragment = getFragmentManager().findFragmentByTag(BILLED_PATIENT_FRAGMENT_TAG);
//        if (fragment instanceof AddBilledPatientFragment){
//            ((AddBilledPatientFragment)fragment).addRecentData();
//            }
    }

    public void setServiceFlagVisibility(boolean connected) {
        Log.d(LOG_TAG, "inside setting vis: " + connected);
        FragmentManager fragmentManager = getFragmentManager();
        BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
        if (billDetailFragment != null) {
            Log.d(LOG_TAG, "inside frag not null: ");
            billDetailFragment.setServiceFlagVisibility(connected);
        }

    }

    @Override
    public void onDifferentBatchItemAdd(BillItem item) {

        FragmentManager fragmentManager = getFragmentManager();
        BillDetailFragment billDetailFragment = (BillDetailFragment) fragmentManager.findFragmentByTag(BILL_DETAIL_FRAGMENT_TAG);
        if (billDetailFragment != null) {
            Log.d(LOG_TAG, "local id of item " + mLocalId);
            if (differentBarCodeDialog != null){
                differentBarCodeDialog.dismiss();
            }
            billDetailFragment.addNewItem(item);
        }
    }
}

