package com.prezcription.pharmacyapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;

public class OpeningActivity extends AppCompatActivity {

    private Boolean mUserLogin;
    private String mPhoneNumber;
    private String mPassword;
    private int mDoctorId;
    private final String DEFAULT = "default_username_password_phone_number";
    private final String LOG_TAG = LoginActivity.class.getSimpleName();
    String mIsNotification = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening);

        if(getIntent()!=null){

            mIsNotification = getIntent().getStringExtra("notify");

        }

        goToMainNavigationIfLoggedIn();

        Button btnSignIn = (Button)findViewById(R.id.btn_sign_in);

        btnSignIn.setTypeface(Utility.getProximaNovaTypeface(getBaseContext(),Utility.REGULAR));

        Button btnRegister = (Button)findViewById(R.id.btn_register_in);

        btnRegister.setTypeface(Utility.getProximaNovaTypeface(getBaseContext(),Utility.REGULAR));

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
                startActivity(intent);

            }
        });

    }


    private void goToMainNavigationIfLoggedIn(){

        SharedPreferences sharedPreferences = getSharedPreferences(Utility.USER_LOG_PREF,MODE_PRIVATE);
        mUserLogin = sharedPreferences.getBoolean(Utility.LOG_KEY,false);
        Log.d("Anubhav",mUserLogin+" ");
        mPhoneNumber = sharedPreferences.getString(Utility.PHONE_NUMBER_KEY,DEFAULT);
        mPassword = sharedPreferences.getString(Utility.PASSWORD_KEY,DEFAULT);
        mDoctorId = sharedPreferences.getInt(Utility.HOSPITAL_ID_KEY,0);

        if(mUserLogin!=null && mUserLogin ){

//            if(mPhoneNumber!=DEFAULT&&mDoctorId==0){
//                getLoginResponse(mPhoneNumber,mPassword);
//            }

            Intent intent = new Intent(this, BillingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.putExtra(Utility.IS_NOTIFICATION,mIsNotification);
            startActivity(intent);
            finish();
        }

    }
}
