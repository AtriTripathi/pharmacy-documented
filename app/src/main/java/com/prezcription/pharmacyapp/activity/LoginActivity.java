package com.prezcription.pharmacyapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.prezcription.pharmacyapp.ApiConfig;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.custom.MyCookieStore;
import com.prezcription.pharmacyapp.data.PharmacyContentProvider;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.dto.DocData;
import com.prezcription.pharmacyapp.data.dto.DoctorData;
import com.prezcription.pharmacyapp.data.gson.Login;

public class LoginActivity extends AppCompatActivity {

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;

    public static Context context;

    private Boolean mUserLogin;
    private String mPhoneNumber;
    private String mPassword;
    private int mHospitalId;
    private final String DEFAULT = "default_username_password_phone_number";
    private final String LOG_TAG = LoginActivity.class.getSimpleName();

    ProgressDialog progressBar;
    String mIsNotification = "false";

    Login loginData;
    String mResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);


//        if(getIntent()!=null){
//
//            mIsNotification = getIntent().getStringExtra("notify");
//
//        }

//        goToMainNavigationIfLoggedIn();

        context = getApplicationContext();

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.integer.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button btnResetPassword = (Button) findViewById(R.id.btn_reset_password);

        btnResetPassword.setTypeface(Utility.getProximaNovaTypeface(this,Utility.SEMI_BOLD));

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(LoginActivity.this);
                View promptsView = li.inflate(R.layout.edit_text_pop_up, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.et_popup1);
                userInput.setInputType(InputType.TYPE_CLASS_NUMBER);

                // set dialog message
                alertDialogBuilder
                        .setTitle("Please enter your 10 digit phone number to reset password")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text
                                        resetPassword(String.valueOf(userInput.getText()));
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);

        mEmailSignInButton.setTypeface(Utility.getProximaNovaTypeface(getBaseContext(),Utility.REGULAR));
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        Button registerButton = (Button) findViewById(R.id.register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    private void goToMainNavigationIfLoggedIn(){

        SharedPreferences sharedPreferences = getSharedPreferences(Utility.USER_LOG_PREF,MODE_PRIVATE);
        mUserLogin = sharedPreferences.getBoolean(Utility.LOG_KEY,false);
        Log.d(LOG_TAG,mUserLogin+" ");
        mPhoneNumber = sharedPreferences.getString(Utility.PHONE_NUMBER_KEY,DEFAULT);
        mPassword = sharedPreferences.getString(Utility.PASSWORD_KEY,DEFAULT);
        mHospitalId = sharedPreferences.getInt(Utility.HOSPITAL_ID_KEY,0);

        if(mUserLogin!=null && mUserLogin ){

//            if(mPhoneNumber!=DEFAULT&&mHospitalId==0){
//                getLoginResponse(mPhoneNumber,mPassword);
//            }

            Intent intent = new Intent(this, BillingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            intent.putExtra(Utility.IS_NOTIFICATION,mIsNotification);
            startActivity(intent);
            finish();
        }

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {


        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String input = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        //Only number
        String regexStr = "^[0-9]*$";

        // Check for a valid input address.
        if (TextUtils.isEmpty(input)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(input)&&!input.matches(regexStr)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        } else if (!isPhoneNumberValid(input)&&input.matches(regexStr)){
            focusView = mEmailView;
            mEmailView.setError(getString(R.string.error_invalid_phone_number));
            cancel=true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            Log.d(LOG_TAG,"gvjj"+input+password);
            getLoginResponse(input,password);
        }
    }

    private void showAlertDialogBox(String response){


        new MaterialDialog.Builder(this)
                .title("Oops..."+response)
                .content("Please check the credentials")
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();


//        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
//                .setTitleText("Oops... "+ response)
//                .setContentText("Please check the credentials")
//                .setConfirmText("Okay")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sweetAlertDialog) {
//                        sweetAlertDialog.dismiss();
//                    }
//                })
//                .show();

//        new AlertDialog.Builder(this)
//                .setTitle("Error: "+ response)
//                .setMessage("Please check the credentials")
//                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // continue with delete
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPhoneNumberValid(String phoneNumber){

        String regexStr = "^[0-9]{10}$";

        if (phoneNumber.matches(regexStr)){
            return true;
        }else {
            return false;
        }

    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }




    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    RequestQueue requestQueue;
    StringRequest stringRequest;
    private static final String VOLLEY_REQUEST_RESET_PASSWORD = "reset password";

    public void resetPassword(final String phoneNumber){

        // prepare for a progress bar dialog
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Loading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();

        requestQueue = Volley.newRequestQueue(getBaseContext());

        stringRequest = new StringRequest(Request.Method.POST, ApiConfig.RESET_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("Ok")){
                    Toast.makeText(LoginActivity.this,"New password has been sent to your registered email id and phone number", Toast.LENGTH_SHORT).show();
                }else if (response.equals("Error")){
                    Toast.makeText(LoginActivity.this,"Something went wrong please try again later", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(LoginActivity.this,response, Toast.LENGTH_SHORT).show();
                }
                progressBar.cancel();
                progressBar.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG,"Some error in reset password");
                Utility.getVolleyErrorResponse(error,LoginActivity.this,progressBar);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("PHONE_NUMBER", phoneNumber);
                return params;
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                super.getHeaders();
//                Map<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/x-www-form-urlencoded");
//                return headers;
//            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        stringRequest.setTag(VOLLEY_REQUEST_RESET_PASSWORD);

        requestQueue.add(stringRequest);


    }


    private final String VOLLEY_TAG_LOGIN_PHONE = "phone login";
    private final String VOLLEY_TAG_LOGIN_EMAIL  = "email login";
    StringRequest gsonRequestPhone;
    StringRequest gsonRequestEmail;



    public Login getLoginResponse(final String input, final String password){

        String regexStr = "^[0-9]{10}$";

        // prepare for a progress bar dialog
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Loading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();

        requestQueue = Volley.newRequestQueue(getBaseContext());

        if(input.matches(regexStr)) {

            gsonRequestPhone = new StringRequest(Request.Method.POST, ApiConfig.LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if(Utility.isJSONValid(response)){

                        SharedPreferences sharedPreferences = getSharedPreferences(Utility.USER_LOG_PREF,MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        Gson gson =new Gson();
                        loginData = gson.fromJson(response, Login.class);

                        editor.putString(Utility.NAME_KEY,loginData.getNAME());
                        editor.putString(Utility.PHONE_NUMBER_KEY,loginData.getPHONE_NUMBER());
                        editor.putString(Utility.EMAIL_KEY,loginData.getEMAIL());
                        editor.putString(Utility.PASSWORD_KEY,password);
                        editor.putInt(Utility.HOSPITAL_ID_KEY,loginData.getHOSPITAL_ID());
                        editor.putInt(Utility.PATIENT_COUNTER_KEY,loginData.getPATIENT_COUNTER());
                        editor.putString(Utility.REFERRAL_CODE_KEY,loginData.getREFERRAL_CODE());
                        editor.putBoolean(Utility.LOG_KEY,true);

                        if(loginData.getADDRESS()!=null) {
                            for (Login.ADDRESSBean addressBean: loginData.getADDRESS()) {
                                editor.putString(Utility.ADDRESS_LINE_1_KEY, addressBean.getLINE_1());
                                editor.putString(Utility.ADDRESS_CITY_KEY,addressBean.getCITY());
                                editor.putString(Utility.ADDRESS_STREET_KEY,addressBean.getSTREET());
                                editor.putString(Utility.ADDRESS_PIN_KEY, String.valueOf(addressBean.getPIN()));
                            }
                        }

                        editor.commit();

                        Log.d(LOG_TAG, loginData.getNAME());

                        for (Login.DOCTORSBean doctor: loginData.getDOCTORS()){
                            DoctorData doctorData = new DoctorData(doctor.getNAME(), doctor.getPHONE_NUMBER(),doctor.getEMAIL() ,doctor.getPHOTO(), doctor.getSPECIALIZATION(),doctor.getQUALIFICATION(),doctor.getAGE(),doctor.getSEX(), doctor.getDOCTOR_ID(),doctor.getREGISTRATION_NUMBER());
                            if(!isDoctorInDatabase(doctorData.getWebId())) {
                                long id = PharmacyContentProvider.insertDoctorData(getContentResolver(), doctorData);

                                String savedDoctorListJson = getSharedPreferences(Utility.DOCTOR_LIST_PREF,Context.MODE_PRIVATE).getString(Utility.DOCTOR_LIST_KEY, Utility.DEFAULT_STRING_VALUE);
                                Log.d(LOG_TAG, "saved doctor list is "+ savedDoctorListJson);
                                ArrayList<DocData> docDatas;
                                if (!savedDoctorListJson.equals(Utility.DEFAULT_STRING_VALUE)) {
                                    Type type = new TypeToken<ArrayList<DocData>>() {
                                    }.getType();
                                    docDatas = gson.fromJson(savedDoctorListJson, type);
                                }else {
                                    docDatas = new ArrayList<>();
                                }
                                DocData docData = new DocData((int)id,doctorData.getName(),doctorData.getSpecialization(),doctorData.getPhoneNumber());
                                docData.setWebId(doctorData.getWebId());
                                docDatas.add(docData);
                                getSharedPreferences(Utility.DOCTOR_LIST_PREF,MODE_PRIVATE).edit().putString(Utility.DOCTOR_LIST_KEY,gson.toJson(docDatas)).apply();



                            }
                        }
                        progressBar.cancel();
                        progressBar.dismiss();

                        Intent intent = new Intent(getBaseContext(),BillingActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                    }else if(response.equals("this device already logged in ...")){

                        getSharedPreferences(MyCookieStore.COOKIE_PREF, Context.MODE_PRIVATE).edit().putString(MyCookieStore.COOKIE_RESPONSE,MyCookieStore.DEFAULT_STRING).apply();
                        CookieHandler.setDefault(new CookieManager(new MyCookieStore(LoginActivity.this), CookiePolicy.ACCEPT_ALL));
                        Toast.makeText(LoginActivity.this,"this device already logged in ...", Toast.LENGTH_SHORT).show();
                        SharedPreferences sharedPreferences = getSharedPreferences(Utility.USER_LOG_PREF,MODE_PRIVATE);
                        getLogoutResponse(sharedPreferences.getInt(Utility.HOSPITAL_ID_KEY,-1));

//                        getSharedPreferences(MyCookieStore.COOKIE_PREF, Context.MODE_PRIVATE).edit().putString(MyCookieStore.COOKIE_RESPONSE,MyCookieStore.DEFAULT_STRING).apply();
//                        CookieHandler.setDefault(new CookieManager(new MyCookieStore(NavigationActivity.this), CookiePolicy.ACCEPT_ALL));
//                        progressBar.cancel();
//                        progressBar.dismiss();
//                        showAlertDialogBox(response);

                    }else {
                        progressBar.cancel();
                        progressBar.dismiss();
                        showAlertDialogBox(response);

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utility.getVolleyErrorResponse(error,LoginActivity.this,progressBar);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("PHONE_NUMBER", input);
                    params.put("PASSWORD", password);
                    return params;
                }

//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    super.getHeaders();
//                    Map<String, String> headers = new HashMap<String, String>();
//                    headers.put("Content-Type", "application/x-www-form-urlencoded");
//                    return headers;
//                }
            };
            gsonRequestPhone.setTag(VOLLEY_TAG_LOGIN_PHONE);
            requestQueue.add(gsonRequestPhone);

        }else {

            gsonRequestEmail = new StringRequest(Request.Method.POST, ApiConfig.LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if(Utility.isJSONValid(response)){

                        SharedPreferences sharedPreferences = getSharedPreferences(Utility.USER_LOG_PREF,MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        Gson gson =new Gson();
                        loginData = gson.fromJson((String)response, Login.class);
                        Log.d(LOG_TAG, "Login response "+response);

                        editor.putString(Utility.NAME_KEY,loginData.getNAME());
                        editor.putString(Utility.PHONE_NUMBER_KEY,loginData.getPHONE_NUMBER());
                        editor.putString(Utility.EMAIL_KEY,loginData.getEMAIL());
                        editor.putString(Utility.PASSWORD_KEY,password);
                        editor.putInt(Utility.HOSPITAL_ID_KEY,loginData.getHOSPITAL_ID());
                        editor.putString(Utility.REFERRAL_CODE_KEY,loginData.getREFERRAL_CODE());
                        if(loginData.getADDRESS()!=null) {
                            for (Login.ADDRESSBean addressBean: loginData.getADDRESS()) {
                                editor.putString(Utility.ADDRESS_LINE_1_KEY, addressBean.getLINE_1());
                                editor.putString(Utility.ADDRESS_CITY_KEY,addressBean.getCITY());
                                editor.putString(Utility.ADDRESS_STREET_KEY,addressBean.getSTREET());
                                editor.putString(Utility.ADDRESS_PIN_KEY, String.valueOf(addressBean.getPIN()));
                            }
                        }
                        editor.putBoolean(Utility.LOG_KEY,true);
                        editor.apply();

                        for (Login.DOCTORSBean doctor: loginData.getDOCTORS()){
                            DoctorData doctorData = new DoctorData(doctor.getNAME(), doctor.getPHONE_NUMBER(),doctor.getEMAIL() ,doctor.getPHOTO(), doctor.getSPECIALIZATION(),doctor.getQUALIFICATION(),doctor.getAGE(),doctor.getSEX(), doctor.getDOCTOR_ID(),doctor.getREGISTRATION_NUMBER());
                            if(!isDoctorInDatabase(doctorData.getWebId())) {
                                long id = PharmacyContentProvider.insertDoctorData(getContentResolver(), doctorData);

                                String savedDoctorListJson = getSharedPreferences(Utility.DOCTOR_LIST_PREF,Context.MODE_PRIVATE).getString(Utility.DOCTOR_LIST_KEY, Utility.DEFAULT_STRING_VALUE);
                                Log.d(LOG_TAG, "saved doctor list is "+ savedDoctorListJson);
                                ArrayList<DocData> docDatas;
                                if (!savedDoctorListJson.equals(Utility.DEFAULT_STRING_VALUE)) {
                                    Type type = new TypeToken<ArrayList<DocData>>() {
                                    }.getType();
                                    docDatas = gson.fromJson(savedDoctorListJson, type);
                                }else {
                                    docDatas = new ArrayList<>();
                                }
                                DocData docData = new DocData((int)id,doctorData.getName(),doctorData.getSpecialization(),doctorData.getPhoneNumber());
                                docData.setWebId(doctorData.getWebId());
                                docDatas.add(docData);
                                getSharedPreferences(Utility.DOCTOR_LIST_PREF,MODE_PRIVATE).edit().putString(Utility.DOCTOR_LIST_KEY,gson.toJson(docDatas)).apply();


                            }
                        }

                        progressBar.cancel();
                        progressBar.dismiss();

//                        Intent intent = new Intent(getBaseContext(),NavigationActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                        finish();

                    }else {

                        progressBar.cancel();
                        progressBar.dismiss();
                        showAlertDialogBox(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utility.getVolleyErrorResponse(error,LoginActivity.this,progressBar);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("EMAIL", input);
                    params.put("PASSWORD", password);
                    return params;
                }

//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    super.getHeaders();
//                    Map<String, String> headers = new HashMap<String, String>();
//                    headers.put("Content-Type", "application/x-www-form-urlencoded");
//                    return headers;
//                }
            };
            gsonRequestEmail.setTag(VOLLEY_TAG_LOGIN_EMAIL);
            requestQueue.add(gsonRequestEmail);


        }

        return loginData;

    }


    public String getLogoutResponse(final int id){

        final String LOG_TAG = "Anubhav";

        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Logging out ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();

        RequestQueue queue = Volley.newRequestQueue(getBaseContext());

        StringRequest sr = new StringRequest(Request.Method.POST, ApiConfig.LOGOUT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(LOG_TAG, "Inside onResponse ------  " + response);

                progressBar.cancel();
                progressBar.dismiss();

                if(response.equals("Ok")){
                    Toast.makeText(getBaseContext(),"Logout sucessfull, please try to login again", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.getVolleyErrorResponse(error,LoginActivity.this,progressBar);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("DOCTOR_ID", String.valueOf(id));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
        return "";

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(stringRequest!=null) {
            stringRequest.cancel();
            if (requestQueue != null)
                requestQueue.cancelAll(VOLLEY_REQUEST_RESET_PASSWORD);
        }
        if(gsonRequestPhone!=null) {
            gsonRequestPhone.cancel();
            if (requestQueue != null)
                requestQueue.cancelAll(VOLLEY_TAG_LOGIN_PHONE);
        }
        if(gsonRequestEmail!=null) {
            gsonRequestEmail.cancel();
            if (requestQueue != null)
                requestQueue.cancelAll(VOLLEY_TAG_LOGIN_EMAIL);
        }
    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public boolean isDoctorInDatabase(int id){

//        Bundle bundle = new Bundle();
//        bundle.putInt(PATIENT_WEB_ID,id);
//        getLoaderManager().initLoader(IS_PATIENT_LOADER,bundle,this);
//        Log.d(LOG_TAG, "111 is false and " + isPatient);
//        return isPatient;

        String[] projection = new String[]{PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID,
                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_NAME,
                PharmacyContract.DoctorEntry._ID};
        String where = PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID +" = "+ id;
        Cursor cursor = getContentResolver().query(PharmacyContract.DoctorEntry.CONTENT_URI,projection,where,null,null);

        try {
            if (cursor.moveToNext()) {
                return true;
            } else {
                return false;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            return false;
        }
        finally {
            if(cursor!=null){
                cursor.close();
            }
        }
    }
}
