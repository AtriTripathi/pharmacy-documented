package com.prezcription.pharmacyapp.activity;

import android.app.Activity;
import android.os.Bundle;

import com.prezcription.pharmacyapp.fragment.SettingsActivityFragment;

public class SettingsActivity extends Activity {

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Display the fragment as the main content.
            getFragmentManager().beginTransaction()
                    .replace(android.R.id.content, new SettingsActivityFragment())
                    .commit();
        }
    }

