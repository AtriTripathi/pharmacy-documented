package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;

/**
 * Created by anubhav on 19/09/16.
 */
public class PatientData extends RecyclerSearchableData {

    private Patient patient;

    public PatientData(String name, int viewType){
        setName(name);
        setViewType(viewType);
    }

    public PatientData(String name, String phoneNumber , String photoUrl , int webId, int viewType) {
        setName(name);
        setViewType(viewType);
        this.phoneNumber = phoneNumber;
        this.webId = webId;
        this.photoUrl = photoUrl;

    }

    public PatientData(String name, String phoneNumber, String photoUrl , String relationship, int webId, int viewType) {
        setName(name);
        setViewType(viewType);
        this.relationship = relationship;
        this.phoneNumber = phoneNumber;
        this.photoUrl = photoUrl;
        this.webId = webId;
    }

    public PatientData(String name, String phoneNumber, String gender, int age, String photoUrl , String relationship, int webId, int profileId , int viewType){
        setName(name);
        setViewType(viewType);
        this.relationship = relationship;
        this.phoneNumber = phoneNumber;
        this.photoUrl = photoUrl;
        this.webId = webId;
        this.gender = gender;
        this.age = age;
        this.profileId = profileId;
    }

    private int profileId;
    private String relationship;
    private String photoUrl;
    private String phoneNumber;
    private int webId;
    private String gender;
    private int age;

    protected PatientData(Parcel in) {
        super(in);
        profileId = in.readInt();
        relationship = in.readString();
        photoUrl = in.readString();
        phoneNumber = in.readString();
        webId = in.readInt();
        gender = in.readString();
        age = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(profileId);
        dest.writeString(relationship);
        dest.writeString(photoUrl);
        dest.writeString(phoneNumber);
        dest.writeInt(webId);
        dest.writeString(gender);
        dest.writeInt(age);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PatientData> CREATOR = new Creator<PatientData>() {
        @Override
        public PatientData createFromParcel(Parcel in) {
            return new PatientData(in);
        }

        @Override
        public PatientData[] newArray(int size) {
            return new PatientData[size];
        }
    };

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getWebId() {
        return webId;
    }

    public void setWebId(int webId) {
        this.webId = webId;
    }

    public String getPhoneNumber() {
        return patient.getPhoneNumber();
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
