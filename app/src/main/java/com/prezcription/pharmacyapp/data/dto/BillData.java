package com.prezcription.pharmacyapp.data.dto;



import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.prezcription.pharmacyapp.BillCreationResponse;
import com.prezcription.pharmacyapp.data.dto.Patient;
import com.prezcription.pharmacyapp.data.dto.RecyclerSearchableData;

/**
 * Created by anubhav on 17/12/17.
 */

public class BillData extends RecyclerSearchableData implements Cloneable {

    private float discountMedicine;
    private float discountVaccine;
    private float price;
    private float itemGross;
    private float quantity;
    private float totalPrice;
    private boolean itemBilled;
    private int localId;
    private boolean billCreated;
    private int itemType;
    private int discountType;
    private String docId;
    private double consultationFees;
    private int availableQty;


    private String billDate;
    private String billNo;
    private double totalsGST;
    private double totalcGST;
    private String totalDiscount;
    private int customerId;
    private String totalAmount;
    private String patientId;
    private List<BillCreationResponse.LineitemsBean> lineitemsBean;
    private String barCode;
    private String globalBillId;

    private boolean availableOnline;

    private Patient patient;

    private String expiry,mnf,itemcGst,itemsGst,itemName,itemId,batchId,mrp,netRate,itemPrice,itemDisc,itemNetAmt;

    public boolean isAvailableOnline() {
        return availableOnline;
    }

    public void setAvailableOnline(boolean availableOnline) {
        this.availableOnline = availableOnline;
    }

    public BillData() {
    }

    public BillData clone() throws CloneNotSupportedException
    {
        BillData billData =  (BillData) super.clone();

        billData.lineitemsBean = new ArrayList<>();
        billData.patient = new Patient();

        return billData;
    }


    public BillData(String name, int viewType,int localId , float price, float quantity,float totalPrice ,boolean itemBilled, boolean billCreated) {
        super(name, viewType);
        this.localId = localId;
        this.price = price;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.itemBilled = itemBilled;
        this.billCreated = billCreated;
    }

    public BillData(String name, int viewType,int localId , float price, float quantity,float totalPrice ,boolean itemBilled, boolean billCreated,int itemType) {
        super(name, viewType);
        this.localId = localId;
        this.price = price;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.itemBilled = itemBilled;
        this.billCreated = billCreated;
        this.itemType = itemType;
    }

    public BillData(String name, int viewType) {
        super(name, viewType);
    }

    public BillData(String name, int viewType, float price) {
        super(name, viewType);
        this.price = price;
    }

    public BillData(String name, int viewType, float price,boolean billCreated) {
        super(name, viewType);
        this.price = price;
        this.billCreated = billCreated;
    }

    public BillData(String name, int viewType, float price,boolean billCreated,int itemType) {
        super(name, viewType);
        this.price = price;
        this.billCreated = billCreated;
        this.itemType = itemType;

    }

    public double getConsultationFees() {
        return consultationFees;
    }

    public void setConsultationFees(double consultationFees) {
        this.consultationFees = consultationFees;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }



    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public boolean isItemBilled() {
        return itemBilled;
    }

    public void setItemBilled(boolean itemBilled) {
        this.itemBilled = itemBilled;
    }

    public boolean isBillCreated() {
        return billCreated;
    }

    public void setBillCreated(boolean billCreated) {
        this.billCreated = billCreated;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public float getDiscountMedicine() {
        return discountMedicine;
    }

    public void setDiscountMedicine(float discountMedicine) {
        this.discountMedicine = discountMedicine;
    }

    public float getDiscountVaccine() {
        return discountVaccine;
    }

    public void setDiscountVaccine(float discountVaccine) {
        this.discountVaccine = discountVaccine;
    }

    public int getDiscountType() {
        return discountType;
    }

    public void setDiscountType(int discountType) {
        this.discountType = discountType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

//    public String getBillNo() {
//        return billNo;
//    }
//
//    public void setBillNo(String billNo) {
//        this.billNo = billNo;
//    }

    public double getTotalsGST() {
        return totalsGST;
    }

    public void setTotalsGST(double totalsGST) {
        this.totalsGST = totalsGST;
    }

    public double getTotalcGST() {
        return totalcGST;
    }

    public void setTotalcGST(double  totalcGST) {
        this.totalcGST = totalcGST;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int   customerId) {
        this.customerId = customerId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public List<BillCreationResponse.LineitemsBean> getLineitemsBean() {
        return lineitemsBean;
    }

    public void setLineitemsBean(List<BillCreationResponse.LineitemsBean> lineitemsBean) {
        this.lineitemsBean = lineitemsBean;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getGlobalBillId() {
        return globalBillId;
    }

    public void setGlobalBillId(String globalBillId) {
        this.globalBillId = globalBillId;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getMnf() {
        return mnf;
    }

    public void setMnf(String mnf) {
        this.mnf = mnf;
    }

    public String getItemcGst() {
        return itemcGst;
    }

    public void setItemcGst(String itemcGst) {
        this.itemcGst = itemcGst;
    }

    public String getItemsGst() {
        return itemsGst;
    }

    public void setItemsGst(String itemsGst) {
        this.itemsGst = itemsGst;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getNetRate() {
        return netRate;
    }

    public void setNetRate(String netRate) {
        this.netRate = netRate;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDisc() {
        return itemDisc;
    }

    public void setItemDisc(String itemDisc) {
        this.itemDisc = itemDisc;
    }

    public String getItemNetAmt() {
        return itemNetAmt;
    }

    public void setItemNetAmt(String itemNetAmt) {
        this.itemNetAmt = itemNetAmt;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public float getItemGross() {
        return itemGross;
    }

    public void setItemGross(float itemGross) {
        this.itemGross = itemGross;
    }

    public int getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(int availableQty) {
        this.availableQty = availableQty;
    }
}
