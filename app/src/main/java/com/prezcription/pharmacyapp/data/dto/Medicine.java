package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by anubhav on 03/12/17.
 */

public class Medicine implements Parcelable, Cloneable {

    private String name;
    private String route;
    private double price;
    private int id;
    private ArrayList<String> composition;
    private String packing;
    private MedicineQuantity medicineQuantity;
    private String reference;
    private String genericName;
    private String company;
    private String note;
    private boolean sos;
    private String goFrugalItemId;

    public Medicine() {
    }

    public Medicine(String name, String route, double price, int id, String packing, MedicineQuantity medicineQuantity) {
        this.name = name;
        this.route = route;
        this.price = price;
        this.id = id;
        this.packing = packing;
        this.medicineQuantity = medicineQuantity;
    }

    public Medicine(String name, MedicineQuantity medicineQuantity, String reference, double price, String route ) {
        this.medicineQuantity = medicineQuantity;
        this.reference = reference;
        this.price = price;
        this.route = route;
        this.name = name;
    }

    public Medicine(String name, MedicineQuantity medicineQuantity, String reference, double price, String route, int id ) {
        this.medicineQuantity = medicineQuantity;
        this.reference = reference;
        this.price = price;
        this.route = route;
        this.name = name;
        this.id = id;
    }

    public Medicine(String name, String route, double price, int id, String packing, String company, String reference, MedicineQuantity medicineQuantity) {
        this.name = name;
        this.route = route;
        this.price = price;
        this.id = id;
        this.packing = packing;
        this.medicineQuantity = medicineQuantity;
        this.company = company;
        this.reference = reference;
    }


    protected Medicine(Parcel in) {
        name = in.readString();
        route = in.readString();
        price = in.readDouble();
        id = in.readInt();
        composition = in.createStringArrayList();
        packing = in.readString();
        medicineQuantity = in.readParcelable(MedicineQuantity.class.getClassLoader());
        reference = in.readString();
        genericName = in.readString();
        company = in.readString();
        note = in.readString();
        sos = in.readByte() != 0;
        goFrugalItemId = in.readString();
    }

    public static final Creator<Medicine> CREATOR = new Creator<Medicine>() {
        @Override
        public Medicine createFromParcel(Parcel in) {
            return new Medicine(in);
        }

        @Override
        public Medicine[] newArray(int size) {
            return new Medicine[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<String> getComposition() {
        return composition;
    }

    public void setComposition(ArrayList<String> composition) {
        this.composition = composition;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public Medicine(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Medicine(String name, double price, int id) {
        this.name = name;
        this.price = price;
        this.id = id;
    }

    public Medicine(String name, double price, int id, String genericName) {
        this.name = name;
        this.price = price;
        this.id = id;
        this.genericName = genericName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public MedicineQuantity getMedicineQuantity() {
        return medicineQuantity;
    }



    public void setMedicineQuantity(MedicineQuantity medicineQuantity) {
        this.medicineQuantity = medicineQuantity;
    }

    public double totalAmount(){
        return getPrice()*getMedicineQuantity().totalQuantity();
    }




    @Override
    public Object clone() throws CloneNotSupportedException {
        Medicine cloned = (Medicine)super.clone();
        cloned.setMedicineQuantity((MedicineQuantity)cloned.getMedicineQuantity().clone());
        return cloned;
    }

    public boolean isSos() {
        return sos;
    }

    public void setSos(boolean sos) {
        this.sos = sos;
    }



    public String getGoFrugalItemId() {
        return goFrugalItemId;
    }

    public void setGoFrugalItemId(String goFrugalItemId) {
        this.goFrugalItemId = goFrugalItemId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(route);
        dest.writeDouble(price);
        dest.writeInt(id);
        dest.writeStringList(composition);
        dest.writeString(packing);
        dest.writeParcelable(medicineQuantity, flags);
        dest.writeString(reference);
        dest.writeString(genericName);
        dest.writeString(company);
        dest.writeString(note);
        dest.writeByte((byte) (sos ? 1 : 0));
        dest.writeString(goFrugalItemId);
    }
}

