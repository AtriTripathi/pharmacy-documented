package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by anubhav on 03/12/17.
 */

public class InvoiceDataFormatted implements Parcelable {

    private int consultation_fee;
    private ArrayList<Medicine> mListOfMedicine;
    private ArrayList<String> mListOfTreatments;
    private ArrayList<String> mListOfProcedures;
    private ArrayList<Vaccination> mVaccinationList;
    private Medicine medicine;
    Patient patient;
    private int viewType = -1;
    private Vaccination vaccination;
    private int doctorId;
    private int regId;

    private boolean heading;
    private boolean isDoseCalculatorPresent;
    private String header;

    //Constructor for billing detail page
    public InvoiceDataFormatted(int consultation_fee, ArrayList<Medicine> mListOfMedicine, ArrayList<Vaccination> mVaccinationList,
                                ArrayList<String> mListOfTreatments, ArrayList<String> mListOfProcedures) {
        this.mListOfMedicine = mListOfMedicine;
        this.mListOfTreatments = mListOfTreatments;
        this.mListOfProcedures = mListOfProcedures;
        this.mVaccinationList = mVaccinationList;
        this.consultation_fee = consultation_fee;
    }

    public InvoiceDataFormatted(int consultation_fee, Patient patient , ArrayList<Medicine> mListOfMedicine, ArrayList<Vaccination> mVaccinationList,
                                ArrayList<String> mListOfTreatments, ArrayList<String> mListOfProcedures) {
        this.mListOfMedicine = mListOfMedicine;
        this.mListOfTreatments = mListOfTreatments;
        this.mListOfProcedures = mListOfProcedures;
        this.mVaccinationList = mVaccinationList;
        this.consultation_fee = consultation_fee;
        this.patient = patient;
    }

    public InvoiceDataFormatted(int consultation_fee, int doctorId, int regId, Patient patient , ArrayList<Medicine> mListOfMedicine, ArrayList<Vaccination> mVaccinationList,
                                ArrayList<String> mListOfTreatments, ArrayList<String> mListOfProcedures) {
        this.mListOfMedicine = mListOfMedicine;
        this.mListOfTreatments = mListOfTreatments;
        this.mListOfProcedures = mListOfProcedures;
        this.mVaccinationList = mVaccinationList;
        this.consultation_fee = consultation_fee;
        this.patient = patient;
        this.doctorId = doctorId;
        this.regId = regId;
    }

    protected InvoiceDataFormatted(Parcel in) {
        consultation_fee = in.readInt();
        mListOfMedicine = in.createTypedArrayList(Medicine.CREATOR);
        mListOfTreatments = in.createStringArrayList();
        mListOfProcedures = in.createStringArrayList();
        mVaccinationList = in.createTypedArrayList(Vaccination.CREATOR);
        medicine = in.readParcelable(Medicine.class.getClassLoader());
        patient = in.readParcelable(Patient.class.getClassLoader());
        viewType = in.readInt();
        vaccination = in.readParcelable(Vaccination.class.getClassLoader());
        heading = in.readByte() != 0;
        isDoseCalculatorPresent = in.readByte() != 0;
        header = in.readString();
        doctorId = in.readInt();
        regId = in.readInt();

    }

    public static final Creator<InvoiceDataFormatted> CREATOR = new Creator<InvoiceDataFormatted>() {
        @Override
        public InvoiceDataFormatted createFromParcel(Parcel in) {
            return new InvoiceDataFormatted(in);
        }

        @Override
        public InvoiceDataFormatted[] newArray(int size) {
            return new InvoiceDataFormatted[size];
        }
    };

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public Vaccination getVaccination() {
        return vaccination;
    }

    public void setVaccination(Vaccination vaccination) {
        this.vaccination = vaccination;
    }

    public boolean isDoseCalculatorPresent() {
        return isDoseCalculatorPresent;
    }

    public InvoiceDataFormatted(Medicine medicine, int viewType){
        this.medicine = medicine;
        this.viewType = viewType;

    }

    public String getHeader() {
        return header;
    }

    public InvoiceDataFormatted(int viewType, String header, boolean heading) {
        this.viewType = viewType;
        this.header = header;

        this.heading = heading;
    }

    public InvoiceDataFormatted(int viewType){this.viewType = viewType;}

    public InvoiceDataFormatted(Vaccination vaccination, int viewType) {
        this.viewType = viewType;
        this.vaccination = vaccination;
    }


    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getConsultation_fee() {
        return consultation_fee;
    }

    public void setConsultation_fee(int consultation_fee) {
        this.consultation_fee = consultation_fee;
    }

    public ArrayList<Vaccination> getVaccinationList() {
        return mVaccinationList;
    }

    public void setVaccinationList(ArrayList<Vaccination> mVaccinationList) {
        this.mVaccinationList = mVaccinationList;
    }

    public ArrayList<Medicine> getListOfMedicine() {
        return mListOfMedicine;
    }

    public void setListOfMedicine(ArrayList<Medicine> mListOfMedicine) {
        this.mListOfMedicine = mListOfMedicine;
    }

    public ArrayList<String> getListOfTreatments() {
        return mListOfTreatments;
    }

    public void setListOfTreatments(ArrayList<String> mListOfTreatments) {
        this.mListOfTreatments = mListOfTreatments;
    }

    public ArrayList<String> getListOfProcedures() {
        return mListOfProcedures;
    }

    public void setListOfProcedures(ArrayList<String> mListOfProcedures) {
        this.mListOfProcedures = mListOfProcedures;
    }


    public void setDoseCalculatorPresent(boolean doseCalculatorPresent) {
        isDoseCalculatorPresent = doseCalculatorPresent;
    }

    public void setHeader(String header){
        this.header = header;
    }

    public Patient getPatient() {
        return patient;
    }

    public int getRegId() {
        return regId;
    }

    public void setRegId(int regId) {
        this.regId = regId;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(consultation_fee);
        parcel.writeTypedList(mListOfMedicine);
        parcel.writeStringList(mListOfTreatments);
        parcel.writeStringList(mListOfProcedures);
        parcel.writeTypedList(mVaccinationList);
        parcel.writeParcelable(medicine, i);
        parcel.writeParcelable(patient, i);
        parcel.writeInt(viewType);
        parcel.writeParcelable(vaccination, i);
        parcel.writeByte((byte) (heading ? 1 : 0));
        parcel.writeByte((byte) (isDoseCalculatorPresent ? 1 : 0));
        parcel.writeString(header);
        parcel.writeInt(doctorId);
        parcel.writeInt(regId);

    }
}
