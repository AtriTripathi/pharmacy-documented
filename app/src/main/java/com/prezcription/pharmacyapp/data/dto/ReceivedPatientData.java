package com.prezcription.pharmacyapp.data.dto;

/**
 * Created by anubhav on 16/02/17.
 */

public class ReceivedPatientData {

    public static final int TYPE_HOSPITAL_SYNC = 1;
    public static final int TYPE_PHARMACY_SYNC = 2;

    int patientId;
    int regId;
    boolean sync;
    int syncType;

    public ReceivedPatientData() {
    }

    public ReceivedPatientData(int patientId, int regId, int syncType) {
        this.patientId = patientId;
        this.regId = regId;
        this.syncType = syncType;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getRegId() {
        return regId;
    }

    public void setRegId(int regId) {
        this.regId = regId;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public int getSyncType() {
        return syncType;
    }

    public void setSyncType(int syncType) {
        this.syncType = syncType;
    }
}
