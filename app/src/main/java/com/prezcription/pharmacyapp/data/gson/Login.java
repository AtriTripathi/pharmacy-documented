package com.prezcription.pharmacyapp.data.gson;

import java.util.List;

/**
 * Created by anubhav on 05/10/16.
 */
public class Login {


    /**
     * PHONE_NUMBER : 9886565435
     * NAME : Laxmi Diagnostic center
     * HOSPITAL_ID : 8
     * CREATED_AT : 2017-02-16T13:34:16.547Z
     * DOCTORS : [{"PHONE_NUMBER":"9886565435","NAME":"Anubhav ","PHOTO":null,"AGE":32,"SEX":"M","QUALIFICATION":"MBBS,MD","DOCTOR_ID":1,"EMAIL":"aman@prezcription.com","REGISTRATION_NUMBER":"12345","SPECIALIZATION":"General physician"},{"PHONE_NUMBER":"8953446804","NAME":"Aman","PHOTO":null,"AGE":23,"SEX":"M","QUALIFICATION":"MBBS, MD","DOCTOR_ID":8,"EMAIL":"aman@preacription.com","REGISTRATION_NUMBER":"45678","SPECIALIZATION":"General physician"},{"PHONE_NUMBER":"8586845855","NAME":"sonalk","PHOTO":null,"AGE":23,"SEX":"F","QUALIFICATION":"mbbs ","DOCTOR_ID":132,"EMAIL":"shonaliv750@gmail.com","REGISTRATION_NUMBER":"8181","SPECIALIZATION":"ent"},{"PHONE_NUMBER":"9972352265","NAME":"Vinay N","PHOTO":null,"AGE":32,"SEX":"M","QUALIFICATION":"MBBS MD","DOCTOR_ID":11,"EMAIL":"vinaynbmc@gmail.com","REGISTRATION_NUMBER":"80743","SPECIALIZATION":"Consultant Physician &  Diabetologist"},{"PHONE_NUMBER":"9901100009","NAME":"Seema Misra","PHOTO":null,"AGE":45,"SEX":"F","QUALIFICATION":"MBBS,DNB","DOCTOR_ID":133,"EMAIL":"drseemamisra@gmail.com","REGISTRATION_NUMBER":"100935","SPECIALIZATION":"Pediatrician "}]
     * UPDATED_AT : 2017-02-16T13:34:16.565Z
     * ADDRESS : [{"CITY":"asd","PIN":12345,"HOSPITAL_ID":8,"STATE":"n/a","STREET":"n/a","ADDRESS_NAME":"n/a","COUNTRY":"n/a","LINE_1":"asdasd","LINE_2":"n/a"}]
     * REFERRAL_CODE : n/a
     * EMAIL : anubhav@prezcription.com
     */

    private String PHONE_NUMBER;
    private String NAME;
    private int HOSPITAL_ID;
    private int PATIENT_COUNTER;
    private String CREATED_AT;
    private String UPDATED_AT;
    private String REFERRAL_CODE;
    private String EMAIL;
    private List<DOCTORSBean> DOCTORS;
    private List<ADDRESSBean> ADDRESS;


    public String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }

    public void setPHONE_NUMBER(String PHONE_NUMBER) {
        this.PHONE_NUMBER = PHONE_NUMBER;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public int getHOSPITAL_ID() {
        return HOSPITAL_ID;
    }

    public void setHOSPITAL_ID(int HOSPITAL_ID) {
        this.HOSPITAL_ID = HOSPITAL_ID;
    }

    public String getCREATED_AT() {
        return CREATED_AT;
    }

    public void setCREATED_AT(String CREATED_AT) {
        this.CREATED_AT = CREATED_AT;
    }

    public String getUPDATED_AT() {
        return UPDATED_AT;
    }

    public void setUPDATED_AT(String UPDATED_AT) {
        this.UPDATED_AT = UPDATED_AT;
    }

    public String getREFERRAL_CODE() {
        return REFERRAL_CODE;
    }

    public void setREFERRAL_CODE(String REFERRAL_CODE) {
        this.REFERRAL_CODE = REFERRAL_CODE;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public List<DOCTORSBean> getDOCTORS() {
        return DOCTORS;
    }

    public void setDOCTORS(List<DOCTORSBean> DOCTORS) {
        this.DOCTORS = DOCTORS;
    }

    public List<ADDRESSBean> getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(List<ADDRESSBean> ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public int getPATIENT_COUNTER() {
        return PATIENT_COUNTER;
    }

    public void setPATIENT_COUNTER(int PATIENT_COUNTER) {
        this.PATIENT_COUNTER = PATIENT_COUNTER;
    }

    public static class DOCTORSBean {
        /**
         * PHONE_NUMBER : 9886565435
         * NAME : Anubhav
         * PHOTO : null
         * AGE : 32
         * SEX : M
         * QUALIFICATION : MBBS,MD
         * DOCTOR_ID : 1
         * EMAIL : aman@prezcription.com
         * REGISTRATION_NUMBER : 12345
         * SPECIALIZATION : General physician
         */

        private String PHONE_NUMBER;
        private String NAME;
        private String PHOTO;
        private int AGE;
        private String SEX;
        private String QUALIFICATION;
        private int DOCTOR_ID;
        private String EMAIL;
        private String REGISTRATION_NUMBER;
        private String SPECIALIZATION;

        public String getPHONE_NUMBER() {
            return PHONE_NUMBER;
        }

        public void setPHONE_NUMBER(String PHONE_NUMBER) {
            this.PHONE_NUMBER = PHONE_NUMBER;
        }

        public String getNAME() {
            return NAME;
        }

        public void setNAME(String NAME) {
            this.NAME = NAME;
        }

        public String getPHOTO() {
            return PHOTO;
        }

        public void setPHOTO(String PHOTO) {
            this.PHOTO = PHOTO;
        }

        public int getAGE() {
            return AGE;
        }

        public void setAGE(int AGE) {
            this.AGE = AGE;
        }

        public String getSEX() {
            return SEX;
        }

        public void setSEX(String SEX) {
            this.SEX = SEX;
        }

        public String getQUALIFICATION() {
            return QUALIFICATION;
        }

        public void setQUALIFICATION(String QUALIFICATION) {
            this.QUALIFICATION = QUALIFICATION;
        }

        public int getDOCTOR_ID() {
            return DOCTOR_ID;
        }

        public void setDOCTOR_ID(int DOCTOR_ID) {
            this.DOCTOR_ID = DOCTOR_ID;
        }

        public String getEMAIL() {
            return EMAIL;
        }

        public void setEMAIL(String EMAIL) {
            this.EMAIL = EMAIL;
        }

        public String getREGISTRATION_NUMBER() {
            return REGISTRATION_NUMBER;
        }

        public void setREGISTRATION_NUMBER(String REGISTRATION_NUMBER) {
            this.REGISTRATION_NUMBER = REGISTRATION_NUMBER;
        }

        public String getSPECIALIZATION() {
            return SPECIALIZATION;
        }

        public void setSPECIALIZATION(String SPECIALIZATION) {
            this.SPECIALIZATION = SPECIALIZATION;
        }
    }

    public static class ADDRESSBean {
        /**
         * CITY : asd
         * PIN : 12345
         * HOSPITAL_ID : 8
         * STATE : n/a
         * STREET : n/a
         * ADDRESS_NAME : n/a
         * COUNTRY : n/a
         * LINE_1 : asdasd
         * LINE_2 : n/a
         */

        private String CITY;
        private int PIN;
        private int HOSPITAL_ID;
        private String STATE;
        private String STREET;
        private String ADDRESS_NAME;
        private String COUNTRY;
        private String LINE_1;
        private String LINE_2;

        public String getCITY() {
            return CITY;
        }

        public void setCITY(String CITY) {
            this.CITY = CITY;
        }

        public int getPIN() {
            return PIN;
        }

        public void setPIN(int PIN) {
            this.PIN = PIN;
        }

        public int getHOSPITAL_ID() {
            return HOSPITAL_ID;
        }

        public void setHOSPITAL_ID(int HOSPITAL_ID) {
            this.HOSPITAL_ID = HOSPITAL_ID;
        }

        public String getSTATE() {
            return STATE;
        }

        public void setSTATE(String STATE) {
            this.STATE = STATE;
        }

        public String getSTREET() {
            return STREET;
        }

        public void setSTREET(String STREET) {
            this.STREET = STREET;
        }

        public String getADDRESS_NAME() {
            return ADDRESS_NAME;
        }

        public void setADDRESS_NAME(String ADDRESS_NAME) {
            this.ADDRESS_NAME = ADDRESS_NAME;
        }

        public String getCOUNTRY() {
            return COUNTRY;
        }

        public void setCOUNTRY(String COUNTRY) {
            this.COUNTRY = COUNTRY;
        }

        public String getLINE_1() {
            return LINE_1;
        }

        public void setLINE_1(String LINE_1) {
            this.LINE_1 = LINE_1;
        }

        public String getLINE_2() {
            return LINE_2;
        }

        public void setLINE_2(String LINE_2) {
            this.LINE_2 = LINE_2;
        }
    }
}
