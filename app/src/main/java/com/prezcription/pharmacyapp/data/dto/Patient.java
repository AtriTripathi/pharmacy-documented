package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by anubhav on 24/09/16.
 */
public class Patient implements Parcelable {

    public static final String MALE = "Male";
    public static final String FEMALE = "Female";

    private String name;
    private int profileId;
    private int assignedDoctorId;
    private String relationship;
    private String photoUrl;
    private String phoneNumber;
    private int webId;
    private String gender;
    private float age;
    private String ageType;
    private String dob;
    private Vitals vitals;
    private String fatherName, motherName;
    private String recentDate;
    private ArrayList<String> allergyList;
    private String patientEmail;
    private String nextVaccineDueDate;
    private String pinCode, area;
    private String username;

    public Patient() {

    }

    public Patient(String name, String username, String gender, float age, String ageType, String phoneNumber, String relationship, int profileId, int webId, String photoUrl) {
        this.webId = webId;
        this.relationship = relationship;
        this.profileId = profileId;
        this.photoUrl = photoUrl;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.username = username;
        this.gender = gender;
        this.age = age;
        this.ageType = ageType;
    }

    public Patient(String name, String username, String gender, float age, String ageType, String phoneNumber, String relationship, int webId, String photoUrl) {
        this.webId = webId;
        this.relationship = relationship;
        this.photoUrl = photoUrl;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.username = username;
        this.gender = gender;
        this.age = age;
        this.ageType = ageType;
    }



    protected Patient(Parcel in) {
        name = in.readString();
        profileId = in.readInt();
        assignedDoctorId = in.readInt();
        relationship = in.readString();
        photoUrl = in.readString();
        phoneNumber = in.readString();
        webId = in.readInt();
        gender = in.readString();
        age = in.readFloat();
        ageType = in.readString();
        dob = in.readString();
        vitals = in.readParcelable(Vitals.class.getClassLoader());
        fatherName = in.readString();
        motherName = in.readString();
        recentDate = in.readString();
        allergyList = in.createStringArrayList();
        patientEmail = in.readString();
        nextVaccineDueDate = in.readString();
        username = in.readString();
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel in) {
            return new Patient(in);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };

    public int getAssignedDoctorId() {
        return assignedDoctorId;
    }

    public void setAssignedDoctorId(int assignedDoctorId) {
        this.assignedDoctorId = assignedDoctorId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public float getAge() {
        return age;
    }

    public void setAge(float age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public Vitals getVitals() {
        return vitals;
    }

    public void setVitals(Vitals vitals) {
        this.vitals = vitals;
    }

    public int getWebId() {
        return webId;
    }

    public void setWebId(int webId) {
        this.webId = webId;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

    public ArrayList<String> getAllergyList() {
        return allergyList;
    }

    public void setAllergyList(ArrayList<String> allergyList) {
        this.allergyList = allergyList;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }


    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getRecentDate() {
        return recentDate;
    }

    public void setRecentDate(String recentDate) {
        this.recentDate = recentDate;
    }

    public String getPatientEmail() {
        return patientEmail;
    }

    public void setPatientEmail(String patientEmail) {
        this.patientEmail = patientEmail;
    }

    public String getNextVaccineDueDate() {
        return nextVaccineDueDate;
    }

    public void setNextVaccineDueDate(String nextVaccineDueDate) {
        this.nextVaccineDueDate = nextVaccineDueDate;
    }

//    @Override
//    public String toString(){
//        Gson gson = new Gson();
//        return gson.toJson(this);
//
//    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(profileId);
        dest.writeInt(assignedDoctorId);
        dest.writeString(relationship);
        dest.writeString(photoUrl);
        dest.writeString(phoneNumber);
        dest.writeInt(webId);
        dest.writeString(gender);
        dest.writeFloat(age);
        dest.writeString(ageType);
        dest.writeString(dob);
        dest.writeParcelable(vitals, flags);
        dest.writeString(fatherName);
        dest.writeString(motherName);
        dest.writeString(recentDate);
        dest.writeStringList(allergyList);
        dest.writeString(patientEmail);
        dest.writeString(nextVaccineDueDate);
        dest.writeString(username);
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}

