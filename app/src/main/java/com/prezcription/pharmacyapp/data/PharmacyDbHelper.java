package com.prezcription.pharmacyapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PharmacyDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 4;

    public static final String DATABASE_NAME = "pharmacy.db";

    private static final String LOG_TAG = PharmacyDbHelper.class.getSimpleName();

    public PharmacyDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BILLS_ENTRY_TABLE = "CREATE TABLE " + PharmacyContract.BillsEntry.TABLE_NAME + " (" +

                PharmacyContract.BillsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                PharmacyContract.BillsEntry.COLUMN_PATIENT_REG_ID + " TEXT NOT NULL, " +

                PharmacyContract.BillsEntry.COLUMN_DOC_ID + " TEXT NOT NULL, " +

                PharmacyContract.BillsEntry.COLUMN_PATIENT_USER_NAME + " TEXT NOT NULL, " +

                PharmacyContract.BillsEntry.COLUMN_BILL_NUMBER + " TEXT," +

                PharmacyContract.BillsEntry.COLUMN_GLOBAL_BILL_ID + " TEXT, " +

//                PharmacyContract.BillsEntry.COLUMN_PRESCRIPTION_ID + " TEXT NOT NULL," +

                PharmacyContract.BillsEntry.COLUMN_STATE_GST + " TEXT," +

                PharmacyContract.BillsEntry.COLUMN_CENTER_GST + " TEXT," +

                //change not null to text
                PharmacyContract.BillsEntry.COLUMN_BILL_DATE + " TEXT," +

                PharmacyContract.BillsEntry.COLUMN_UPDATED_DATE + " TEXT NOT NULL," +

                PharmacyContract.BillsEntry.COLUMN_CONSULTATION_FEE + " TEXT," +

                PharmacyContract.BillsEntry.COLUMN_MEDICINE_DISCOUNT + " TEXT," +

                PharmacyContract.BillsEntry.COLUMN_VACCINE_DISCOUNT + " TEXT," +

                PharmacyContract.BillsEntry.COLUMN_TOTAL_DISCOUNT_GIVEN + " TEXT," +

                PharmacyContract.BillsEntry.COLUMN_PAID_AMOUNT + " REAL NOT NULL," +

                PharmacyContract.BillsEntry.COLUMN_BILL_CREATED + " INTEGER NOT NULL DEFAULT 0 ," +

                "FOREIGN KEY (" + PharmacyContract.BillsEntry.COLUMN_PATIENT_REG_ID + ") REFERENCES " +
                PharmacyContract.PatientRegistrationEntry.TABLE_NAME + "(" + PharmacyContract.PatientRegistrationEntry._ID + ")" + " );";

        final String SQL_CREATE_PATIENT_REG_ENTRY_TABLE = "CREATE TABLE " + PharmacyContract.PatientRegistrationEntry.TABLE_NAME + " (" +

                PharmacyContract.PatientRegistrationEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID + " TEXT NOT NULL, " +

                PharmacyContract.PatientRegistrationEntry.COLUMN_DOCTOR_ID + " TEXT," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_DATE + " TEXT NOT NULL," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_TOKEN + " TEXT," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_SYNC_FLAG + " TEXT," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_PRESCRIPTION_TIMESTAMP + " INTEGER," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_VISIT_TYPE + " TEXT NOT NULL ," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_RE_REGISTER_FLAG + " INTEGER," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_PRESCRIPTION_FLAG + " INTEGER  DEFAULT 0," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_SERVER_SYNC_FLAG + " INTEGER  DEFAULT 0," +

                PharmacyContract.PatientRegistrationEntry.COLUMN_UPDATE_FLAG + " INTEGER," +

                "FOREIGN KEY (" +  PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID + ") REFERENCES " + PharmacyContract.PatientEntry.TABLE_NAME + "(" + PharmacyContract.PatientEntry._ID + ")" +

                " );";

        final String SQL_CREATE_PATIENT_TABLE = "CREATE TABLE " + PharmacyContract.PatientEntry.TABLE_NAME + " (" +
                PharmacyContract.PatientEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME + " TEXT NOT NULL, " +

                PharmacyContract.PatientEntry.COLUMN_MOTHERS_NAME + " TEXT , " +

                PharmacyContract.PatientEntry.COLUMN_FATHERS_NAME + " TEXT , " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE + " REAL NOT NULL," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE_TYPE + " TEXT NOT NULL," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_AREA + " TEXT, " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_PIN_CODE + " TEXT, " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_DOB + " TEXT ," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME + " TEXT ," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_IMAGE + " TEXT," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_IMAGE_LOCAL + " TEXT," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_EMAIL + " TEXT, " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE + "  TEXT NOT NULL," +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_SEX + " TEXT NOT NULL, " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_ADDRESS + " TEXT, " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_RECENT + " TEXT, " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_RELATIONSHIP + " TEXT , " +

                PharmacyContract.PatientEntry.COLUMN_PATIENT_WEB_ID + " INTEGER " +
                " );";
        final String SQL_CREATE_DOCTOR_TABLE = "CREATE TABLE " + PharmacyContract.DoctorEntry.TABLE_NAME + " (" +
                PharmacyContract.DoctorEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_NAME + " TEXT NOT NULL, " +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_AGE + " INTEGER NOT NULL," +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_IMAGE + " TEXT," +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_IMAGE_LOCAL + " TEXT," +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_PHONE + "  TEXT NOT NULL," +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_EMAIL + "  TEXT ," +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_SEX + " TEXT NOT NULL, " +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_QUALIFICATION + " TEXT , " +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_SPECIALIZATION + " TEXT , " +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_CREATED_AT + "  TEXT ," +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_REGISTRATION_NUMBER + " TEXT NOT NULL, " +

                PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID + " INTEGER " +
                " );";

        final String SQL_CREATE_MEDICINE_BILLS_ENTRY_TABLE = "CREATE TABLE " + PharmacyContract.MedicineInBillsEntry.TABLE_NAME + " (" +

                PharmacyContract.MedicineInBillsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID + " TEXT ," +

                //can be removed

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_NAME + " TEXT NOT NULL," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY + " REAL," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_BATCH_ID + " TEXT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_BAR_CODE + " TEXT," +


                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MNF + " TEXT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_EXPIRY + " TEXT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MRP + " TEXT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_CGST + " TEXT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_SGST + " TEXT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AMOUNT + " TEXT," +


                //can be fetched from medicine table
                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_PRICE + " REAL," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ITEM_GROSS + " REAL," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID + " TEXT NOT NULL," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_GLOBAL_BILL_ID + " TEXT ," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_DISCOUNT + " TEXT," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AVAILABLE_QUANTITY + " REAL DEFAULT -1," +

                PharmacyContract.MedicineInBillsEntry.COLUMN_ITEM_BILLED + " INTEGER NOT NULL DEFAULT 1 ," +

                "FOREIGN KEY (" + PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID + ") REFERENCES " +
                PharmacyContract.BillsEntry.TABLE_NAME + "(" + PharmacyContract.BillsEntry._ID + ")" + " );";

        final String SQL_CREATE_VACCINE_BILLS_ENTRY_TABLE = "CREATE TABLE " + PharmacyContract.VaccineInBillsEntry.TABLE_NAME + " (" +

                PharmacyContract.VaccineInBillsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ID + " TEXT ," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_NAME + " TEXT NOT NULL," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_QUANTITY + " REAL," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_BATCH_ID + " TEXT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_BAR_CODE + " TEXT," +


                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_MNF + " TEXT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_EXPIRY + " TEXT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_MRP + " TEXT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_CGST + " TEXT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_SGST + " TEXT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_AMOUNT + " TEXT," +

                //can be fetched from medicine table
                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_PRICE + " REAL," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ITEM_GROSS + " REAL," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_GLOBAL_BILL_ID + " TEXT ," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_BILL_ID + " TEXT NOT NULL," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_DISCOUNT + " TEXT," +

                PharmacyContract.VaccineInBillsEntry.COLUMN_ITEM_BILLED + " INTEGER NOT NULL DEFAULT 1 ," +

                "FOREIGN KEY (" + PharmacyContract.VaccineInBillsEntry.COLUMN_BILL_ID + ") REFERENCES " +
                PharmacyContract.BillsEntry.TABLE_NAME + "(" + PharmacyContract.BillsEntry._ID + ")" + " );";



        final String SQL_CREATE_PATIENT_INDEX = "CREATE UNIQUE INDEX " + PharmacyContract.PatientEntry.INDEX_PATIENT_USERNAME + " ON "
                + PharmacyContract.PatientEntry.TABLE_NAME + " (" + PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME + ")";



        db.execSQL(SQL_CREATE_DOCTOR_TABLE);
        db.execSQL(SQL_CREATE_BILLS_ENTRY_TABLE);
        db.execSQL(SQL_CREATE_PATIENT_REG_ENTRY_TABLE);
        db.execSQL(SQL_CREATE_PATIENT_TABLE);
        db.execSQL(SQL_CREATE_MEDICINE_BILLS_ENTRY_TABLE);
        db.execSQL(SQL_CREATE_VACCINE_BILLS_ENTRY_TABLE);
        db.execSQL(SQL_CREATE_PATIENT_INDEX);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion < 2){
            String addItemGrossInMedicineEntry = "ALTER TABLE " + PharmacyContract.MedicineInBillsEntry.TABLE_NAME + " ADD COLUMN "
                    + PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ITEM_GROSS + "  REAL ";
            String addItemGrossInVaccineEntry = "ALTER TABLE " + PharmacyContract.VaccineInBillsEntry.TABLE_NAME + " ADD COLUMN "
                    + PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ITEM_GROSS + "  REAL ";

            db.execSQL(addItemGrossInVaccineEntry);
            db.execSQL(addItemGrossInMedicineEntry);
        }

        if (oldVersion < 4){
            String addAvailableQuantityInMedicineBills = "ALTER TABLE " + PharmacyContract.MedicineInBillsEntry.TABLE_NAME + " ADD COLUMN "
                    + PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AVAILABLE_QUANTITY + "  REAL DEFAULT -1";

            db.execSQL(addAvailableQuantityInMedicineBills);

        }

    }
}
