package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anubhav on 19/09/16.
 */
public class RecyclerSearchableData implements Parcelable {

    public RecyclerSearchableData() {
    }

    public RecyclerSearchableData(String name, int viewType) {
        this.name = name;
        this.viewType = viewType;
    }

    private String name;
    private int viewType;

    protected RecyclerSearchableData(Parcel in) {
        name = in.readString();
        viewType = in.readInt();
    }

    public static final Creator<RecyclerSearchableData> CREATOR = new Creator<RecyclerSearchableData>() {
        @Override
        public RecyclerSearchableData createFromParcel(Parcel in) {
            return new RecyclerSearchableData(in);
        }

        @Override
        public RecyclerSearchableData[] newArray(int size) {
            return new RecyclerSearchableData[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(viewType);
    }
}
