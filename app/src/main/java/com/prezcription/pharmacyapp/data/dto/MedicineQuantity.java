package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;



import java.util.Date;

/**
 * Created by anubhav on 03/12/17.
 */

public class MedicineQuantity implements Parcelable, Cloneable {

    public static final int NO_CALCULATOR=0;
    public static final int WEIGHT_BASED_DOSE_CALCULATOR=1;
    public static final int AGE_BASED_DOSE_CALCULATOR=2;

    private int refill;
    private float dose;
    private int frequency;
    private int duration;
    private String durationValuesType;
    private String doseValueType;
    private long startDate;
    private long finishDate;
    private int beforeMeal;
    private float morningDose;
    private float afternoonDose;
    private float eveningDose;
    private float nightDose;
    private boolean morning;
    private boolean afternoon;
    private boolean evening;
    private boolean night;
    private String otherFrequency;
    private boolean dosePresent;
    private DoseCalculator doseCalculator;
    private float quantity;
    private boolean isMultiDose;
    private AgeBasedDoseCalculator ageBasedDoseCalculator;
    private int doseCalculatorType;


    protected MedicineQuantity(Parcel in) {
        refill = in.readInt();
        dose = in.readFloat();
        frequency = in.readInt();
        duration = in.readInt();
        durationValuesType = in.readString();
        doseValueType = in.readString();
        startDate = in.readLong();
        finishDate = in.readLong();
        beforeMeal = in.readInt();
        morningDose = in.readFloat();
        afternoonDose = in.readFloat();
        eveningDose = in.readFloat();
        nightDose = in.readFloat();
        morning = in.readByte() != 0;
        afternoon = in.readByte() != 0;
        evening = in.readByte() != 0;
        night = in.readByte() != 0;
        otherFrequency = in.readString();
        dosePresent = in.readByte() != 0;
        doseCalculator = in.readParcelable(DoseCalculator.class.getClassLoader());
        quantity = in.readFloat();
        isMultiDose = in.readByte() != 0;
        ageBasedDoseCalculator = in.readParcelable(AgeBasedDoseCalculator.class.getClassLoader());
        doseCalculatorType = in.readInt();
    }

    public static final Creator<MedicineQuantity> CREATOR = new Creator<MedicineQuantity>() {
        @Override
        public MedicineQuantity createFromParcel(Parcel in) {
            return new MedicineQuantity(in);
        }

        @Override
        public MedicineQuantity[] newArray(int size) {
            return new MedicineQuantity[size];
        }
    };

    public void setDose(float dose) {
        this.dose = dose;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public MedicineQuantity() {
        setBeforeMeal(-1);
    }

    public MedicineQuantity(int dose, int frequency, int duration) {
        this.dose = dose;
        this.frequency = frequency;
        this.duration = duration;
        setBeforeMeal(-1);
    }


    public MedicineQuantity(float dose, String doseValueType, int frequency, int duration, String durationValuesType, int refill, boolean morning, boolean afternoon, boolean evening, boolean night, int beforeMeal, boolean dosePresent) {
        this.refill = refill;
        this.dose = dose;
        this.frequency = frequency;
        this.duration = duration;
        this.durationValuesType = durationValuesType;
        this.morning = morning;
        this.afternoon = afternoon;
        this.evening = evening;
        this.night = night;
        this.beforeMeal = beforeMeal;
        this.doseValueType = doseValueType;
        this.dosePresent = dosePresent;

    }

    public MedicineQuantity(float dose, String doseValueType, int frequency, String otherFrequency , int duration, String durationValuesType, int refill, boolean morning, boolean afternoon, boolean evening, boolean night, int beforeMeal, boolean dosePresent) {
        this.refill = refill;
        this.dose = dose;
        this.frequency = frequency;
        this.duration = duration;
        this.durationValuesType = durationValuesType;
        this.morning = morning;
        this.afternoon = afternoon;
        this.evening = evening;
        this.night = night;
        this.beforeMeal = beforeMeal;
        this.doseValueType = doseValueType;
        this.otherFrequency = otherFrequency;
        this.dosePresent = dosePresent;
    }


    public MedicineQuantity(float dose, String doseValueType, int frequency, String otherFrequency , int duration, String durationValuesType, int refill, boolean morning, float morningDose, boolean afternoon, float afternoonDose, boolean evening, float eveningDose, boolean night, float nightDose, int beforeMeal, boolean dosePresent) {
        this.refill = refill;
        this.dose = dose;
        this.frequency = frequency;
        this.duration = duration;
        this.durationValuesType = durationValuesType;
        this.morning = morning;
        this.afternoon = afternoon;
        this.evening = evening;
        this.night = night;
        this.morningDose = morningDose;
        this.afternoonDose = afternoonDose;
        this.eveningDose = eveningDose;
        this.nightDose = nightDose;
        this.beforeMeal = beforeMeal;
        this.doseValueType = doseValueType;
        this.otherFrequency = otherFrequency;
        this.dosePresent = dosePresent;
    }


    public String getDoseValueType() {
        return doseValueType;
    }

    public void setDoseValueType(String doseValueType) {
        this.doseValueType = doseValueType;
    }

    public boolean isMorning() {
        return morning;
    }

    public void setMorning(boolean morning) {
        this.morning = morning;
    }

    public boolean isAfternoon() {
        return afternoon;
    }

    public void setAfternoon(boolean afternoon) {
        this.afternoon = afternoon;
    }

    public boolean isEvening() {
        return evening;
    }

    public void setEvening(boolean evening) {
        this.evening = evening;
    }

    public boolean isNight() {
        return night;
    }

    public void setNight(boolean night) {
        this.night = night;
    }

    public String getOtherFrequency() {
        return otherFrequency;
    }

    public void setOtherFrequency(String otherFrequency) {
        this.otherFrequency = otherFrequency;
    }

    public String getDurationValuesType() {
        return durationValuesType;
    }

    public void setDurationValuesType(String durationValuesType) {
        this.durationValuesType = durationValuesType;
    }

    public int getDoseCalculatorType() {
        return doseCalculatorType;
    }

    public void setDoseCalculatorType(int doseCalculatorType) {
        this.doseCalculatorType = doseCalculatorType;
    }

    public int getBeforeMeal() {
        return beforeMeal;
    }

    public void setBeforeMeal(int beforeMeal) {
        this.beforeMeal = beforeMeal;
    }

    public int getRefill() {
        return refill;
    }

    public void setRefill(int refill) {
        this.refill = refill;
    }

    public float getDose() {
        return dose;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getDuration() {
        return duration;
    }

    public float getEveningDose() {
        return eveningDose;
    }

    public void setEveningDose(float eveningDose) {
        this.eveningDose = eveningDose;
    }

    public float getAfternoonDose() {
        return afternoonDose;
    }

    public void setAfternoonDose(float afternoonDose) {
        this.afternoonDose = afternoonDose;
    }

    public float getMorningDose() {
        return morningDose;
    }

    public void setMorningDose(float morningDose) {
        this.morningDose = morningDose;
    }

    public float getNightDose() {
        return nightDose;
    }

    public void setNightDose(float nightDose) {
        this.nightDose = nightDose;
    }

    public boolean isDosePresent() {
        return dosePresent;
    }

    public void setDosePresent(boolean dosePresent) {
        this.dosePresent = dosePresent;
    }

    public DoseCalculator getDoseCalculator() {
        return doseCalculator;
    }

    public void setDoseCalculator(DoseCalculator doseCalculator) {
        this.doseCalculator = doseCalculator;
    }

    public AgeBasedDoseCalculator getAgeBasedDoseCalculator() {
        return ageBasedDoseCalculator;
    }

    public void setAgeBasedDoseCalculator(AgeBasedDoseCalculator ageBasedDoseCalculator) {
        this.ageBasedDoseCalculator = ageBasedDoseCalculator;
    }

    //TODO: add quantity when doses are different
    public float totalQuantity(){

        float quantity;

        boolean multiDose = false;

        float morningQuantity = 0f;
        float afternoonQuantity = 0f;
        float eveningQuantity = 0f;
        float nightQuantity = 0f;

        if((morning&&morningDose!=0f)){
            morningQuantity = ((float)duration)*morningDose;
            multiDose = true;
        }
        if((afternoon&&afternoonDose!=0f)){
            afternoonQuantity = ((float)duration)*afternoonDose;
            multiDose = true;
        }
        if((evening&&eveningDose!=0f)){
            eveningQuantity = ((float)duration)*eveningDose;
            multiDose = true;
        }
        if((night&&nightDose!=0f)){
            nightQuantity = ((float)duration)*nightDose;
            multiDose = true;
        }

        if(multiDose){
            quantity = morningQuantity+afternoonQuantity+eveningQuantity+nightQuantity;
        }else {
            quantity = (dose*(float)frequency*(float)duration);
        }

//        Log.d(Utility.LOG_TAG,"quant is "+ quantity);

        float quantityWithoutFrequency = (dose*(float)duration);

        if(durationValuesType.toLowerCase().equals("months")||durationValuesType.toLowerCase().equals("month")){
            quantity = quantity*(float)30;
            quantityWithoutFrequency = quantityWithoutFrequency*(float)30;
        }else if(durationValuesType.toLowerCase().equals("years")||durationValuesType.toLowerCase().equals("year")){
            quantity = quantity*(float)365;
            quantityWithoutFrequency = quantityWithoutFrequency*(float)365;
        }else if(durationValuesType.toLowerCase().equals("weeks")||durationValuesType.toLowerCase().equals("week")){
            quantity = quantity*(float)7;
            quantityWithoutFrequency = quantityWithoutFrequency*(float)7;
        }

        if(otherFrequency==null){
            return quantity;
        }else if(otherFrequency.equals("daily")){
            return quantity;
        }else if(otherFrequency.equals("weekly")||otherFrequency.equals("every week")){
            return quantity/7f;
        }else if(otherFrequency.equals("monthly")||otherFrequency.equals("every month")){
            return quantity/30f;
        }else if(otherFrequency.equals("yearly")){
            return quantity/365f;
        }else if(otherFrequency.equals("once a day")&&frequency==0){
            return quantityWithoutFrequency;
        }else if(otherFrequency.equals("twice a day")&&frequency==0){
            return 2f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("once a week")&&frequency==0){
            return quantityWithoutFrequency/7f;
        }else if(otherFrequency.equals("twice a week")&&frequency==0){
            return 2f*(quantityWithoutFrequency/7f);
        }else if(otherFrequency.equals("thrice a week")&&frequency==0){
            return 3f*(quantityWithoutFrequency/7f);
        }else if(otherFrequency.equals("thrice a week")&&frequency==0){
            return 3f*(quantityWithoutFrequency/7f);
        }else if(otherFrequency.equals("every 2 hours")&&frequency==0){
            return 12f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("every 3 hours")&&frequency==0){
            return 8f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("every 4 hours")&&frequency==0){
            return 6f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("every 5 hours")&&frequency==0){
            return 5f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("every 6 hours")&&frequency==0){
            return 4f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("every 7 hours")&&frequency==0){
            return 3.5f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("every 8 hours")&&frequency==0){
            return 3f*quantityWithoutFrequency;
        }else if(otherFrequency.equals("every other day")){
            return quantity/2f;
        }else {
            return quantity;
        }
    }

    public Date getStartDate() {
        return new Date(startDate);
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate.getTime();
        long deltaTime = duration*24*60*60*1000;
        long finish = this.startDate+ deltaTime;
//        Log.d("Finish date in millisec "+  String.valueOf(finish) + " And difference is " + deltaTime);
        finishDate = finish;


    }

    public Date getFinishDate() {
        return new Date(finishDate);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        MedicineQuantity cloned = (MedicineQuantity) super.clone();
        if(cloned.getDoseCalculator()!=null) {
            cloned.setDoseCalculator((DoseCalculator) cloned.getDoseCalculator().clone());
        }else {
            cloned.setDoseCalculator(null);
        }

        if(cloned.getAgeBasedDoseCalculator()!=null) {
            cloned.setAgeBasedDoseCalculator((AgeBasedDoseCalculator) cloned.getAgeBasedDoseCalculator().clone());
        }else {
            cloned.setAgeBasedDoseCalculator(null);
        }

        return cloned;
    }



    @Override
    public String toString() {
        return "MedicineQuantity{" +
                "refill=" + refill +
                ", dose=" + dose +
                ", frequency=" + frequency +
                ", duration=" + duration +
                ", durationValuesType='" + durationValuesType + '\'' +
                ", doseValueType='" + doseValueType + '\'' +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                ", beforeMeal=" + beforeMeal +
                ", morningDose=" + morningDose +
                ", afternoonDose=" + afternoonDose +
                ", eveningDose=" + eveningDose +
                ", nightDose=" + nightDose +
                ", morning=" + morning +
                ", afternoon=" + afternoon +
                ", evening=" + evening +
                ", night=" + night +
                ", otherFrequency='" + otherFrequency + '\'' +
                ", dosePresent=" + dosePresent +
                ", doseCalculator=" + doseCalculator +
                ", quantity=" + quantity +
                ", isMultiDose=" + isMultiDose +
                '}';
    }

    public boolean isMultiDose() {
        return isMultiDose;
    }

    public void setMultiDose(boolean multiDose) {
        isMultiDose = multiDose;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(refill);
        dest.writeFloat(dose);
        dest.writeInt(frequency);
        dest.writeInt(duration);
        dest.writeString(durationValuesType);
        dest.writeString(doseValueType);
        dest.writeLong(startDate);
        dest.writeLong(finishDate);
        dest.writeInt(beforeMeal);
        dest.writeFloat(morningDose);
        dest.writeFloat(afternoonDose);
        dest.writeFloat(eveningDose);
        dest.writeFloat(nightDose);
        dest.writeByte((byte) (morning ? 1 : 0));
        dest.writeByte((byte) (afternoon ? 1 : 0));
        dest.writeByte((byte) (evening ? 1 : 0));
        dest.writeByte((byte) (night ? 1 : 0));
        dest.writeString(otherFrequency);
        dest.writeByte((byte) (dosePresent ? 1 : 0));
        dest.writeParcelable(doseCalculator, flags);
        dest.writeFloat(quantity);
        dest.writeByte((byte) (isMultiDose ? 1 : 0));
        dest.writeParcelable(ageBasedDoseCalculator, flags);
        dest.writeInt(doseCalculatorType);
    }
}