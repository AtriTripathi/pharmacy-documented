package com.prezcription.pharmacyapp.data.gson;

public class PatientInfo {

    private String name;
    private String username;
    private String age;
    private String gender;
    private String phone;
    private String email;

    public PatientInfo() {
    }

    public PatientInfo(String name, String username) {
        this.name = name;
        this.username = username;
    }

    public PatientInfo(String name, String username, String age, String gender, String phone, String email) {
        this.name = name;
        this.username = username;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
