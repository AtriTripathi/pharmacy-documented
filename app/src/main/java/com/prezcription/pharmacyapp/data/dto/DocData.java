package com.prezcription.pharmacyapp.data.dto;

/**
 * Created by anubhav on 17/02/17.
 */

//Just need it for id and doctor name

public class DocData {

    public DocData() {
    }

    public DocData(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public DocData(int id, String name, String specialization) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
    }

    public DocData(int id, String name, String specialization, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.specialization = specialization;
    }

    int id;
    String name;
    String specialization;
    String phone;
    int webId;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public int getWebId() {
        return webId;
    }

    public void setWebId(int webId) {
        this.webId = webId;
    }
}