package com.prezcription.pharmacyapp.data.gson;

public class DoctorInfo {
    private String name;
    private String gstNumber;
    private String kmcNumber;
    private String address;
    private String clinicName;
    private String qualification;
    private String doctorId;

    public DoctorInfo() {
    }

    public DoctorInfo(String name, String gstNumber, String kmcNumber, String doctorId) {
        this.name = name;
        this.gstNumber = gstNumber;
        this.kmcNumber = kmcNumber;
        this.doctorId = doctorId;
    }

    public DoctorInfo(String doctorId) {
        this.doctorId = doctorId;
    }

    public DoctorInfo(String name, String gstNumber, String kmcNumber, String address, String clinicName, String qualification, String doctorId) {
        this.name = name;
        this.gstNumber = gstNumber;
        this.kmcNumber = kmcNumber;
        this.address = address;
        this.clinicName = clinicName;
        this.qualification = qualification;
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getKmcNumber() {
        return kmcNumber;
    }

    public void setKmcNumber(String kmcNumber) {
        this.kmcNumber = kmcNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }
}
