package com.prezcription.pharmacyapp.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.custom.MyCookieStore;
import com.prezcription.pharmacyapp.data.dto.DoctorData;

import static com.prezcription.pharmacyapp.data.PharmacyContract.CONTENT_AUTHORITY;

public class PharmacyContentProvider extends ContentProvider {

    public static final String LOG_TAG = PharmacyContentProvider.class.getSimpleName();

    public static final int PATIENT = 100;
    public static final int PATIENT_REGISTRATION = 200;
    public static final int BILLS = 300;
    public static final int BILLS_WITH_BILL_ID = 301;
    public static final int DOCTOR = 400;

    public static final int MEDICINE_IN_BILLS = 500;

    public static final int VACCINE_IN_BILLS = 600;


    private static SQLiteQueryBuilder sBillListQueryBuilder;
    private static SQLiteQueryBuilder sBillDetailsQueryBuilder;

    static {

        sBillListQueryBuilder = new SQLiteQueryBuilder();

        sBillDetailsQueryBuilder = new SQLiteQueryBuilder();

        sBillListQueryBuilder.setTables(
                PharmacyContract.BillsEntry.TABLE_NAME +
                        " LEFT OUTER JOIN ("
                        + PharmacyContract.PatientRegistrationEntry.TABLE_NAME +
                        " LEFT OUTER JOIN "
                        + PharmacyContract.PatientEntry.TABLE_NAME +
                        " ON " + PharmacyContract.PatientRegistrationEntry.TABLE_NAME +
                        "." + PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID +
                        " = " + PharmacyContract.PatientEntry.TABLE_NAME +
                        "." + PharmacyContract.PatientEntry._ID
                        + " ) " +
                        " ON " + PharmacyContract.PatientRegistrationEntry.TABLE_NAME +
                        "." + PharmacyContract.PatientRegistrationEntry._ID +
                        " = " + PharmacyContract.BillsEntry.TABLE_NAME +
                        "." + PharmacyContract.BillsEntry.COLUMN_PATIENT_REG_ID +

                        " LEFT OUTER JOIN " + PharmacyContract.DoctorEntry.TABLE_NAME +
                        " ON " + PharmacyContract.BillsEntry.TABLE_NAME +
                        "." + PharmacyContract.BillsEntry.COLUMN_DOC_ID +
                        " = " + PharmacyContract.DoctorEntry.TABLE_NAME +
                        "." + PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID
        );

        sBillDetailsQueryBuilder = new SQLiteQueryBuilder();

        sBillDetailsQueryBuilder.setTables(
                PharmacyContract.BillsEntry.TABLE_NAME +
                        " LEFT OUTER JOIN ("
                        + PharmacyContract.PatientRegistrationEntry.TABLE_NAME +
                        " LEFT OUTER JOIN "
                        + PharmacyContract.PatientEntry.TABLE_NAME+
                        " ON " + PharmacyContract.PatientRegistrationEntry.TABLE_NAME +
                        "." + PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID +
                        " = " + PharmacyContract.PatientEntry.TABLE_NAME +
                        "." + PharmacyContract.PatientEntry._ID
                        +" ) "+
                        " ON " + PharmacyContract.PatientRegistrationEntry.TABLE_NAME +
                        "." + PharmacyContract.PatientRegistrationEntry._ID +
                        " = " + PharmacyContract.BillsEntry.TABLE_NAME +
                        "." + PharmacyContract.BillsEntry.COLUMN_PATIENT_REG_ID+

                        " LEFT OUTER JOIN " + PharmacyContract.DoctorEntry.TABLE_NAME+
                        " ON " + PharmacyContract.BillsEntry.TABLE_NAME +
                        "." + PharmacyContract.BillsEntry.COLUMN_DOC_ID +
                        " = " + PharmacyContract.DoctorEntry.TABLE_NAME +
                        "." + PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID+

                        " LEFT OUTER JOIN " + PharmacyContract.MedicineInBillsEntry.TABLE_NAME+
                        " ON " + PharmacyContract.BillsEntry.TABLE_NAME +
                        "." + PharmacyContract.BillsEntry._ID +
                        " = " + PharmacyContract.MedicineInBillsEntry.TABLE_NAME +
                        "." + PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID+

                        " LEFT OUTER JOIN " + PharmacyContract.VaccineInBillsEntry.TABLE_NAME+
                        " ON " + PharmacyContract.BillsEntry.TABLE_NAME +
                        "." + PharmacyContract.BillsEntry._ID +
                        " = " + PharmacyContract.VaccineInBillsEntry.TABLE_NAME +
                        "." + PharmacyContract.VaccineInBillsEntry.COLUMN_BILL_ID
        );
    }



    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private PharmacyDbHelper mOpenHelper;

    static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = CONTENT_AUTHORITY;

        matcher.addURI(authority, PharmacyContract.PATH_DOCTOR, DOCTOR);
        matcher.addURI(authority, PharmacyContract.PATH_PATIENT, PATIENT);
        matcher.addURI(authority, PharmacyContract.PATH_PATIENT_REG, PATIENT_REGISTRATION);
        matcher.addURI(authority, PharmacyContract.PATH_BILLS, BILLS);

        matcher.addURI(authority, PharmacyContract.PATH_MEDICINE_IN_BILLS, MEDICINE_IN_BILLS);
        matcher.addURI(authority, PharmacyContract.PATH_VACCINE_IN_BILLS, VACCINE_IN_BILLS);
        matcher.addURI(authority, PharmacyContract.PATH_BILLS+"/#", BILLS_WITH_BILL_ID);

        return matcher;

    }

    @Override
    public boolean onCreate() {
        CookieManager cookieManager = new CookieManager(new MyCookieStore(getContext()), CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);
        mOpenHelper = new PharmacyDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            case DOCTOR: {
                retCursor = mOpenHelper.getReadableDatabase().query(PharmacyContract.DoctorEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            case PATIENT: {
                retCursor = mOpenHelper.getReadableDatabase().query(PharmacyContract.PatientEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            case PATIENT_REGISTRATION: {
                retCursor = mOpenHelper.getReadableDatabase().query(PharmacyContract.PatientRegistrationEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            case BILLS: {
                Log.d(LOG_TAG, "inside bills query");
                retCursor = sBillListQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case BILLS_WITH_BILL_ID:

                retCursor = getBillDetailByBillId(uri,projection,sortOrder);
                break;
            case MEDICINE_IN_BILLS:
                retCursor = mOpenHelper.getReadableDatabase().query(PharmacyContract.MedicineInBillsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            case VACCINE_IN_BILLS:
                retCursor = mOpenHelper.getReadableDatabase().query(PharmacyContract.VaccineInBillsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;

    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {

            case DOCTOR:
                rowsUpdated = db.update(PharmacyContract.DoctorEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case PATIENT:
                rowsUpdated = db.update(PharmacyContract.PatientEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;


            case PATIENT_REGISTRATION:
                rowsUpdated = db.update(PharmacyContract.PatientRegistrationEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
        case BILLS:
        rowsUpdated = db.update(PharmacyContract.BillsEntry.TABLE_NAME, values, selection,
                selectionArgs);
        break;
        case MEDICINE_IN_BILLS:
        rowsUpdated = db.update(PharmacyContract.MedicineInBillsEntry.TABLE_NAME, values, selection,
                selectionArgs);
        break;
        case VACCINE_IN_BILLS:
        rowsUpdated = db.update(PharmacyContract.VaccineInBillsEntry.TABLE_NAME, values, selection,
                selectionArgs);
        break;

        default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case DOCTOR: {
                long _id = db.insert(PharmacyContract.DoctorEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = PharmacyContract.DoctorEntry.buildPatientUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case PATIENT: {
                long _id = db.insert(PharmacyContract.PatientEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = PharmacyContract.PatientEntry.buildPatientUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case PATIENT_REGISTRATION: {
                long _id = db.insert(PharmacyContract.PatientRegistrationEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = PharmacyContract.PatientRegistrationEntry.buildPatientRegUri(_id);
                else throw new SQLException("Failed to insert row " + uri);
                break;
            }
            case BILLS: {
                long _id = db.insert(PharmacyContract.BillsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = PharmacyContract.BillsEntry.buildBillUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case MEDICINE_IN_BILLS:{
                long _id = db.insert(PharmacyContract.MedicineInBillsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = PharmacyContract.MedicineInBillsEntry.buildMedicineInBillsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case VACCINE_IN_BILLS:{
                long _id = db.insert(PharmacyContract.VaccineInBillsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = PharmacyContract.VaccineInBillsEntry.buildVaccineInBillsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }



    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        // this makes delete all rows return the number of rows deleted
        if (null == selection) selection = "1";
        switch (match) {

            case DOCTOR:
                rowsDeleted = db.delete(
                        PharmacyContract.DoctorEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case PATIENT:
                rowsDeleted = db.delete(
                        PharmacyContract.PatientEntry.TABLE_NAME, selection, selectionArgs);
                break;


            case PATIENT_REGISTRATION:
                rowsDeleted = db.delete(
                        PharmacyContract.PatientRegistrationEntry.TABLE_NAME, selection, selectionArgs);
                break;
        case BILLS:
        rowsDeleted = db.delete(
                PharmacyContract.BillsEntry.TABLE_NAME, selection, selectionArgs);
        break;
        case MEDICINE_IN_BILLS:
        rowsDeleted = db.delete(
                PharmacyContract.MedicineInBillsEntry.TABLE_NAME, selection, selectionArgs);
        break;
        case VACCINE_IN_BILLS:
        rowsDeleted = db.delete(
                PharmacyContract.MedicineInBillsEntry.TABLE_NAME, selection, selectionArgs);
        break;

        default:

                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case DOCTOR:
                return PharmacyContract.DoctorEntry.CONTENT_TYPE;
            case PATIENT:
                return PharmacyContract.PatientEntry.CONTENT_TYPE;

            case PATIENT_REGISTRATION:
                return PharmacyContract.PatientRegistrationEntry.CONTENT_TYPE;
            case MEDICINE_IN_BILLS:
                return PharmacyContract.MedicineInBillsEntry.CONTENT_TYPE;
            case VACCINE_IN_BILLS:
                return PharmacyContract.MedicineInBillsEntry.CONTENT_TYPE;
            case BILLS:
                return PharmacyContract.BillsEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

    }

    public static long insertDoctorData(ContentResolver cr, DoctorData doctorData) {

        ContentValues insertValue = new ContentValues();
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_NAME, doctorData.getName());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_PHONE, doctorData.getPhoneNumber());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_EMAIL, doctorData.getEmail());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_AGE, doctorData.getAge());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_SEX, doctorData.getGender());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_SPECIALIZATION, doctorData.getSpecialization());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_QUALIFICATION, doctorData.getQualification());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_WEB_ID, doctorData.getWebId());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_IMAGE, doctorData.getPhotoUrl());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_IMAGE_LOCAL, Utility.NOT_AVAILABLE);
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_REGISTRATION_NUMBER, doctorData.getRegistrationNumber());
        insertValue.put(PharmacyContract.DoctorEntry.COLUMN_DOCTOR_CREATED_AT, Utility.getCurrentDate());

        Uri uri = cr.insert(PharmacyContract.DoctorEntry.CONTENT_URI, insertValue);
        return ContentUris.parseId(uri);

    }

    private static final String sBillIdForBillSelection =
            PharmacyContract.BillsEntry.TABLE_NAME+
                    "." + PharmacyContract.BillsEntry._ID + " = ? ";

    private Cursor getBillDetailByBillId(Uri uri, String[] projection, String sortOrder) {
        int billId = PharmacyContract.BillsEntry.getDoctorIdFromUri(uri);

        Log.d(LOG_TAG,"bill id is "+ billId);

        return sBillDetailsQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                sBillIdForBillSelection,
                new String[]{String.valueOf(billId)},
                null,
                null,
                sortOrder
        );
    }
}
