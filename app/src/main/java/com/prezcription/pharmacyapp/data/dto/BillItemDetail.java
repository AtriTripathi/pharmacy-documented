package com.prezcription.pharmacyapp.data.dto;

import java.util.List;

/**
 * Created by amit on 01/02/18.
 */

public class BillItemDetail {

    /**
     * doctorId : id
     * patientId : id
     * items : [{"itemId":"id","itemCount":"count","batchId":"id"}]
     */

    private String doctorId;
    private String patientId;
    private List<ItemsBean> items;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * itemId : id
         * itemCount : count
         * batchId : id
         */

        private String itemId;
        private String itemCount;
        private String batchId;

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        public String getItemCount() {
            return itemCount;
        }

        public void setItemCount(String itemCount) {
            this.itemCount = itemCount;
        }

        public String getBatchId() {
            return batchId;
        }

        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }
    }
}
