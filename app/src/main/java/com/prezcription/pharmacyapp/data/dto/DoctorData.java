package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anubhav on 25/09/16.
 */
public class DoctorData extends RecyclerSearchableData implements Parcelable {

    private String phoneNumber;
    private int webId;
    private String gender;
    private int age;
    private String specialization;
    private String qualification;
    private String address;
    private String photoUrl;
    private String registrationNumber;
    private String email;
    private int localId;

    public DoctorData(String name, int viewType){
        setName(name);
        setViewType(viewType);
    }

    public DoctorData() {
    }

    public DoctorData(String name, String phoneNumber, String photoUrl , int webId, int viewType) {
        setName(name);
        setViewType(viewType);
        this.phoneNumber = phoneNumber;
        this.photoUrl = photoUrl;
        this.webId = webId;
    }

    public DoctorData(String name, String phoneNumber, String photoUrl, String specialization, String qualification, int age, String gender, int webId, int viewType) {
        setName(name);
        setViewType(viewType);
        this.phoneNumber = phoneNumber;
        this.photoUrl = photoUrl;
        this.webId = webId;
        this.specialization = specialization;
        this.qualification = qualification;
        this.age = age;
        this.gender = gender;
    }

    public DoctorData(String name, String phoneNumber, String photoUrl, String specialization, String qualification, int age, String gender, int webId, String registrationNumber, int viewType) {
        setName(name);
        setViewType(viewType);
        this.phoneNumber = phoneNumber;
        this.photoUrl = photoUrl;
        this.webId = webId;
        this.specialization = specialization;
        this.qualification = qualification;
        this.age = age;
        this.gender = gender;
        this.registrationNumber = registrationNumber;
    }

    public DoctorData(String name, String phoneNumber, String email, String photoUrl, String specialization, String qualification, int age, String gender, int localId, int webId, String registrationNumber, int viewType) {
        setName(name);
        setViewType(viewType);
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoUrl = photoUrl;
        this.localId = localId;
        this.webId = webId;
        this.specialization = specialization;
        this.qualification = qualification;
        this.age = age;
        this.gender = gender;
        this.registrationNumber = registrationNumber;
    }



    public DoctorData(String name, String phoneNumber, String email, String photoUrl, String specialization, String qualification, int age, String gender, int webId, String registrationNumber, int viewType) {
        setName(name);
        setViewType(viewType);
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoUrl = photoUrl;
        this.webId = webId;
        this.specialization = specialization;
        this.qualification = qualification;
        this.age = age;
        this.gender = gender;
        this.registrationNumber = registrationNumber;
    }

    public DoctorData(String name, String phoneNumber, String email, String photoUrl, String specialization, String qualification, int age, String gender, int webId, String registrationNumber) {
        setName(name);
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoUrl = photoUrl;
        this.webId = webId;
        this.specialization = specialization;
        this.qualification = qualification;
        this.age = age;
        this.gender = gender;
        this.registrationNumber = registrationNumber;
    }

    protected DoctorData(Parcel in) {
        super(in);
        phoneNumber = in.readString();
        webId = in.readInt();
        gender = in.readString();
        age = in.readInt();
        localId = in.readInt();
        specialization = in.readString();
        qualification = in.readString();
        address = in.readString();
        photoUrl = in.readString();
        email = in.readString();
        registrationNumber = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(phoneNumber);
        dest.writeInt(webId);
        dest.writeInt(localId);
        dest.writeString(gender);
        dest.writeInt(age);
        dest.writeString(specialization);
        dest.writeString(qualification);
        dest.writeString(address);
        dest.writeString(photoUrl);
        dest.writeString(registrationNumber);
        dest.writeString(email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DoctorData> CREATOR = new Creator<DoctorData>() {
        @Override
        public DoctorData createFromParcel(Parcel in) {
            return new DoctorData(in);
        }

        @Override
        public DoctorData[] newArray(int size) {
            return new DoctorData[size];
        }
    };

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public int getWebId() {
        return webId;
    }

    public void setWebId(int webId) {
        this.webId = webId;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }
}
