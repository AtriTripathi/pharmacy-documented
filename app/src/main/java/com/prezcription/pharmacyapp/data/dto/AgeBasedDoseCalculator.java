package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Created by anubhav on 03/12/17.
 */

public class AgeBasedDoseCalculator implements Parcelable, Cloneable {

    private NavigableMap<Integer, AgeBasedDose> ageMap;
    private ArrayList<AgeBasedDose> ageBasedDoseArrayList;
    private boolean active;
    private String doseValueType;

    public AgeBasedDoseCalculator() {
        ageMap = new TreeMap<Integer, AgeBasedDose>();
    }

    public AgeBasedDoseCalculator(NavigableMap<Integer, AgeBasedDose> ageMap) {
        this.ageMap = ageMap;
    }


    protected AgeBasedDoseCalculator(Parcel in) {
        ageBasedDoseArrayList = in.createTypedArrayList(AgeBasedDose.CREATOR);
        active = in.readByte() != 0;
        doseValueType = in.readString();
//        ageMap = (NavigableMap<Integer, AgeBasedDose>) in.readSerializable();
        ageMap = new TreeMap<Integer, AgeBasedDose>();
        int size = in.readInt();
        for(int i = 0; i < size; i++){
            int key = in.readInt();
            AgeBasedDose value = in.readParcelable(AgeBasedDose.class.getClassLoader());
            this.getAgeMap().put(key,value);
        }
    }

    public static final Creator<AgeBasedDoseCalculator> CREATOR = new Creator<AgeBasedDoseCalculator>() {
        @Override
        public AgeBasedDoseCalculator createFromParcel(Parcel in) {
            return new AgeBasedDoseCalculator(in);
        }

        @Override
        public AgeBasedDoseCalculator[] newArray(int size) {
            return new AgeBasedDoseCalculator[size];
        }
    };

    public AgeBasedDose getAgeBasedDose(int age){

        if (age < 0 ) {
            // out of range
            return null;
        } else {
            if(ageMap!=null) {
                return ageMap.floorEntry(age).getValue();
            }else {
                return null;
            }
        }

    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDoseValueType() {
        return doseValueType;
    }

    public void setDoseValueType(String doseValueType) {
        this.doseValueType = doseValueType;
    }

    public NavigableMap<Integer, AgeBasedDose> getAgeMap() {
        return ageMap;
    }

    public void setAgeMap(NavigableMap<Integer, AgeBasedDose> ageMap) {
        this.ageMap = ageMap;
    }

    public ArrayList<AgeBasedDose> getAgeBasedDoseArrayList() {
        return ageBasedDoseArrayList;
    }

    public void setAgeBasedDoseArrayList(ArrayList<AgeBasedDose> ageBasedDoseArrayList) {
        this.ageBasedDoseArrayList = ageBasedDoseArrayList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(ageBasedDoseArrayList);
        parcel.writeByte((byte) (active ? 1 : 0));
        parcel.writeString(doseValueType);
//        parcel.writeSerializable((Serializable) this.getAgeMap());
        parcel.writeInt(this.getAgeMap().size());
        for(Map.Entry<Integer,AgeBasedDose> entry : this.getAgeMap().entrySet()){
            parcel.writeInt(entry.getKey());
            parcel.writeParcelable(entry.getValue(),i);
        }
    }
}
