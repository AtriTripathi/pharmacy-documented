package com.prezcription.pharmacyapp.data.dto;

import com.prezcription.pharmacyapp.data.gson.DoctorInfo;
import com.prezcription.pharmacyapp.data.gson.PatientInfo;

/**
 * Created by amit on 01/02/18.
 */

public class BillItem {

    private String itemId;
    private String batchId;
    private int itemCount;
    private String name;
    private int availableCount;
    private int viewType;
    private double cGst;
    private double sGst;
    private double amount;
    private double mrp;
    private double netRate;
    private String expiry;
    private double discount;
    private double consultationFee;
    private boolean differentBarCodeItem;
    PatientInfo patientInfo;
    DoctorInfo doctorInfo;
    private int itemType;


    public BillItem(){};

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    @Override
//    public boolean equals(Object obj) {
//
//        return  obj instanceof BillItem &&
//                this.itemId.equals(((BillItem) obj).getItemId());
//    }

    public int getAvailableCount() {
        return availableCount;
    }

    public void setAvailableCount(int availableCount) {
        this.availableCount = availableCount;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }


    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public double getcGst() {
        return cGst;
    }

    public void setcGst(double cGst) {
        this.cGst = cGst;
    }

    public double getsGst() {
        return sGst;
    }

    public void setsGst(double sGst) {
        this.sGst = sGst;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getNetRate() {
        return netRate;
    }

    public void setNetRate(double netRate) {
        this.netRate = netRate;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public PatientInfo getPatientInfo() {
        return patientInfo;
    }

    public void setPatientInfo(PatientInfo patientInfo) {
        this.patientInfo = patientInfo;
    }

    public DoctorInfo getDoctorInfo() {
        return doctorInfo;
    }

    public void setDoctorInfo(DoctorInfo doctorInfo) {
        this.doctorInfo = doctorInfo;
    }

    public double getConsultationFee() {
        return consultationFee;
    }

    public void setConsultationFee(double consultationFee) {
        this.consultationFee = consultationFee;
    }

    public boolean isDifferentBarCodeItem() {
        return differentBarCodeItem;
    }

    public void setDifferentBarCodeItem(boolean differentBarCodeItem) {
        this.differentBarCodeItem = differentBarCodeItem;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public String getBarcode() {

        if (itemId != null && batchId != null){
            return batchId + itemId;
        }

        return "";

    }
}
