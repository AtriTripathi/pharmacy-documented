package com.prezcription.pharmacyapp.data.gson;

import java.util.List;

/**
 * Created by amit on 01/02/18.
 */

public class BillResponse {


    /**
     * datetime : timestamp
     * billNumber : id
     * totalDue : amount
     * totalcGST : number
     * totalsGST : number
     * totalDiscount : discount
     * lineitems : [{"lineItem":"number","itemId":"id","batchId":"id","mfgr":"Abbott","expiry":"12-18","mrp":"10.12","itemCount":"realCountAvailable","NetRate":"10.00","cGST":"1.0","sGST":"0.5","NetAmt":"100.00"}]
     */

    private String datetime;
    private String billNumber;
    private String totalDue;
    private String totalcGST;
    private String totalsGST;
    private String totalDiscount;
    private List<LineitemsBean> lineitems;

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getTotalDue() {
        return totalDue;
    }

    public void setTotalDue(String totalDue) {
        this.totalDue = totalDue;
    }

    public String getTotalcGST() {
        return totalcGST;
    }

    public void setTotalcGST(String totalcGST) {
        this.totalcGST = totalcGST;
    }

    public String getTotalsGST() {
        return totalsGST;
    }

    public void setTotalsGST(String totalsGST) {
        this.totalsGST = totalsGST;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public List<LineitemsBean> getLineitems() {
        return lineitems;
    }

    public void setLineitems(List<LineitemsBean> lineitems) {
        this.lineitems = lineitems;
    }

    public static class LineitemsBean {
        /**
         * lineItem : number
         * itemId : id
         * batchId : id
         * mfgr : Abbott
         * expiry : 12-18
         * mrp : 10.12
         * itemCount : realCountAvailable
         * NetRate : 10.00
         * cGST : 1.0
         * sGST : 0.5
         * NetAmt : 100.00
         */

        private String lineItem;
        private String itemId;
        private String batchId;
        private String mfgr;
        private String expiry;
        private String mrp;
        private String itemCount;
        private String NetRate;
        private String cGST;
        private String sGST;
        private String NetAmt;

        public String getLineItem() {
            return lineItem;
        }

        public void setLineItem(String lineItem) {
            this.lineItem = lineItem;
        }

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        public String getBatchId() {
            return batchId;
        }

        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }

        public String getMfgr() {
            return mfgr;
        }

        public void setMfgr(String mfgr) {
            this.mfgr = mfgr;
        }

        public String getExpiry() {
            return expiry;
        }

        public void setExpiry(String expiry) {
            this.expiry = expiry;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getItemCount() {
            return itemCount;
        }

        public void setItemCount(String itemCount) {
            this.itemCount = itemCount;
        }

        public String getNetRate() {
            return NetRate;
        }

        public void setNetRate(String NetRate) {
            this.NetRate = NetRate;
        }

        public String getCGST() {
            return cGST;
        }

        public void setCGST(String cGST) {
            this.cGST = cGST;
        }

        public String getSGST() {
            return sGST;
        }

        public void setSGST(String sGST) {
            this.sGST = sGST;
        }

        public String getNetAmt() {
            return NetAmt;
        }

        public void setNetAmt(String NetAmt) {
            this.NetAmt = NetAmt;
        }
    }
}
