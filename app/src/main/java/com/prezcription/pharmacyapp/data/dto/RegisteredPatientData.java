package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 10/23/16.
 */

public class RegisteredPatientData extends RecyclerSearchableData implements Parcelable {

    private int regId;
    private Patient patient;
    private String doctorName;
    private String doctorSpecialization;
    private String doctorPhone;
    private int doctorId;
    private int tokenNo;
    private String regDate;
    private boolean syncFlag;
    private boolean updateFlag;
    private boolean reRegisterFlag;
    private String visitType;
    private int prescriptionCount;
    private int billId;
    private String billUpdateDate;

    private boolean isSelected = false;
    private boolean prescriptionFlag = false;

    public RegisteredPatientData() {
    }

    public RegisteredPatientData(String name, int viewType) {
        super(name, viewType);
    }

    public RegisteredPatientData(Patient patient, int viewType) {
        this.patient = patient;
        setName(patient.getName());
        setViewType(viewType);
    }

    public RegisteredPatientData(Patient patient, int tokenNo, int  regId , String regDate, boolean syncFlag, boolean updateFlag, String doctorName, String doctorPhone, String doctorSpecialization, int doctorId , int viewType, int billId) {
        this.patient = patient;
        this.tokenNo = tokenNo;
        this.regDate = regDate;
        this.syncFlag = syncFlag;
        this.updateFlag = updateFlag;
        this.doctorName = doctorName;
        this.doctorPhone = doctorPhone;
        this.doctorId = doctorId;
        this.doctorSpecialization =doctorSpecialization;
        this.regId = regId;
        this.billId = billId;
        setName(patient.getName());
        setViewType(viewType);
    }

    public RegisteredPatientData(Patient patient, int tokenNo, int  regId , String regDate, boolean syncFlag, boolean updateFlag, String doctorName, String doctorPhone, String doctorSpecialization, int doctorId , int viewType, boolean reRegisterFlag, String visitType, boolean prescriptionFlag) {
        this.regId = regId;
        this.patient = patient;
        this.doctorName = doctorName;
        this.doctorSpecialization = doctorSpecialization;
        this.doctorPhone = doctorPhone;
        this.doctorId = doctorId;
        this.tokenNo = tokenNo;
        this.regDate = regDate;
        this.syncFlag = syncFlag;
        this.updateFlag = updateFlag;
        this.reRegisterFlag = reRegisterFlag;
        this.visitType = visitType;
        this.prescriptionFlag = prescriptionFlag;
        setName(patient.getName());
        setViewType(viewType);
    }

    public RegisteredPatientData(Patient patient, int regId, boolean syncFlag, int viewType){
        this.patient = patient;
        this.regId = regId;
        this.syncFlag = syncFlag;
        setName(patient.getName());
        setViewType(viewType);
    }

    public RegisteredPatientData(Patient patient, int tokenNo, String regDate, boolean syncFlag) {
        this.patient = patient;
        this.tokenNo = tokenNo;
        this.regDate = regDate;
        this.syncFlag = syncFlag;

        setName(patient.getName());

    }


    protected RegisteredPatientData(Parcel in) {
        regId = in.readInt();
        patient = in.readParcelable(Patient.class.getClassLoader());
        doctorName = in.readString();
        doctorSpecialization = in.readString();
        doctorPhone = in.readString();
        doctorId = in.readInt();
        tokenNo = in.readInt();
        regDate = in.readString();
        syncFlag = in.readByte() != 0;
        updateFlag = in.readByte() != 0;
        reRegisterFlag = in.readByte() != 0;
        visitType = in.readString();
        prescriptionCount = in.readInt();
        isSelected = in.readByte() != 0;
        prescriptionFlag = in.readByte() != 0;
    }


    public static final Creator<RegisteredPatientData> CREATOR = new Creator<RegisteredPatientData>() {
        @Override
        public RegisteredPatientData createFromParcel(Parcel in) {
            return new RegisteredPatientData(in);
        }

        @Override
        public RegisteredPatientData[] newArray(int size) {
            return new RegisteredPatientData[size];
        }
    };

    @Override
    public void setName(String name) {
        super.setName(name);
        patient.setName(name);
    }

    public int getRegId() {
        return regId;
    }

    public void setRegId(int regId) {
        this.regId = regId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
        setName(patient.getName());
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public boolean isSyncFlag() {
        return syncFlag;
    }

    public void setSyncFlag(boolean syncFlag) {
        this.syncFlag = syncFlag;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getTokenNo() {
        return tokenNo;
    }

    public void setTokenNo(int tokenNo) {
        this.tokenNo = tokenNo;
    }

    public String getDoctorPhone() {
        return doctorPhone;
    }

    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone;
    }


    public String getDoctorSpecialization() {
        return doctorSpecialization;
    }

    public void setDoctorSpecialization(String doctorSpecialization) {
        this.doctorSpecialization = doctorSpecialization;
    }

    @Override
    public String toString() {
        return "RegisteredPatientData{" +
                "doctorId=" + doctorId +
                ", regId=" + regId +
                ", patient=" + patient +
                ", doctorName='" + doctorName + '\'' +
                ", doctorSpecialization='" + doctorSpecialization + '\'' +
                ", doctorPhone='" + doctorPhone + '\'' +
                ", tokenNo=" + tokenNo +
                ", regDate='" + regDate + '\'' +
                ", syncFlag=" + syncFlag +
                ", updateFlag=" + updateFlag +
                ", reRegisterFlag=" + reRegisterFlag +
                '}';
    }

    public void setUpdateFlag(boolean updateFlag){
        this.updateFlag = updateFlag;
    }

    public boolean isUpdateFlag() {
        return updateFlag;
    }


    public boolean isReRegisterFlag() {
        return reRegisterFlag;
    }

    public void setReRegisterFlag(boolean reRegisterFlag) {
        this.reRegisterFlag = reRegisterFlag;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public int getPrescriptionCount() {
        return prescriptionCount;
    }

    public void setPrescriptionCount(int prescriptionCount) {
        this.prescriptionCount = prescriptionCount;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isPrescriptionFlag() {
        return prescriptionFlag;
    }

    public void setPrescriptionFlag(boolean prescriptionFlag) {
        this.prescriptionFlag = prescriptionFlag;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(regId);
        dest.writeParcelable(patient, flags);
        dest.writeString(doctorName);
        dest.writeString(doctorSpecialization);
        dest.writeString(doctorPhone);
        dest.writeInt(doctorId);
        dest.writeInt(tokenNo);
        dest.writeString(regDate);
        dest.writeByte((byte) (syncFlag ? 1 : 0));
        dest.writeByte((byte) (updateFlag ? 1 : 0));
        dest.writeByte((byte) (reRegisterFlag ? 1 : 0));
        dest.writeString(visitType);
        dest.writeInt(prescriptionCount);
        dest.writeInt(billId);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeByte((byte) (prescriptionFlag ? 1 : 0));
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public String getBillUpdateDate() {
        return billUpdateDate;
    }

    public void setBillUpdateDate(String billUpdateDate) {
        this.billUpdateDate = billUpdateDate;
    }
}
