package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;



import com.prezcription.pharmacyapp.Utility;

/**
 * Created by anubhav on 03/12/17.
 */

public class DoseCalculator implements Parcelable, Cloneable {

    //For dose calculator
    private float dosage;
    private float volume;
    private float weight;
    private float mass;
    private int dosageValueType;
    private int volumeValueType;
    private int massValueType;
    private float maxDosage;
    private boolean isLiquid;
    private boolean isActive;

    public static final int MCG = 0;
    public static final int MG = 1;
    public static final int GM = 2;

    public static final int MCG_PER_KG = 0;
    public static final int MG_PER_KG = 1;
    public static final int GM_PER_KG = 2;

    public static final int ML = 0;
    public static final int L = 1;


    public DoseCalculator() {
    }

    public DoseCalculator(float dosage, int dosageValueType, boolean isLiquid, float maxDosage, boolean isActive) {
        this.dosage = dosage;
        this.dosageValueType = dosageValueType;
        this.isLiquid = isLiquid;
        this.isActive = isActive;
        this.maxDosage = maxDosage;
    }

    public DoseCalculator(float dosage, int dosageValueType, boolean isLiquid, float mass, int massValueType, float volume, int volumeValueType, float maxDosage, boolean isActive) {
        this.dosage = dosage;
        this.dosageValueType = dosageValueType;
        this.isLiquid = isLiquid;
        this.mass = mass;
        this.massValueType = massValueType;
        this.volume = volume;
        this.volumeValueType = volumeValueType;
        this.maxDosage = maxDosage;
        this.isActive = isActive;
    }

    protected DoseCalculator(Parcel in) {
        dosage = in.readFloat();
        volume = in.readFloat();
        weight = in.readFloat();
        mass = in.readFloat();
        dosageValueType = in.readInt();
        volumeValueType = in.readInt();
        massValueType = in.readInt();
        maxDosage = in.readFloat();
        isLiquid = in.readByte() != 0;
        isActive = in.readByte() != 0;
    }

    public static final Creator<DoseCalculator> CREATOR = new Creator<DoseCalculator>() {
        @Override
        public DoseCalculator createFromParcel(Parcel in) {
            return new DoseCalculator(in);
        }

        @Override
        public DoseCalculator[] newArray(int size) {
            return new DoseCalculator[size];
        }
    };

    public boolean isLiquid() {
        return isLiquid;
    }

    public void setLiquid(boolean liquid) {
        isLiquid = liquid;
    }

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    public int getDosageValueType() {
        return dosageValueType;
    }

    public void setDosageValueType(int dosageValueType) {
        this.dosageValueType = dosageValueType;
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public int getMassValueType() {
        return massValueType;
    }

    public void setMassValueType(int massValueType) {
        this.massValueType = massValueType;
    }

    public float getMaxDosage() {
        return maxDosage;
    }

    public void setMaxDosage(float maxDosage) {
        this.maxDosage = maxDosage;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public int getVolumeValueType() {
        return volumeValueType;
    }

    public void setVolumeValueType(int volumeValueType) {
        this.volumeValueType = volumeValueType;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(dosage);
        dest.writeFloat(volume);
        dest.writeFloat(weight);
        dest.writeFloat(mass);
        dest.writeInt(dosageValueType);
        dest.writeInt(volumeValueType);
        dest.writeInt(massValueType);
        dest.writeFloat(maxDosage);
        dest.writeByte((byte) (isLiquid ? 1 : 0));
        dest.writeByte((byte) (isActive ? 1 : 0));
    }

    public float calculateDosePerDay(){

        float dose = dosage*weight;

        if(dose<maxDosage){
            return Utility.getValueUptoTwoPlacesOfDecimal(dose);
        }else {
            return Utility.getValueUptoTwoPlacesOfDecimal(maxDosage);
        }

    }

    public float calculateLiquidDosePerDay(){

        float dose = getValueCorrectUnit(dosage*weight*(volume/mass));

        float maxDose = getValueCorrectUnit(maxDosage*weight*(volume/mass));

        if(dose<maxDose){
            return Utility.getValueUptoTwoPlacesOfDecimal(dose);
        }else {
            return Utility.getValueUptoTwoPlacesOfDecimal(maxDose);
        }

    }

    private float getValueCorrectUnit(float dose){


        if(dosageValueType != massValueType){

            if(dosageValueType > massValueType){

                if(dosageValueType ==MG_PER_KG&& massValueType ==MCG){
                    dose = dose*1000;
                }else if(dosageValueType ==GM_PER_KG&& massValueType ==MG){
                    dose = dose*1000;
                }else if(dosageValueType ==GM_PER_KG&& massValueType ==MCG){
                    dose = dose*1000000;
                }
            }else {
                if(massValueType ==MG_PER_KG&& dosageValueType ==MCG){
                    dose = dose/1000;
                }else if(massValueType ==GM_PER_KG&& dosageValueType ==MG){
                    dose = dose/1000;
                }else if(massValueType ==GM_PER_KG&& dosageValueType ==MCG){
                    dose = dose/1000000;
                }
            }
        }

        return dose;

    }

    @Override
    public String toString() {
        return "DoseCalculator{" +
                "dosage=" + dosage +
                ", volume=" + volume +
                ", weight=" + weight +
                ", mass=" + mass +
                ", dosageValueType=" + dosageValueType +
                ", volumeValueType=" + volumeValueType +
                ", massValueType=" + massValueType +
                ", maxDosage=" + maxDosage +
                ", isLiquid=" + isLiquid +
                '}';
    }
}

