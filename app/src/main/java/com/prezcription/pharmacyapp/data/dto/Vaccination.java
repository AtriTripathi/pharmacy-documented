package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by anubhav on 03/12/17.
 */

public class Vaccination implements Parcelable, Comparable {

    String name;
    String preferredDate;
    String vaccinationDate;
    int vaccinationAge=-1;
    double price;
    int itemType;
    private int ageGroup;
    private boolean isSelected = false;
    private int selectedPosition;
    private String id;
    private boolean isGiven = false;
    private String givenVaccineName;
    private boolean isMissed = false;
    private boolean isGivenOutside = false;
    private boolean isScheduledForFuture = false;

    private static final String LOG_TAG = Vaccination.class.getSimpleName();

    public static final Creator<Vaccination> CREATOR = new Creator<Vaccination>() {
        @Override
        public Vaccination createFromParcel(Parcel in) {
            return new Vaccination(in);
        }

        @Override
        public Vaccination[] newArray(int size) {
            return new Vaccination[size];
        }
    };

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Vaccination() {
    }

    public Vaccination(String name) {
        this.name = name;
    }

    public Vaccination(String name, int itemType, int ageGroup, String preferredDate, String id){
        this.name = name;
        this.itemType = itemType;
        this.ageGroup = ageGroup;
        this.preferredDate = preferredDate;
        this.id = id;

    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public Vaccination(String name, String preferredDate) {
        this.name = name;
        this.preferredDate = preferredDate;
    }

    public Vaccination(String name, double price){
        this.name = name;
        this.price = price;
    }

    public Vaccination(String name, String preferredDate, String vaccinationDate) {
        this.name = name;
        this.preferredDate = preferredDate;
        this.vaccinationDate = vaccinationDate;
    }

    public Vaccination(String name, String preferredDate, int vaccinationAge, String vaccinationDate) {
        this.name = name;
        this.preferredDate = preferredDate;
        this.vaccinationAge = vaccinationAge;
        this.vaccinationDate = vaccinationDate;
    }

    public Vaccination(String name, String preferredDate, int vaccinationAge, double price) {
        this.name = name;
        this.preferredDate = preferredDate;
        this.vaccinationAge = vaccinationAge;
        this.price = price;
    }

    public Vaccination(String date, boolean isGivenOutside, boolean isMissed){
        this.vaccinationDate = date;
        this.isGivenOutside = isGivenOutside;
        this.isMissed = isMissed;
    }

    protected Vaccination(Parcel in) {
        name = in.readString();
        preferredDate = in.readString();
        vaccinationDate = in.readString();
        vaccinationAge = in.readInt();
        price = in.readDouble();
        itemType = in.readInt();
        ageGroup = in.readInt();
        id = in.readString();
        isMissed = in.readByte() !=0;
        isGivenOutside = in.readByte() !=0;
        givenVaccineName = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreferredDate() {
        return preferredDate;
    }

    public void setPreferredDate(String preferredDate) {
        this.preferredDate = preferredDate;
    }

    public String getVaccinationDate() {
        return vaccinationDate;
    }

    public void setVaccinationDate(String vaccinationDate) {
        this.vaccinationDate = vaccinationDate;
    }

    public int getVaccinationAge() {
        return vaccinationAge;
    }

    public void setVaccinationAge(int vaccinationAge) {
        this.vaccinationAge = vaccinationAge;
    }


    @Override
    public int compareTo(Object o) {

        if(o instanceof Vaccination) {

            Vaccination vaccination = (Vaccination) o;

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault());

            int val = 0;

            try {
                Date date1 = sdf.parse(vaccination.getVaccinationDate());
                Date date2 = sdf.parse(this.vaccinationDate);


                if (date2.compareTo(date1) > 0) {
                    val = 1;
//                    Log.d(Utility.LOG_TAG + "Vaccination", "Date greater");
                } else if (date2.compareTo(date1) < 0) {
                    val = -1;
//                    Log.d(Utility.LOG_TAG + "Vaccination", "Date smaller");
                } else {
                    val = 0;
//                    Log.d(Utility.LOG_TAG + "Vaccination", "Date equal");

                }
            } catch (ParseException e) {
                e.printStackTrace();
//                Log.d(Utility.LOG_TAG + "Vaccination", "Date parse error");
            }

            return val;
        }else {
            return 0;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(preferredDate);
        dest.writeString(vaccinationDate);
        dest.writeInt(vaccinationAge);
        dest.writeDouble(price);
        dest.writeInt(itemType);
        dest.writeInt(ageGroup);
        dest.writeString(id);
        dest.writeByte((byte) (isMissed ? 1 : 0));
        dest.writeByte((byte) (isGivenOutside ? 1 : 0));
        dest.writeString(givenVaccineName);
    }

    public int getAgeGroup() {
        return ageGroup;
    }

    public boolean isGiven() {
        return isGiven;
    }

    public void setGiven(boolean given) {
        isGiven = given;
    }

    public void setAgeGroup(int ageGroup) {
        this.ageGroup = ageGroup;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public String getGivenVaccineName() {
        return givenVaccineName;
    }

    public void setGivenVaccineName(String givenVaccineName) {
        this.givenVaccineName = givenVaccineName;
    }

    public boolean isMissed() {
        return isMissed;
    }

    public void setMissed(boolean missed) {
        isMissed = missed;
    }

    public boolean isGivenOutside() {
        return isGivenOutside;
    }

    public void setGivenOutside(boolean givenOutside) {
        isGivenOutside = givenOutside;
    }

    public boolean isScheduledForFuture() {
        return isScheduledForFuture;
    }

    public void setScheduledForFuture(boolean scheduledForFuture) {
        isScheduledForFuture = scheduledForFuture;
    }

    @Override
    public boolean equals(Object o){
        Log.d(LOG_TAG, "inside equals: "+((Vaccination) o).getId() + "this.id: "+this.getId());

        if (o instanceof Vaccination && o!= null){
            return this.getId().equals(((Vaccination) o).getId());
        }
        return false;
    }
}

