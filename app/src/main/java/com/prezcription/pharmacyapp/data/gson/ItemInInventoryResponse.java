package com.prezcription.pharmacyapp.data.gson;

import java.util.List;

public class ItemInInventoryResponse {


        private List<LineitemsBean> lineitems;

        public List<LineitemsBean> getLineitems() {
            return lineitems;
        }

        public void setLineitems(List<LineitemsBean> lineitems) {
            this.lineitems = lineitems;
        }

        public static class LineitemsBean {
            /**
             * lineitem : 0
             * itemId : 10089
             * available : true
             */

            private int lineitem;
            private String itemId;
            private boolean available;

            public int getLineitem() {
                return lineitem;
            }

            public void setLineitem(int lineitem) {
                this.lineitem = lineitem;
            }

            public String getItemId() {
                return itemId;
            }

            public void setItemId(String itemId) {
                this.itemId = itemId;
            }

            public boolean isAvailable() {
                return available;
            }

            public void setAvailable(boolean available) {
                this.available = available;
            }
        }
    }
