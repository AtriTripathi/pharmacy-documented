package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anubhav on 03/12/17.
 */

public class AgeBasedDose implements Parcelable {

    private float dose;
    private float maxDose;
    private int maxAge;
    private int minAge;


    public AgeBasedDose(float dose, float maxDose) {
        this.dose = dose;
        this.maxDose = maxDose;
    }

    public AgeBasedDose() {
    }

    protected AgeBasedDose(Parcel in) {
        dose = in.readFloat();
        maxDose = in.readFloat();
        maxAge = in.readInt();
        minAge = in.readInt();
    }

    public static final Creator<AgeBasedDose> CREATOR = new Creator<AgeBasedDose>() {
        @Override
        public AgeBasedDose createFromParcel(Parcel in) {
            return new AgeBasedDose(in);
        }

        @Override
        public AgeBasedDose[] newArray(int size) {
            return new AgeBasedDose[size];
        }
    };

    public float getDose() {
        return dose;
    }

    public void setDose(float dose) {
        this.dose = dose;
    }

    public float getMaxDose() {
        return maxDose;
    }

    public void setMaxDose(float maxDose) {
        this.maxDose = maxDose;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(dose);
        parcel.writeFloat(maxDose);
        parcel.writeInt(maxAge);
        parcel.writeInt(minAge);

    }
}
