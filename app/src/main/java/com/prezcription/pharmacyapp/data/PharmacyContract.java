package com.prezcription.pharmacyapp.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class PharmacyContract {

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "com.prezcription.pharmacy";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_BILLS = "bills";
    public static final String PATH_PATIENT_REG = "patient_reg";

    public static final String PATH_MEDICINE_IN_BILLS = "medicine_in_bills";

    public static final String PATH_VACCINE_IN_BILLS = "vaccine_in_bills";

    public static final String PATH_PATIENT = "patient";

    public static final String PATH_DOCTOR = "doctor";



    public static final class BillsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_BILLS).build();

        //Mime type
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BILLS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BILLS;

        public static final String TABLE_NAME = "bills_entry";

        public static final String COLUMN_PATIENT_REG_ID = "patient__reg_id";

        //new
        public static final String COLUMN_PATIENT_USER_NAME = "billed_patient_user_name";

        //global_bill_id = bill_number

        public static final String COLUMN_GLOBAL_BILL_ID = "global_bill_id";

        //date at which bill is created for first time

        public static final String COLUMN_BILL_DATE = "bill_date";

        //updated date till bill is not created

        public static final String COLUMN_UPDATED_DATE = "updated_date";

        //new

        public static final String COLUMN_BILL_NUMBER = "bill_number";

        //new

//        public static final String COLUMN_PRESCRIPTION_ID = "prescription_key";

        public static final String COLUMN_DOC_ID = "doc_id";

        public static final String COLUMN_CONSULTATION_FEE = "consultation_fee";

        public static final String COLUMN_MEDICINE_DISCOUNT = "medicine_discount";

        public static final String COLUMN_VACCINE_DISCOUNT = "vaccine_discount";

        public static final String COLUMN_TOTAL_DISCOUNT_GIVEN = "total_discount";

        //new

        public static final String COLUMN_STATE_GST = "total_sgst";

        //new

        public static final String COLUMN_CENTER_GST = "total_cgst";

        public static final String COLUMN_PAID_AMOUNT = "amount_paid";

        public static final String COLUMN_BILL_CREATED = "bill_created";

        public static Uri buildBillUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildBillTypeWithDisplayTime(String date){
            return CONTENT_URI.buildUpon().appendPath(date).build();
        }

        public static Uri buildBillUriWithDoctorIdAndRegId(int doctorId, int regId) {
            return CONTENT_URI.buildUpon().appendPath(String.valueOf(doctorId))
                    .appendPath(String.valueOf(regId)).build();
        }

        public static Uri buildBillUriWithBillId(int billId) {
            return CONTENT_URI.buildUpon().appendPath(String.valueOf(billId))
                    .build();
        }

        public static String getDateFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static int getDoctorIdFromUri(Uri uri) {
            return Integer.parseInt(uri.getPathSegments().get(1));
        }

        public static int getBillIdFromUri(Uri uri) {
            return Integer.parseInt(uri.getPathSegments().get(1));
        }

        public static int getRegIdFromUri(Uri uri) {
            return Integer.parseInt(uri.getPathSegments().get(2));
        }
    }

    public static final class PatientRegistrationEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PATIENT_REG).build();


        //Mime type
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PATIENT_REG;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PATIENT_REG;

        public static final String TABLE_NAME = "patient_reg_entry";
        public static final String COLUMN_PATIENT_ID = "patient_id";
        public static final String COLUMN_DOCTOR_ID = "doctor_id";
        public static final String COLUMN_DATE = "reg_date";
        public static final String COLUMN_TOKEN = "patient_token";
        public static final String COLUMN_SYNC_FLAG = "sync_flag";
        public static final String COLUMN_SERVER_SYNC_FLAG = "server_sync_flag";
        public static final String COLUMN_UPDATE_FLAG = "update_flag";
        public static final String COLUMN_RE_REGISTER_FLAG = "re_register_flag";
        public static final String COLUMN_PATIENT_VISIT_TYPE = "visit_type";
        public static final String COLUMN_PRESCRIPTION_FLAG = "prescription_flag";
        public static final String COLUMN_PRESCRIPTION_TIMESTAMP = "prescription_timeStamp";


        public static Uri buildPatientRegUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildRegistrationWithStartDate(long startDate) {
            return CONTENT_URI.buildUpon()
                    .appendQueryParameter(COLUMN_DATE, Long.toString(startDate)).build();
        }



        public static Uri buildRegistrationTypeWithDate(long typeId, String typeDetail, String date) {
            return CONTENT_URI.buildUpon().appendPath(Long.toString(typeId))
                    .appendPath(typeDetail)
                    .appendQueryParameter(COLUMN_DATE, date).build();
        }

        public static Uri buildRegistrationDetailWithAssignedDoctorIdAndDate(long doctorId, String date) {
            return CONTENT_URI.buildUpon().appendPath(Long.toString(doctorId))
                    .appendPath(date).build();
        }

        public static Uri buildRegistrationPatientDetails(long patientId, long regId) {
            return CONTENT_URI.buildUpon().appendPath(Long.toString(patientId))
                    .appendQueryParameter(_ID, Long.toString(regId)).build();
        }

        public static int getTypeIdFromUri(Uri uri) {
            return Integer.parseInt(uri.getPathSegments().get(1));
        }

        public static String getTypeDetailFromUri(Uri uri) {
            return uri.getPathSegments().get(2);
        }

        public static String getStartDateFromUri(Uri uri) {
            String dateString = uri.getQueryParameter(COLUMN_DATE);
            if (null != dateString && dateString.length() > 0)
                return dateString;
            else if (dateString.equals("0"))
                return "0";
            else
                return null;
        }

        public static int getRegistrationIdFromUri(Uri uri) {
            String regId = uri.getQueryParameter(_ID);
            if (null != regId && regId.length() > 0)
                return Integer.parseInt(regId);
            else
                return -1;
        }

    }

    public static final class MedicineInBillsEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MEDICINE_IN_BILLS).build();

        //Mime type
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MEDICINE_IN_BILLS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MEDICINE_IN_BILLS;

        public static final String TABLE_NAME = "medicine_bills_entry";

        public static final String COLUMN_MEDICINE_ID = "medicine_id";

        public static final String COLUMN_BATCH_ID = "batch_id";

        public static final String COLUMN_BAR_CODE = "med_bar_code";

        public static final String COLUMN_MEDICINE_NAME = "medicine_name";

        public static final String COLUMN_MEDICINE_QUANTITY = "med_quantity";

        public static final String COLUMN_MEDICINE_MNF = "med_mnf";

        public static final String COLUMN_MEDICINE_MRP = "med_mrp";

        public static final String COLUMN_MEDICINE_EXPIRY = "med_exp";

        public static final String COLUMN_MEDICINE_AMOUNT = "med_amount";

        public static final String COLUMN_MEDICINE_CGST = "med_center_gst";

        public static final String COLUMN_MEDICINE_SGST = "med_state_gst";

        public static final String COLUMN_MEDICINE_PRICE = "med_price";

        public static final String COLUMN_MEDICINE_ITEM_GROSS = "med_item_gross";

        public static final String COLUMN_BILL_ID = "bill_id";

        public static final String COLUMN_GLOBAL_BILL_ID = "gloabal_bill_id";

        public static final String COLUMN_MEDICINE_DISCOUNT = "med_discount";

        public static final String COLUMN_MEDICINE_AVAILABLE_QUANTITY = "med_avail_qty";

        public static final String COLUMN_ITEM_BILLED = "med_item_billed";

        public static Uri buildMedicineInBillsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class VaccineInBillsEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_VACCINE_IN_BILLS).build();

        //Mime type
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_VACCINE_IN_BILLS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_VACCINE_IN_BILLS;

        public static final String TABLE_NAME = "vaccine_bills_entry";

        public static final String COLUMN_VACCINE_ID = "vacc_id";

        public static final String COLUMN_BATCH_ID = "vac_batch_id";

        public static final String COLUMN_BAR_CODE = "vac_bar_code";

        public static final String COLUMN_VACCINE_NAME = "vaccine_name";

        public static final String COLUMN_VACCINE_QUANTITY = "vacc_quantity";

        public static final String COLUMN_VACCINE_MNF = "vac_mnf";

        public static final String COLUMN_VACCINE_MRP = "vac_mrp";

        public static final String COLUMN_VACCINE_EXPIRY = "vac_exp";

        public static final String COLUMN_VACCINE_AMOUNT = "vac_amount";

        public static final String COLUMN_VACCINE_CGST = "vac_center_gst";

        public static final String COLUMN_VACCINE_SGST = "vac_state_gst";

        public static final String COLUMN_GLOBAL_BILL_ID = "vac_gloabal_bill_id";

        public static final String COLUMN_VACCINE_PRICE = "vacc_price";

        public static final String COLUMN_VACCINE_ITEM_GROSS = "vacc_item_gross";

        public static final String COLUMN_BILL_ID = "bill_id";

        public static final String COLUMN_VACCINE_DISCOUNT = "vacc_discount";

        public static final String COLUMN_ITEM_BILLED = "vacc_item_billed";

        public static Uri buildVaccineInBillsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class PatientEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PATIENT).build();

        //Mime type
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PATIENT;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PATIENT;

        public static final String TABLE_NAME = "patient";

        //Column with patient name
        public static final String COLUMN_PATIENT_NAME = "patient_name";

        //Column with patient mother's name
        public static final String COLUMN_MOTHERS_NAME = "mothers_name";

        //Column with patient father's name
        public static final String COLUMN_FATHERS_NAME = "fathers_name";

        //Column with patient age
        public static final String COLUMN_PATIENT_AGE = "patient_age";

        //Column with patient pin
        public static final String COLUMN_PATIENT_PIN_CODE = "pin_code";

        //Column with patient area
        public static final String COLUMN_PATIENT_AREA = "address_area";

        public static final String COLUMN_PATIENT_AGE_TYPE = "patient_age_type";

        public static final String COLUMN_PATIENT_IMAGE = "patient_image";

        //Column with patient email
        public static final String COLUMN_PATIENT_EMAIL = "patient_email";

        public static final String COLUMN_PATIENT_PHONE = "patient_phone";

        //Column with patient sex
        public static final String COLUMN_PATIENT_SEX = "patient_sex";

        //Column with patient address
        public static final String COLUMN_PATIENT_ADDRESS = "patient_address";

        public static final String COLUMN_PATIENT_RECENT = "recent";

        public static final String COLUMN_PATIENT_RELATIONSHIP = "relationship";

        public static final String COLUMN_PATIENT_WEB_ID = "patient_web_id";

        public static final String COLUMN_PATIENT_IMAGE_LOCAL = "patient_image_local";

        public static final String COLUMN_PATIENT_DOB = "dob";

        public static final String COLUMN_PATIENT_USERNAME = "pat_username";


        public static final String INDEX_PATIENT_USERNAME = "patient_username_idx";

        public static Uri buildPatientUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }

    //Inner class that defines table contents of patient database
    public static final class DoctorEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_DOCTOR).build();

        //Mime type
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_DOCTOR;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_DOCTOR;

        public static final String TABLE_NAME = "doctor";

        //Column with doctor's name
        public static final String COLUMN_DOCTOR_NAME = "doctor_name";

        //Column with doctor's specialization
        public static final String COLUMN_DOCTOR_SPECIALIZATION = "specialization";

        //Column with doctor's qualification
        public static final String COLUMN_DOCTOR_QUALIFICATION = "qualification";

        //Column with doctor's registration number
        public static final String COLUMN_DOCTOR_REGISTRATION_NUMBER = "registration_number";

        //Column with doctor's age
        public static final String COLUMN_DOCTOR_AGE = "doctor_age";

        public static final String COLUMN_DOCTOR_IMAGE = "doctor_image";

        //Column with doctor's email
        public static final String COLUMN_DOCTOR_EMAIL = "doctor_email";

        public static final String COLUMN_DOCTOR_PHONE = "doctor_phone";

        //Column with doctor's sex
        public static final String COLUMN_DOCTOR_SEX = "doctor_sex";

        public static final String COLUMN_DOCTOR_WEB_ID = "doctor_web_id";

        public static final String COLUMN_DOCTOR_IMAGE_LOCAL = "doctor_image_local";

        public static final String COLUMN_DOCTOR_CREATED_AT = "created_at";

        public static Uri buildPatientUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }
}
