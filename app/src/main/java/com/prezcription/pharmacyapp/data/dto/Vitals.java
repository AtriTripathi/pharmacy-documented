package com.prezcription.pharmacyapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anubhav on 22/09/16.
 */
public class Vitals implements Parcelable {

    private long id;
    private int heartRate;
    private int systolicPressure;
    private int diastolicPressure;
    private float weight;
    private float temperature;
    private float height;
    private float headCircumference;
    private int respiratoryRate;
    private int spo2;



    public Vitals() {
    }

    public Vitals(int heartRate, int systolicPressure, int diastolicPressure) {
        this.heartRate = heartRate;
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
    }

    public Vitals(int weight) {
        this.weight = weight;
    }

    public Vitals(int heartRate, int systolicPressure, int diastolicPressure, int weight) {
        this.heartRate = heartRate;
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
        this.weight = weight;
    }

    public Vitals(int heartRate, int systolicPressure, int diastolicPressure, int weight, float temperature) {
        this.heartRate = heartRate;
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
        this.weight = weight;
        this.temperature = temperature;
    }

    public Vitals(int heartRate, int systolicPressure, int diastolicPressure, float weight, float temperature, float height) {
        this.heartRate = heartRate;
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
        this.weight = weight;
        this.temperature = temperature;
        this.height = height;
    }

    public Vitals(int heartRate, int systolicPressure, int diastolicPressure, float weight, float temperature, float height, float headCircumference, int respiratoryRate) {
        this.heartRate = heartRate;
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
        this.weight = weight;
        this.temperature = temperature;
        this.height = height;
        this.headCircumference = headCircumference;
        this.respiratoryRate = respiratoryRate;
    }


    protected Vitals(Parcel in) {
        id = in.readLong();
        heartRate = in.readInt();
        systolicPressure = in.readInt();
        diastolicPressure = in.readInt();
        weight = in.readFloat();
        temperature = in.readFloat();
        height = in.readFloat();
        headCircumference = in.readFloat();
        respiratoryRate = in.readInt();
        spo2 = in.readInt();
    }

    public static final Creator<Vitals> CREATOR = new Creator<Vitals>() {
        @Override
        public Vitals createFromParcel(Parcel in) {
            return new Vitals(in);
        }

        @Override
        public Vitals[] newArray(int size) {
            return new Vitals[size];
        }
    };

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getSystolicPressure() {
        return systolicPressure;
    }

    public void setSystolicPressure(int systolicPressure) {
        this.systolicPressure = systolicPressure;
    }

    public int getDiastolicPressure() {
        return diastolicPressure;
    }

    public void setDiastolicPressure(int diastolicPressure) {
        this.diastolicPressure = diastolicPressure;
    }

    public float getHeadCircumference() {
        return headCircumference;
    }

    public void setHeadCircumference(float headCircumference) {
        this.headCircumference = headCircumference;
    }

    public int getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(int respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSpo2() {
        return spo2;
    }

    public void setSpo2(int spo2) {
        this.spo2 = spo2;
    }


    @Override
    public String toString() {
        return "Vitals{" +
                "id=" + id +
                ", heartRate=" + heartRate +
                ", systolicPressure=" + systolicPressure +
                ", diastolicPressure=" + diastolicPressure +
                ", weight=" + weight +
                ", temperature=" + temperature +
                ", height=" + height +
                ", headCircumference=" + headCircumference +
                ", respiratoryRate=" + respiratoryRate +
                ", spo2=" + spo2 +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(heartRate);
        dest.writeInt(systolicPressure);
        dest.writeInt(diastolicPressure);
        dest.writeFloat(weight);
        dest.writeFloat(temperature);
        dest.writeFloat(height);
        dest.writeFloat(headCircumference);
        dest.writeInt(respiratoryRate);
        dest.writeInt(spo2);
    }
}
