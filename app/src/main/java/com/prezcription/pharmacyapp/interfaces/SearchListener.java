package com.prezcription.pharmacyapp.interfaces;

/**
 * Created by anubhav on 10/02/17.
 */

public interface SearchListener {

    /* do search with char sequence*/
    void doSearch(CharSequence s);

    /* add items and get string if there is any in the search bar */
    void addItem(String string);

    void clearSearch();

}
