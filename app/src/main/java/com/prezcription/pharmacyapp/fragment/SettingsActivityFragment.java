package com.prezcription.pharmacyapp.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.custom.SeekBarPreference;

public class SettingsActivityFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener  {

    private static final String LOG_TAG = SettingsActivityFragment.class.getSimpleName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.pref_settings);
        initSummary(getPreferenceScreen());

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

//       if (key.equals(Utility.SETTINGS_DEFAULT_DEVICE_NAME)) {
//
//            Log.d(LOG_TAG, "inside on device name change " + getResources().getString(R.string.pref_default_device_name, sharedPreferences.getString(key, "")));
//
//            Preference defaultDevicePref = findPreference(key);
//            // Set summary to be the user-description for the selected value
//            defaultDevicePref.setSummary(getResources().getString(R.string.pref_default_device_name, sharedPreferences.getString(key, "")));
//        }
//        else
            if(key.equals(Utility.SETTINGS_PDF_HEADER_SPACING)){

            Preference headerSpacingPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            headerSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_header,sharedPreferences.getInt(key,0)));

        }else if(key.equals(Utility.SETTINGS_PDF_FOOTER_SPACING)){

            Preference footerSpacingPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            footerSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_footer,sharedPreferences.getInt(key,0)));

        }else if(key.equals(Utility.SETTINGS_PDF_LEFT_SPACING)){

            Preference leftSpacingPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            sharedPreferences.getInt(key,0);
            leftSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_left,sharedPreferences.getInt(key,0)));

        }else if(key.equals(Utility.SETTINGS_PDF_RIGHT_SPACING)){

            Preference rightSpacingPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            rightSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_right,sharedPreferences.getInt(key,0)));
        }else if(key.equals(Utility.SETTINGS_PDF_FONT_SIZING)){

            Preference rightSpacingPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            rightSpacingPref.setSummary(getResources().getString(R.string.pref_description_font_sizing,sharedPreferences.getInt(key,0)));
        }else if(key.equals(Utility.SETTINGS_PDF_ORGANISATION_NAME)){

            Preference organisationNamePref = findPreference(Utility.SETTINGS_PDF_ORGANISATION_NAME);
            // Set summary to be the user-description for the selected value
            organisationNamePref.setSummary(getResources().getString(R.string.pref_description_clinic_or_hospital_name,sharedPreferences.getString(key,"")));
            ((EditTextPreference)organisationNamePref).setText(sharedPreferences.getString(key,""));

        }else if(key.equals(Utility.SETTINGS_PDF_WEBSITE_NAME)){

            Preference websiteNamePref = findPreference(Utility.SETTINGS_PDF_WEBSITE_NAME);
            // Set summary to be the user-description for the selected value
            websiteNamePref.setSummary(getResources().getString(R.string.pref_description_website_name,sharedPreferences.getString(key,"")));
            ((EditTextPreference)websiteNamePref).setText(sharedPreferences.getString(key,""));

        }else if(key.equals(Utility.SETTINGS_PDF_ADDRESS)){

            Preference addressPref = findPreference(Utility.SETTINGS_PDF_ADDRESS);
            // Set summary to be the user-description for the selected value
            addressPref.setSummary(getResources().getString(R.string.pref_description_address,sharedPreferences.getString(key,"")));
            ((EditTextPreference)addressPref).setText(sharedPreferences.getString(key,""));

        }

    }


    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    private void initSummary(Preference p) {

        if(p instanceof PreferenceScreen) {
            PreferenceScreen preferenceScreen = (PreferenceScreen)p;
            for (int i =0 ;i<preferenceScreen.getPreferenceCount();i++) {
                initSummary(preferenceScreen.getPreference(i));
            }
        }
        else if (p instanceof PreferenceGroup) {
            PreferenceGroup pGrp = (PreferenceGroup) p;
            for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
                initSummary(pGrp.getPreference(i));
            }
        } else {
            updatePrefSummary(p);
        }
    }


    private void updatePrefSummary(Preference preference){

        if(preference instanceof EditTextPreference){

            String key =preference.getKey();
//
//            if (key.equals(Utility.SETTINGS_DEFAULT_DEVICE_NAME)) {
//
//                Log.d(LOG_TAG, "inside on device name change " + getResources().getString(R.string.pref_default_device_name, ((EditTextPreference) preference).getText()));
//
//                Preference defaultDevicePref = findPreference(key);
//                // Set summary to be the user-description for the selected value
//                defaultDevicePref.setSummary(getResources().getString(R.string.pref_default_device_name,((EditTextPreference) preference).getText()));
//            }else
            if(key.equals(Utility.SETTINGS_PDF_ORGANISATION_NAME)){

                Preference organisationNamePref = findPreference(Utility.SETTINGS_PDF_ORGANISATION_NAME);
                // Set summary to be the user-description for the selected value
                organisationNamePref.setSummary(getResources().getString(R.string.pref_description_clinic_or_hospital_name,((EditTextPreference) preference).getText()));

            }else if(key.equals(Utility.SETTINGS_PDF_WEBSITE_NAME)){

                Preference websiteNamePref = findPreference(Utility.SETTINGS_PDF_WEBSITE_NAME);
                // Set summary to be the user-description for the selected value
                websiteNamePref.setSummary(getResources().getString(R.string.pref_description_website_name,((EditTextPreference) preference).getText()));

            }else if(key.equals(Utility.SETTINGS_PDF_ADDRESS)){

                Preference addressPref = findPreference(Utility.SETTINGS_PDF_ADDRESS);
                // Set summary to be the user-description for the selected value
                addressPref.setSummary(getResources().getString(R.string.pref_description_address,((EditTextPreference) preference).getText()));

            }
        }

        if(preference instanceof SeekBarPreference){

            String key =preference.getKey();

            if(key.equals(Utility.SETTINGS_PDF_HEADER_SPACING)){

                Preference headerSpacingPref = findPreference(key);
                // Set summary to be the user-description for the selected value
                headerSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_header,((SeekBarPreference) preference).getValue()));

            }else if(key.equals(Utility.SETTINGS_PDF_FOOTER_SPACING)){

                Preference footerSpacingPref = findPreference(key);
                // Set summary to be the user-description for the selected value
                footerSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_footer,((SeekBarPreference) preference).getValue()));

            }else if(key.equals(Utility.SETTINGS_PDF_LEFT_SPACING)){

                Preference leftSpacingPref = findPreference(key);
                // Set summary to be the user-description for the selected value
                leftSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_left,((SeekBarPreference) preference).getValue()));

            }else if(key.equals(Utility.SETTINGS_PDF_RIGHT_SPACING)){

                Preference rightSpacingPref = findPreference(key);

                // Set summary to be the user-description for the selected value
                rightSpacingPref.setSummary(getResources().getString(R.string.pref_description_pdf_right,((SeekBarPreference) preference).getValue()));

            }else if(key.equals(Utility.SETTINGS_PDF_FONT_SIZING)){

                SeekBarPreference fontSizingPref = (SeekBarPreference) findPreference(key);
                fontSizingPref.setMaxValue(20);
                // Set summary to be the user-description for the selected value
                fontSizingPref.setSummary(getResources().getString(R.string.pref_description_font_sizing,((SeekBarPreference) preference).getValue()));

            }

        }

    }
}
