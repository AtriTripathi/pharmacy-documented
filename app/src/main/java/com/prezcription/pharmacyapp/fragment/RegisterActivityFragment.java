package com.prezcription.pharmacyapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import java.util.HashMap;
import java.util.Map;

import com.prezcription.pharmacyapp.ApiConfig;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.activity.LoginActivity;

/**
 * Created by anubhav on 16/02/17.
 */

public class RegisterActivityFragment extends Fragment {

    //Required fields for registration
    private EditText mName;
    private EditText mEmail;
    private EditText mPin;
    private EditText mPhoneNumber;
    private EditText mBuildingNo;
    private EditText mStreet;
    private EditText mCity;
    private EditText mPassword;
    private Button mBtnRegister;

    private ProgressDialog progressBar;

    public static final int MALE =1;
    public static final int FEMALE =2;
    private int gender =0 ;

    private SharedPreferences mPreferences;

    private static final String LOG_TAG = RegisterActivityFragment.class.getSimpleName();

    Map<String,String> mParams;

    public RegisterActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mPreferences = getActivity().getSharedPreferences(Utility.USER_LOG_PREF, Context.MODE_PRIVATE);

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        mName = (EditText) view.findViewById(R.id.etName);
        mPhoneNumber = (EditText) view.findViewById(R.id.etPhoneNumber);
        mEmail = (EditText) view.findViewById(R.id.etEmailRegister);
        mPassword = (EditText) view.findViewById(R.id.etPassword);
        mPin = (EditText) view.findViewById(R.id.etPIN);
        mStreet = (EditText) view.findViewById(R.id.etStreet);
        mBuildingNo = (EditText) view.findViewById(R.id.etBuildingNo);
        mCity = (EditText) view.findViewById(R.id.etCity);
        mBtnRegister = (Button) view.findViewById(R.id.btnRegister_button);
        mBtnRegister.setTypeface(Utility.getProximaNovaTypeface(getContext(),Utility.REGULAR));

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                attemptRegister();


            }
        });

        return view;
    }


    public void attemptRegister() {

        mParams = new HashMap<String, String>();

        // Reset errors.m
        mName.setError(null);
        mPhoneNumber.setError(null);
        mEmail.setError(null);
        mPassword.setError(null);
        mPin.setError(null);
        mStreet.setError(null);
        mBuildingNo.setError(null);
        mCity.setError(null);

        // Store values at the time of the login attempt.
        String name = mName.getText().toString();
        String phoneNumber = mPhoneNumber.getText().toString();
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        String pin = mPin.getText().toString();
        String street = mStreet.getText().toString();
        String building = mBuildingNo.getText().toString();
        String city = mCity.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check if the user entered .
        if(TextUtils.isEmpty(name) ){
            mName.setError(getString(R.string.error_field_required));
            focusView = mName;
            cancel = true;
        }

        // Check for a valid phone, if the user entered one.
        if(TextUtils.isEmpty(phoneNumber) ){
            mPhoneNumber.setError(getString(R.string.error_field_required));
            focusView = mPhoneNumber;
            cancel = true;
        }else if (!Utility.isPhoneNumberValid(phoneNumber)) {
            mPhoneNumber.setError(getString(R.string.error_invalid_phone_number));
            focusView = mPhoneNumber;
            cancel = true;
        }

        // Check for a valid email address.
//        if (TextUtils.isEmpty(email)) {
//            mEmail.setError(getString(R.string.error_field_required));
//            focusView = mEmail;
//            cancel = true;
//        }
        if (!Utility.isEmailValid(email)&&!TextUtils.isEmpty(email)) {
            mEmail.setError(getString(R.string.error_invalid_email));
            focusView = mEmail;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if(TextUtils.isEmpty(password) ){
            mPassword.setError(getString(R.string.error_field_required));
            focusView = mPassword;
            cancel = true;
        }
        else if (!Utility.isPasswordValid(password)) {
            mPassword.setError(getString(R.string.error_invalid_password));
            focusView = mPassword;
            cancel = true;
        }

        // Check for a valid Age, if the user entered one.
        if(TextUtils.isEmpty(pin) ){
            mPin.setError(getString(R.string.error_field_required));
            focusView = mPin;
            cancel = true;
        }else if (!Utility.isPINNumberValid(pin)) {
            mPin.setError(getString(R.string.error_invalid_pin));
            focusView = mPin;
            cancel = true;
        }

        if(TextUtils.isEmpty(street) ){
            mStreet.setError(getString(R.string.error_field_required));
            focusView = mStreet;
            cancel = true;
        }

        if(TextUtils.isEmpty(building) ){
            mBuildingNo.setError(getString(R.string.error_field_required));
            focusView = mBuildingNo;
            cancel = true;
        }

        if(TextUtils.isEmpty(city) ){
            mCity.setError(getString(R.string.error_field_required));
            focusView = mCity;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            if(!name.equals("")){
                mParams.put("NAME",name);
            }
            if(!phoneNumber.equals("")){
                mParams.put("PHONE_NUMBER",phoneNumber);
            }
            if(!email.equals("")){
                mParams.put("EMAIL",email);
            }
            if(!password.equals("")){
                mParams.put("PASSWORD",password);
            }
            if(!pin.equals("")){
                mParams.put("PIN",pin);
            }
            if(!street.equals("")){
                mParams.put("STREET",street);
            }
            if(!building.equals("")){
                mParams.put("LINE_1",building);
            }
            if(!city.equals("")){
                mParams.put("CITY",city);
            }

            getRegisterResponse(password);
        }
    }

    RequestQueue queue;
    StringRequest sr;
    private static final String VOLLEY_TAG_REGISTER = "register_tag";

    public String getRegisterResponse(final String password){

        final String[] status = new String[1];
        queue = Volley.newRequestQueue(getContext());

        // prepare for a progress bar dialog
        progressBar = new ProgressDialog(getContext());
        progressBar.setCancelable(true);
        progressBar.setMessage("Loading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();

        sr = new StringRequest(Request.Method.POST, ApiConfig.REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("Anubhav","Response is "+ response);

                if(response.equals("Success")){

//                    SharedPreferences.Editor editor = mPreferences.edit();
//                    editor.putBoolean(LoginActivity.LOG_KEY,true);
//                    editor.putString(LoginActivity.PHONE_NUMBER_KEY,mParams.get("PHONE_NUMBER"));
//                    editor.putString(LoginActivity.PASSWORD_KEY,password);
//                    editor.commit();

                    progressBar.cancel();
                    progressBar.dismiss();

                    showAlertDialogBox(response);

                }else {

                    new AlertDialog.Builder(getActivity())
                            .setTitle("Error")
                            .setMessage(response)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    progressBar.cancel();
                                    progressBar.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.getVolleyErrorResponse(error,getActivity(),progressBar);
            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Log.d(LOG_TAG,mParams.toString());

                return mParams;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };

        sr.setTag(VOLLEY_TAG_REGISTER);
        queue.add(sr);

        return status[0];

    }

    @Override
    public void onStop() {
        super.onStop();
        if(sr!=null)
            sr.cancel();
        if(queue!=null)
            queue.cancelAll(VOLLEY_TAG_REGISTER);
    }

    private void showAlertDialogBox(String response){

        new AlertDialog.Builder(getContext())
                .setTitle(response)
                .setMessage("Registration successful, Do you want to login?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(getContext(),LoginActivity.class);
                        startActivity(intent);

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();

    }

}
