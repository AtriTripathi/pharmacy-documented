package com.prezcription.pharmacyapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;


import java.util.ArrayList;

import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.adapter.BilledPatientAdapter;
import com.prezcription.pharmacyapp.adapter.SearchDialogAdapter;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.dto.Patient;
import com.prezcription.pharmacyapp.data.dto.RegisteredPatientData;

/**
 * Created by amit on 02/12/17.
 */

public class AddBilledPatientFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,
            BilledPatientAdapter.OnItemClickListener{

    int mDisplayTime;

    private ArrayList<RegisteredPatientData> patientDatas;
    private ArrayList<RegisteredPatientData> mRecentPatientData;
    private ArrayList<Integer> mBillIdList;

    private RecyclerView mRecyclerView;
    int lastItemClicked = -1;

    private BilledPatientAdapter mAdapter;

    private OnBillItemFragmentInteractionListener mListener;

    SharedPreferences mSettingsPreferences;
    String userNametoBill;

    public static final String LOG_TAG =  AddBilledPatientFragment.class.getSimpleName();


    public AddBilledPatientFragment() {
    }

    ;

    public static AddBilledPatientFragment newInstance() {
        return new AddBilledPatientFragment();
    }

    public static AddBilledPatientFragment newInstance(String userNameToBill) {
        Bundle bundle = new Bundle();
        bundle.putString("username", userNameToBill);
        AddBilledPatientFragment fragment = new AddBilledPatientFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    public interface OnBillItemFragmentInteractionListener {

        void onItemClick(int patientId, int regId, int doctorId, int billId);

    }

    private static final String[] REGISTRATION_COLUMNS = {
            PharmacyContract.PatientRegistrationEntry.TABLE_NAME + "." + PharmacyContract.PatientRegistrationEntry._ID,
            PharmacyContract.PatientRegistrationEntry.COLUMN_DATE,
            PharmacyContract.PatientRegistrationEntry.COLUMN_SYNC_FLAG,
            PharmacyContract.PatientRegistrationEntry.COLUMN_TOKEN,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE_TYPE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_SEX,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_EMAIL,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_ADDRESS,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_WEB_ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_RELATIONSHIP,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_IMAGE,
            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_NAME,
            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_SPECIALIZATION,
            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_PHONE,
            PharmacyContract.PatientRegistrationEntry.COLUMN_DOCTOR_ID,
            PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID,
            PharmacyContract.PatientRegistrationEntry.COLUMN_PRESCRIPTION_FLAG,
            PharmacyContract.PatientRegistrationEntry.COLUMN_UPDATE_FLAG,
            PharmacyContract.PatientRegistrationEntry.COLUMN_RE_REGISTER_FLAG,
            PharmacyContract.BillsEntry.TABLE_NAME + "." + PharmacyContract.BillsEntry._ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME,
            PharmacyContract.BillsEntry.COLUMN_UPDATED_DATE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_DOB
    };

    private final int COL_REGISTRATION_ID = 0;
    private final int COL_REGISTRATION_DATE = 1;
    private final int COL_REGISTRATION_SYNC_FLAG = 2;
    private final int COL_REGISTRATION_TOKEN = 3;
    private final int COL_PATIENT_NAME = 4;
    private final int COL_PATIENT_AGE = 5;
    private final int COL_PATIENT_AGE_TYPE = 6;
    private final int COL_PATIENT_SEX = 7;
    private final int COL_PATIENT_PHONE = 8;
    private final int COL_PATIENT_EMAIL = 9;
    private final int COL_PATIENT_ADDRESS = 10;
    private final int COL_PATIENT_WEB_ID = 11;
    private final int COL_PATIENT_RELATIONSHIP = 12;
    private final int COL_PATIENT_IMAGE = 13;
    private final int COL_DOCTOR_NAME = 14;
    private final int COL_DOCTOR_SPECIALIZATION = 15;
    private final int COL_DOCTOR_PHONE = 16;
    private final int COL_DOCTOR_ID = 17;
    private final int COL_PATIENT_ID = 18;
    private final int COL_PRESCRIPTION_FLAG = 19;
    private final int COL_REGISTRATION_UPDATE_FLAG = 20;
    private final int COL_REGISTRATION_RE_REGISTER_FLAG = 21;
    private final int COL_BILL_ID = 22;
    private final int COL_PATIENT_USERNAME = 23;
    private final int COL_BILL_UPDATE_DATE = 24;
    private final int COL_BILL_PATIENT_DOB = 25;

    ImageButton topBarSearchButton;
    TextView tvHeading;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_billed_patient_layout, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        tvHeading = (TextView) view.findViewById(R.id.tvHeading);

        tvHeading.setText("BILLING QUEUE");

        topBarSearchButton = (ImageButton) view.findViewById(R.id.btn_search);
        topBarSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSearchDialogAdded();
            }
        });

        mSettingsPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        mRecentPatientData = new ArrayList<>();
        mBillIdList = new ArrayList<>();
        patientDatas = new ArrayList<>();
        mAdapter = new BilledPatientAdapter(patientDatas, this);

        addRecentData();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        if (getArguments() != null){
            userNametoBill = getArguments().getString("username");

        }


        return view;


    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if (mSettingsPreferences.getString(Utility.SETTINGS_PATIENT_DISPLAY_TIME, "12").equals("Show All")) {
            mDisplayTime = 0;
        } else {
            mDisplayTime = Integer.parseInt(mSettingsPreferences.getString(Utility.SETTINGS_PATIENT_DISPLAY_TIME, "12"));
        }


        String selection =
                PharmacyContract.BillsEntry.COLUMN_UPDATED_DATE + " >= ? ";
        String[] selectionArgs = {Utility.getHoursBeforeDate(mDisplayTime)};
        String sort = PharmacyContract.BillsEntry.COLUMN_UPDATED_DATE + " DESC ";

        Uri uri = PharmacyContract.BillsEntry.CONTENT_URI;
//                buildRegistrationTypeWithDate(Utility.DATE_WITH_PRES_FLAG, null, Utility.getHoursBeforeDate(mDisplayTime));

        return new CursorLoader(getActivity(),
                uri,
                REGISTRATION_COLUMNS,
                selection,
                selectionArgs,
                sort);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

//        Log.d(LOG_TAG, "Cursor detail is " + DatabaseUtils.dumpCursorToString(cursor));

        mRecentPatientData.clear();
        mBillIdList.clear();

        while (cursor.moveToNext()) {
            int regId = cursor.getInt(COL_REGISTRATION_ID);
            int billId = cursor.getInt(COL_BILL_ID);
            String name = cursor.getString(COL_PATIENT_NAME);
            String number = cursor.getString(COL_PATIENT_PHONE);
            int webId = cursor.getInt(COL_PATIENT_WEB_ID);
            int age = cursor.getInt(COL_PATIENT_AGE);
            String sex = cursor.getString(COL_PATIENT_SEX);
            String relationship = cursor.getString(COL_PATIENT_RELATIONSHIP);
            String photoUrl = cursor.getString(COL_PATIENT_IMAGE);
            String ageType = cursor.getString(COL_PATIENT_AGE_TYPE);
            String doctorName = cursor.getString(COL_DOCTOR_NAME);
            String doctorPhone = cursor.getString(COL_DOCTOR_PHONE);
            String registrationDate = cursor.getString(COL_REGISTRATION_DATE);
            String doctorSpecialization = cursor.getString(COL_DOCTOR_SPECIALIZATION);
            String username = cursor.getString(COL_PATIENT_USERNAME);
            int doctorId = cursor.getInt(COL_DOCTOR_ID);
            int patientId = cursor.getInt(COL_PATIENT_ID);
            int token = cursor.getInt(COL_REGISTRATION_TOKEN);
            int syncFlag = cursor.getInt(COL_REGISTRATION_SYNC_FLAG);
            int prescriptionSyncFlag = cursor.getInt(COL_PRESCRIPTION_FLAG);
            int updateFlag = cursor.getInt(COL_REGISTRATION_UPDATE_FLAG);
            int reRegisterFlag = cursor.getInt(COL_REGISTRATION_RE_REGISTER_FLAG);
            String billUpdateDate = cursor.getString(COL_BILL_UPDATE_DATE);
            String dob = cursor.getString(COL_BILL_PATIENT_DOB);

            Log.d(LOG_TAG,"dob: "+dob);

            Patient patient = new Patient(name,username, sex, age, ageType, number, relationship, patientId, webId, photoUrl);

            Log.d(LOG_TAG, "Doctor id " + doctorId + ", doctor name " + doctorName
                    + ", patient id " + patientId + ", patient name " + name + ", token "
                    + token + ", syncFlag " + syncFlag + ", prescriptionFlag " + prescriptionSyncFlag
                    + ", updateFlag " + updateFlag + ", reRegisterFlag " + reRegisterFlag+ "billUpdateDate: "+billUpdateDate);

            RegisteredPatientData registeredPatientData = new RegisteredPatientData(patient, token, regId, registrationDate, syncFlag == 1,
                    updateFlag == 1, doctorName, doctorPhone, doctorSpecialization, doctorId, 0, billId);

            registeredPatientData.setBillUpdateDate(billUpdateDate);

            mRecentPatientData.add(registeredPatientData);
        }

        if (mRecentPatientData.size() != 0) {
//            mRecentPatientData.add(0, new RegisteredPatientData("Recent", SearchPatientAdapter.HEADER_ITEM));
            patientDatas.clear();
            patientDatas.addAll(mRecentPatientData);
            mAdapter.notifyDataSetChanged();

            Log.d(LOG_TAG, "bill item pos"+lastItemClicked);

//            if (lastItemClicked != -1){
//                onItemCLick(lastItemClicked);
//            }else {
//                onItemCLick(0);
//            }

            if (lastItemClicked == -1 && userNametoBill == null) {
                onItemCLick(0);
            }

            Log.d(LOG_TAG, "usename to bill: "+userNametoBill);

            if (userNametoBill != null){
                setBillForUserName(userNametoBill);
            }
        }


    }

    @Override
    public void onItemCLick(int position) {


        setBillAdapter(position);


        mListener.onItemClick(patientDatas.get(position).getPatient().getProfileId(), patientDatas.get(position).getRegId(),
                patientDatas.get(position).getDoctorId(), patientDatas.get(position).getBillId());

        userNametoBill = patientDatas.get(position).getPatient().getUsername();


        lastItemClicked = position;

    }

    private void setBillAdapter(int position) {


        patientDatas.get(position).setSelected(true);

        if (lastItemClicked != -1 && lastItemClicked != position) {
            patientDatas.get(lastItemClicked).setSelected(false);
            mAdapter.notifyItemChanged(lastItemClicked);
        }

        mAdapter.notifyItemChanged(position);
    }

    public void addRecentData() {
        Log.d(LOG_TAG, "adding recent data");
        getLoaderManager().restartLoader(Utility.BILL_SEARCH_LOADER, null, this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(Utility.BILL_SEARCH_LOADER, null, this);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBillItemFragmentInteractionListener) {
            mListener = (OnBillItemFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnBillItemFragmentInteractionListener) {
            mListener = (OnBillItemFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onSearchDialogAdded() {

        AddSearchDialogFragment addSearchDialogFragment = AddSearchDialogFragment.newInstance(SearchDialogAdapter.PATIENT_BILLED);
        addSearchDialogFragment.setTargetFragment(AddBilledPatientFragment.this, 200);
        addSearchDialogFragment.show(getActivity().getFragmentManager(), "searchDialog");

    }

    public void onSearchItemSelected(final int billId, String patientName) {
        int pos = -1;

        for (int i = 0; i < patientDatas.size(); i++) {
            if (patientDatas.get(i).getBillId() == billId) {
                pos = i;
                break;
            }
        }
        if (pos != -1) {
            Log.d(LOG_TAG, "on item click");
            onItemCLick(pos);
        } else {
            Log.d(LOG_TAG, "no item found");
            new MaterialDialog.Builder(getActivity()).
                    title("Patient not in current list").
                    positiveText("Recreate Bill").
                    negativeText("Cancel").
                    onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (updateBillIntoCurrentList(billId)) {
                                selectRecentAddedBillItem(billId);
                            } else {
                                Toast.makeText(getActivity(), "Oopss..Selected patient could not be retrieved",
                                        Toast.LENGTH_SHORT).show();
                            }

                        }
                    }).
                    onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).show();

        }

    }

    private void selectRecentAddedBillItem(int billId) {
        int pos = -1;

        for (int i = 0; i < patientDatas.size(); i++) {
            if (patientDatas.get(i).getBillId() == billId) {
                pos = i;
                break;
            }
        }
        if (pos != -1) {
            onItemCLick(pos);
        }
    }

    private boolean updateBillIntoCurrentList(int billId) {

        String selection = PharmacyContract.BillsEntry._ID + " =? ";
        String selectionArgs[] = new String[]{String.valueOf(billId)};

        ContentValues contentValues = new ContentValues();
        contentValues.put(PharmacyContract.BillsEntry.COLUMN_UPDATED_DATE, Utility.getCurrentDate());

        int rowsUpdated = getActivity().getContentResolver().update(PharmacyContract.BillsEntry.CONTENT_URI, contentValues,
                selection, selectionArgs);

        return rowsUpdated > 0;


    }

    public void setBillForUserName(String userName){
        int position = -1;

        for (int i=0; i <patientDatas.size(); i++){
            if (patientDatas.get(i).getPatient().getUsername().equals(userName)){
                position = i;
                break;
            }
        }

        Log.d(LOG_TAG, "position:setBillForUserName "+position);

        if (position != -1) {
            mRecyclerView.smoothScrollToPosition(position);
            onItemCLick(position);
        }

    }

}
