package com.prezcription.pharmacyapp.fragment;


import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.prezcription.pharmacyapp.custom_camera.CameraSource;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import java.io.IOException;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.custom_camera.BarcodeScanningProcessor;
import com.prezcription.pharmacyapp.custom_camera.CameraSourcePreview;

/**
 * Created by amit on 02/02/18.
 */

public class BarCodeScannerFragment extends Fragment implements BarcodeScanningProcessor.onBarCodeScannerInteraction {
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;

    CameraSourcePreview cameraView;

    private static final String ITEM_POSIITON = "item_pos";

    private int mPosiiton;

    private int RC_HANDLE_CAMERA_PERM = 333;

    private  boolean once;

    public BarCodeScannerFragment(){}


    public static final String LOG_TAG = BarCodeScannerFragment.class.getSimpleName();

    public static BarCodeScannerFragment newInstance(){
        return new BarCodeScannerFragment();
    }
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.scan_fragment_camera_surface_layout, container,false);
        cameraView = (CameraSourcePreview) view.findViewById(R.id.firePreview);
        Log.d(LOG_TAG, "inside on create view");

//        Log.d(TAG, "Selected model: " + selectedModel);
        cameraView.stop();

        int rc = ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            Log.d(LOG_TAG, "all permissions granted");
            createCameraSource();
            startCameraSource();
        } else {
            requestCameraPermission();
        }

        return view;

    }



    private void createCameraSource(){
        if (cameraSource == null) {
            cameraSource = new CameraSource(getActivity());
        }
        Log.i(LOG_TAG, "Using Barcode Detector Processor");
        cameraSource.setMachineLearningFrameProcessor(new BarcodeScanningProcessor(getActivity(), this));


    }

    private void startCameraSource() {
        if (cameraSource != null) {
            try {
                if (cameraView == null) {
                    Log.d(LOG_TAG, "resume: Preview is null");
                }

                cameraView.start(cameraSource);
            } catch (IOException e) {
                Log.e(LOG_TAG, "Unable to start camera source.", e);
                cameraSource.release();
                cameraSource = null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume");
        startCameraSource();
    }

    /** Stops the camera. */
    @Override
    public void onPause() {
        super.onPause();
        cameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cameraSource != null) {
            cameraSource.release();
        }
    }


    private void requestCameraPermission() {
        Log.w(LOG_TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{android.Manifest.permission.CAMERA};

        ActivityCompat.requestPermissions(getActivity(), permissions, RC_HANDLE_CAMERA_PERM);



    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(LOG_TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(LOG_TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        Log.e(LOG_TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));


    }


    @Override
    public void onBarCodeProcessed() {
        onPause();
    }
}
