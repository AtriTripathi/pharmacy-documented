package com.prezcription.pharmacyapp.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import com.prezcription.pharmacyapp.DividerItemDecoration;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.adapter.SearchDialogAdapter;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.dto.Patient;
import com.prezcription.pharmacyapp.data.dto.PatientData;
import com.prezcription.pharmacyapp.data.dto.RecyclerSearchableData;
import com.prezcription.pharmacyapp.data.dto.RegisteredPatientData;

/**
 * Created by amit on 30/10/17.
 * This will serve as a uniform
 * search layout for search across app
 */



public class AddSearchDialogFragment extends DialogFragment implements SearchDialogAdapter.OnItemClickListener,
        LoaderManager.LoaderCallbacks<Cursor>{

    private static final String TYPE = "type";
    private static final String SCOPE = "scope";
    private static final String LIST_KEY ="list_key";
    private RecyclerView recyclerView;
    private SearchDialogAdapter mAdapter;
    private int type;
    private int scope;
    private Context mContext;
    private static final String QUERY = "query";
    private String searchQuery;
    private OnSearchDialogFragmentListener mListener;

    private static final int SEARCH_FRAG_CALLER = 1;
    private static final int ADD_PATIENT_SEARCH_CALLER = 2;

//    private ArrayList<RegisteredPatientData> mRecentPatientData;

    int mDisplayTime;

    private String[] PATIENT_COLUMNS = {
            PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE_TYPE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_SEX,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_EMAIL,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_ADDRESS,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_WEB_ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_RELATIONSHIP,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_IMAGE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_PIN_CODE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_AREA,
            PharmacyContract.PatientEntry.COLUMN_FATHERS_NAME,
            PharmacyContract.PatientEntry.COLUMN_MOTHERS_NAME,
            PharmacyContract.PatientEntry.TABLE_NAME+"."+PharmacyContract.PatientEntry._ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME,
    };

    public static final int COL_PATIENT_NAME_SCOPE_ALL = 0;
    public static final int COL_PATIENT_PHONE_SCOPE_ALL = 1;
    public static final int COL_PATIENT_AGE_SCOPE_ALL = 2;
    public static final int COL_PATIENT_AGE_TYPE_SCOPE_ALL = 3;
    public static final int COL_PATIENT_SEX_SCOPE_ALL = 4;
    public static final int COL_PATIENT_EMAIL_SCOPE_ALL = 5;
    public static final int COL_PATIENT_ADDRESS_SCOPE_ALL = 6;
    public static final int COL_PATIENT_WEB_ID_SCOPE_ALL = 7;
    public static final int COL_PATIENT_RELATIONSHIP_SCOPE_ALL = 8;
    public static final int COL_PATIENT_IMAGE_SCOPE_ALL = 9;
    public static final int COL_PATIENT_PIN_CODE_SCOPE_ALL = 10;
    public static final int COL_PATIENT_AREA_SCOPE_ALL = 11;
    public static final int COL_PATIENT_FATHER_NAME_SCOPE_ALL = 12;
    public static final int COL_PATIENT_MOTHER_NAME_SCOPE_ALL = 13;
    public static final int COL_PATIENT_ID_SCOPE_ALL = 14;
    public static final int COL_PATIENT_USERNAME_SCOPE_ALL = 15;



    private static final String[] REGISTRATION_COLUMNS = {
            PharmacyContract.PatientRegistrationEntry.TABLE_NAME + "." + PharmacyContract.PatientRegistrationEntry._ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE,
            PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME,
    };

    // These indices are tied to PRESCRIPTION_COLUMNS.  If PRESCRIPTION_COLUMNS changes, these
    // must change.
    public final int COL_REGISTRATION_ID = 0;
    public static final int COL_PATIENT_NAME = 1;
    public static final int COL_PATIENT_PHONE = 2;
    public static final int COL_PATIENT_ID = 3;
    public static final int COL_PATIENT_USERNAME = 4;

    private static final String[] REGISTERED_PATIENT_BILL_COLUMNS = {
            PharmacyContract.PatientRegistrationEntry.TABLE_NAME + "." + PharmacyContract.PatientRegistrationEntry._ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_RELATIONSHIP,
            PharmacyContract.PatientRegistrationEntry.COLUMN_DOCTOR_ID,
            PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID,
            PharmacyContract.PatientRegistrationEntry.COLUMN_PRESCRIPTION_FLAG,
            PharmacyContract.BillsEntry.TABLE_NAME + "." + PharmacyContract.BillsEntry._ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME,
    };

    // These indices are tied to PRESCRIPTION_COLUMNS.  If PRESCRIPTION_COLUMNS changes, these
    // must change.
    public final int COL_PAT_BILL_REG_ID = 0;
    public static final int COL_PAT_BILL_PATIENT_NAME = 1;
    public static final int COL_PAT_BILL_PATIENT_PHONE = 2;
    public static final int COL_PAT_BILL_PATIENT_RELATIONSHIP = 3;
    public static final int COL_PAT_BILL_DOCTOR_ID = 4;
    public static final int COL_PAT_BILL_PATIENT_ID = 5;
    public static final int COL_PAT_BILL_REGISTRATION_PRESCRIPTION_FLAG = 6;
    public static final int COL_PAT_BILL_BILL_ID = 7;
    public static final int COL_PAT_BILL_USERNAME = 8;







    public static final String LOG_TAG = AddSearchDialogFragment.class.getSimpleName();

    List<? extends RecyclerSearchableData> searchableDatas;

    ArrayList<RegisteredPatientData> mRegisteredPatientData;
    ArrayList<PatientData> mPatientData;


    public AddSearchDialogFragment(){};

    @SuppressWarnings("unchecked")
    public static AddSearchDialogFragment newInstance(ArrayList<? extends RecyclerSearchableData> dataArrayList, int type){

        AddSearchDialogFragment fragment = new AddSearchDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        bundle.putParcelableArrayList(LIST_KEY, dataArrayList);
        fragment.setArguments(bundle);

        return fragment;
    };

    @SuppressWarnings("unchecked")
    public static AddSearchDialogFragment newInstance(int type){

        AddSearchDialogFragment fragment = new AddSearchDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        fragment.setArguments(bundle);

        return fragment;
    };

    public static AddSearchDialogFragment newInstance(int type, int scope){
        AddSearchDialogFragment fragment = new AddSearchDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(TYPE, type);
        bundle.putInt(SCOPE, scope);
        fragment.setArguments(bundle);

        return fragment;



    }



    public interface OnSearchDialogFragmentListener{
        void onSearchItemSelected(int id, String name);
        void callerFragment(int calledId);
    }

    SharedPreferences mSettingsPreferences;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getActivity();
        Log.d(LOG_TAG, "inside onCreateView: ");

        MaterialDialog.Builder alertDialogBuilder = new MaterialDialog.Builder(getActivity());

        alertDialogBuilder.customView(R.layout.uniform_search_layout,false);

        MaterialDialog dialog = alertDialogBuilder.build();

        View view = dialog.getCustomView();

        recyclerView = (RecyclerView) view.findViewById(R.id.searchable_items_recycler_view);

//        this.mRecentPatientData = new ArrayList<>();

        if (getArguments() != null) {
            type = getArguments().getInt(TYPE);
            scope = getArguments().getInt(SCOPE);
//            searchQuery = getArguments().getString(QUERY);
        }

        mSettingsPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (mSettingsPreferences.getString(Utility.SETTINGS_PATIENT_DISPLAY_TIME, "12").equals("Show All")) {
            mDisplayTime = 0;
        } else {
            mDisplayTime = Integer.parseInt(mSettingsPreferences.getString(Utility.SETTINGS_PATIENT_DISPLAY_TIME, "12"));
        }

        this.searchableDatas = new ArrayList<>();

        getLoaderManager().initLoader(Utility.UNIFORM_SEARCH_LOADER,null,this);
        addData();

        ImageButton btnRemove;
        ImageView searchIcon;
        EditText edtSearch;

        edtSearch = (EditText) view.findViewById(R.id.edtSearch);
        btnRemove = (ImageButton) view.findViewById(R.id.btn_remove);
        searchIcon = (ImageView) view.findViewById(R.id.search_icon);
//        til = (TextInputLayout) view.findViewById(R.id.text_input_layout);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                doSearch(s.toString());
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSearch();
            }
        });

        switch (type){
            case SearchDialogAdapter.PATIENT:
                this.mPatientData = new ArrayList<>();
                searchIcon.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.male));
                searchIcon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
                edtSearch.setHint("Add - to search by user id");
                mAdapter = new SearchDialogAdapter(mPatientData,type,this);
                break;
            case SearchDialogAdapter.PATIENT_QUEUED:
                this.mRegisteredPatientData = new ArrayList<>();
                searchIcon.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.male));
                searchIcon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
                edtSearch.setHint("Search Patient");
                mAdapter = new SearchDialogAdapter(mRegisteredPatientData, type, this);
                break;
            case SearchDialogAdapter.PATIENT_BILLED:
                this.mRegisteredPatientData = new ArrayList<>();
                searchIcon.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.male));
                searchIcon.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
                edtSearch.setHint("Search Patient For Billing");
                mAdapter = new SearchDialogAdapter(mRegisteredPatientData, type, this);
                break;

        }


        Log.d(LOG_TAG, "searchable data: "+searchableDatas.size());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        if(Build.VERSION.SDK_INT < 23) {
//            linearLayoutManager.setAutoMeasureEnabled(false);
//        }
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        return dialog;
    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//
//    }



    @Override
    public void onItemEdit(int position) {

    }

    @Override
    public void onItemForwarded(int position) {

        if (mPatientData != null){
            Patient patient = mPatientData.get(position).getPatient();
            mListener.callerFragment(ADD_PATIENT_SEARCH_CALLER);
            mListener.onSearchItemSelected(patient.getProfileId(), patient.getName());
        }else if (mRegisteredPatientData != null){
          switch (type){
              case SearchDialogAdapter.PATIENT_QUEUED:
                  mListener.callerFragment(SEARCH_FRAG_CALLER);
                  mListener.onSearchItemSelected(mRegisteredPatientData.get(position).getPatient().getProfileId(),
                          mRegisteredPatientData.get(position).getName());

                  break;
              case SearchDialogAdapter.PATIENT_BILLED:
                  mListener.onSearchItemSelected(mRegisteredPatientData.get(position).getBillId(),
                          mRegisteredPatientData.get(position).getName());
                  break;
          }


        }

        getDialog().dismiss();

    }




   //Todo: filter results using single method
    @SuppressWarnings("unchecked")
    @Override
    public void doSearch(CharSequence s) {
        String searchString;

        Bundle bundle = new Bundle();
        searchString = "'%" + s.toString() + "%'";
        bundle.putString(QUERY, searchString);
        Log.d(LOG_TAG, "selectionArgs: "+searchString);
        getLoaderManager().restartLoader(Utility.UNIFORM_SEARCH_LOADER, bundle, this);
    }




    @Override
    public void clearSearch() {
        getDialog().dismiss();

    }

    @Override
    public void addItem(String string) {

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri searchUri = Uri.EMPTY;
        String[] projectionColumns = {""};
        String sortOrder = "";
        String[] selectionArgs = null;
        String selection = null;
        boolean isNumberQuery = false;

        //Since COLUMN_PATIENT_PHONE and COLUMN_PATIENT_NAME are same across tables, same selection args are used for searching
        // inside Registration table

        if (args!= null) {

            String str = args.getString(QUERY).replace("'","").replace("%","");
            Log.d(LOG_TAG, "is digits only: "+ TextUtils.isDigitsOnly(str)+" qq:- "+ str);
            if (TextUtils.isDigitsOnly(str)) {
                Log.d(LOG_TAG, "is digits only: ");
                selection = PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE + " LIKE " + args.getString(QUERY);
            }else if (str.contains("-")) {
                selection = PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME + " LIKE " + args.getString(QUERY);
            }else {
                selection = PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME + " LIKE " +args.getString(QUERY);

            }

        }


        switch (type) {
            case SearchDialogAdapter.PATIENT:
                searchUri = PharmacyContract.PatientEntry.CONTENT_URI;
                sortOrder = "datetime(" + PharmacyContract.PatientEntry.COLUMN_PATIENT_RECENT + ") DESC";
                projectionColumns = PATIENT_COLUMNS;
                break;
            case SearchDialogAdapter.PATIENT_QUEUED:
                searchUri = PharmacyContract.PatientRegistrationEntry.buildRegistrationTypeWithDate(Utility.PATIENT_QUEUED_QUERY, null,
                        Utility.getHoursBeforeDate(mDisplayTime));
                sortOrder = "datetime(" + PharmacyContract.PatientEntry.COLUMN_PATIENT_RECENT + ") DESC";
                projectionColumns = REGISTRATION_COLUMNS;
                break;
            case SearchDialogAdapter.PATIENT_BILLED:
                //display time changed to 0, to show all patients
                searchUri = PharmacyContract.BillsEntry.CONTENT_URI;

                sortOrder =  PharmacyContract.BillsEntry.COLUMN_UPDATED_DATE + " DESC ";
                projectionColumns = REGISTERED_PATIENT_BILL_COLUMNS;
                break;
        }

        Log.d(LOG_TAG, "search Query: "+searchUri+" selection: "+selection);



            return new CursorLoader(getActivity(),
                    searchUri,
                    projectionColumns,
                    selection,
                    null,
                    sortOrder);
        }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        Log.d(LOG_TAG, " cursor data: search " + DatabaseUtils.dumpCursorToString(data));

        switch (type) {

            case SearchDialogAdapter.PATIENT:

                Log.d(LOG_TAG, "inside onLoadFinsish Patient: ");
                ArrayList<PatientData> patientDataArrayList = new ArrayList<>();

                mPatientData.clear();

                int syncCount = 0;
                int totalCount = 0;


                while (data.moveToNext()) {

                    String name = data.getString(COL_PATIENT_NAME_SCOPE_ALL);
                    String number = data.getString(COL_PATIENT_PHONE_SCOPE_ALL);
                    int patientId = data.getInt(COL_PATIENT_ID_SCOPE_ALL);
                    float age = data.getFloat(COL_PATIENT_AGE_SCOPE_ALL);
                    String gender = data.getString(COL_PATIENT_SEX_SCOPE_ALL);
                    String relationship = data.getString(COL_PATIENT_RELATIONSHIP_SCOPE_ALL);
                    String photoUrl = data.getString(COL_PATIENT_IMAGE_SCOPE_ALL);
                    String ageType = data.getString(COL_PATIENT_AGE_TYPE_SCOPE_ALL);
                    String email = data.getString(COL_PATIENT_EMAIL_SCOPE_ALL);
                    String address = data.getString(COL_PATIENT_ADDRESS_SCOPE_ALL);
                    int webId = data.getInt(COL_PATIENT_WEB_ID_SCOPE_ALL);
                    String fatherName = data.getString(COL_PATIENT_FATHER_NAME_SCOPE_ALL);
                    String motherName = data.getString(COL_PATIENT_MOTHER_NAME_SCOPE_ALL);
                    String username = data.getString(COL_PATIENT_USERNAME_SCOPE_ALL);


                    Patient patient = new Patient();
                    patient.setName(name);
                    patient.setPhoneNumber(number);
                    patient.setProfileId(patientId);
                    patient.setGender(gender);
                    patient.setAge(age);
                    patient.setAgeType(ageType);
                    patient.setFatherName(fatherName);
                    patient.setMotherName(motherName);
                    patient.setPatientEmail(email);
                    patient.setPhotoUrl(photoUrl);
                    patient.setRelationship(relationship);
                    patient.setUsername(username);

                    Log.d(LOG_TAG, "patient phone: " + number);

                    PatientData patientData = new PatientData(name, SearchDialogAdapter.PATIENT_VIEW_TYPE);
                    patientData.setPatient(patient);

                    Log.d(LOG_TAG, "patient phone from patient data: " + patientData.getPhoneNumber());


                    patientDataArrayList.add(patientData);

//                        Patient patient =  new Patient(name,sex, age,ageType, number,relationship, patientId, webId,photoUrl);
//
//                        patientDataArrayList.add(new RegisteredPatientData(patient,token,regId,registrationDate,syncFlag==1,
//                                updateFlag==1,doctorName,doctorPhone,doctorSpecialization,doctorId, SearchPatientAdapter.SEARCHABLE_ITEM));
//                    Log.d(LOG_TAG,"Inside loader "+ name + " "+ number + " " + webId);

                }

                if (patientDataArrayList.size() > 0) {
                    mPatientData.addAll(patientDataArrayList);
                    Log.d(LOG_TAG, "phone number from data: " + mPatientData.get(0).getPhoneNumber());
                }
                break;

            case SearchDialogAdapter.PATIENT_QUEUED:
                ArrayList<RegisteredPatientData> registeredPatientDataArrayList = new ArrayList<>();

                mRegisteredPatientData.clear();

                while (data.moveToNext()) {
//                    Log.d(LOG_TAG,"size of dataList is jjjj "+ patientDatas.size() );


                    int regId = data.getInt(COL_REGISTRATION_ID);
                    String name = data.getString(COL_PATIENT_NAME);
                    String number = data.getString(COL_PATIENT_PHONE);
                    String username = data.getString(COL_PATIENT_USERNAME);
                    int patientId = data.getInt(COL_PATIENT_ID);

                    Log.d(LOG_TAG, "patientId: " + patientId);

                    Patient patient = new Patient();

                    patient.setName(name);
                    patient.setProfileId(patientId);
                    patient.setPhoneNumber(number);
                    patient.setUsername(username);

                    RegisteredPatientData registeredPatientData = new RegisteredPatientData(patient, SearchDialogAdapter.REGISTERED_PATIENT_VIEW_TYPE);
                    registeredPatientDataArrayList.add(registeredPatientData);

                }


                if (registeredPatientDataArrayList.size() != 0) {
                    mRegisteredPatientData.addAll(registeredPatientDataArrayList);
                }

                break;

            case SearchDialogAdapter.PATIENT_BILLED:
                ArrayList<RegisteredPatientData> patientRegisteredForBill = new ArrayList<>();

                mRegisteredPatientData.clear();
                ArrayList<RegisteredPatientData> registeredPatientData = new ArrayList<>();

                try {


                while (data.moveToNext()) {
                    int regId = data.getInt(COL_PAT_BILL_REG_ID);
                    int billId = data.getInt(COL_PAT_BILL_BILL_ID);
                    String name = data.getString(COL_PAT_BILL_PATIENT_NAME);
                    String number = data.getString(COL_PAT_BILL_PATIENT_PHONE);
                    int doctorId = data.getInt(COL_PAT_BILL_DOCTOR_ID);
                    int patientId = data.getInt(COL_PAT_BILL_PATIENT_ID);
                    int prescriptionSyncFlag = data.getInt(COL_PAT_BILL_REGISTRATION_PRESCRIPTION_FLAG);
                    String relationship = data.getString(COL_PAT_BILL_PATIENT_RELATIONSHIP);
                    String username = data.getString(COL_PAT_BILL_USERNAME);

                    Patient patient = new Patient();
                    patient.setName(name);
                    patient.setProfileId(patientId);
                    patient.setPhoneNumber(number);
                    patient.setRelationship(relationship);
                    patient.setUsername(username);



                    RegisteredPatientData registeredPatient = new RegisteredPatientData();
                    registeredPatient.setPatient(patient);
                    registeredPatient.setRegId(regId);
                    registeredPatient.setBillId(billId);
                    registeredPatient.setDoctorId(doctorId);
                    registeredPatient.setPrescriptionFlag(prescriptionSyncFlag == 1);
                    registeredPatient.setViewType(SearchDialogAdapter.REGISTERED_PATIENT_VIEW_TYPE);

                    registeredPatientData.add(registeredPatient);
                }

                if (registeredPatientData.size() != 0) {
                    mRegisteredPatientData.addAll(registeredPatientData);
                }
                break;

        }catch (NullPointerException e){
                e.printStackTrace();}
                finally {
                    if (data != null){
                        data.close();
                    }
                }
        }

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public void addData(){
        getLoaderManager().restartLoader(Utility.UNIFORM_SEARCH_LOADER, null,this);
    }

//
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnSearchDialogFragmentListener) {
            mListener = (OnSearchDialogFragmentListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchDialogFragmentListener) {
            mListener = (OnSearchDialogFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void closeDialog(){
        this.getDialog().dismiss();
    }




}
