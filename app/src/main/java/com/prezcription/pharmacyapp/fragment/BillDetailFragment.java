package com.prezcription.pharmacyapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.prezcription.pharmacyapp.ApiConfig;
import com.prezcription.pharmacyapp.BillCreationResponse;
import com.prezcription.pharmacyapp.R;
import com.prezcription.pharmacyapp.Utility;
import com.prezcription.pharmacyapp.activity.BillingActivity;
import com.prezcription.pharmacyapp.activity.OpeningActivity;
import com.prezcription.pharmacyapp.activity.SettingsActivity;
import com.prezcription.pharmacyapp.adapter.AvailableDoctorAdapter;
import com.prezcription.pharmacyapp.adapter.BillItemDetailAdapter;
import com.prezcription.pharmacyapp.adapter.DetailBillRecyclerAdapter;
import com.prezcription.pharmacyapp.custom.VolleySingleton;
import com.prezcription.pharmacyapp.data.PharmacyContract;
import com.prezcription.pharmacyapp.data.dto.BillData;
import com.prezcription.pharmacyapp.data.dto.BillItem;
import com.prezcription.pharmacyapp.data.dto.BillItemDetail;
import com.prezcription.pharmacyapp.data.dto.DocData;
import com.prezcription.pharmacyapp.data.dto.Patient;
import com.prezcription.pharmacyapp.data.gson.ItemInInventoryResponse;
import com.prezcription.pharmacyapp.dnssd.DNSSDHelper;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by amit on 02/12/17.
 */

public class BillDetailFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>, DetailBillRecyclerAdapter.OnFragmentInteractionListener,
        AvailableDoctorAdapter.OnDoctorSelectedListener{

    RecyclerView mRecyclerView;

    DetailBillRecyclerAdapter mAdapter;
    public static final String LOG_TAG = BillDetailFragment.class.getSimpleName();
    private TextView tv_totalPrice;
    //    private OnDialogInteractionListener mListener;
    private TextView tvPatientName, tvPayableAmount, tvPhoneNumber;
    private ImageView patientImageVIew;
    private Button done;

    int patientId;
    int regId;
    int doctorId;
    int billId;
    boolean billed;
    float mConsultationFee;

//    private float medicineDiscount = 0f;
//    private float vaccineDiscount = 0f;

    private float medicinePercentDiscount = 0f;
    private float vaccinePercentDiscount = 0f;

    private ArrayList<BillData> mBillDataArrayList;

    Patient mPatient;

    private OnBillItemClickListener mListener;

    SharedPreferences mSettingsPreferences;
    private ImageButton btnRegister;

//    OnFragmentInteractionListener mListener;
//
//    public interface OnFragmentInteractionListener{
//
//        void setTotalAmount(float amount);
//
//    }

    private BillData mBillDataDetail;

    public BillDetailFragment() {
    }

    public interface OnBillItemClickListener {
        void onItemClickScan(int localId, int itemType, int pos);

        void onBarCodeSet(String itemId, String batchId,String barcode, int localId, int itemType);

        void onNewBillsInserted();
    }

    public static BillDetailFragment newInstance() {
        return new BillDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bill_layout, container, false);

        tv_totalPrice = (TextView) view.findViewById(R.id.amount);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) view.findViewById(R.id.bill_toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        setHasOptionsMenu(true);

//        cancelDialogButton = (Button) view.findViewById(R.id.cancel_bill_dialog);
//        compositeBillButton = (Button) view.findViewById(R.id.composite_bill_btn);
        tvPatientName = (TextView) view.findViewById(R.id.patient_name);
//        tvPayableAmount = (TextView) view.findViewById(R.id.bill_amount);
//        patientImageVIew = (ImageView) view.findViewById(R.id.patient_image_view);
        tvPhoneNumber = (TextView) view.findViewById(R.id.patient_phone_number);
        mSettingsPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

//        ImageButton ibFetch = (ImageButton) view.findViewById(R.id.ib_fetch_bills);
//        ibFetch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                fetchBills();
//            }
//        });

//        btnRegister = (ImageButton) view.findViewById(R.id.btn_register);
//
//        btnRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(LOG_TAG, "inside register onclick");
//                if (DNSSDHelper.getInstance().isInitialised()) {
//                    if (Utility.isNetworkAvailable(getActivity())) {
//                        if (!DNSSDHelper.getInstance().isConnected()) {
//                            DNSSDHelper.getInstance().register(getActivity());
//                            Toast.makeText(getActivity(), "Registering on network, please wait..", Toast.LENGTH_LONG).show();
//                        }else {
//                            Log.d(LOG_TAG, "registration subscription not null");
//                            Toast.makeText(getActivity(), "Device already registered", Toast.LENGTH_LONG).show();
//                        }
//
//                        DNSSDHelper.getInstance().startBrowse(getActivity());
//
//                    }else {
//                        Toast.makeText(getActivity(), "Please check your Wifi connection", Toast.LENGTH_LONG).show();
//                    }
//                }else {
//                    Log.d(LOG_TAG, "registering on network");
//                    Toast.makeText(getActivity(), "Registering on network, please wait..",Toast.LENGTH_LONG).show();
//                    registerOnNetwork();
//                }
//
//
//
//            }
//        });

//        done = (Button) view.findViewById(R.id.btn_done);

//        done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getDialog().dismiss();
//            }
//        });


//        cancelDialogButton.setOnClickListener(this);
//        compositeBillButton.setOnClickListener(this);
//
//
//        formatData();
//
//        setPatientInfo();
////        Log.d(LOG_TAG, "invoiceDataList: "+mInvoiceDataFormattedArrayList.size());
//
//        mAdapter = new DetailBillRecyclerAdapter(mContext,mInvoiceDataFormattedArrayList, this);
//
//        Log.d(LOG_TAG, "invoice adpater: "+mAdapter.getItemCount());


        mBillDataArrayList = new ArrayList<>();

        mAdapter = new DetailBillRecyclerAdapter(getActivity(), mBillDataArrayList, this);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.bill_items_recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        registerOnNetwork();

        return view;


    }

    private void fetchBills(){
        //considering that app is connected to service

        Gson gson = new Gson();
        String savedDoctorListJson = getActivity().getSharedPreferences(Utility.DOCTOR_LIST_PREF, MODE_PRIVATE).getString(Utility.DOCTOR_LIST_KEY, Utility.DEFAULT_STRING_VALUE);

        Log.d(LOG_TAG, "saved doctor list is " + savedDoctorListJson);

        if (!savedDoctorListJson.equals(Utility.DEFAULT_STRING_VALUE)) {

            Type type = new TypeToken<ArrayList<DocData>>() {
            }.getType();

            ArrayList<DocData> docDatas = gson.fromJson(savedDoctorListJson, type);
            if (docDatas.size() > 1) {
//            docDatas.remove(0);
                showDialogToChooseDoc(docDatas);
            } else if (docDatas.size() == 1) {
                onDoctorSelected(docDatas.get(0).getPhone(), docDatas.get(0).getId());
            }
        }
//        for (DocData data : docDatas) {
//            if (data.)
//        }



    }

    @Override
    public void onDoctorSelected(String phoneNUmber,int docId) {
        Log.d(LOG_TAG,"phone number: "+phoneNUmber);

        if (phoneNUmber != null && DNSSDHelper.getInstance().isConnected()) {
            DNSSDHelper.getInstance().setDoctorServiceName(phoneNUmber);
//            DNSSDHelper.getInstance().setCallingContext(getActivity());
            boolean success = DNSSDHelper.getInstance().fetchBillForService(getActivity());
            if (success){
                Toast.makeText(getActivity(), "Bill items updated", Toast.LENGTH_SHORT).show();
                if (dialog != null) {
                    dialog.dismiss();
                }
//             mListener.onNewBillsInserted();
            }

        }else {
            Toast.makeText(getActivity(), "Connection broken..Registering device on network, Please Wait",Toast.LENGTH_LONG).show();
            DNSSDHelper.getInstance().register(getActivity());
            Log.d(LOG_TAG,  "phone number is null: ");
        }

    }

    MaterialDialog dialog;

    private void showDialogToChooseDoc(ArrayList<DocData> docData){

        AvailableDoctorAdapter mAdapter = new AvailableDoctorAdapter(getActivity(),this, docData);

        dialog =  new MaterialDialog.Builder(getActivity())
                .title(R.string.doctor_list)
                .content(R.string.available_doctor_list_content)
                .adapter(mAdapter, null)
                .positiveText("OK")
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
//                        getItemDetailFromServer(list, billId);
                    }
                })
                .show();
    }




//    private void setPatientInfo(int patientId) {
//
//        Log.d(LOG_TAG, "Patient id is " + patientId);
//
//        if (patientId != 0) {
//
//            Patient patient = new Patient();
//
//            String[] projections = {PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME,
//                    PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE,
//                    PharmacyContract.PatientEntry.COLUMN_PATIENT_SEX,
//                    PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE_TYPE,
//                    PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE};
//
//            String selection = PharmacyContract.PatientEntry._ID + " = "+ patientId;
//
//            Cursor cursor = getActivity().getContentResolver().query(PharmacyContract.PatientEntry.CONTENT_URI,projections,selection,null,null);
//
//            try {
//                while (cursor.moveToNext()) {
//
//                    patient.setName(cursor.getString(0));
//                    patient.setAge(cursor.getFloat(1));
//                    patient.setGender(cursor.getString(2));
//                    patient.setAgeType(cursor.getString(3));
//                    patient.setPhoneNumber(cursor.getString(4));
//                }
//            }finally {
//                if(cursor!=null)
//                    cursor.close();
//            }
//
//            tvPatientName.setText(patient.getName());
//            tvPhoneNumber.setText(patient.getPhoneNumber());
//
//        }
//    }

    public void setIds(int patientId, int regId, int doctorId, int billId) {
        this.patientId = patientId;
        this.regId = regId;
        this.doctorId = doctorId;
        this.billId = billId;
        getLoaderManager().restartLoader(Utility.BILL_DETAIL_LOADER, null, this);

    }

    public int getPatientId() {
        return patientId;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public ArrayList<BillData> getBillDataArrayList() {

        Iterator<BillData> itr = mBillDataArrayList.iterator();

        Log.d(LOG_TAG, "unfiltered bill data list" + mBillDataArrayList.size());

        // checking the next element availabilty
        while (itr.hasNext()) {
            //  moving cursor to next element
            BillData data = itr.next();
            if ((data.getItemId() == null || data.getItemId().equals("-1"))&&!(data.getViewType()==DetailBillRecyclerAdapter.CONSULTATION_HEADER)) {
                itr.remove();
                Log.d(LOG_TAG, "item removed: " + data.getName() + "item id: " + data.getItemId());
            }
        }
        Log.d(LOG_TAG, "filtered bill data list" + mBillDataArrayList.size());
        return mBillDataArrayList;
    }


    private static final String[] BILL_COLUMNS = {
            PharmacyContract.BillsEntry.TABLE_NAME + "." + PharmacyContract.BillsEntry._ID,
            PharmacyContract.BillsEntry.COLUMN_BILL_DATE,
            PharmacyContract.BillsEntry.COLUMN_CONSULTATION_FEE,
            PharmacyContract.BillsEntry.COLUMN_PAID_AMOUNT,
            PharmacyContract.BillsEntry.COLUMN_TOTAL_DISCOUNT_GIVEN,
            PharmacyContract.PatientRegistrationEntry.TABLE_NAME + "." + PharmacyContract.PatientRegistrationEntry._ID,
            PharmacyContract.PatientRegistrationEntry.COLUMN_DATE,
            PharmacyContract.PatientRegistrationEntry.COLUMN_SYNC_FLAG,
            PharmacyContract.PatientRegistrationEntry.COLUMN_TOKEN,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_NAME,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_AGE_TYPE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_SEX,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_PHONE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_EMAIL,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_ADDRESS,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_WEB_ID,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_RELATIONSHIP,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_IMAGE,
            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_NAME,
            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_SPECIALIZATION,
            PharmacyContract.DoctorEntry.COLUMN_DOCTOR_PHONE,
            PharmacyContract.PatientRegistrationEntry.COLUMN_DOCTOR_ID,
            PharmacyContract.PatientRegistrationEntry.COLUMN_PATIENT_ID,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_NAME,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_PRICE,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_DISCOUNT,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_NAME,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_PRICE,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_QUANTITY,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_DISCOUNT,
            PharmacyContract.MedicineInBillsEntry.TABLE_NAME + "." + PharmacyContract.MedicineInBillsEntry._ID,
            PharmacyContract.VaccineInBillsEntry.TABLE_NAME + "." + PharmacyContract.VaccineInBillsEntry._ID,
            PharmacyContract.BillsEntry.COLUMN_BILL_CREATED,
            PharmacyContract.MedicineInBillsEntry.COLUMN_ITEM_BILLED,
            PharmacyContract.VaccineInBillsEntry.COLUMN_ITEM_BILLED,
            PharmacyContract.BillsEntry.COLUMN_MEDICINE_DISCOUNT,
            PharmacyContract.BillsEntry.COLUMN_VACCINE_DISCOUNT,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_USERNAME,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ID,
            PharmacyContract.MedicineInBillsEntry.COLUMN_BATCH_ID,
            PharmacyContract.VaccineInBillsEntry.COLUMN_BATCH_ID,
            PharmacyContract.MedicineInBillsEntry.COLUMN_GLOBAL_BILL_ID,
            PharmacyContract.VaccineInBillsEntry.COLUMN_GLOBAL_BILL_ID,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MRP,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_MNF,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_CGST,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_SGST,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_EXPIRY,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AMOUNT,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_MRP,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_MNF,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_CGST,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_SGST,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_EXPIRY,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_AMOUNT,
            PharmacyContract.BillsEntry.COLUMN_GLOBAL_BILL_ID,
            PharmacyContract.BillsEntry.COLUMN_CENTER_GST,
            PharmacyContract.BillsEntry.COLUMN_STATE_GST,
            PharmacyContract.BillsEntry.COLUMN_PATIENT_USER_NAME,
            PharmacyContract.MedicineInBillsEntry.COLUMN_BAR_CODE,
            PharmacyContract.VaccineInBillsEntry.COLUMN_BAR_CODE,
            PharmacyContract.PatientEntry.COLUMN_PATIENT_DOB,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ITEM_GROSS,
            PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ITEM_GROSS,
            PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AVAILABLE_QUANTITY

    };

    private final int COL_BILL_ID = 0;
    private final int COL_BILL_DATE = 1;
    private final int COL_BILL_CONSULTATION_FEE = 2;
    private final int COL_BILL_PAID_AMOUNT = 3;
    private final int COL_BILL_TOTAL_DISCOUNT = 4;
    private final int COL_REGISTRATION_ID = 5;
    private final int COL_REGISTRATION_DATE = 6;
    private final int COL_REGISTRATION_SYNC_FLAG = 7;
    private final int COL_REGISTRATION_TOKEN = 8;
    private final int COL_PATIENT_NAME = 9;
    private final int COL_PATIENT_AGE = 10;
    private final int COL_PATIENT_AGE_TYPE = 11;
    private final int COL_PATIENT_SEX = 12;
    private final int COL_PATIENT_PHONE = 13;
    private final int COL_PATIENT_EMAIL = 14;
    private final int COL_PATIENT_ADDRESS = 15;
    private final int COL_PATIENT_WEB_ID = 16;
    private final int COL_PATIENT_RELATIONSHIP = 17;
    private final int COL_PATIENT_IMAGE = 18;
    private final int COL_DOCTOR_NAME = 19;
    private final int COL_DOCTOR_SPECIALIZATION = 20;
    private final int COL_DOCTOR_PHONE = 21;
    private final int COL_DOCTOR_ID = 22;
    private final int COL_PATIENT_ID = 23;
    private final int COL_MED_BILLS_NAME = 24;
    private final int COL_MED_BILLS_PRICE = 25;
    private final int COL_MED_BILLS_QUANTITY = 26;
    private final int COL_MED_BILLS_DISCOUNT = 27;
    private final int COL_VAC_BILLS_NAME = 28;
    private final int COL_VAC_BILLS_PRICE = 29;
    private final int COL_VAC_BILLS_QUANTITY = 30;
    private final int COL_VAC_BILLS_DISCOUNT = 31;
    private final int COL_MED_LOCAL_ID = 32;
    private final int COL_VAC_LOCAL_ID = 33;
    private final int COL_BILL_CREATED = 34;
    private final int COL_MED_ITEM_BILLED = 35;
    private final int COL_VACCINE_ITEM_BILLED = 36;
    private final int COL_MED_DISCOUNT = 37;
    private final int COL_VACCINE_DISCOUNT = 38;
    private final int COL_PATIENT_USERNAME = 39;
    private final int COL_MEDICINE_ITEM_ID = 40;
    private final int COL_VACCINE_ITEM_ID = 41;
    private final int COL_MEDICINE_BATCH_ID = 42;
    private final int COL_VACCINE_BATCH_ID = 43;
    private final int COL_MEDICINE_GLOBAL_BILL_ID = 44;
    private final int COL_VACCINE_GLOBAL_BILL_ID = 45;

    private final int COL_MEDICINE_MRP = 46;
    private final int COL_MEDICINE_MNF = 47;
    private final int COL_MEDICINE_CGST = 48;
    private final int COL_MEDICINE_SGST = 49;
    private final int COL_MEDICINE_EXPIRY = 50;
    private final int COL_MEDICINE_AMOUNT = 51;

    private final int COL_VACCINE_MRP = 52;
    private final int COL_VACCINE_MNF = 53;
    private final int COL_VACCINE_CGST = 54;
    private final int COL_VACCINE_SGST = 55;
    private final int COL_VACCINE_EXPIRY = 56;
    private final int COL_VACCINE_AMOUNT = 57;

    private final int COL_GLOBAL_BILL_ID = 58;
    private final int COL_BILL_CGST = 59;
    private final int COL_BILL_SGST = 60;
    private final int COL_BILL_PATIENT_USER_NAME = 61;
    private final int COL_MEDICINE_BAR_CODE = 62;
    private final int COL_VACCINE_BAR_CODE = 63;
    private final int COL_PATIENT_DOB = 64;
    private final int COL_MED_ITEM_GROSS = 65;
    private final int COL_VAC_ITEM_GROSS = 66;
    private final int COL_MED_ITEM_AVAIL_QTY = 67;



    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {

        Uri uri = PharmacyContract.BillsEntry.buildBillUriWithBillId(billId);

        return new CursorLoader(getActivity(),
                uri,
                BILL_COLUMNS,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {


        Log.d(LOG_TAG, "Cursor count is " + cursor.getCount());

        Set<String> medName = new HashSet<>();
        Set<String> batchNumberSet = new HashSet<>();
        Set<String> vaccName = new HashSet<>();
        boolean isFirst = true;
        HashMap<String, String> quantityHashMap = new HashMap<>();

        ArrayList<BillData> medicineBillDatas = new ArrayList<>();
        ArrayList<BillData> vaccineBillDatas = new ArrayList<>();

//        ArrayList<BillResponse.LineitemsBean> beanList = new ArrayList<>();

        float medPercentDiscount = 0f;
        float vaccPercentDiscount = 0f;

        float allMedicineAmount = 0f;
        float allVaccineAmount = 0f;


        boolean billCreated = false;

        boolean vaccineBillingEnabled = mSettingsPreferences.getBoolean(Utility.SETTINGS_VACCINE_BILLING, true);

        mBillDataArrayList.clear();

        float totalAmount = 0;

        while (cursor.moveToNext()) {

            if (isFirst) {

                float totalPaidAmount = cursor.getFloat(COL_BILL_PAID_AMOUNT);
                float discount = cursor.getFloat(COL_BILL_TOTAL_DISCOUNT);
                float consultationFees = cursor.getFloat(COL_BILL_CONSULTATION_FEE);
                String doctorName = cursor.getString(COL_DOCTOR_NAME);
                billCreated = cursor.getInt(COL_BILL_CREATED) == 1;
                billId = cursor.getInt(COL_BILL_ID);
                String docId = cursor.getString(COL_DOCTOR_ID);

                String billDate = cursor.getString(COL_BILL_DATE);
                String billNumber = cursor.getString(COL_GLOBAL_BILL_ID);
                double totlacGst = cursor.getDouble(COL_BILL_CGST);
                double totalsGst = cursor.getDouble(COL_BILL_SGST);
//                String patientUserNAme = cursor.getString(COL_BILL_PATIENT_USER_NAME);

                Log.d(LOG_TAG, "cGst:bill detail "+totlacGst);
                Log.d(LOG_TAG, "sGst:bill detail "+totalsGst);
                Log.d(LOG_TAG, "billDate:bill detail "+billDate);
                Log.d(LOG_TAG, "billNumber:bill detail "+billNumber);
                Log.d(LOG_TAG, "totalAmt:bill detail "+totalAmount);
                Log.d(LOG_TAG, "global id "+billNumber);


                String dob = cursor.getString(COL_PATIENT_DOB);

                Log.d(LOG_TAG, "patient dob: "+dob);




                setBilled(billCreated);


                Log.d(LOG_TAG, "bill id is " + billId);

                mPatient = new Patient();
                mPatient.setName(cursor.getString(COL_PATIENT_NAME));
                mPatient.setAge(cursor.getFloat(COL_PATIENT_AGE));
                mPatient.setGender(cursor.getString(COL_PATIENT_SEX));
                mPatient.setAgeType(cursor.getString(COL_PATIENT_AGE_TYPE));
                mPatient.setPhoneNumber(cursor.getString(COL_PATIENT_PHONE));
                mPatient.setUsername(cursor.getString(COL_PATIENT_USERNAME));
                mPatient.setPatientEmail(cursor.getString(COL_PATIENT_EMAIL));

                if (dob != null && !dob.equals("n/a")){
                    mPatient.setDob(dob);
                }else {
                    String dateofBirth = Utility.getDob(Utility.getNumberOfDaysFromAge(getActivity(),cursor.getFloat(COL_PATIENT_AGE),
                            cursor.getString(COL_PATIENT_AGE_TYPE)));
                    Log.d(LOG_TAG, "dob od patient: "+dob);
                    mPatient.setDob(dateofBirth);

                }


                Log.d(LOG_TAG,"Username: "+mPatient.getUsername());

                tvPatientName.setText(mPatient.getName());
                tvPhoneNumber.setText(mPatient.getPhoneNumber());

                medPercentDiscount = cursor.getFloat(COL_MED_DISCOUNT);
                vaccPercentDiscount = cursor.getFloat(COL_VACCINE_DISCOUNT);


                Log.d(LOG_TAG, "doc name is " + doctorName + " patient name " + mPatient.getName()
                        + " fees " + consultationFees + " total paid amount " + totalPaidAmount);


//                BillData doctorData = new BillData(doctorName,DetailBillRecyclerAdapter.DOCTOR);
//                mBillDataArrayList.add(doctorData);

                BillData consultationFeesData = new BillData("CONSULTATION FEES", DetailBillRecyclerAdapter.CONSULTATION_HEADER, consultationFees,
                        billCreated, DetailBillRecyclerAdapter.CONSULTATION_HEADER);
                mBillDataArrayList.add(consultationFeesData);

                if (billCreated) {
                    Log.d(LOG_TAG, "inside bill created:creating bill detail"+billNumber);
                    mBillDataDetail = new BillData();
                    mBillDataDetail.setDocId(docId);
                    mBillDataDetail.setPatientId(String.valueOf(patientId));
                    mBillDataDetail.setBillDate(billDate);
                    mBillDataDetail.setGlobalBillId(billNumber);
                    mBillDataDetail.setTotalDiscount(String.valueOf(discount));
                    mBillDataDetail.setTotalsGST(totalsGst);
                    mBillDataDetail.setTotalcGST(totlacGst);
                    mBillDataDetail.setTotalAmount(String.valueOf(totalPaidAmount));
                    mBillDataDetail.setConsultationFees(consultationFees);
                    mBillDataDetail.setViewType(DetailBillRecyclerAdapter.BILL_TOTAL_AMOUNT);
//                    mBillDataDetail.setItemType(DetailBillRecyclerAdapter.DETAILS);
//                    mBillDataDetail.setItemType(DetailBillRecyclerAdapter.DETAILS);

//                    mBillDataArrayList.add(billData);
                }

                totalAmount = totalAmount + consultationFees;

                BillData headerItem ;

                Log.d(LOG_TAG,"is bill created "+billCreated);

                if(billCreated){
                    headerItem = new BillData("Medicine", DetailBillRecyclerAdapter.BILL_HEADER_AFTER_BILL);
                }else {
                    headerItem = new BillData("Medicine", DetailBillRecyclerAdapter.BILL_HEADER);
                }

                mBillDataArrayList.add(headerItem);

                isFirst = false;

            }



            String medicineName = cursor.getString(COL_MED_BILLS_NAME);
            String batchNumber = cursor.getString(COL_MEDICINE_BATCH_ID);

            Log.d(LOG_TAG, "med bar code: "+cursor.getString(COL_MEDICINE_BAR_CODE));



            if (medicineName != null && medName.add(cursor.getString(COL_MED_BILLS_NAME))
                    || batchNumber != null ) {

                Log.d(LOG_TAG, "med name is " + medicineName);

                float price = cursor.getFloat(COL_MED_BILLS_PRICE);
                int availableQty = cursor.getInt(COL_MED_ITEM_AVAIL_QTY);
                float gross = cursor.getFloat(COL_MED_ITEM_GROSS);
                float discount = cursor.getFloat(COL_MED_BILLS_DISCOUNT);
                float quantity = cursor.getFloat(COL_MED_BILLS_QUANTITY);

                int localId = cursor.getInt(COL_MED_LOCAL_ID);
                boolean itemBilled = cursor.getInt(COL_MED_ITEM_BILLED) == 1;
                float medicineAmount = (price * quantity);
                allMedicineAmount += medicineAmount;
                totalAmount = totalAmount + medicineAmount;
                int viewType;
                if(billCreated){
                    viewType = DetailBillRecyclerAdapter.DISPLAY_ONLY;
                }else {
                    viewType = DetailBillRecyclerAdapter.MEDICINE;
                }
//                viewType = DetailBillRecyclerAdapter.MEDICINE;
                BillData data = new BillData(medicineName, viewType, localId, price, quantity, medicineAmount, itemBilled, billCreated, DetailBillRecyclerAdapter.MEDICINE);
//                Log.d(LOG_TAG, "medicine item id: " + cursor.getInt(COL_MEDICINE_ITEM_ID));
                Log.d(LOG_TAG, "medicine item id:string " + cursor.getString(COL_MEDICINE_ITEM_ID));
                Log.d(LOG_TAG, "medicine item name: " +medicineName);
                Log.d(LOG_TAG, "item gross: "+gross);
                if (cursor.getString(COL_MEDICINE_ITEM_ID) != null) {
                    data.setItemId(cursor.getString(COL_MEDICINE_ITEM_ID));
                }else {
                    data.setItemId("-1");
                }

                Log.d(LOG_TAG, "available qty: "+availableQty);

                if (availableQty == -1){
                    data.setAvailableQty(Integer.MAX_VALUE);
                }else {
                    data.setAvailableQty(availableQty);
                }
                data.setBatchId(cursor.getString(COL_MEDICINE_BATCH_ID));
//                Log.d(LOG_TAG, "medicine batch id: " + cursor.getString(COL_MEDICINE_BATCH_ID));
//                Log.d(LOG_TAG, "medicine cGst: " + cursor.getString(COL_MEDICINE_CGST));
//                Log.d(LOG_TAG, "medicine sGst: " + cursor.getString(COL_MEDICINE_SGST));
//                Log.d(LOG_TAG, "medicine amt: " + cursor.getString(COL_MEDICINE_AMOUNT));
//                Log.d(LOG_TAG, "medicine exp: " + cursor.getString(COL_MEDICINE_EXPIRY));
//                Log.d(LOG_TAG, "medicine dis: " + cursor.getString(COL_MED_DISCOUNT));
//                Log.d(LOG_TAG, "medicine rate: " + cursor.getString(COL_MED_BILLS_PRICE));
//                Log.d(LOG_TAG, "medicine name: " + cursor.getString(COL_MED_BILLS_NAME));
//                Log.d(LOG_TAG, "medicine global id:" + cursor.getString(COL_MEDICINE_GLOBAL_BILL_ID));

                data.setGlobalBillId(cursor.getString(COL_MEDICINE_GLOBAL_BILL_ID));
                data.setMrp(cursor.getString(COL_MEDICINE_MRP));
                data.setItemGross(cursor.getFloat(COL_MED_ITEM_GROSS));
                data.setMnf(cursor.getString(COL_MEDICINE_MNF));
                data.setItemcGst(cursor.getString(COL_MEDICINE_CGST));
                data.setItemsGst(cursor.getString(COL_MEDICINE_SGST));
                data.setItemNetAmt(cursor.getString(COL_MEDICINE_AMOUNT));
                data.setExpiry(cursor.getString(COL_MEDICINE_EXPIRY));
                data.setItemDisc(cursor.getString(COL_MED_DISCOUNT));
                data.setNetRate(cursor.getString(COL_MED_BILLS_PRICE));
                data.setItemName(cursor.getString(COL_MED_BILLS_NAME));
                data.setDiscountMedicine(discount);
                if (cursor.getString(COL_MEDICINE_BAR_CODE) != null) {
                    data.setBarCode(cursor.getString(COL_MEDICINE_BAR_CODE));
                }

                if (cursor.getString(COL_MEDICINE_ITEM_ID) != null) {
                    quantityHashMap.put(cursor.getString(COL_MEDICINE_ITEM_ID),String.valueOf(quantity));
                }


//                if (cursor.getString(COL_MEDICINE_BATCH_ID) != null) {
//                    data.setBarCode(cursor.getString(COL_MEDICINE_BATCH_ID) + cursor.getString(COL_MEDICINE_ITEM_ID));
//                }
                medicineBillDatas.add(data);
            }


            String vaccineName = cursor.getString(COL_VAC_BILLS_NAME);

            if (vaccineName != null && vaccName.add(cursor.getString(COL_VAC_BILLS_NAME)) && vaccineBillingEnabled) {


                Log.d(LOG_TAG, "vacc name is " + vaccineName);
                float price = cursor.getFloat(COL_VAC_BILLS_PRICE);
                float discount = cursor.getFloat(COL_VAC_BILLS_DISCOUNT);
                float quantity = cursor.getFloat(COL_VAC_BILLS_QUANTITY);
                int localId = cursor.getInt(COL_VAC_LOCAL_ID);
                boolean itemBilled = cursor.getInt(COL_VACCINE_ITEM_BILLED) == 1;
                float vaccineAmount = (price * quantity);
                allVaccineAmount += vaccineAmount;
                totalAmount = totalAmount + vaccineAmount;
                int viewType;
                if(billCreated){
                    viewType = DetailBillRecyclerAdapter.DISPLAY_ONLY;
                }else {
                    viewType = DetailBillRecyclerAdapter.VACCINATION;
                }
//                viewType = DetailBillRecyclerAdapter.VACCINATION;
                BillData data = new BillData(vaccineName, viewType, localId, price, quantity, vaccineAmount, itemBilled, billCreated, DetailBillRecyclerAdapter.VACCINATION);
                Log.d(LOG_TAG, "vaccine item id: " + cursor.getInt(COL_VACCINE_ITEM_ID));
                data.setItemId(cursor.getString(COL_VACCINE_ITEM_ID));
                data.setBatchId(cursor.getString(COL_VACCINE_BATCH_ID));
//                if (cursor.getString(COL_VACCINE_BATCH_ID) != null) {
//                    data.setBarCode(cursor.getString(COL_VACCINE_BATCH_ID) + cursor.getString(COL_VACCINE_ITEM_ID));
//                }
                data.setGlobalBillId(cursor.getString(COL_VACCINE_GLOBAL_BILL_ID));
                data.setMrp(cursor.getString(COL_VACCINE_MRP));
                data.setItemGross(cursor.getFloat(COL_VAC_ITEM_GROSS));
                data.setMnf(cursor.getString(COL_VACCINE_MNF));
                data.setItemcGst(cursor.getString(COL_VACCINE_CGST));
                data.setItemsGst(cursor.getString(COL_VACCINE_SGST));
                data.setItemNetAmt(cursor.getString(COL_VACCINE_AMOUNT));
                data.setExpiry(cursor.getString(COL_VACCINE_EXPIRY));
                data.setItemDisc(cursor.getString(COL_VACCINE_DISCOUNT));
                data.setNetRate(cursor.getString(COL_VAC_BILLS_PRICE));
                data.setItemName(cursor.getString(COL_VAC_BILLS_NAME));

                if (cursor.getString(COL_VACCINE_BAR_CODE) != null) {
                    data.setBarCode(cursor.getString(COL_VACCINE_BAR_CODE));
                }
                vaccineBillDatas.add(data);
            }


        }

        boolean billableItemsPresent = false;



        if (medicineBillDatas.size() > 0) {
            billableItemsPresent = true;
            mBillDataArrayList.addAll(medicineBillDatas);
        }


        if (vaccineBillDatas.size() > 0) {
//            billableItemsPresent = true;
            mBillDataArrayList.addAll(vaccineBillDatas);
        }

        Log.d(LOG_TAG, " Bill data size " + mBillDataArrayList.size());

        if (mBillDataArrayList.size() > 0) {
            if(billCreated) {
                mBillDataArrayList.add(mBillDataDetail);
            }
            mAdapter.notifyDataSetChanged();
        }

        checkItemsAvailabity(billableItemsPresent);


        Log.d(LOG_TAG, " total amount is  " + totalAmount);


    }

    private void checkItemsAvailabity(boolean billableItemsPresent){


        if (doctorId != -1 && billableItemsPresent) {
            if (Utility.isNetworkAvailable(getActivity()) ) {

                RequestQueue requestQueue;
                StringRequest stringRequest;
                final String VOLLEY_REQUEST_ITEM_AVAIL = "item availability";


//                requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

                String checkItemUrl = ApiConfig.DoctorApi.DOCTOR_INVENTORY_CHECK;
//                String checkItemUrl = "http://192.168.0.167:3000/inventories/query";

                Uri.Builder itemCheckBuilder = Uri.parse(checkItemUrl).buildUpon().appendQueryParameter("doc", String.valueOf(doctorId));

                for (int i=0; i<mBillDataArrayList.size(); i++){
                    if (mBillDataArrayList.get(i).getViewType() == DetailBillRecyclerAdapter.MEDICINE ||
                            mBillDataArrayList.get(i).getViewType() == DetailBillRecyclerAdapter.VACCINATION){
                        itemCheckBuilder.appendQueryParameter("items[][itemId]",mBillDataArrayList.get(i).getItemId());
                    }
                }


                final String itemCheckUrl = itemCheckBuilder.build().toString();
                Log.d(LOG_TAG, "urlString: " + itemCheckUrl);

                stringRequest = new StringRequest(Request.Method.GET, itemCheckUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(LOG_TAG, "response from server: item check" + response);

                        try {
                            Gson gson = new Gson();
                            ItemInInventoryResponse itemInInventoryResponse = gson.fromJson(response, ItemInInventoryResponse.class);

                            HashMap<String, Boolean> availabilityMap = new HashMap<>();
                            List<ItemInInventoryResponse.LineitemsBean> list = itemInInventoryResponse.getLineitems();

                            for (int i=0; i<list.size(); i++){
                                availabilityMap.put(list.get(i).getItemId(),list.get(i).isAvailable());
                            }
                            setItemsAvailability(availabilityMap);

                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(LOG_TAG, "Some error in item detail post" + error);

                    }
                });

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                stringRequest.setTag(VOLLEY_REQUEST_ITEM_AVAIL);

                VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);


            } else {
                Toast.makeText(getActivity(), "Please connect to internet to fetch latest data", Toast.LENGTH_SHORT).show();

            }
        }
//        else {
//            Toast.makeText(getActivity(), "doctor id is not valid, try again", Toast.LENGTH_SHORT).show();
//
//        }



    }

    private void setItemsAvailability(HashMap<String,Boolean> availabilityMap){

        for (int j=0; j<mBillDataArrayList.size(); j++){
            if (mBillDataArrayList.get(j).getViewType() == DetailBillRecyclerAdapter.MEDICINE ||
                    mBillDataArrayList.get(j).getViewType() == DetailBillRecyclerAdapter.VACCINATION){
                Boolean available = availabilityMap.get(mBillDataArrayList.get(j).getItemId());
                if (available != null && available){
                    mBillDataArrayList.get(j).setAvailableOnline(true);
                }else {
                    mBillDataArrayList.get(j).setAvailableOnline(false);
                }
                mAdapter.notifyItemChanged(j);
            }
        }

    }


    @Override
    public void onLoaderReset(Loader loader) {

    }

    @Override
    public void onConsultationEdit() {

    }

    @Override
    public void onMedicineItemClicked() {

    }

    @Override
    public void onItemClickForScan(int localId, int itemType, int pos) {
        mListener.onItemClickScan(localId, itemType, pos);
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public boolean isBilled() {
        return billed;
    }

    public void setBilled(boolean billed) {
        this.billed = billed;
    }

    public void refreshAdapter() {
        getLoaderManager().restartLoader(Utility.BILL_DETAIL_LOADER, null, this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BillingActivity) {
            mListener = (OnBillItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof BillingActivity) {
            mListener = (OnBillItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    public float getMedicineDiscount() {
        return medicinePercentDiscount;
    }

    public float getVaccineDiscount() {
        return vaccinePercentDiscount;
    }


    public boolean isValid() {
        boolean isValid = false;
        String message ="";

        //considering itemid to be 5 digit and batch id of 4 digits

        for (BillData data : mBillDataArrayList) {
            if (data.getViewType() == DetailBillRecyclerAdapter.MEDICINE) {
                Log.d(LOG_TAG, "qty: " + data.getQuantity() + "barcode: " + data.getBarCode());

                if (!data.isItemBilled()){
                    isValid = true;
                }else {
                    try {
                        if ((data.getQuantity() != 0 && data.getBarCode().length() >= 9)) {
                            isValid = true;
                        } else {
                            Log.d(LOG_TAG, "quantity: " + data.getQuantity());
                            if (!(data.getQuantity() > 0)) {
                                message = "Action Required: Item quantity can not be 0 for "+data.getName();
                            }
                            if (data.getBarCode().length() < 9) {
                                message = "Action Required: Item barcode length must be equal to 9 for "+data.getName();
                            }

                            isValid = false;
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }catch (NullPointerException e){
                        e.printStackTrace();
                        isValid = false;
                        Toast.makeText(getActivity(), "Action Required: Incorrect bar code for item "+data.getName(), Toast.LENGTH_LONG).show();
                        break;
                    }
                }

            } else if (data.getViewType() == DetailBillRecyclerAdapter.VACCINATION) {
                if (!data.isItemBilled()) {
                    isValid = true;
                } else {
                    try {
                        if ((data.getQuantity() != 0 && data.getBarCode().length() >= 9)) {
                            isValid = true;
                        } else {
                            Log.d(LOG_TAG, "quantity: " + data.getQuantity());
                            if (!(data.getQuantity() > 0)) {
                                message = "Action Required: Item quantity can not be 0 for "+data.getName();
                            }
                            if (data.getBarCode().length() < 9) {
                                message = "Action Required: Item barcode length must be equal to 9 "+data.getName();
                            }

                            isValid = false;
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            break;
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        isValid = false;
                        Toast.makeText(getActivity(), "Action Required: Item barcode length must be equal to 9 for"+data.getName(), Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }else if (data.getViewType() == DetailBillRecyclerAdapter.CONSULTATION_HEADER){
                isValid = true;
            }
        }

        return isValid;
    }

    public void updateBillItem(String itemId, String batchId,String barcode, int localId, int itemType) {

        Log.d(LOG_TAG, "item id to update" + itemId + "batch Id" + batchId + "localId: " + localId);

        //Todo: identify and add for vaccine item as well
        if (itemType == DetailBillRecyclerAdapter.MEDICINE) {

            String selection = PharmacyContract.MedicineInBillsEntry._ID + " =? ";
            String[] selectionArgs = new String[]{String.valueOf(localId)};

            ContentValues contentValues = new ContentValues();
            contentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BATCH_ID, batchId);
            contentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID, itemId);
            contentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BAR_CODE, barcode);

            int rowsUpdated = getActivity().getContentResolver().update(
                    PharmacyContract.MedicineInBillsEntry.CONTENT_URI,
                    contentValues,
                    selection,
                    selectionArgs);

            Log.d(LOG_TAG, "rows updated:medi " + rowsUpdated);
        } else {

            String selection = PharmacyContract.VaccineInBillsEntry._ID + " =? ";
            String[] selectionArgs = new String[]{String.valueOf(localId)};

            ContentValues contentValues = new ContentValues();
            contentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_BATCH_ID, batchId);
            contentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_VACCINE_ID, itemId);
            contentValues.put(PharmacyContract.VaccineInBillsEntry.COLUMN_BAR_CODE, barcode);

            int vacRowsUpdated = getActivity().getContentResolver().update(
                    PharmacyContract.VaccineInBillsEntry.CONTENT_URI,
                    contentValues,
                    selection,
                    selectionArgs);

            Log.d(LOG_TAG, "rows updated:vaccine " + vacRowsUpdated);

        }


    }

    public void  updateBarCode(int pos, String barCode) {
        Log.d(LOG_TAG, "position to change item");
        if (pos != -1) {
            mBillDataArrayList.get(pos).setBarCode(barCode);
            mAdapter.notifyItemChanged(pos);
        }
    }

    public ArrayList<BillItem> getBillItems() {

        ArrayList<BillItem> list = new ArrayList<>();
        for (int i = 0; i < mBillDataArrayList.size(); i++) {
            if (mBillDataArrayList.get(i).getItemId() != null && !mBillDataArrayList.get(i).getItemId().equals("-1")
                    && mBillDataArrayList.get(i).getQuantity() != 0 && mBillDataArrayList.get(i).isItemBilled()) {
                BillItem item = new BillItem();
                item.setBatchId(mBillDataArrayList.get(i).getBatchId());
                item.setItemId(mBillDataArrayList.get(i).getItemId());
                item.setItemCount((int) mBillDataArrayList.get(i).getQuantity());
                item.setName(mBillDataArrayList.get(i).getItemName());
                list.add(item);
            }
        }

        return list;

    }

    private List<BillItemDetail.ItemsBean> getItemBeanList() {
        ArrayList<BillItemDetail.ItemsBean> list = new ArrayList<>();

        for (int i = 0; i < mBillDataArrayList.size(); i++) {
            BillItemDetail.ItemsBean itemBean = new BillItemDetail.ItemsBean();
            itemBean.setItemId(String.valueOf(mBillDataArrayList.get(i).getItemId()));
            itemBean.setBatchId(String.valueOf(mBillDataArrayList.get(i).getBatchId()));
            itemBean.setItemCount(String.valueOf(mBillDataArrayList.get(i).getQuantity()));
            list.add(itemBean);
        }

        return list;
    }

    @Override
    public void onBarCodeSet(String itemId, String batchId,String barcode, int localId, int itemType) {
        mListener.onBarCodeSet(itemId, batchId,barcode, localId, itemType);
    }

    public ArrayList<BillData> getBillDataAfterBilled() {


        ArrayList<BillData> billDataArrayList = new ArrayList<>();
        ArrayList<BillCreationResponse.LineitemsBean> beanArrayList = new ArrayList<>();

        BillData billData = new BillData();

        billData.setBillDate(mBillDataDetail.getBillDate());
        billData.setGlobalBillId(mBillDataDetail.getGlobalBillId());
        billData.setTotalcGST((mBillDataDetail.getTotalcGST()));
        billData.setTotalsGST((mBillDataDetail.getTotalsGST()));
//        billData.setCustomerId(mBillDataDetail.getCustomerId());
        billData.setTotalDiscount(String.valueOf(mBillDataDetail.getTotalDiscount()));
        billData.setTotalAmount(String.valueOf(mBillDataDetail.getTotalAmount()));
        billData.setConsultationFees(mBillDataDetail.getConsultationFees());
        billData.setItemType(DetailBillRecyclerAdapter.DETAILS);
        billData.setPatientId(mBillDataDetail.getPatientId());
        billData.setDocId(mBillDataDetail.getDocId());

        for (int i = 0; i < mBillDataArrayList.size(); i++) {

            if ( mBillDataArrayList.get(i).getGlobalBillId() != null && mBillDataArrayList.get(i).getItemType() == DetailBillRecyclerAdapter.MEDICINE ) {
                Log.d(LOG_TAG,   " med global id not null");
                String medGlobalBillId = mBillDataArrayList.get(i).getGlobalBillId();
                String billGlobalId = mBillDataDetail.getGlobalBillId();
                Log.d(LOG_TAG, "is med have global id"+medGlobalBillId.equals(billGlobalId));
            }



            if (mBillDataArrayList.get(i).getItemType() == DetailBillRecyclerAdapter.MEDICINE &&
                    mBillDataArrayList.get(i).getGlobalBillId() != null){

                BillCreationResponse.LineitemsBean bean = new BillCreationResponse.LineitemsBean();

                Log.d(LOG_TAG, "inside get BillData after billed:"+ mBillDataArrayList.get(i).getItemName());
                Log.d(LOG_TAG, "inside get BillData after billed:"+(Double.valueOf(mBillDataArrayList.get(i).getItemcGst())));
                Log.d(LOG_TAG, "inside get BillData after billed:"+Double.valueOf(mBillDataArrayList.get(i).getItemsGst()));
                Log.d(LOG_TAG, "inside get BillData after billed:"+Double.valueOf(mBillDataArrayList.get(i).getMrp()));
                Log.d(LOG_TAG, "inside get BillData after billed:"+mBillDataArrayList.get(i).getMnf());
                Log.d(LOG_TAG, "inside get BillData after billed:"+mBillDataArrayList.get(i).getExpiry());
                Log.d(LOG_TAG, "inside get BillData after billed:"+Double.valueOf(mBillDataArrayList.get(i).getItemNetAmt()));
                Log.d(LOG_TAG, "inside get BillData after billed:"+mBillDataArrayList.get(i).getQuantity());
                Log.d(LOG_TAG, "inside get BillData after billed:"+Double.valueOf(mBillDataArrayList.get(i).getNetRate()));

                bean.setItemid(mBillDataArrayList.get(i).getItemId());
                bean.setBatchid(mBillDataArrayList.get(i).getBatchId());
                bean.setItemName(mBillDataArrayList.get(i).getItemName());
                bean.setCGST((Double.valueOf(mBillDataArrayList.get(i).getItemcGst())));
                bean.setSGST(Double.valueOf(mBillDataArrayList.get(i).getItemsGst()));
                bean.setMrp(Double.valueOf(mBillDataArrayList.get(i).getMrp()));
                bean.setItemGross(mBillDataArrayList.get(i).getItemGross());
                bean.setMfgr(mBillDataArrayList.get(i).getMnf());
                bean.setExpiry(mBillDataArrayList.get(i).getExpiry());
                bean.setNetAmt(Double.valueOf(mBillDataArrayList.get(i).getItemNetAmt()));
                bean.setItemCount(mBillDataArrayList.get(i).getQuantity());
                bean.setNetRate(Double.valueOf(mBillDataArrayList.get(i).getNetRate()));

                beanArrayList.add(bean);
            }else if (mBillDataArrayList.get(i).getItemType() == DetailBillRecyclerAdapter.VACCINATION &&
                    mBillDataArrayList.get(i).getGlobalBillId() != null){

                BillCreationResponse.LineitemsBean bean = new BillCreationResponse.LineitemsBean();


                bean.setItemid(mBillDataArrayList.get(i).getItemId());
                bean.setBatchid(mBillDataArrayList.get(i).getBatchId());
                bean.setCGST((Double.valueOf(mBillDataArrayList.get(i).getItemcGst())));
                bean.setSGST(Double.valueOf(mBillDataArrayList.get(i).getItemsGst()));
                bean.setMrp(Double.valueOf(mBillDataArrayList.get(i).getMrp()));
                bean.setItemGross(mBillDataArrayList.get(i).getItemGross());
                bean.setMfgr(mBillDataArrayList.get(i).getMnf());
                bean.setExpiry(mBillDataArrayList.get(i).getExpiry());
                bean.setNetAmt(Double.valueOf(mBillDataArrayList.get(i).getItemNetAmt()));
                bean.setItemCount(mBillDataArrayList.get(i).getQuantity());
                bean.setNetRate(Double.valueOf(mBillDataArrayList.get(i).getNetRate()));
                bean.setItemName(mBillDataArrayList.get(i).getItemName());

                beanArrayList.add(bean);

            }
        }

        billData.setLineitemsBean(beanArrayList);
        billDataArrayList.add(billData);


        if(mBillDataDetail.getConsultationFees()!=0){

            Log.d(LOG_TAG,"Consultation fees is "+mBillDataDetail.getConsultationFees() );

            BillData consultData = new BillData();
            consultData.setViewType(DetailBillRecyclerAdapter.CONSULTATION_HEADER);
            consultData.setPrice((float) mBillDataDetail.getConsultationFees());
            consultData.setConsultationFees(mBillDataDetail.getConsultationFees());

            billDataArrayList.add(consultData);

        }

        return billDataArrayList;
    }

    public float getConsultationFees(){
        for (int i=0;i<mBillDataArrayList.size();i++){
            if(mBillDataArrayList.get(i).getViewType()==DetailBillRecyclerAdapter.CONSULTATION_HEADER){
                return mBillDataArrayList.get(i).getPrice();
            }
        }
        return 0;
    }

    public Patient getPatient() {
        return mPatient;
    }

    public BillData getBillDetail(){
        return mBillDataDetail;

    }

    public boolean checkIfCorrectItemIsScanned(int localId, String itemId){
        Log.d(LOG_TAG, "item id to be checked: "+itemId);


        for (int i=0; i <mBillDataArrayList.size(); i++){
            Log.d(LOG_TAG, "item id of bill data: "+mBillDataArrayList.get(i).getItemId());
            Log.d(LOG_TAG, "item name to be checked: "+mBillDataArrayList.get(i).getName());

            if (mBillDataArrayList.get(i).getLocalId() == localId &&
                    mBillDataArrayList.get(i).getItemId().equals(itemId)) {
                return true;
            }
        }
        return false;
    }

    private void registerOnNetwork() {

        if (Utility.isNetworkAvailable(getActivity())) {
            if(!DNSSDHelper.getInstance().isInitialised()) {
                Log.d(LOG_TAG,"intializing dns server");
                DNSSDHelper.getInstance().initialize(getActivity(), null);
            }
            if (!DNSSDHelper.getInstance().isConnected()) {
                Log.d(LOG_TAG, "register process start");
                DNSSDHelper.getInstance().register(getActivity());
            } else {
                Log.d(LOG_TAG, "device already registered");
            }
            DNSSDHelper.getInstance().startBrowse(getActivity());

        }else {
            Toast.makeText(getActivity(), "Please check your Wifi connection", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_bill_detail, menu);
        btnRegister = (ImageButton) menu.findItem(R.id.service_flag).getActionView();
        super.onCreateOptionsMenu(menu, inflater);

    }



    public void setServiceFlagVisibility(boolean connected){
        Log.d(LOG_TAG, "connected: "+connected);

        if (connected){
            btnRegister.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.green_500));
        }else {
            btnRegister.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.gray));
        }

    }

//    public boolean isOnlyConsultation(){
//        Log.d(LOG_TAG, "inside is only");
//        boolean value = false;
//        for (BillData data : mBillDataArrayList){
//            Log.d(LOG_TAG, "data name: "+data.getItemName());
//            if ((data.getViewType() == DetailBillRecyclerAdapter.VACCINATION ||
//                    data.getViewType() == DetailBillRecyclerAdapter.MEDICINE) ){
//                value = false;
//                break;
//            }else {
//                value = true;
//            }
//        }
//        return value;
//    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.service_flag:
                if (DNSSDHelper.getInstance().isInitialised()) {
                    if (Utility.isNetworkAvailable(getActivity())) {
                        if (!DNSSDHelper.getInstance().isConnected()) {
                            DNSSDHelper.getInstance().register(getActivity());
                            Toast.makeText(getActivity(), "Registering on network, please wait..", Toast.LENGTH_LONG).show();
                        }else {
                            Log.d(LOG_TAG, "registration subscription not null");
                            Toast.makeText(getActivity(), "Device already registered", Toast.LENGTH_LONG).show();
                        }

                        DNSSDHelper.getInstance().startBrowse(getActivity());

                    }else {
                        Toast.makeText(getActivity(), "Please check your Wifi connection", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Log.d(LOG_TAG, "registering on network");
                    Toast.makeText(getActivity(), "Registering on network, please wait..",Toast.LENGTH_LONG).show();
                    registerOnNetwork();
                }
                return true;

            case R.id.fetch_bills:
                fetchBills();
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_log_out:
                getLogoutResponse();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ProgressDialog progressBar;


    public String getLogoutResponse(){

        final SharedPreferences userpreferences = getActivity().getSharedPreferences(Utility.USER_LOG_PREF,MODE_PRIVATE);

        final int mHospitalId = userpreferences.getInt(Utility.HOSPITAL_ID_KEY,0);

        final String LOG_TAG = "Anubhav";



//        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest sr = new StringRequest(Request.Method.POST, ApiConfig.LOGOUT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(LOG_TAG, "Inside onResponse ------  " + response);


                if(response.equals("Ok")){
//                    SharedPreferences sharedPreferences = getSharedPreferences(Utility.USER_LOG_PREF,MODE_PRIVATE);
                    SharedPreferences.Editor editor = userpreferences.edit();
                    editor.putBoolean(Utility.LOG_KEY,false).apply();
                    Intent intent = new Intent(getActivity(),OpeningActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Toast.makeText(getActivity(),"Logout sucessfull, please try to login again",Toast.LENGTH_SHORT).show();
                    getActivity().finish();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.getVolleyErrorResponse(error,getActivity(),progressBar);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("DOCTOR_ID",String.valueOf(mHospitalId));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(sr);
        return "";

    }

    public void addNewItem(BillItem item){

        float initialMedQuantity = getItemQuantity(item.getItemId(),item.getAvailableCount());

        Log.d(LOG_TAG, "initailMedQuant "+initialMedQuantity);

        float updatedQuantity = initialMedQuantity - item.getAvailableCount();

        Log.d(LOG_TAG, "updatedQuant "+updatedQuantity);

        Uri medUpdateUri = PharmacyContract.MedicineInBillsEntry.CONTENT_URI;
        ContentValues contentValues = new ContentValues();

        if (initialMedQuantity != -1) {
            contentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, updatedQuantity);
            contentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AVAILABLE_QUANTITY, updatedQuantity);
        }
        else {
            contentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_AVAILABLE_QUANTITY, 0);
            contentValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, 0);
        }

        String where = PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID + " = ? AND " +
                PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID + " = ?";

        String whereArgs [] = new String[]{String.valueOf(billId),item.getItemId()};

        getActivity().getContentResolver().update(medUpdateUri,contentValues,where,whereArgs);

        Log.d(LOG_TAG, "inserting item: med item id"+item.getItemId()+ "batch "+item.getBatchId());

        ContentValues medValues = new ContentValues();

        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_NAME, item.getName());
        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_PRICE, item.getNetRate());

//        if ((!Utility.checkDoseExist(medicine.getRoute()))|| Utility.checkIfDefaultQuantity(medicine.getRoute())) {
//            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, 1);
//            lastQuantity = 1;
//        } else {
//            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, medicine.getMedicineQuantity().totalQuantity());
//            lastQuantity = medicine.getMedicineQuantity().totalQuantity();
//        }

        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_QUANTITY, item.getAvailableCount());

        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BILL_ID, billId);
        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BATCH_ID,item.getBatchId());
        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_DISCOUNT, 0);
        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_MEDICINE_ID, item.getItemId());
        if (!item.getBarcode().equals("")) {
            medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_BAR_CODE,item.getBarcode());
        }
        medValues.put(PharmacyContract.MedicineInBillsEntry.COLUMN_ITEM_BILLED, 1);

        Uri uri = getActivity().getContentResolver().insert(PharmacyContract.MedicineInBillsEntry.CONTENT_URI,
                medValues);




        refreshAdapter();

    }

   //update item quantity to accomodate new item
    private float getItemQuantity(String itemId, int itemQuantityToSubtract){
//        int positionToUpdate = -1;
        for (int i=0; i<mBillDataArrayList.size(); i++){
         if (mBillDataArrayList.get(i).getItemId() != null &&
                 mBillDataArrayList.get(i).getItemId().equals(itemId)){
             if (mBillDataArrayList.get(i).getQuantity() > itemQuantityToSubtract)
                 return mBillDataArrayList.get(i).getQuantity();

//             positionToUpdate = i;
         }
        }

        return -1;


    }





}

