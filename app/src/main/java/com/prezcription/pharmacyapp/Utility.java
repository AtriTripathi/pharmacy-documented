package com.prezcription.pharmacyapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.joda.time.PeriodType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

//import static com.google.android.gms.wallet.LineItem.Role.REGULAR;

public class Utility {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 112;

    public static final int BILL_SEARCH_LOADER = 8;
    public static final int BILL_DETAIL_LOADER = 9;
    public static final int UNIFORM_SEARCH_LOADER = 7;
    public static final int DOCTOR_SEARCH_LOADER = 11;



    public static final int PATIENT_QUEUED_QUERY = 10;

    public static final int ASSIGNED_DOCTOR_ID_AND_DATE = 6;
    public static final String NOT_AVAILABLE = "n/a";


    public static String DEFAULT_STRING_VALUE = "default_string_value";

    // Doctor list pref
    public static final String DOCTOR_LIST_PREF = "doctor_list_pref";
    public static final String DOCTOR_LIST_KEY = "doctor_list_key";


    public static final String SETTINGS_PDF_ALLOW_ORGANISATION_DETAILS = "allow_pdf_organisation_details";
    public static final String SETTINGS_PDF_ORGANISATION_NAME = "pdf_clinic_or_hospital_name";
    public static final String SETTINGS_PDF_WEBSITE_NAME = "pdf_website_name";
    public static final String SETTINGS_PDF_ADDRESS = "pdf_address";
    public static final String SETTINGS_PDF_ALLOW_RX_SYMBOL = "allow_rx_symbol";
    public static final String SETTINGS_PDF_SIZE_TYPE = "pdf_size_type";
    public static final String SETTINGS_PDF_FONT_TYPE = "pdf_font_type";
    public static final String SETTINGS_PDF_HEADER_SPACING = "set_pdf_header_spacing";
    public static final String SETTINGS_PDF_ALLOW_HEADER_SPACING = "allow_header_spacing";
    public static final String SETTINGS_PDF_FOOTER_SPACING = "set_pdf_footer_spacing";
    public static final String SETTINGS_PDF_ALLOW_FOOTER_SPACING = "allow_footer_spacing";
    public static final String SETTINGS_PDF_LEFT_SPACING = "set_pdf_left_spacing";
    public static final String SETTINGS_PDF_ALLOW_LEFT_SPACING = "allow_left_spacing";
    public static final String SETTINGS_PDF_RIGHT_SPACING = "set_pdf_right_spacing";
    public static final String SETTINGS_PDF_ALLOW_RIGHT_SPACING = "allow_right_spacing";
    public static final String SETTINGS_PDF_FONT_SIZING = "set_pdf_font_sizing";
    public static final String SETTINGS_PDF_ALLOW_FONT_SIZING = "allow_font_sizing";
    public static final String SETTINGS_ALLOW_INK_LITE_MODE="enable_lite_mode";
    public static final String SETTINGS_ALLOW_LOGO="enable_logo";
    public static final String SETTINGS_VACCINE_BILLING = "allow_vaccine_billing";
    public static final String SETTINGS_BILL_DISCOUNT = "bill_discount_settings";

    public static final String SETTINGS_PATIENT_DISPLAY_TIME = "patient_display_time_settings";

    //    Hospital data pref constants
    public static final String USER_LOG_PREF = "user_log";
    public static final String LOG_KEY = "log_info";
    public static final String NAME_KEY = "name";
    public static final String PHONE_NUMBER_KEY = "phone_number";


    public static final String PATIENT_COUNTER_KEY = "patient_counter_key";
    public static final String EMAIL_KEY = "email";
    public static final String PHOTO_KEY = "photo_path";
    public static final String LOGO_KEY_LOCAL = "logo_key_path";
    public static final String PASSWORD_KEY = "password";
    public static final String HOSPITAL_ID_KEY = "hospital_id";
    public static final String HISTORY_PAGE_NUMBER_KEY = "history_page_number";
    public static final String REFERRAL_CODE_KEY = "referral_code";
    public static final String ADDRESS_PIN_KEY = "address_pin_key";
    public static final String ADDRESS_STREET_KEY = "address_street_key";
    public static final String ADDRESS_CITY_KEY = "address_city_key";
    public static final String ADDRESS_LINE_1_KEY = "address_line_1_key";

    public static final String SETTINGS_GST_NUMBER = "gst_number";


    public static void getVolleyErrorResponse(VolleyError error, Activity activity, final ProgressDialog progressBar){

        NetworkResponse networkResponse = error.networkResponse;
        if(activity!=null) {
            String errorMessage = activity.getString(R.string.unknown_error);
            if (networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                    errorMessage = activity.getString(R.string.request_timeout);
                } else if (error.getClass().equals(NoConnectionError.class)) {
                    errorMessage = activity.getString(R.string.no_connection);
                }
            } else {
                String result = new String(networkResponse.data);
                try {
                    JSONObject response = new JSONObject(result);
                    String status = response.getString("status");
                    String message = response.getString("message");

                    Log.e("Error Status", status);
                    Log.e("Error Message", message);

                    if (networkResponse.statusCode == 404) {
                        errorMessage = activity.getString(R.string.error_400);
                    } else if (networkResponse.statusCode == 401) {
                        errorMessage = message + activity.getString(R.string.error_401);
                    } else if (networkResponse.statusCode == 400) {
                        errorMessage = message + activity.getString(R.string.error_400);
                    } else if (networkResponse.statusCode == 500) {
                        errorMessage = message + activity.getString(R.string.error_500);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if(progressBar!=null) {

                new AlertDialog.Builder(activity)
                        .setTitle("Error")
                        .setMessage(errorMessage)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                progressBar.cancel();
                                progressBar.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
            Log.i("Error", errorMessage);
        }
        error.printStackTrace();

    }

    public static String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return sdf.format(new Date());
    }

    public static final int REGULAR = 0;
    public static final int SEMI_BOLD = 1;
    public static final int BOLD = 2;
    public static final int ITALIC = 3;
    public static final int THIN = 4;

    public static Typeface getProximaNovaTypeface(Context context, int type){

        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fonts/proxima-nova-regular.ttf");

        switch (type){
            case REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(),"fonts/proxima-nova-regular.ttf");
                break;
            case SEMI_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(),"fonts/proxima-nova-semibold.ttf");
                break;
            case BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(),"fonts/proxima-nova-bold.ttf");
                break;
            case ITALIC:
                typeface = Typeface.createFromAsset(context.getAssets(),"fonts/proxima-nova-italic.ttf");
                break;
            case THIN:
                typeface = Typeface.createFromAsset(context.getAssets(),"fonts/proxima-nova-thin.ttf");
                break;

        }

        return typeface;
    }

    public static String getHoursBeforeDate(int hours){
        if (hours !=0) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.HOUR_OF_DAY, -hours);
            return sdf.format(calendar.getTime());
        }
        else return "0";
    }

    public static boolean checkWritePermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
//            Log.d(LOG_TAG,"Inside check permission MMMM");
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

//                    Log.d(LOG_TAG,"Inside check permission");

//                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
//                    alertBuilder.setCancelable(true);
//                    alertBuilder.setTitle("Permission necessary");
//                    alertBuilder.setMessage("External storage permission is necessary");
//                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//                        public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
//                        }
//                    });
//                    AlertDialog alert = alertBuilder.create();
//                    alert.show();
                } else {
//                    Log.d(LOG_TAG,"Inside check permission");
                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                return false;
            } else {
//                Log.d(LOG_TAG,"Inside check permission granted");
                return true;
            }
        } else {
            return true;
        }
    }

    public static boolean checkReadPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static String getTimeInSimpleDateFormat(String timestamp) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"	, Locale.getDefault());
        SimpleDateFormat sdfChangeFormat = new SimpleDateFormat("MM-yy", Locale.getDefault());
        Date date = null;
        String time = sdfChangeFormat.format(new Date()) ;
        try {
            date = sdf.parse(timestamp);
            time = sdfChangeFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        return time;
    }

    public static float getValueUptoTwoPlacesOfDecimal(float value){

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        return (float) (Math.round(value * 100.0) / 100.0);

    }

    public static double getValueUptoTwoPlacesOfDecimal(double value){

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        return (Math.round(value * 100.0) / 100.0);

    }

    public static String getAge(int year, int month, int day) {

        org.joda.time.LocalDate birthdate = new org.joda.time.LocalDate(year, month, day);          //Birth date
        org.joda.time.LocalDate now = new org.joda.time.LocalDate();                    //Today's date
        org.joda.time.Period period = new org.joda.time.Period(birthdate, now, PeriodType.yearMonthDay());
        int ageYear = period.getYears();
        int ageMonth = period.getMonths();
        int days = period.getDays();
        String ageString = "";

        if (ageYear > 0) {
            if (ageYear == 1) {
                ageString += ageYear + " year ";
            } else {
                ageString += ageYear + " years ";
            }
        }
        if (ageMonth > 0) {
            if (ageMonth == 1) {
                ageString += ageMonth + " month ";
            } else {
                ageString += ageMonth + " months ";
            }

        }
        if (days > 0) {
            if (days == 1) {
                ageString += days + " day ";
            } else {
                ageString += days + " days ";
            }

        }

        return ageString;
    }

    public static String getDob(float days){
        return Utility.getHoursBeforeSimpleDate((int)days*24);
    }

    public static String getHoursBeforeSimpleDate(int hours){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, -hours);
        return sdf.format(calendar.getTime());

    }

    public static float getNumberOfDaysFromAge(Context context,float age, String ageType){

        float numberOfDays = -1;

        String[] ageArray = context.getResources().getStringArray(R.array.age_array);

        if(ageArray[0].equals(ageType)){
            numberOfDays = age*365;
        }else if(ageArray[1].equals(ageType)){
            numberOfDays = age*30;
        }else if(ageArray[2].equals(ageType)){
            numberOfDays = age*7;
        }else if(ageArray[3].equals(ageType)){
            numberOfDays = age*1;
        }else if(ageArray[4].equals(ageType)){
            numberOfDays = 0;
        }

        return numberOfDays;
    }

    public static String getTimeInCurrentDateFormat(String timestamp) {
//        Log.d(LOG_TAG, "inside before converting zulut time");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"	, Locale.getDefault());
        SimpleDateFormat sdfChangeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));

        Date date = null;
        String time = sdfChangeFormat.format(new Date()) ;
        try {
            date = sdf.parse(timestamp);
            time = sdfChangeFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        return time;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean checkIfLiquidDoseOrNot(String route){

        if(route.equals("SYRUP")||route.equals("SOLUTION")||route.equals("SUSPENSION")||route.equals("DROPS")||route.equals("DROPS(ORAL)")){
            return true;
        }else {
            return false;
        }

    }

    public static boolean checkIfDefaultQuantity(String route){

        if (route.equals("TABLET") || route.equals("CAPSULE") || route.equals("PILLS") || route.equals("NEBULISER"))
            return false;
        else
            return true;
    }

    public static boolean checkDoseExist(String route){

        return !(route.toLowerCase().equals("cream")
                || route.toLowerCase().equals("gel")
                || route.toLowerCase().equals("lotion")
                || route.toLowerCase().equals("soap")
                || route.toLowerCase().equals("shampoo")
                || route.toLowerCase().equals("face wash")
                || route.toLowerCase().equals("cleansing lotion")
                || route.toLowerCase().equals("baby powder")
                || route.toLowerCase().equals("ointment"));

    }

    public static String formatDateToSimpleFormat(String input) {

        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
        DateFormat targetFormat = new SimpleDateFormat("EEE, d MMM, HH:mm",Locale.getDefault());
        String formattedDate;

        try {

            Date date = originalFormat.parse(input);
            formattedDate = targetFormat.format(date);

            return formattedDate;

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPhoneNumberValid(String phoneNumber) {

        String regexStr = "^[0-9]{10}$";

        if (phoneNumber.matches(regexStr)) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isPINNumberValid(String pin) {

        String regexStr = "[0-9]{3,9}";

        if (pin.matches(regexStr)) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }






}
