package com.prezcription.pharmacyapp;

import java.util.List;

/**
 * Created by amit on 05/02/18.
 */

public class BillCreationResponse {


    /**
     * id : 52
     * CustomerId : 40
     * BillNo : 40/2018/52
     * BillDate : 2018-08-07T10:21:15.000Z
     * patientId : H1-28-2335
     * totalAmtDue : 220.07699999999994
     * totalcGST : 11.789839285714283
     * totalsGST : 11.789839285714283
     * totalDiscount : 21.833035714285714
     * created_at : 2018-08-07T10:21:15.000Z
     * updated_at : 2018-08-07T10:21:15.000Z
     * consultationFee : 300.0
     * doctorDiscountPercent : 10.0
     * doctorDiscountAmount : 21.833035714285714
     * lineitems : [{"lineitem":0,"itemid":"49465","itemName":"ZENTEL 400MG TAB*  ","batchid":"EP10","mfgr":"BIOCON  ","expiry":"2020-07-01T00:00:00.000Z","mrp":7.51,"itemCount":"3","NetRate":7.51,"itemGross":6.705357142857142,"itemDisc":2.011607142857143,"netItemGross":20.116071428571427,"cGST":1.086267857142857,"sGST":1.086267857142857,"NetAmt":20.276999999999997},{"lineitem":1,"itemid":"15717","itemName":"SOLIWAX EAR DROPS*  ","batchid":"AT00","mfgr":"NULIFE PHARMACEUTICALS  ","expiry":"2021-04-01T00:00:00.000Z","mrp":111,"itemCount":"2","NetRate":111,"itemGross":99.10714285714285,"itemDisc":19.82142857142857,"netItemGross":198.2142857142857,"cGST":10.703571428571426,"sGST":10.703571428571426,"NetAmt":199.79999999999995}]
     */

    private int id;
    private int CustomerId;
    private String BillNo;
    private String BillDate;
    private String patientId;
    private double totalAmtDue;
    private double totalcGST;
    private double totalsGST;
    private double totalDiscount;
    private String created_at;
    private String updated_at;
    private double consultationFee;
    private double doctorDiscountPercent;
    private double doctorDiscountAmount;
    private List<LineitemsBean> lineitems;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int CustomerId) {
        this.CustomerId = CustomerId;
    }

    public String getBillNo() {
        return BillNo;
    }

    public void setBillNo(String BillNo) {
        this.BillNo = BillNo;
    }

    public String getBillDate() {
        return BillDate;
    }

    public void setBillDate(String BillDate) {
        this.BillDate = BillDate;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public double getTotalAmtDue() {
        return totalAmtDue;
    }

    public void setTotalAmtDue(double totalAmtDue) {
        this.totalAmtDue = totalAmtDue;
    }

    public double getTotalcGST() {
        return totalcGST;
    }

    public void setTotalcGST(double totalcGST) {
        this.totalcGST = totalcGST;
    }

    public double getTotalsGST() {
        return totalsGST;
    }

    public void setTotalsGST(double totalsGST) {
        this.totalsGST = totalsGST;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public double getConsultationFee() {
        return consultationFee;
    }

    public void setConsultationFee(double consultationFee) {
        this.consultationFee = consultationFee;
    }

    public double getDoctorDiscountPercent() {
        return doctorDiscountPercent;
    }

    public void setDoctorDiscountPercent(double doctorDiscountPercent) {
        this.doctorDiscountPercent = doctorDiscountPercent;
    }

    public double getDoctorDiscountAmount() {
        return doctorDiscountAmount;
    }

    public void setDoctorDiscountAmount(double doctorDiscountAmount) {
        this.doctorDiscountAmount = doctorDiscountAmount;
    }

    public List<LineitemsBean> getLineitems() {
        return lineitems;
    }

    public void setLineitems(List<LineitemsBean> lineitems) {
        this.lineitems = lineitems;
    }

    public static class LineitemsBean {
        /**
         * lineitem : 0
         * itemid : 49465
         * itemName : ZENTEL 400MG TAB*
         * batchid : EP10
         * mfgr : BIOCON
         * expiry : 2020-07-01T00:00:00.000Z
         * mrp : 7.51
         * itemCount : 3
         * NetRate : 7.51
         * itemGross : 6.705357142857142
         * itemDisc : 2.011607142857143
         * netItemGross : 20.116071428571427
         * cGST : 1.086267857142857
         * sGST : 1.086267857142857
         * NetAmt : 20.276999999999997
         */

        private int lineitem;
        private String itemid;
        private String itemName;
        private String batchid;
        private String mfgr;
        private String expiry;
        private double mrp;
        private float itemCount;
        private double NetRate;
        private double itemGross;
        private double itemDisc;
        private double netItemGross;
        private double cGST;
        private double sGST;
        private double NetAmt;

        public int getLineitem() {
            return lineitem;
        }

        public void setLineitem(int lineitem) {
            this.lineitem = lineitem;
        }

        public String getItemid() {
            return itemid;
        }

        public void setItemid(String itemid) {
            this.itemid = itemid;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getBatchid() {
            return batchid;
        }

        public void setBatchid(String batchid) {
            this.batchid = batchid;
        }

        public String getMfgr() {
            return mfgr;
        }

        public void setMfgr(String mfgr) {
            this.mfgr = mfgr;
        }

        public String getExpiry() {
            return expiry;
        }

        public void setExpiry(String expiry) {
            this.expiry = expiry;
        }

        public double getMrp() {
            return mrp;
        }

        public void setMrp(double mrp) {
            this.mrp = mrp;
        }

        public float getItemCount() {
            return itemCount;
        }

        public void setItemCount(float itemCount) {
            this.itemCount = itemCount;
        }

        public double getNetRate() {
            return NetRate;
        }

        public void setNetRate(double NetRate) {
            this.NetRate = NetRate;
        }

        public double getItemGross() {
            return itemGross;
        }

        public void setItemGross(double itemGross) {
            this.itemGross = itemGross;
        }

        public double getItemDisc() {
            return itemDisc;
        }

        public void setItemDisc(double itemDisc) {
            this.itemDisc = itemDisc;
        }



        public double getCGST() {
            return cGST;
        }

        public void setCGST(double cGST) {
            this.cGST = cGST;
        }

        public double getSGST() {
            return sGST;
        }

        public void setSGST(double sGST) {
            this.sGST = sGST;
        }

        public double getNetAmt() {
            return NetAmt;
        }

        public void setNetAmt(double NetAmt) {
            this.NetAmt = NetAmt;
        }
    }
}
