// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.prezcription.pharmacyapp.custom_camera;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
//import com.prezcription.hospitalapp.activity.BillingActivity;
import com.prezcription.pharmacyapp.activity.BillingActivity;

import java.io.IOException;
import java.util.List;

/** Barcode Detector. */
public class BarcodeScanningProcessor extends VisionProcessorBase<List<FirebaseVisionBarcode>> {

  private static final String TAG = "BarcodeScanProc";

  private static final String LOG_TAG = BarcodeScanningProcessor.class.getSimpleName();

  public interface onBarCodeScannerInteraction{
      void onBarCodeProcessed();
  }

  private onBarCodeScannerInteraction mListener;

  private final FirebaseVisionBarcodeDetector detector;
  private Activity parentActivity;

  public BarcodeScanningProcessor(Activity activity, onBarCodeScannerInteraction listener) {
    // Note that if you know which format of barcode your app is dealing with, detection will be
    // faster to specify the supported barcode formats one by one, e.g.
    FirebaseVisionBarcodeDetectorOptions options =  new FirebaseVisionBarcodeDetectorOptions.Builder()
         .setBarcodeFormats(FirebaseVisionBarcode.FORMAT_CODE_128)
         .build();
    parentActivity = activity;
    mListener = listener;
    detector = FirebaseVision.getInstance().getVisionBarcodeDetector(options);
  }

  @Override
  public void stop() {
    try {
      detector.close();
    } catch (IOException e) {
      Log.e(TAG, "Exception thrown while trying to close Barcode Detector: " + e);
    }
  }

  @Override
  protected Task<List<FirebaseVisionBarcode>> detectInImage(FirebaseVisionImage image) {
    return detector.detectInImage(image);
  }

  @Override
  protected void onSuccess(
      @NonNull List<FirebaseVisionBarcode> barcodes,
      @NonNull FrameMetadata frameMetadata) {
//    graphicOverlay.clear();


    if (barcodes.size() >0) {
        Log.d(LOG_TAG, "inside success: "+barcodes.get(0).getRawValue());
      final FirebaseVisionBarcode barcode = barcodes.get(0);

      new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("UI thread", "I am the UI thread");
                                    Activity activity = parentActivity;

                                    if (activity instanceof BillingActivity) {
//                                        stop();
                                        mListener.onBarCodeProcessed();
                                        Log.d(LOG_TAG, "raw value"+ (barcode.getRawValue())) ;

                                        ((BillingActivity) activity).setScanResult((barcode.getRawValue()));
                                    }
                                }
                            });
    }


  }

  @Override
  protected void onFailure(@NonNull Exception e) {
    Log.e(TAG, "Barcode detection failed " + e);
  }
}
