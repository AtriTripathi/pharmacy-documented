package com.prezcription.pharmacyapp.custom;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.List;

/**
 * Created by anubhav on 05/10/16.
 */
public class MyCookieStore implements CookieStore {

    private CookieStore mStore;
    private Context mContext;
    private Gson mGson;
    private SharedPreferences preferences;

    public static final String COOKIE_PREF = "session_cookie";
    public static final String COOKIE_RESPONSE = "cookie_response";
    public static final String DEFAULT_STRING = "default_value";

    public MyCookieStore(Context context) {

        // prevent context leaking by getting the application context
        mContext = context.getApplicationContext();
        mGson = new Gson();

        preferences = context.getSharedPreferences(COOKIE_PREF, Context.MODE_PRIVATE);

        // get the default in memory store and if there is a cookie stored in shared preferences,
        // we added it to the cookie store
        mStore = new CookieManager().getCookieStore();
        String jsonSessionCookie = preferences.getString(COOKIE_RESPONSE,DEFAULT_STRING);

//        Log.d("Anubhav","Cookie is there(Constructor,out): " + jsonSessionCookie);

        if (!jsonSessionCookie.equals(DEFAULT_STRING)) {
//            Log.d("Anubhav","Cookie is there(Constructor,in): " + jsonSessionCookie);
            HttpCookie cookie = mGson.fromJson(jsonSessionCookie, HttpCookie.class);
            mStore.add(URI.create(cookie.getDomain()), cookie);
        }

    }

    @Override
    public void add(URI uri, HttpCookie cookie) {

        if(cookie.getValue().equals("")){

//           Intent intent = new Intent(mContext, LoginActivity.class);
//           intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//           mContext.startActivity(intent);
//           showAlertDialogBox();
        }

        SharedPreferences.Editor editor = preferences.edit();
//        Log.d("Anubhav", "Cookie Store(out) : "  +mGson.toJson(cookie) );

        if (cookie.getName().equals("sessionid")) {
            // if the cookie that the cookie store attempt to add is a session cookie,
            // we remove the older cookie and save the new one in shared preferences
            remove(URI.create(cookie.getDomain()), cookie);
            editor.putString(COOKIE_RESPONSE,mGson.toJson(cookie));
            editor.commit();
//            Log.d("Anubhav", "Cookie Store(in) : "  +mGson.toJson(cookie) );
        }

        mStore.add(URI.create(cookie.getDomain()), cookie);
    }

    @Override
    public List<HttpCookie> get(URI uri) {
        return mStore.get(uri);
    }

    @Override
    public List<HttpCookie> getCookies() {
        return mStore.getCookies();
    }

    @Override
    public List<URI> getURIs() {
        return mStore.getURIs();
    }

    @Override
    public boolean remove(URI uri, HttpCookie cookie) {
        return mStore.remove(uri, cookie);
    }

    @Override
    public boolean removeAll() {
        return mStore.removeAll();
    }


}